﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;

public partial class SpotPlacementDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DateTime dtStartDate = (DateTime)Session["dtStartDate"];
        DateTime dtEndDate = (DateTime)Session["dtEndDate"];
        string strStartDate = getFormattedDate(dtStartDate);
        string strEndDate = getFormattedDate(dtEndDate);

        label_Date_Range.Text = strStartDate + " to " + strEndDate;

        label_search.Text = Session["strSearch"].ToString();
        label_position.Text = Request.QueryString["position"];
        label_break_start_time.Text = Request.QueryString["breakStartTime"];
        string strCategory = Session["strCategory"].ToString();

        if (Session["dataReadOnly"].ToString() == "True")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "HideOutIcons", "HideIcons();", true);

        }


        bindGrid(strCategory, Session["strSearch"].ToString(), Request.QueryString["position"]);
    }

    protected string getFormattedDate(DateTime dt)
    {
        string strDate = "";
        string strDateFormat = (string)Session["dateFormat"];
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dt.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dt.ToString("MM/dd/yyyy");
        }
        return strDate;
    }

    protected void bindGrid(string strCategory, string strSearch, string position)
    {
        int stationID = int.Parse(Session["stationID"].ToString());

        DateTime StartDate = (DateTime)Session["dtStartDate"];
        DateTime EndDate = (DateTime)Session["dtEndDate"];


        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("SpotPlacementReportDetail", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@stationID", stationID);
        da.SelectCommand.Parameters.AddWithValue("@startDate", StartDate.ToString());
        da.SelectCommand.Parameters.AddWithValue("@endDate", EndDate.ToString());
        da.SelectCommand.Parameters.AddWithValue("@strSearch", strSearch);
        da.SelectCommand.Parameters.AddWithValue("@category", strCategory);
        da.SelectCommand.Parameters.AddWithValue("@positionToSearch", position);
        da.SelectCommand.Parameters.AddWithValue("@breakStartTimeToSearch", changeDateFormat(Request.QueryString["breakStartTime"]));

        DataSet ds = new DataSet();
        da.Fill(ds);

        if (ds.Tables[0].Rows.Count > 0)
        {
            datagrid_disp_details.DataSource = ds;
            datagrid_disp_details.DataBind();
        }
        else
        {
            datagrid_disp_details.DataSource = null;
            datagrid_disp_details.DataBind();
        }

    }
    protected string changeDateFormat(string strDate)
    {
        if (strDate == "All")
        {
            return strDate;
        }

        int intTimePart = strDate.IndexOf(" ");
        string strTimePart = strDate.Substring(intTimePart, strDate.Length - intTimePart);

        string dd;
        string mm;
        string yyyy;

        int intIndexOfFirstHash = strDate.IndexOf("/");
        dd = strDate.Substring(0, intIndexOfFirstHash);
        strDate = strDate.Substring(intIndexOfFirstHash + 1, strDate.Length - (intIndexOfFirstHash + 1));

        intIndexOfFirstHash = strDate.IndexOf("/");
        mm = strDate.Substring(0, intIndexOfFirstHash);
        strDate = strDate.Substring(intIndexOfFirstHash + 1, strDate.Length - (intIndexOfFirstHash + 1));

        yyyy = strDate.Substring(0, strDate.IndexOf(" "));

        strDate = mm.Trim() + "/" + dd.Trim() + "/" + yyyy.Trim() + " " + strTimePart.Trim();

        return strDate;
    }

    protected string changeDateFormatDDMMYYYY(string strDate)
    {
        int intTimePart = strDate.IndexOf(" ");
        string strTimePart = strDate.Substring(intTimePart, strDate.Length - intTimePart);

        string dd;
        string mm;
        string yyyy;

        int intIndexOfFirstHash = strDate.IndexOf("/");
        dd = strDate.Substring(0, intIndexOfFirstHash);
        strDate = strDate.Substring(intIndexOfFirstHash + 1, strDate.Length - (intIndexOfFirstHash + 1));

        intIndexOfFirstHash = strDate.IndexOf("/");
        mm = strDate.Substring(0, intIndexOfFirstHash);
        strDate = strDate.Substring(intIndexOfFirstHash + 1, strDate.Length - (intIndexOfFirstHash + 1));

        yyyy = strDate.Substring(0, strDate.IndexOf(" "));

        strDate = mm.Trim() + "/" + dd.Trim() + "/" + yyyy.Trim() + " " + strTimePart.Trim();

        return strDate;
    }

    protected void datagrid_disp_details_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = (string)Session["headerFontName"];
        string headerFontSize = (string)Session["headerFontSize"];

        string actualDataFontName = (string)Session["dataFontName"];
        string actualDataFontSize = (string)Session["dataFontSize"];
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            try
            {
                e.Item.Cells[0].Text = changeDateFormatDDMMYYYY(e.Item.Cells[0].Text);
            }
            catch { }
        }
    }
}
