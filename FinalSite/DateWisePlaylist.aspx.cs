﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using Telerik.Web.UI;
using System.Collections.Generic;

public partial class DateWisePlaylist : System.Web.UI.Page
{
    bool isPdfExport = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        getDateFormat();
        string strDateFormat = (string)Session["dateFormat"];

        System.Text.StringBuilder onloadScript = new System.Text.StringBuilder();
        //onloadScript.Append("<script type='text/javascript'>");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("ShowReport();");//Commented by Vijay
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("</script>");

        this.ClientScript.RegisterStartupScript(this.GetType(), "onLoadCall", onloadScript.ToString());

        //Literal lit = new Literal();
        //lit.Text = "<script language='javascript'>tdhide('leftMenu', 'ctl00_showhide', 'hide');</script>";
        //Page.Controls.Add(lit);

        if (strDateFormat == "DD/MM/YYYY")
        {
            dtStart.DateInput.DateFormat = "dd/MM/yyyy";
            dtEnd.DateInput.DateFormat = "dd/MM/yyyy";

            dtStart.DateInput.DisplayDateFormat = "dd/MM/yyyy";
            dtEnd.DateInput.DisplayDateFormat = "dd/MM/yyyy";
        }
        if (!IsPostBack)
        {
            setDate();
            bindTypes();
            GenerateTreeView();

        }
        Session["StationLeft"] = txtStationSelect.Text;
       // Session["StationRight"] = txtStationCompare.Text;
        Session["s_date"] = dtStart.SelectedDate;
        Session["e_date"] = dtEnd.SelectedDate;
        

        BtnCompare.Attributes.Add("onClick", "return compare_report();");

        Pg_Title.Text = "Datewise Playlist";
    }
    protected void setDate()
    {
        DateTime lockDataBefore = DateTime.Parse(Session["lockDataBefore"].ToString());
        DateTime lockDataAfter = DateTime.Parse(Session["lockDataAfter"].ToString());
        dtStart.MinDate = lockDataBefore;
        dtEnd.MinDate = lockDataBefore;

        dtStart.MaxDate = lockDataAfter;
        dtEnd.MaxDate = lockDataAfter;

        dtStart.SelectedDate = System.DateTime.Now;
        dtEnd.SelectedDate = System.DateTime.Now;
    }
    protected void GenerateTreeView()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getStationsForUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);
        //da.SelectCommand.Parameters.AddWithValue("@username", "Administrator");
        DataSet ds = new DataSet();
        da.Fill(ds);
        var txtStationsTree = (RadTreeView)txtStationSelect.Items[0].FindControl("radStationSelect");
        var txtStationsCompareTree = (RadTreeView)txtStationCompare.Items[0].FindControl("radStationComapre");
        txtStationsTree.DataFieldID = "name";
        txtStationsTree.DataFieldParentID = "m_name";
        txtStationsTree.DataTextField = "name";
        txtStationsTree.DataSource = ds.Tables[0];
        txtStationsTree.DataBind();

        txtStationsCompareTree.DataFieldID = "name";
        txtStationsCompareTree.DataFieldParentID = "m_name";
        txtStationsCompareTree.DataTextField = "name";
        txtStationsCompareTree.DataSource = ds.Tables[0];
        txtStationsCompareTree.DataBind();

    }
    protected void bindTypes()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getDatewisePlaylistTypesForUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@user", (string)Session["user"]);
       // da.SelectCommand.Parameters.AddWithValue("@user", "Administrator");

        DataSet ds = new DataSet();
        da.Fill(ds);

        string strTypes = "";
        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (strTypes == "")
                {
                    strTypes = dr["Type"].ToString();
                }
                else
                {
                    strTypes = strTypes + "," + dr["Type"].ToString();
                }
            }

            Session["strTypes"] = strTypes;
        }

        ddType.DataSource = ds;
        ddType.DataTextField = "Types";
        ddType.DataValueField = "Type";
        ddType.DataBind();
        ddType.SortItems();
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        Session["category"] = ddType.SelectedItem.Value;

        string str_station_name = txtStationSelect.Text;
        DateTime startTime = (DateTime)dtStart.SelectedDate;
        DateTime endTime = (DateTime)dtEnd.SelectedDate;
        bindGrid(ddType.SelectedItem.Value, "");
        ShowGirdContainerButtons.Visible = true;
    }
    protected void bindGrid(string type, string sortColumn)
    {

        dgResult.EnableViewState = false;



        if (dtStart.SelectedDate.ToString() == "" || dtEnd.SelectedDate.ToString() == "")
        {
            return;
        }

        if (txtStationSelect.Text == "Select Station")
        {
            return;
        }

        string str_station_name = txtStationSelect.Text;



        DateTime startTime = (DateTime)dtStart.SelectedDate;
        DateTime endTime = (DateTime)dtEnd.SelectedDate;



        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("DatewisePlaylist", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;

        da.SelectCommand.Parameters.AddWithValue("@stationName", str_station_name);
        da.SelectCommand.Parameters.AddWithValue("@element", type);
        da.SelectCommand.Parameters.AddWithValue("@startNoEarlierThan", startTime);
        da.SelectCommand.Parameters.AddWithValue("@endNoLaterThan", endTime);
        da.SelectCommand.Parameters.AddWithValue("@untitledSuspect", "Don't show");
        da.SelectCommand.Parameters.AddWithValue("@titledUnknown", "Show");
        da.SelectCommand.Parameters.AddWithValue("@subsequentUnknown", "Show");
        da.SelectCommand.Parameters.AddWithValue("@repeatedTitle", "Don't show");
        da.SelectCommand.Parameters.AddWithValue("@OrderByExpression", "StartTime");
        da.SelectCommand.Parameters.AddWithValue("@user", (string)Session["user"]);
       // da.SelectCommand.Parameters.AddWithValue("@user", "Administrator");

        da.SelectCommand.CommandTimeout = 0;
        DataSet ds = new DataSet();
        da.Fill(ds);

        if (type == "All")
        {
            DataTable dtNew = ds.Tables[0];
            Session["countSongs"] = getCount(dtNew, "M");
            Session["countLinks"] = getCount(dtNew, "L");
            Session["countSpots"] = getCount(dtNew, "S");
        }



        if (isPdfExport)
        {
            ds.Tables[0].Columns.Remove("Type");
            //ds.Tables[0].Columns.Remove("Hour");
            ds.Tables[0].Columns["Hour"].ColumnName = "Date";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (dr["Air Time"].ToString() == "*")
                {
                    dr.Delete();
                }
            }
            ds.AcceptChanges();
            dgResult.ShowFooter = true;
        }
        else
        {
            dgResult.ShowFooter = false;
        }

        dgResult.Visible = true;

        DataView dv = ds.Tables[0].DefaultView;

        if (sortColumn != "")
        {

        }

       // dgResult.DataSource = dv;

        Session["totalRecordCount"] = dv.Count;


        Session["myDataTableExport"] = ds.Tables[0];

        dgResult.DataSource = ds;
        dgResult.DataBind();

        if (ds.Tables[0].Rows.Count > 0)
        {
            //lblError.Visible = false;
            showHideTypeButtons();
            showButtons();
            NoRecordsDiv.Visible = false;
            NoRecordsLiteral.Visible = false;
            StringBuilder sb1 = new StringBuilder();
            sb1.Append("<script type='text/javascript'>");
            sb1.Append(Environment.NewLine);
            sb1.Append("ShowLaneDiv();");
            sb1.Append(Environment.NewLine);
            sb1.Append("</script>");

            this.ClientScript.RegisterStartupScript(this.GetType(), "DisplayBlockedDivs", sb1.ToString());
        }
        else
        {
            dgResult.Visible = false;
            ShowGirdContainerButtons.Visible = false;
            NoRecordsDiv.Visible = true;
            NoRecordsLiteral.Visible = true;
            //lblError.Visible = true;
            //lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";

            hideButtons();
        }
    }
    protected void hideButtons()
    {
        litExportTo.Visible = false;
        btnExportToExcel.Visible = false;
        btnExportToCsv.Visible = false;
        btnExportToPdf.Visible = false;
        btnPrint.Visible = false;
    }
    protected void showButtons()
    {
        litExportTo.Visible = true;
        btnExportToExcel.Visible = true;
        btnExportToCsv.Visible = true;
        btnExportToPdf.Visible = true;
        btnPrint.Visible = false;
    }
    protected void showHideTypeButtons()
    {
        if (Session["strTypes"] != null)
        {
            string strTypes = Session["strTypes"].ToString();
            string[] arrTypes = strTypes.Split(',');
            List<string> Types = new List<string>(arrTypes);

            if (Types.Contains("Song"))
            {
                btn1.Visible = true;
            }
            else
            {
                btn1.Visible = false;
            }

            if (Types.Contains("Spot"))
            {
                btn2.Visible = true;
            }
            else
            {
                btn2.Visible = false;
            }

            if (Types.Contains("Links"))
            {
                btn4.Visible = true;
            }
            else
            {
                btn4.Visible = false;
            }

            if (Types.Contains("All"))
            {
                btn3.Visible = true;
            }
            else
            {
                btn3.Visible = false;
            }
        }
    }
    protected int getCount(DataTable dt, string type)
    {
        DataRow[] foundRows = null;

        if (type == "M")
        {
            foundRows = dt.Select("Type='M'");
        }
        else if (type == "L")
        {
            foundRows = dt.Select("Type='L'");
        }
        else if (type == "S")
        {
            foundRows = dt.Select("Type='S'");
        }

        return foundRows.Length;
    }

    protected string getHourHeaderFormatted(string hourHeader)
    {
        string strDateFormat = (string)Session["dateFormat"];
        if (strDateFormat == "DD/MM/YYYY")
        {
            String dt = hourHeader.Substring(0, 10);
            hourHeader = hourHeader.Replace(dt, changeDateFormat(dt));
        }

        hourHeader = hourHeader.Replace("13:00:00 PM", "01:00:00 PM");
        hourHeader = hourHeader.Replace("14:00:00 PM", "02:00:00 PM");
        hourHeader = hourHeader.Replace("15:00:00 PM", "03:00:00 PM");
        hourHeader = hourHeader.Replace("16:00:00 PM", "04:00:00 PM");
        hourHeader = hourHeader.Replace("17:00:00 PM", "05:00:00 PM");
        hourHeader = hourHeader.Replace("18:00:00 PM", "06:00:00 PM");
        hourHeader = hourHeader.Replace("19:00:00 PM", "07:00:00 PM");
        hourHeader = hourHeader.Replace("20:00:00 PM", "08:00:00 PM");
        hourHeader = hourHeader.Replace("21:00:00 PM", "09:00:00 PM");
        hourHeader = hourHeader.Replace("22:00:00 PM", "10:00:00 PM");
        hourHeader = hourHeader.Replace("23:00:00 PM", "11:00:00 PM");
        hourHeader = hourHeader.Replace("24:00:00 AM", "12:00:00 AM");

        return hourHeader;
    }
    protected string changeDateFormat(string strDate)
    {
        string dd;
        string mm;
        string yyyy;

        dd = strDate.Substring(3, 2);
        mm = strDate.Substring(0, 2);
        yyyy = strDate.Substring(6, 4);

        strDate = dd + "/" + mm + "/" + yyyy;

        return strDate;
    }
    protected void dgResult_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (isPdfExport && e.Item is GridFooterItem)
        {
            GridFooterItem footerItem = (GridFooterItem)e.Item;

            string strDateFormat = (string)Session["dateFormat"];

            DateTime dtDate = System.DateTime.Now;
            string strDate = "";
            if (strDateFormat == "DD/MM/YYYY")
            {
                strDate = dtDate.ToString("dd/MM/yyyy");
            }
            else
            {
                strDate = dtDate.ToString("MM/dd/yyyy");
            }


            string strTime = System.DateTime.Now.ToString();
            strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


            string strCountText = "<b>" + Session["totalRecordCount"].ToString() + " rows </b>";
            if ((string)Session["category"] == "All")
            {
                strCountText = strCountText + " (" + Session["countSongs"].ToString() + " song instances, " + Session["countLinks"].ToString() + " link instances , " + Session["countSpots"].ToString() + " spot instances)";
            }

            string strFooterText = "";

            strFooterText = "This data was generated and exported on " + strDate + " at " + strTime + ".";
            strFooterText += " (" + strCountText + ") ";

            strFooterText += "AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved. ";
            strFooterText += "AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India. ";
            strFooterText += "The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.";

            footerItem.Cells[2].ColumnSpan = 2;
            footerItem.Cells[2].Text = strFooterText;
            footerItem.Cells[2].Style["color"] = "red";


        }

        if (e.Item.ItemType == GridItemType.Header)
        {
            GridHeaderItem headerItem = dgResult.MasterTableView.GetItems(GridItemType.Header)[0] as GridHeaderItem;

            string headerFontName = ConfigurationSettings.AppSettings["headerFontNameDP"].ToString();
            string headerFontSize = ConfigurationSettings.AppSettings["headerFontSizeDP"].ToString();
            int intHeaderFontSize = int.Parse(headerFontSize.Replace("Px", "").Trim());

            dgResult.HeaderStyle.Font.Name = headerFontName;
            dgResult.HeaderStyle.Font.Size = FontUnit.Point(intHeaderFontSize);

            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();

            //dgResult.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            //dgResult.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
            if ((string)Session["headerFontStyle"] == "Bold")
            {
                dgResult.HeaderStyle.Font.Bold = true;
            }
            else
            {
                dgResult.HeaderStyle.Font.Bold = false;
            }

            if (!isPdfExport)
            {
                headerItem["Hour"].Visible = false;
                headerItem["Type"].Visible = false;
            }
            Boolean hasClass = true;
            try
            {
                string ss = headerItem["Category"].Text;
            }
            catch
            {
                hasClass = false;
            }

            #region Width
            if (hasClass)
            {
                GridColumn col;
                GridHeaderItem gridHeaderItem = e.Item as GridHeaderItem;
                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[0];
                col.HeaderStyle.Width = Unit.Percentage(10);

                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[1];
                col.HeaderStyle.Width = Unit.Percentage(10);

                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[2];
                col.HeaderStyle.Width = Unit.Percentage(30);

                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[3];
                col.HeaderStyle.Width = Unit.Percentage(30);

                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[4];
                col.HeaderStyle.Width = Unit.Percentage(20);
            }
            else
            {
                GridColumn col;
                GridHeaderItem gridHeaderItem = e.Item as GridHeaderItem;
                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[0];
                col.HeaderStyle.Width = Unit.Percentage(15);

                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[1];
                col.HeaderStyle.Width = Unit.Percentage(10);

                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[2];
                col.HeaderStyle.Width = Unit.Percentage(40);

                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[3];
                col.HeaderStyle.Width = Unit.Percentage(35);
            }
            #endregion

            if (!isPdfExport)
            {
                headerItem.Style["font-size"] = "100px";
            }
        }


        if (e.Item is GridGroupHeaderItem)
        {
            GridGroupHeaderItem item = (GridGroupHeaderItem)e.Item;

            item.DataCell.Text = item.DataCell.Text.Replace(" 13:00:00 PM", " 01:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 14:00:00 PM", " 02:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 15:00:00 PM", " 03:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 16:00:00 PM", " 04:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 17:00:00 PM", " 05:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 18:00:00 PM", " 06:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 19:00:00 PM", " 07:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 20:00:00 PM", " 08:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 21:00:00 PM", " 09:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 22:00:00 PM", " 10:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 23:00:00 PM", " 11:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 00:00:00 AM", " 12:00:00 AM");


        }

        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            GridDataItem dataItem = e.Item as GridDataItem;

            //string dataFontName = ConfigurationSettings.AppSettings["headerFontNameDP"].ToString();
            //string dataFontSize = ConfigurationSettings.AppSettings["headerFontSizeDP"].ToString();
            //int intDataFontSize = int.Parse(dataFontSize.Replace("Px", "").Trim());

            //dataItem.Font.Name = dataFontName;
            //dataItem.Font.Size = FontUnit.Point(intDataFontSize);
            dataItem.Height = Unit.Pixel(2);



            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();

            //dataItem.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            //dataItem.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);
            if ((string)Session["dataFontStyle"] == "Bold")
            {
                dataItem.Font.Bold = true;
            }
            else
            {
                dataItem.Font.Bold = false;
            }


            if (isPdfExport)
            {
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 00:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 01:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 02:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 03:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 04:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 05:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 06:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 07:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 08:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 09:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 10:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 11:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 12:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 13:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 14:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 15:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 16:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 17:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 18:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 19:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 20:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 21:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 22:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 23:00:00 PM", "");
            }
            string strAirTime = dataItem["Air Time"].Text;

            try
            {
                dataItem["Air Time"].HorizontalAlign = HorizontalAlign.Center;
            }
            catch { }

             try
            {
                dataItem["Duration (in sec)"].HorizontalAlign = HorizontalAlign.Center;
            }
            catch { }

            try
            {
                dataItem["Account"].HorizontalAlign = HorizontalAlign.Left;
            }
            catch { }

            try
            {
                dataItem["Song Title"].HorizontalAlign = HorizontalAlign.Left;
            }
            catch { }


            try
            {
                dataItem["Account/Song Title"].HorizontalAlign = HorizontalAlign.Left;
            }
            catch { }


            try
            {
                dataItem["Parent"].HorizontalAlign = HorizontalAlign.Left;
            }
            catch { }

            try
            {
                dataItem["Category"].HorizontalAlign = HorizontalAlign.Left;
            }
            catch { }

            try
            {
                dataItem["Movie/Artist"].HorizontalAlign = HorizontalAlign.Left;
            }
            catch { }

            try
            {
                dataItem["Parent/Movie/Artist"].HorizontalAlign = HorizontalAlign.Left;
            }
            catch { }

            if (strAirTime == "*")
            {

                try
                {
                    dataItem["Account"].HorizontalAlign = HorizontalAlign.Center;
                }
                catch { }

                try
                {
                    dataItem["Song Title"].HorizontalAlign = HorizontalAlign.Center;
                }
                catch { }

                try
                {
                    dataItem["Account/Song Title"].HorizontalAlign = HorizontalAlign.Center;
                }
                catch { }


                try
                {
                    dataItem["Parent"].HorizontalAlign = HorizontalAlign.Center;
                }
                catch { }



                try
                {
                    dataItem["Movie/Artist"].HorizontalAlign = HorizontalAlign.Center;
                }
                catch { }

                try
                {
                    dataItem["Parent/Movie/Artist"].HorizontalAlign = HorizontalAlign.Center;
                }
                catch { }

                dataItem["Air Time"].Text = "";
                dataItem["Duration (in sec)"].Text = "";
                dataItem.BackColor = System.Drawing.Color.Black;
                dataItem.ForeColor = System.Drawing.Color.White;


                try
                {
                    dataItem["Account"].Text = getHourHeaderFormatted(dataItem["Account"].Text);
                    dataItem["Account"].HorizontalAlign = HorizontalAlign.Center;
                }
                catch
                {
                    try
                    {
                        dataItem["Song Title"].Text = getHourHeaderFormatted(dataItem["Song Title"].Text);
                        dataItem["Song Title"].HorizontalAlign = HorizontalAlign.Center;
                    }
                    catch
                    {
                        try
                        {
                            dataItem["Account/Song Title"].Text = getHourHeaderFormatted(dataItem["Account/Song Title"].Text);
                            dataItem["Account/Song Title"].HorizontalAlign = HorizontalAlign.Center;
                        }
                        catch
                        {

                        }
                    }
                }

            }


            try
            {
                if (!isPdfExport)
                {
                    dataItem["Hour"].Visible = false;
                    dataItem["Type"].Visible = false;
                }
            }
            catch { }


            string xCol = "#CCCCFF";
            System.Drawing.Color cM = System.Drawing.ColorTranslator.FromHtml(xCol);

            xCol = "#FFCCCC";
            System.Drawing.Color cL = System.Drawing.ColorTranslator.FromHtml(xCol);

            xCol = "#FFFFFF";
            System.Drawing.Color cS = System.Drawing.ColorTranslator.FromHtml(xCol);

            if (!isPdfExport)
            {
                if (dataItem["Type"].Text == "M")
                {
                    dataItem.BackColor = System.Drawing.Color.Aqua;
                }
                if (dataItem["Type"].Text == "L")
                {
                    dataItem.BackColor = System.Drawing.Color.Yellow;
                }
                if (dataItem["Type"].Text == "S")
                {
                    dataItem.BackColor = System.Drawing.Color.LightGreen;
                }
            }
        }
    }
    protected void getUserPreferencesForCurrentUser()
    {

        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getUserPreferenceForAUserAndReport", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@user", (string)Session["user"]);
        da.SelectCommand.Parameters.AddWithValue("@reportName", "Datewise Playlist");
        DataSet ds = new DataSet();
        da.Fill(ds);

        Session["headerFontName"] = ds.Tables[0].Rows[0]["headerFontName"].ToString();
        Session["headerFontSize"] = ds.Tables[0].Rows[0]["headerFontSize"].ToString();
        Session["headerFontColor"] = ds.Tables[0].Rows[0]["headerFontColor"].ToString();
        Session["headerRowColor"] = ds.Tables[0].Rows[0]["headerRowColor"].ToString();
        Session["headerFontStyle"] = ds.Tables[0].Rows[0]["headerFontStyle"].ToString();
        Session["dataFontName"] = ds.Tables[0].Rows[0]["dataFontName"].ToString();
        Session["dataFontSize"] = ds.Tables[0].Rows[0]["dataFontSize"].ToString();
        Session["dataFontColor"] = ds.Tables[0].Rows[0]["dataFontColor"].ToString();
        Session["alternateRowColor"] = ds.Tables[0].Rows[0]["alternateRowColor"].ToString();
        Session["dataFontStyle"] = ds.Tables[0].Rows[0]["dataFontStyle"].ToString();

    }

    

    protected void getDateFormat()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("select * from India_Report_New..Country_dateFormat where isSelected=1", conn);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                Session["dateFormat"] = dr["DateFormat"].ToString();
            }
        }

        conn.Close();
    }
    protected bool isValidUser(string userName)
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("select * from India_Report_New..Users where username = '" + userName + "'", conn);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            return true;
        }
        else
        {
            return false;
        }
        conn.Close();
    }

    public static string DecryptText(String pass)
    {
        return Decrypt(pass, "&amp;%#@?,:*");
    }

    public static string Decrypt(string stringToDecrypt, string sEncryptionKey)
    {
        byte[] key = { };
        byte[] IV = { 10, 20, 30, 40, 50, 60, 70, 80 };
        byte[] inputByteArray = new byte[stringToDecrypt.Length];
        try
        {
            key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            inputByteArray = Convert.FromBase64String(stringToDecrypt);
            //MemoryStream ms = new MemoryStream(); 
            Encoding encoding = Encoding.UTF8;
            return encoding.GetString(inputByteArray);
        }
        catch (System.Exception ex)
        {
            return ("");
        }
    }

    protected void btn1_onclick(object sender, EventArgs e)
    {
      //  lit.Text = "";

        if (dtStart.SelectedDate == null || dtEnd.SelectedDate == null)
        {
            return;
        }

        string str_station_name = txtStationSelect.Text;
        DateTime startTime = (DateTime)dtStart.SelectedDate;
        DateTime endTime = (DateTime)dtEnd.SelectedDate;
        addAuditLog(startTime, endTime, "", str_station_name, false, false, false);

        bindGrid("Song", "");
        Session["category"] = "Song";

    }
    protected void btn2_onclick(object sender, EventArgs e)
    {
      //  lit.Text = "";
        if (dtStart.SelectedDate == null || dtEnd.SelectedDate == null)
        {
            return;
        }
        string str_station_name = txtStationSelect.Text;
        DateTime startTime = (DateTime)dtStart.SelectedDate;
        DateTime endTime = (DateTime)dtEnd.SelectedDate;
        addAuditLog(startTime, endTime, "", str_station_name, false, false, false);

        bindGrid("Spot", "");
        Session["category"] = "Spot";
        //ddType.SelectedItem.Text = "Spot";
    }
    protected void btn3_onclick(object sender, EventArgs e)
    {
        if (dtStart.SelectedDate == null || dtEnd.SelectedDate == null)
        {
            return;
        }
        string str_station_name = txtStationSelect.Text;
        DateTime startTime = (DateTime)dtStart.SelectedDate;
        DateTime endTime = (DateTime)dtEnd.SelectedDate;
        addAuditLog(startTime, endTime, "", str_station_name, false, false, false);

        bindGrid("All", "");
        Session["category"] = "All";
        //ddType.SelectedItem.Text = "All";
    }
    protected void btn4_onclick(object sender, EventArgs e)
    {
       // lit.Text = "";
        if (dtStart.SelectedDate == null || dtEnd.SelectedDate == null)
        {
            return;
        }
        string str_station_name = txtStationSelect.Text;
        DateTime startTime = (DateTime)dtStart.SelectedDate;
        DateTime endTime = (DateTime)dtEnd.SelectedDate;
        addAuditLog(startTime, endTime, "", str_station_name, false, false, false);

        bindGrid("Links", "");
        Session["category"] = "Links";
        //ddType.SelectedItem.Text = "Links";
    }

    protected void dgResult_SortCommand(object source, GridSortCommandEventArgs e)
    {
        bindGrid((string)Session["category"], e.SortExpression);
    }
    protected void addAuditLog(DateTime startDate, DateTime endDate, string market, string station,
       Boolean exportedToExcel, Boolean exportedToPdf, Boolean exportedToText)
    {
        //auditTrail at = new auditTrail();
        //at.connectionString = ConfigurationSettings.AppSettings["connectionString"].ToString();
        //at.userID = (int)Session["userID"];
        //at.IPAddress = Request.ServerVariables["REMOTE_ADDR"];
        //at.pageViewed = "Datewise Playlist/" + Session["category"].ToString();
        //at.startDate = startDate;
        //at.endDate = endDate;
        //at.market = market;
        //at.station = station;
        //at.exportedToExcel = exportedToExcel;
        //at.exportedToPdf = exportedToPdf;
        //at.exportedToText = exportedToText;
        //at.addUserActivityLog();
        //at = null;
    }
    protected string getPageTitle(string exportType)
    {
        string strDateFormat = (string)Session["dateFormat"];
        string str_station_name = txtStationSelect.Text;

        string str_market_name = getMarketOfStation(str_station_name);

        String strStationName = "Station: " + str_station_name;
        String strMarket;
        if (exportType == "PDF")
        {
            strMarket = " (Market: " + str_market_name + ")";
        }
        else
        {
            strMarket = "<BR>Market: " + str_market_name;
        }
        DateTime dt1 = (DateTime)Session["s_date"];
        DateTime dt2 = (DateTime)Session["e_date"];

        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }



        string strType = "";
        if ((string)Session["category"] == "Song")
        {
            strType = "Songs";
        }
        else if ((string)Session["category"] == "Links")
        {
            strType = "Links";
        }
        else if ((string)Session["category"] == "Spot")
        {
            strType = "Spots";
        }
        else if ((string)Session["category"] == "All")
        {
            strType = "All";
        }

        String strDateRange;
        if (exportType == "PDF")
        {
            strDateRange = " from: " + sDt1 + "  to:  " + sDt2 + " (" + strType + ")";
        }
        else
        {
            strDateRange = "<BR>Date Range: " + sDt1 + "  to:  " + sDt2;
        }

        string str = strStationName + strMarket + strDateRange;
        return str;
    }

    protected string getMarketOfStation(string strStation)
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("getMarketOfStation", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@stationName", strStation);
        conn.Open();
        dr = comm.ExecuteReader();
        string strMarketName = "";
        while (dr.Read())
        {
            strMarketName = dr[0].ToString();
        }

        dr.Close();
        conn.Close();

        return strMarketName;
    }

    protected void dgResult_ItemCreated(object sender, GridItemEventArgs e)
    {

        if (e.Item is GridHeaderItem)
        {
            GridHeaderItem header = (GridHeaderItem)e.Item;
            if (isPdfExport)
            {
                header.Style["font-size"] = "8pt";
            }
        }
        if (e.Item is GridDataItem)
        {
            GridDataItem dataItem = (GridDataItem)e.Item;
            if (isPdfExport)
            {
                dataItem.Style["font-size"] = "7pt";
            }
        }

    }
  
    protected DataTable getDataTableToExport()
    {
        string strDateFormat = (string)Session["dateFormat"];
        DataTable dt = new DataTable();
        dt = (DataTable)Session["myDataTableExport"];
        try
        {
            dt.Columns.Remove("Type");
        }
        catch { }

        try
        {
            dt.Columns["Hour"].ColumnName = "Date";
            //dt.Columns.Remove("Hour");
        }
        catch { }

        int i = 0;
        if (dt == null)
        {
            return null;
        }
        foreach (DataRow dr in dt.Rows)
        {
            try
            {
                String strDate = (string)dr["Date"];
                if (strDate.IndexOf(" ") > 0)
                {
                    strDate = strDate.Substring(0, strDate.IndexOf(" "));
                }

                DateTime dtDate = DateTime.Parse(strDate);

                if (strDateFormat == "DD/MM/YYYY")
                {
                    strDate = dtDate.ToString("dd/MM/yyyy");
                }
                else
                {
                    strDate = dtDate.ToString("MM/dd/yyyy");
                }
                dr["Date"] = strDate;
            }
            catch { }

            string strAirTime = (string)dr["Air Time"];
            if (strAirTime == "*")
            {
                dt.Rows[i][0] = "";
                dt.Rows[i][1] = DBNull.Value;
                dt.Rows[i].Delete();
            }
            i++;
        }


        dt.AcceptChanges();
        return dt;
    }
    public void ExportToExcel()
    {
        string strDateFormat = (string)Session["dateFormat"];
        DataTable dt = getDataTableToExport();


        string str_station_name = txtStationSelect.Text;

        string str_market_name = getMarketOfStation(str_station_name);

        String strStationName = "<BR><BR>Station: " + str_station_name;
        String strMarket = "<BR><BR>Market: " + str_market_name;

        DateTime dt1 = (DateTime)Session["s_date"];
        DateTime dt2 = (DateTime)Session["e_date"];

        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }

        String strDateRange = "<BR><BR>From: " + sDt1 + "  to  " + sDt2;

        ExportToExcel(dt, Response, strStationName, strMarket, strDateRange);
    }

    public void ExportToExcel(DataTable dt, HttpResponse response, String strStationName, string strMarket, String strDateRange)
    {

        //first let's clean up the response.object
        response.Clear();
        response.Charset = "";
        //set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel";
        //create a string writer
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //create an htmltextwriter which uses the stringwriter
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //instantiate a datagrid
        DataGrid dg = new DataGrid();
        //set the datagrid datasource to the dataset passed in
        if (dt == null)
        {
            return;
        }
        else if (dt.Rows.Count == 0)
        {
            return;
        }
        dg.DataSource = dt;
        //bind the datagrid
        dg.DataBind();

        dg.RenderControl(htmlWrite);


        String str = stringWrite.ToString();

        int start, end;
        start = 0;
        end = 0;

        int colspan = 0;

        colspan = dt.Columns.Count;

        string strType = "";
        if ((string)Session["category"] == "Song")
        {
            strType = "Songs";
        }
        else if ((string)Session["category"] == "Links")
        {
            strType = "Links";
        }
        else if ((string)Session["category"] == "Spot")
        {
            strType = "Spots";
        }
        else if ((string)Session["category"] == "All")
        {
            strType = "All";
        }


        String strReportName = "Aircheck Datewise Playlist Report (" + strType + ")";



        string strFinal = "<tr><td align=center colspan=" + colspan + " style='background-color:#FFFF66;'>" + strReportName + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strStationName + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strMarket + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strDateRange + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + ">&nbsp;</td></tr>";



        while (str.IndexOf("<select name=") > 0)
        {
            start = str.IndexOf("<select name=");
            end = str.IndexOf("</select>") + 9;
            str = str.Replace(str.Substring(start, end - start), "");
        }
        //str = str.Replace("<tr>\r\n\t\t<td>&nbsp;</td><td>&nbsp;</td>", "<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td>");

        string strAirTime = "";
        string strPlayTime = "";
        string strNewPlayTime = "";

        int intCount = 0;

        foreach (DataRow dr in dt.Rows)
        {
            strAirTime = dr[0].ToString();
            if (strAirTime == "")
            {
                strPlayTime = dr[2].ToString();
                strNewPlayTime = dr[2].ToString() + " ";

                if ((string)Session["category"] == "Song")
                {
                    str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=4 align='Center'>" + strNewPlayTime + "</td></tr>");
                }
                else
                {
                    str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=5 align='Center'>" + strNewPlayTime + "</td></tr>");
                }
            }
            else
            {
                intCount = intCount + 1;
            }
        }


        str = strFinal + "<BR><BR>" + str;


        string strDateFormat = (string)Session["dateFormat"];

        DateTime dtDate = System.DateTime.Now;
        string strDate = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dtDate.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dtDate.ToString("MM/dd/yyyy");
        }


        string strTime = System.DateTime.Now.ToString();
        //strTime = strTime.Substring(10, strTime.Length - 10);

        strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);

        string strCountText = "<b>" + intCount + " rows </b>";
        if ((string)Session["category"] == "All")
        {
            strCountText = strCountText + " (" + Session["countSongs"].ToString() + " song instances, " + Session["countLinks"].ToString() + " link instances , " + Session["countSpots"].ToString() + " spot instances)";
        }

        string strFooterText = "";

        strFooterText = "<BR><table>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>This data was generated and exported on " + strDate + " at " + strTime + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>" + strCountText + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><BR></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.</td></tr>";
        strFooterText += "</table>";

        str = "<table>" + str + strFooterText;

         
        response.Write(str);
        response.End();
    }

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (dtStart.SelectedDate == null || dtEnd.SelectedDate == null)
        {
            return;
        }
        string str_station_name = txtStationSelect.Text;
        DateTime startTime = (DateTime)dtStart.SelectedDate;
        DateTime endTime = (DateTime)dtEnd.SelectedDate;
        addAuditLog(startTime, endTime, "", str_station_name, true, false, false);

        bindGrid((string)Session["category"], "");
        ExportToExcel();
    }
    protected void btnExportToCsv_Click(object sender, EventArgs e)
    {
        if (dtStart.SelectedDate == null || dtEnd.SelectedDate == null)
        {
            return;
        }
        string str_station_name = txtStationSelect.Text;
        DateTime startTime = (DateTime)dtStart.SelectedDate;
        DateTime endTime = (DateTime)dtEnd.SelectedDate;
        addAuditLog(startTime, endTime, "", str_station_name, false, false, true);

        bindGrid((string)Session["category"], "");
        ExportToCSV();
    }
    public void ExportToCSV()
    {
        DataTable dt = getDataTableToExport();

        string strData = Export(dt);
        Byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(strData);

        Response.Clear();
        Response.AddHeader("Content-Type", "application/Excel");
        Response.AddHeader("Content-Disposition", "inline;filename=DatewisePlaylist.csv");
        Response.BinaryWrite(data);
        Response.End();
    }
    protected string Export(DataTable dt)
    {

        String Start_Date, End_Date;
        Int32 i, j;
        StringBuilder sb = new StringBuilder();

        DateTime dt1 = (DateTime)Session["s_date"];
        DateTime dt2 = (DateTime)Session["e_date"];
        string strDateFormat = (string)Session["dateFormat"];
        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }

        Start_Date = sDt1;
        End_Date = sDt2;

        string strType = "";
        if ((string)Session["category"] == "Song")
        {
            strType = "Songs";
        }
        else if ((string)Session["category"] == "Links")
        {
            strType = "Links";
        }
        else if ((string)Session["category"] == "Spot")
        {
            strType = "Spots";
        }
        else if ((string)Session["category"] == "All")
        {
            strType = "All";
        }

        sb.Append("Aircheck Datewise Playlist Report (" + strType + ")");
        sb.Append("\r\n");
        sb.Append("\r\n");

        string str_station_name = txtStationSelect.Text;

        string str_market_name = getMarketOfStation(str_station_name);

        sb.Append("Station");
        sb.Append(":");
        sb.Append(str_station_name);
        sb.Append("\r\n");

        sb.Append("Market");
        sb.Append(":");
        sb.Append(str_market_name);
        sb.Append("\r\n");



        sb.Append("Date");
        sb.Append(":");
        sb.Append(Start_Date + " to " + End_Date);
        sb.Append("\r\n");
        sb.Append("\r\n");


        for (j = 1; j <= dt.Columns.Count; j++)
        {
            //if (j == 2)
            //{
            //    sb.Append("Market");
            //    sb.Append("|");
            //}
            sb.Append(dt.Columns[j - 1].ColumnName.ToString());
            sb.Append("|");

        }


        sb.Replace("|", "", sb.Length - 1, 1);
        sb.Append("\r\n");


        Int32 intNumberOfRows = dt.Rows.Count;
        Int32 intNumberOfColumns = dt.Columns.Count;
        for (i = 1; i <= intNumberOfRows; i++)
        {
            for (j = 1; j <= intNumberOfColumns; j++)
            {
                sb.Append(dt.Rows[i - 1][j - 1].ToString());
                sb.Append("|");
            }
            sb.Replace("|", "", sb.Length - 1, 1);
            sb.Append("\r\n");
        }
        sb.Append("\r\n");
        sb.Append("\r\n");




        DateTime dtDate = System.DateTime.Now;
        string strDate = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dtDate.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dtDate.ToString("MM/dd/yyyy");
        }


        string strTime = System.DateTime.Now.ToString();
        strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


        string strCountText = intNumberOfRows.ToString() + " rows ";
        if ((string)Session["category"] == "All")
        {
            strCountText = strCountText + " (" + Session["countSongs"].ToString() + " song instances " + Session["countLinks"].ToString() + " link instances " + Session["countSpots"].ToString() + " spot instances)";
        }




        sb.Append("This data was generated and exported on " + strDate + " at " + strTime + "");
        sb.Append("\r\n");
        sb.Append(strCountText);
        sb.Append("\r\n");
        sb.Append("\r\n");
        sb.Append("\r\n");
        sb.Append("AirCheck India services are Copyright 2002-2009 AirCheck India. All Rights Reserved.");
        sb.Append("\r\n");
        sb.Append("AirCheck India its logo and \'The New Broadcast Monitoring\' are registered trademarks of AirCheck India.");
        sb.Append("\r\n");
        sb.Append("The AirCheck India broadcast content recognition process is protected by U.S. Patents 5437050 and 7386047 with additional patents pending.");


        return sb.ToString();
    }
    protected void btnExportToPdf_Click(object sender, EventArgs e)
    {
        isPdfExport = true;
        if (dtStart.SelectedDate == null || dtEnd.SelectedDate == null)
        {
            return;
        }
        string str_station_name = txtStationSelect.Text;
        DateTime startTime = (DateTime)dtStart.SelectedDate;
        DateTime endTime = (DateTime)dtEnd.SelectedDate;
        addAuditLog(startTime, endTime, "", str_station_name, false, true, false);

        bindGrid((string)Session["category"], "");

        dgResult.ExportSettings.FileName = "DatewisePlaylist";
        dgResult.ExportSettings.IgnorePaging = true;
        dgResult.ExportSettings.OpenInNewWindow = true;


        dgResult.ExportSettings.Pdf.PageTitle = getPageTitle("PDF");
        dgResult.ExportSettings.ExportOnlyData = false;

        dgResult.MasterTableView.ExportToPdf();
    }
   
}
