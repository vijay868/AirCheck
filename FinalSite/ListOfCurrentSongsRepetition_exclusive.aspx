﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListOfCurrentSongsRepetition_exclusive.aspx.cs" Inherits="ListOfCurrentSongsRepetition_exclusive" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>List Of Current Songs & Repetition Exclusive</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="DownloadOptions" content="noopen" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="css/report_caption.css" rel="stylesheet" text="text/css">
		<script language="javascript">
		    function HideIcons() {
//		        prn.style.display = 'none';
		    }
		</script>
	</head>
	<body leftmargin="0" rightmargin="0" topmargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="53" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="top" align="top" width="760" height="53" border="0"><img src="images/bgTop.gif"></td>
					<td vAlign="top" width="100%"><IMG height="28" src="images/bgTopExt.gif" width="100%" border="0"></td>
				</tr>
			</table>
			<table width="100%" background="images/contentheader_bgr.gif" border="0" cellpadding="6px" cellspacing="0px" style="margin-left:-5px; margin-right:300px;">
                <tr>
                    
                    <td align="left" 
                        style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #0066CC">::
                        List Of Current Songs & Repetition Exclusive&nbsp;::
                    </td>
                    <td>
                    </td>
                    <td align="right">
                       
                    </td>
                </tr>
            </table>
			<span name="prn" ID="prn">
				<table height="0" cellSpacing="0" cellPadding="0" width="100%" border="0">
					<tr>
						<td width="100%" align="right" colspan="2">
						<asp:ImageButton id="img_btn_export_to_excel" Runat="server" ImageUrl="images/Excel.GIF" 
                                AlternateText="Export to excel" CausesValidation="False" 
                                onclick="img_btn_export_to_excel_Click"></asp:ImageButton>
						<a href="#" onClick="print();"><IMG src="images/icon_print.gif" border="0"></a>&nbsp;&nbsp;</td>
					</tr>
				</table>
			</span>
			<table width="100%">
				<tr>
					<td class="date" width="117" style="WIDTH: 117px">Date Range</td>
					<td class="date" width="1%">:</td>
					<td width="85%"><asp:Label ID="label_Date_Range" Runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr>
					<td class="date" width="117" style="WIDTH: 117px">Market</td>
					<td class="date" width="1%">:</td>
					<td width="85%"><asp:Label ID="label_market" Runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr>
					<td class="date" width="117" style="WIDTH: 117px">Station</td>
					<td class="date" width="1%">:</td>
					<td width="85%"><asp:Label ID="label_station" Runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr>
					<td class="date" width="100%" colspan="3">&nbsp;</td>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<td width="100%">
						<asp:DataGrid ID="datagrid_disp_details" Runat="server" Width="100%" 
                            onitemdatabound="datagrid_disp_details_ItemDataBound">
							<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
							<AlternatingItemStyle HorizontalAlign="Center"></AlternatingItemStyle>
						</asp:DataGrid>
					</td>
				</tr>
				<tr>
					<td width="100%">&nbsp;</td>
				</tr>
			</table>
		</form>
	</body>
</html>