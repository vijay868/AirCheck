﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using Telerik.Web.UI;

public partial class nationalAdvertisersByDateHour : System.Web.UI.Page
{
    bool isPdfExport = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder onloadScript = new StringBuilder();
        //onloadScript.Append("<script type='text/javascript'>");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("ShowReport();");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("</script>");
        //this.ClientScript.RegisterStartupScript(this.GetType(), "onLoadCall", onloadScript.ToString());

        //((Label)this.Master.FindControl("lblReportName")).Text = "National Advertiser/National Advertiser By Brand";

        //Literal lit = new Literal();
        //lit.Text = "<script language='javascript'>tdhide('leftMenu', 'ctl00_showhide', 'hide');</script>";
        //Page.Controls.Add(lit);



        if (!IsPostBack)
        {
            Session["DTds"] = null;
            //ds1 = null;
            getUserPreferencesForCurrentUser();

            lbl_brand.Text = doStringCleanup(Request.QueryString["brandnames"]);
            lbl_advt.Text = doStringCleanup(Request.QueryString["advt"]);
            lbl_category.Text = doStringCleanup(Request.QueryString["tvbrand"]);
            lbl_daterange.Text = GetUsDate(Request.QueryString["sdate"]) + " To " + GetUsDate(Request.QueryString["edate"]);
            lbl_market.Text = Request.QueryString["market"];
            lbl_station.Text = Request.QueryString["stationnameid"];
            bindGrid(Request.QueryString["StationIDQ"], Request.QueryString["sdate"], Request.QueryString["edate"], Request.QueryString["scrtxt"], Request.QueryString["scrcriteria"], Request.QueryString["advt"], Request.QueryString["brandnames"], Request.QueryString["market"], Request.QueryString["stationnameid"]);
            if (Session["dataReadOnly"].ToString() == "True")
            {
                btnExportToExcel.Enabled = false;
                btnExportToPdf.Enabled = false;
            }
            else
            {
                btnExportToExcel.Enabled = true;
                btnExportToPdf.Enabled = true;
            }
        }
        Pg_Title.Text = "National Advertisers By Date Hour";
    }
    protected string doStringCleanup(string str)
    {
        str = str.Replace("456456", "&");
        str = str.Replace("123123", "'");
        return str;
    }
    protected void getUserPreferencesForCurrentUser()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getUserPreferenceForAUserAndReport", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@user", (string)Session["user"]);
        da.SelectCommand.Parameters.AddWithValue("@reportName", "Top Advertisers By Brands");

        DataSet ds = new DataSet();
        da.Fill(ds);

        Session["headerFontName"] = ds.Tables[0].Rows[0]["headerFontName"].ToString();
        Session["headerFontSize"] = ds.Tables[0].Rows[0]["headerFontSize"].ToString();
        Session["headerFontColor"] = ds.Tables[0].Rows[0]["headerFontColor"].ToString();
        Session["headerRowColor"] = ds.Tables[0].Rows[0]["headerRowColor"].ToString();
        Session["headerFontStyle"] = ds.Tables[0].Rows[0]["headerFontStyle"].ToString();
        Session["dataFontName"] = ds.Tables[0].Rows[0]["dataFontName"].ToString();
        Session["dataFontSize"] = ds.Tables[0].Rows[0]["dataFontSize"].ToString();
        Session["dataFontColor"] = ds.Tables[0].Rows[0]["dataFontColor"].ToString();
        Session["alternateRowColor"] = ds.Tables[0].Rows[0]["alternateRowColor"].ToString();
        Session["dataFontStyle"] = ds.Tables[0].Rows[0]["dataFontStyle"].ToString();

    }

    protected void displayNoRecordFound()
    {
        lblError.Visible = true;
        lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
        radData.Visible = false;
        litExportTo.Visible = false;
        btnExportToExcel.Visible = false;
        btnExportToPdf.Visible = false;
    }

    protected void addAuditLog(DateTime startDate, DateTime endDate, string station,
Boolean exportedToExcel, Boolean exportedToPdf, Boolean exportedToText)
    {
        auditTrail at = new auditTrail();
        at.connectionString = ConfigurationSettings.AppSettings["connectionString"].ToString();
        at.userID = (int)Session["userID"];
        at.IPAddress = Request.ServerVariables["REMOTE_ADDR"];
        //at.pageViewed = "National Advertiser/Station Wise";
        at.pageViewed = "National Advertiser/National Advertiser By Brand";
        at.startDate = startDate;
        at.endDate = endDate;
        at.station = station;
        at.exportedToExcel = exportedToExcel;
        at.exportedToPdf = exportedToPdf;
        at.exportedToText = exportedToText;
        at.addUserActivityLogPassingStationIDs();
        at = null;
    }
    private string GetUsDate(string indDate)
    {
        string str = "";
        char[] sep = { '/' };
        string[] vals = indDate.Split(sep);
        str = vals[1] + "/" + vals[0] + "/" + vals[2];
        return str;
    }

    public string gethourval(int cellno)
    {
        string returnval;
        if (cellno == 3)
        {
            returnval = "12 AM";
        }
        else
        {
            if (cellno <= 14)
            {
                returnval = (cellno - 3) + " AM";
            }
            else if (cellno == 15)
            {
                returnval = "12 PM";
            }
            else
            {
                returnval = (cellno - 15) + " PM";
            }

        }
        return returnval;
    }
    protected int getSumForAColumn(DataTable dt, DataColumn dc)
    {
        int totalOfColumn = 0;
        int avgOfRow = 0;
        int colIndex = 0;

        foreach (DataRow dr in dt.Rows)
        {
            if (dr[0] != "Total")
            {
                colIndex = 0;
                foreach (DataColumn dc1 in dt.Columns)
                {

                    if (dc1 == dc)
                    {
                        try
                        {
                            avgOfRow += int.Parse(dr[colIndex].ToString());
                            totalOfColumn = avgOfRow;
                        }
                        catch
                        {
                            avgOfRow = 0;
                        }

                    }
                    colIndex = colIndex + 1;
                }
            }
        }

        //try
        //{
        //    totalOfColumn = int.Parse(dt.Compute("sum([" + dc.ColumnName + "])", "[Category Name] <> 'Total'").ToString());
        //}
        //catch
        //{

        //}
        return totalOfColumn;
    }

    protected void radData_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        string hourname; string wDate;
        int totalDuration = 0;
        DataTable dt = (DataTable)Session["DTds"];

        if (e.Item.ItemType == GridItemType.Header)
        {
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            for (int i = 0; i < e.Item.Cells.Count; i++)
            {

                //if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                //{
                //    e.Item.Cells[i].Font.Bold = true;
                //}
                //else
                //{
                //    e.Item.Cells[i].Font.Bold = true;
                //}
                e.Item.Cells[i].Font.Name = "Helvetica";
                e.Item.Cells[i].Font.Size = FontUnit.Parse("16px");
                i++;
            }
            //e.Item.BackColor = System.Drawing.Color.FromArgb(0, 112, 192);
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        }



        if (e.Item.ItemType == Telerik.Web.UI.GridItemType.Item || e.Item.ItemType == Telerik.Web.UI.GridItemType.AlternatingItem)
        {
            string actualDataFontName = "Helvetica";
            string actualDataFontSize = "12px";

            Telerik.Web.UI.GridDataItem dataItem = e.Item as Telerik.Web.UI.GridDataItem;


            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            dataItem.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            dataItem.Font.Size = FontUnit.Parse("16px");
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                dataItem.Font.Bold = true;
            }
            else
            {
                dataItem.Font.Bold = false;
            }


            // hourname = e.Item.Cells[e.Item.RowIndex].Text;
            dataItem["Date"].Font.Name = actualDataFontName;
            dataItem["Date"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());

            for (int i = 3; i < e.Item.Cells.Count; i++)
            {
                //hourname = e.Item.Cells[0].Text;
                //hourname = e.Item.Cells[1].Text;
                wDate = GetUsDate(e.Item.Cells[2].Text);
                if (e.Item.Cells[i].Text != "&nbsp;")
                {
                    hourname = gethourval(i);

                    // e.Item.Cells[i].Text = "<a href='nationalAdvertisersByStationHourDetails.aspx?StationIDQ=" + Request.QueryString["StationIDQ"] + "&sdate=" + Request.QueryString["sdate"] + "&edate=" + Request.QueryString["edate"] + "&scrtxt=" + Request.QueryString["scrtxt"] + "&scrcriteria=" + Request.QueryString["scrcriteria"] + "&advt=" + doStringSanitation(Request.QueryString["advt"]) + "&brandnames=" + doStringSanitation(Request.QueryString["brandnames"]) + "&market=" + doStringSanitation(Request.QueryString["market"]) + "&hourstr=" + hourname + "&datestr=" + e.Item.Cells[2].Text + "&stationnameid=" + Request.QueryString["stationnameid"] + "' style='color:black;font-size:12 px'> " + e.Item.Cells[i].Text + "</a>";
                    // e.Item.Cells[i].Text = "<a href='#' onclick=\"javascript:return disp_details('" + Request.QueryString["StationIDQ"] + "','" + Request.QueryString["sdate"] + "','" + Request.QueryString["edate"] + "','" + Request.QueryString["scrtxt"] + "','" + Request.QueryString["scrcriteria"] + "','" + doStringSanitation(Request.QueryString["advt"]) + "','" + doStringSanitation(Request.QueryString["brandnames"]) + "','" + doStringSanitation(Request.QueryString["market"]) + "','" + hourname + "','" + e.Item.Cells[2].Text + "','" + Request.QueryString["stationnameid"] + "')\" style='color:black;font-size:12 px'> " + e.Item.Cells[i].Text + "</a>";
                    e.Item.Cells[i].Text = "<div align=Center><a href='#' onclick=\"javascript:return disp_details('" + Request.QueryString["StationIDQ"] + "','" + wDate + "','" + wDate + "','" + Request.QueryString["scrtxt"] + "','" + Request.QueryString["scrcriteria"] + "','" + doStringSanitation(Request.QueryString["advt"]) + "','" + doStringSanitation(Request.QueryString["brandnames"]) + "','" + doStringSanitation(Request.QueryString["market"]) + "','" + hourname + "','" + e.Item.Cells[2].Text + "','" + Request.QueryString["stationnameid"] + "')\" style='color:black;font-size:12 px'> " + e.Item.Cells[i].Text + "</a></div>";


                }

            }

            int c = 0;

            foreach (DataColumn dc in dt.Columns)
            {
                if (c > 0)
                {
                    if (dc.ColumnName.IndexOf("AM") > 0 || dc.ColumnName.IndexOf("PM") > 0)
                    {
                        totalDuration += getSumForAColumn(dt, dc);
                        lbl_duration.Text = totalDuration.ToString();
                    }

                }
                c++;
            }




            // dataItem["Advertiser"].Font.Name = actualDataFontName;
            //dataItem["Advertiser"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            //dataItem["Brand Name"].Font.Name = actualDataFontName;
            //dataItem["Brand Name"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());

            //dataItem["Category"].Font.Name = actualDataFontName;
            //dataItem["Category"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());




            //    dataItem["Advertiser"].Font.Name = actualDataFontName;
            //    dataItem["Advertiser"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());

            //    dataItem["Brand Name"].Font.Name = actualDataFontName;
            //    dataItem["Brand Name"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());

            //    dataItem["Category"].Font.Name = actualDataFontName;
            //    dataItem["Category"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());

            //    dataItem["No Of Plays"].Font.Name = actualDataFontName;
            //    dataItem["No Of Plays"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            //    dataItem["No Of Plays"].HorizontalAlign = HorizontalAlign.Center;

            //    dataItem["Total Seconds"].Font.Name = actualDataFontName;
            //    dataItem["Total Seconds"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            //    dataItem["Total Seconds"].HorizontalAlign = HorizontalAlign.Center;

            //    strSong = doStringSanitation(e.Item.Cells[3].Text.ToString());
            //    strMovie = doStringSanitation(e.Item.Cells[4].Text.ToString());

            //    if (!isPdfExport)
            //    {

            //        string strBrandName = doStringSanitation(dataItem["Brand Name"].Text);
            //        string strParentName = doStringSanitation(dataItem["Advertiser"].Text);
            //        string strCategory = doStringSanitation(dataItem["Category"].Text);


            // dataItem["Advertiser"].Text = "<a href='http://yahoo.com'>dfds</a>";
            //        dataItem["Total Seconds"].Text = "<a href='' onclick=\"javascript:return disp_details('" + strBrandName + "','" + strParentName + "','" + strCategory + "')\"  alt='Click here to see the details'>" + dataItem["Total Seconds"].Text + "</a>";
            //    }
            //}

            if (e.Item.ItemType == GridItemType.AlternatingItem)
            {
                if (!isPdfExport)
                {
                    System.Drawing.ColorConverter conv1 = new System.Drawing.ColorConverter();
                    e.Item.BackColor = (System.Drawing.Color)conv1.ConvertFromString((string)Session["alternateRowColor"]);
                }
            }




        }
        if (e.Item.ItemType == GridItemType.Footer)
        {
            if (!isPdfExport)
            {
                e.Item.Visible = false;
            }
            else
            {
                e.Item.Visible = true;

                GridFooterItem footerItem = (GridFooterItem)e.Item;

                string strDateFormat = (string)Session["dateFormat"];

                DateTime dtDate = System.DateTime.Now;
                string strDate = "";
                strDate = dtDate.ToString("dd/MM/yyyy");


                string strTime = System.DateTime.Now.ToString();
                strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


                string strCountText = "<b>" + Session["totalRecordCount"].ToString() + " rows </b>";

                string strFooterText = "";

                strFooterText = "This data was generated and exported on " + strDate + " at " + strTime + ".";
                strFooterText += " (" + strCountText + ") ";

                strFooterText += "AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved. ";
                strFooterText += "AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India. ";
                strFooterText += "The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.";


                e.Item.Cells[2].ColumnSpan = 2;
                e.Item.Cells[2].Text = strFooterText;
                e.Item.Cells[2].Style["color"] = "red";
                e.Item.Font.Size = FontUnit.Point(5);
                e.Item.Font.Bold = false;

            }

        }
    }

    protected void bindGrid(string strStationIDs, string strStartDate, string strEndDate, string searchtxt, string searchcri, string advt, string brandname, string market, string stationname)
    {
        string rep_str_advt;

        rep_str_advt = advt;
        //rep_str_advt = rep_str_advt.Replace("456456", "&");

        try
        {
            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
            SqlDataAdapter da = new SqlDataAdapter("SelectedStation_HourlyTotals", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.CommandTimeout = 0;

            //da.SelectCommand.Parameters.AddWithValue("@stationIDs", strStationIDs);
            da.SelectCommand.Parameters.AddWithValue("@startDate", strStartDate);
            da.SelectCommand.Parameters.AddWithValue("@endDate", strEndDate);
            da.SelectCommand.Parameters.AddWithValue("@SearchText", searchtxt);
            //da.SelectCommand.Parameters.AddWithValue("@SearchCriteria", searchcri);
            da.SelectCommand.Parameters.AddWithValue("@Advertiser", rep_str_advt);
            da.SelectCommand.Parameters.AddWithValue("@BrandName", brandname);
            da.SelectCommand.Parameters.AddWithValue("@stationName", stationname);
            da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

            Session["strStationIDs"] = strStationIDs;
            Session["strStartDate"] = strStartDate;
            Session["strEndDate"] = strEndDate;

            DataSet ds = new DataSet();
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count == 0)
            {
                displayNoRecordFound();
                Session["totalRecordCount"] = 0;
            }
            else
            {
                Session["DTds"] = ds.Tables[0];
                DataTable dt = ds.Tables[0];
                radData.DataSource = ds.Tables[0];
                radData.DataBind();
                litExportTo.Visible = true;
                btnExportToExcel.Visible = true;
                btnExportToPdf.Visible = true;
                radData.Visible = true;
                lblError.Visible = false;
                Session["totalRecordCount"] = ds.Tables[0].Rows.Count;

                Session["myDataTableExport"] = dt;

            }
            addAuditLog(Convert.ToDateTime(strStartDate), Convert.ToDateTime(strEndDate), strStationIDs, false, false, false);
        }
        catch
        {
            displayNoRecordFound();
        }
    }



    protected void btnExportToPdf_Click(object sender, EventArgs e)
    {

        isPdfExport = true;
        // bindGrid(Request.QueryString["StationIDQ"], Request.QueryString["sdate"], Request.QueryString["edate"], Request.QueryString["scrtxt"], Request.QueryString["scrcriteria"], Request.QueryString["advt"]);

        string strDateFormat = (string)Session["dateFormat"];
        DateTime dt1 = DateTime.Parse(Session["strStartDate"].ToString());
        DateTime dt2 = DateTime.Parse(Session["strEndDate"].ToString());

        string sDt1 = "";
        string sDt2 = "";
        sDt1 = dt1.ToString("dd/MM/yyyy");
        sDt2 = dt2.ToString("dd/MM/yyyy");

        String strDateRange = "From: " + sDt1 + "  -  " + sDt2;

        radData.ExportSettings.FileName = "National Advertiser - Station wise Brand Details";
        radData.ExportSettings.IgnorePaging = true;
        radData.ExportSettings.OpenInNewWindow = true;

        String strReportName = "";

        strReportName = "AirCheck India National Advertiser - Station wise Brand Details";

        radData.ExportSettings.Pdf.PageTitle = strReportName + " " + strDateRange;
        radData.ExportSettings.Pdf.PageHeight = Unit.Parse("162mm");
        radData.ExportSettings.Pdf.PageWidth = Unit.Parse("600mm");
        radData.ExportSettings.ExportOnlyData = false;
        radData.MasterTableView.ExportToPdf();

        addAuditLog(Convert.ToDateTime(sDt1), Convert.ToDateTime(sDt2), "", false, true, false);


    }
    protected DataTable getDataTableToExport()
    {
        string strDateFormat = (string)Session["dateFormat"];
        DataTable dt = new DataTable();
        dt = (DataTable)Session["myDataTableExport"];

        return dt;
    }

    protected void img_btn_export_to_excel_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    public void ExportToExcel()
    {
        string strDateFormat = (string)Session["dateFormat"];
        DataTable dt = getDataTableToExport();


        string str_station_name = "";

        string str_market_name = "";// getMarketOfStation(str_station_name);

        String strStationName = "<BR><BR>Station: " + str_station_name;
        String strMarket = "<BR><BR>Market: " + str_market_name;

        DateTime dt1 = DateTime.Parse(Request.QueryString["sdate"].ToString());
        DateTime dt2 = DateTime.Parse(Request.QueryString["edate"].ToString());

        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }


        String strDateRange = "<BR><BR>From: " + sDt1 + "  to  " + sDt2;
        String strMark1 = "<BR>Market: " + Request.QueryString["market"];
        String strStation = "<BR>Station: " + Request.QueryString["stationnameid"];
        String strBrandName = "<BR>Brand Name: " + Request.QueryString["brandnames"];
        String strCategory = "<BR>Category: " + Request.QueryString["advt"];
        String strTotalplays = "<BR>Market: " + lbl_duration.Text;

        ExportToExcel(dt, Response, strStationName, strMarket, strDateRange, strMark1, strStation, strBrandName, strCategory, strTotalplays);

        addAuditLog(Convert.ToDateTime(sDt1), Convert.ToDateTime(sDt2), "", true, false, false);


    }

    public void ExportToExcel(DataTable dt, HttpResponse response, String strStationName, string strMarket, String strDateRange, string strMark1, string strStation, string strBrandName, string strCategory, string strTotalplays)
    {

        //first let's clean up the response.object
        response.Clear();
        response.Charset = "";
        //set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel";
        //create a string writer
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //create an htmltextwriter which uses the stringwriter
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //instantiate a datagrid
        DataGrid dg = new DataGrid();
        //set the datagrid datasource to the dataset passed in
        if (dt == null)
        {
            return;
        }
        else if (dt.Rows.Count == 0)
        {
            return;
        }
        dg.DataSource = dt;
        //bind the datagrid
        dg.DataBind();

        dg.RenderControl(htmlWrite);


        String str = stringWrite.ToString();

        int start, end;
        start = 0;
        end = 0;

        int colspan = 0;

        colspan = dt.Columns.Count;


        String strReportName = "Aircheck National Advertiser - Station wise Brand Details";



        string strFinal = "<tr><td align=center colspan=" + colspan + " style='background-color:#FFFF66;'>" + strReportName + "</td></tr>";
        //strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strStationName + "</td></tr>";
        //strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strMarket + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strDateRange + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strMark1 + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strStation + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strBrandName + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strCategory + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strTotalplays + "</td></tr>";

        strFinal = strFinal + "<tr><td colspan=" + colspan + ">&nbsp;</td></tr>";



        while (str.IndexOf("<select name=") > 0)
        {
            start = str.IndexOf("<select name=");
            end = str.IndexOf("</select>") + 9;
            str = str.Replace(str.Substring(start, end - start), "");
        }
        //str = str.Replace("<tr>\r\n\t\t<td>&nbsp;</td><td>&nbsp;</td>", "<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td>");

        string strAirTime = "";
        string strPlayTime = "";
        string strNewPlayTime = "";

        int intCount = 0;

        foreach (DataRow dr in dt.Rows)
        {
            strAirTime = dr[0].ToString();
            if (strAirTime == "")
            {
                strPlayTime = dr[2].ToString();
                strNewPlayTime = dr[2].ToString() + " ";

                if ((string)Session["category"] == "Song")
                {
                    str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=4 align='Center'>" + strNewPlayTime + "</td></tr>");
                }
                else
                {
                    str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=5 align='Center'>" + strNewPlayTime + "</td></tr>");
                }
            }
            else
            {
                intCount = intCount + 1;
            }
        }


        str = strFinal + "<BR><BR>" + str;


        string strDateFormat = (string)Session["dateFormat"];

        DateTime dtDate = System.DateTime.Now;
        string strDate = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dtDate.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dtDate.ToString("MM/dd/yyyy");
        }


        string strTime = System.DateTime.Now.ToString();
        strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


        string strCountText = "<b>" + intCount + " rows </b>";

        string strFooterText = "";

        strFooterText = "<BR><table>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>This data was generated and exported on " + strDate + " at " + strTime + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>" + strCountText + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><BR></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.</td></tr>";
        strFooterText += "</table>";

        str = "<table>" + str + strFooterText;



        response.Write(str);
        response.End();
    }
    protected string doStringSanitation(string str)
    {
        if (str.IndexOf("'") >= 0)
        {
            str = str.Replace("'", "123123");
        }
        if (str.IndexOf("&") >= 0)
        {
            str = str.Replace("&", "456456");
        }

        return str;
    }



    protected void radData_SortCommand(object source, GridSortCommandEventArgs e)
    {
        bindGrid(Request.QueryString["StationIDQ"], Request.QueryString["sdate"], Request.QueryString["edate"], Request.QueryString["scrtxt"], Request.QueryString["scrcriteria"], Request.QueryString["advt"], Request.QueryString["brandnames"], Request.QueryString["market"], Request.QueryString["stationnameid"]);
    }
}
