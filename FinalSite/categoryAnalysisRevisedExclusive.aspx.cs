﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using Telerik.Web.UI;

public partial class categoryAnalysisRevisedExclusive : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DateTime dtStartDate = DateTime.Parse(Request.QueryString["startDate"]);
        DateTime dtEndDate = DateTime.Parse(Request.QueryString["endDate"]);

        string strStartDate = getFormattedDate(dtStartDate);
        string strEndDate = getFormattedDate(dtEndDate);

        lblStartDate.Text = strStartDate;
        lblEndDate.Text = strEndDate;

        string strStationName, strMarketName;
        int station_id;

        string strStationIDs, strStation;
        strStation = Request.QueryString["strStationName"].Replace("(Played)", "");
        strStationIDs = Request.QueryString["strStationIDs"];

        getStationAndMarketNames(out station_id, out strStationName, out strMarketName);
        lblStation.Text = strStationName;
        lblMarket.Text = strMarketName;

        if (Session["dataReadOnly"].ToString() == "True")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "HideOutIcons", "HideIcons();", true);
            btnExportToExcel.Enabled = false;
        }
        else
        {
            btnExportToExcel.Enabled = true;
        }


        bindGrid(strStationIDs, station_id);
    }

    protected void getStationAndMarketNames(out int station_id, out string strStationName, out string strMarketName)
    {
        station_id = 0;
        strStationName = "";
        strMarketName = "";

        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("SELECT station_id,[name] AS 'Station',(SELECT [name] FROM aircheck..Markets WHERE market_id=Stations.market_id) AS 'Market' FROM Aircheck..Stations WHERE [name] = @stationPassed", conn);
        comm.Parameters.AddWithValue("@stationPassed", Request.QueryString["strStationName"].Trim());
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                station_id = int.Parse(dr["station_id"].ToString());
                strStationName = dr["Station"].ToString();
                strMarketName = dr["Market"].ToString();
            }
        }

        conn.Close();
    }

    protected string getFormattedDate(DateTime dt)
    {
        string strDate = "";
        string strDateFormat = (string)Session["dateFormat"];
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dt.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dt.ToString("MM/dd/yyyy");
        }
        return strDate;
    }

    protected void get_station_and_market(string strStationShort, out int stationID, out string strStationName, out string strMarketName)
    {
        stationID = 0;
        strStationName = "";
        strMarketName = "";

        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("get_station_and_market", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@stationName", strStationShort);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                stationID = int.Parse(dr["station_id"].ToString());
                strStationName = dr["Station"].ToString();
                strMarketName = dr["Market"].ToString();
            }
        }

        conn.Close();
    }

    protected void bindGrid(string strStationIDs, int stationIDExclusive)
    {
        DateTime dtStartDate = DateTime.Parse(Request.QueryString["startDate"]);
        DateTime dtEndDate = DateTime.Parse(Request.QueryString["endDate"]);

        string strStartDate = dtStartDate.ToString("d");
        string strEndDate = dtEndDate.ToString("d");

        string strSearch = Session["searchString"].ToString();

        try
        {

            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
            SqlDataAdapter da = new SqlDataAdapter("categoryAnalysisRevisedExclusive", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.CommandTimeout = 0;

            da.SelectCommand.Parameters.AddWithValue("@stationIDs", strStationIDs);
            da.SelectCommand.Parameters.AddWithValue("@stationIDExclusive", stationIDExclusive);
            da.SelectCommand.Parameters.AddWithValue("@startDate", strStartDate);
            da.SelectCommand.Parameters.AddWithValue("@endDate", strEndDate);
            da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);
            da.SelectCommand.Parameters.AddWithValue("@searchText", strSearch);



            Session["strStationIDs"] = strStationIDs;
            Session["strStartDate"] = strStartDate;
            Session["strEndDate"] = strEndDate;

            DataSet ds = new DataSet();
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count == 0)
            {
                displayNoRecordFound();
            }
            else
            {
                DataRelation dataRel = new DataRelation("advertiser_brands", ds.Tables[0].Columns["Category"], ds.Tables[1].Columns["Category"], false);
                ds.Relations.Add(dataRel);


                dgHierarchical.DataSource = ds.Tables[0];
                dgHierarchical.DataBind();
                lblError.Visible = false;
                btnExportToExcel.Visible = true;
                dgHierarchical.Visible = true;
                Session["ds"] = ds;
                Session["totalRecordCount"] = ds.Tables[0].Rows.Count;
            }
        }
        catch
        {
            displayNoRecordFound();
        }
    }

    protected void displayNoRecordFound()
    {
        lblError.Visible = true;
        lblError.Text = "<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
        btnExportToExcel.Visible = false;
        btnExportToExcel.Visible = false;
    }


    protected void dgHierarchical_TemplateSelection(object sender, DBauer.Web.UI.WebControls.HierarGridTemplateSelectionEventArgs e)
    {
        e.TemplateFilename = "categoryAnalysisRevisedExclusive.ascx";
    }

    protected void dgHierarchical_ItemDataBound(object sender, DataGridItemEventArgs e)
    {

        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            for (int i = 0; i < e.Item.Cells.Count; i++)
            {
                bool isANumber = false;
                int intNum;
                isANumber = int.TryParse(e.Item.Cells[i].Text, out intNum);
                if (isANumber)
                {
                    e.Item.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                }
            }
        }

    }

    protected DataTable GetDataTable(int i, string strCategory)
    {
        DataTable dt = null;
        if (Session["dsExclusive"] != null)
        {
            DataSet ds = (DataSet)Session["dsExclusive"];
            if (strCategory == "")
            {
                dt = ds.Tables[i];
            }
            else
            {
                dt = ds.Tables[i];
                DataView dv = new DataView(dt);
                dv.RowFilter = "[Category] = '" + strCategory + "'";
                dt = dv.ToTable();
            }
        }
        return dt;
    }

    protected void img_btn_export_to_excel_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    public void ExportToExcel()
    {
        string strDateFormat = (string)Session["dateFormat"];

        DataSet ds = (DataSet)Session["ds"];
        DataTable dt = new DataTable("tblExport");
        dt = ds.Tables[0].Copy();



        string str_station_name = "";

        string str_market_name = "";// getMarketOfStation(str_station_name);

        String strStationName = "<BR><BR>Station: " + str_station_name;
        String strMarket = "<BR><BR>Market: " + str_market_name;

        DateTime dt1 = DateTime.Parse(Session["strStartDate"].ToString());
        DateTime dt2 = DateTime.Parse(Session["strEndDate"].ToString());

        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }

        String strDateRange = "<BR><BR>From: " + sDt1 + "  to  " + sDt2;

        ExportToExcel(dt, Response, strStationName, strMarket, strDateRange);
    }

    public void ExportToExcel(DataTable dt, HttpResponse response, String strStationName, string strMarket, String strDateRange)
    {

        //first let's clean up the response.object
        response.Clear();
        response.Charset = "";
        //set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel";
        //create a string writer
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //create an htmltextwriter which uses the stringwriter
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //instantiate a datagrid
        DataGrid dg = new DataGrid();
        //set the datagrid datasource to the dataset passed in
        if (dt == null)
        {
            return;
        }
        else if (dt.Rows.Count == 0)
        {
            return;
        }
        dg.DataSource = dt;
        //bind the datagrid
        dg.DataBind();

        dg.RenderControl(htmlWrite);


        String str = stringWrite.ToString();

        int start, end;
        start = 0;
        end = 0;

        int colspan = 0;

        colspan = dt.Columns.Count;


        String strReportName = "";
        strReportName = "AirCheck India Category Analysis Exclusive Report for Station: " + lblStation.Text;



        string strFinal = "<tr><td align=center colspan=" + colspan + " style='background-color:#FFFF66;'>" + strReportName + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strDateRange + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + ">&nbsp;</td></tr>";



        while (str.IndexOf("<select name=") > 0)
        {
            start = str.IndexOf("<select name=");
            end = str.IndexOf("</select>") + 9;
            str = str.Replace(str.Substring(start, end - start), "");
        }

        string strAirTime = "";
        string strPlayTime = "";
        string strNewPlayTime = "";

        int intCount = 0;

        foreach (DataRow dr in dt.Rows)
        {
            strAirTime = dr[0].ToString();
            if (strAirTime == "")
            {
                strPlayTime = dr[2].ToString();
                strNewPlayTime = dr[2].ToString() + " ";

                str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=5 align='Center'>" + strNewPlayTime + "</td></tr>");

            }
            else
            {
                intCount = intCount + 1;
            }
        }


        str = strFinal + "<BR><BR>" + str;


        string strDateFormat = (string)Session["dateFormat"];

        DateTime dtDate = System.DateTime.Now;
        string strDate = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dtDate.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dtDate.ToString("MM/dd/yyyy");
        }


        string strTime = System.DateTime.Now.ToString();
        strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


        string strCountText = "<b>" + intCount + " rows </b>";

        string strFooterText = "";

        strFooterText = "<BR><table>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>This data was generated and exported on " + strDate + " at " + strTime + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>" + strCountText + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><BR></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.</td></tr>";
        strFooterText += "</table>";

        str = "<table>" + str + strFooterText;



        response.Write(str);
        response.End();
    }
}
