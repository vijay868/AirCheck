﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;

public partial class brandAnalysis_details : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DateTime dtStartDate = DateTime.Parse(Request.QueryString["startDate"]);
        DateTime dtEndDate = DateTime.Parse(Request.QueryString["endDate"]);

        string strStartDate = getFormattedDate(dtStartDate);
        string strEndDate = getFormattedDate(dtEndDate);


        label_Date_Range.Text = strStartDate + " to " + strEndDate;

        string strStationName, strMarketName;
        //int station_id;

        //get_station_and_market(Request.QueryString["strStationName"].Replace("(Played)", ""), out station_id, out strStationName, out strMarketName);

        int station_id;
        get_station_and_market(Request.QueryString["strStationName"].Replace("(Played)", ""), out station_id, out strStationName, out strMarketName);


        label_station.Text = strStationName;
        label_market.Text = strMarketName;
        label_brand.Text = undoStringSanitation(Request.QueryString["strAccountName"]);
        label_parent.Text = undoStringSanitation(Request.QueryString["strParentName"]);
        label_category.Text = get_class_for_account_and_parent(undoStringSanitation(Request.QueryString["strAccountName"]), undoStringSanitation(Request.QueryString["strParentName"]));

        bindGrid(station_id);
    }

    protected void get_station_and_market(string strStationShort, out int stationID, out string strStationName, out string strMarketName)
    {
        stationID = 0;
        strStationName = "";
        strMarketName = "";

        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("get_station_and_market_for_full_station_name", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@stationName", strStationShort);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                stationID = int.Parse(dr["station_id"].ToString());
                strStationName = dr["Station"].ToString();
                strMarketName = dr["Market"].ToString();
            }
        }

        conn.Close();
    }

    protected void bindGrid(int station_id)
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("BrandAnalysisDetail", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@stationIDs", station_id);
        da.SelectCommand.Parameters.AddWithValue("@startDate", Request.QueryString["startDate"]);
        da.SelectCommand.Parameters.AddWithValue("@endDate", Request.QueryString["endDate"]);
        da.SelectCommand.Parameters.AddWithValue("@searchString", Session["searchString"].ToString().Replace("'", "''"));
        da.SelectCommand.Parameters.AddWithValue("@stationName", Request.QueryString["strStationName"].Replace("(Played)", ""));
        da.SelectCommand.Parameters.AddWithValue("@accountName", undoStringSanitation(Request.QueryString["strAccountName"]));
        da.SelectCommand.Parameters.AddWithValue("@parentName", undoStringSanitation(Request.QueryString["strParentName"]));

        DataSet ds = new DataSet();
        da.Fill(ds);

        label_duration.Text = getMinutesAndSeconds(int.Parse(ds.Tables[0].Rows[0][0].ToString()));

        datagrid_disp_details.DataSource = ds.Tables[1];
        datagrid_disp_details.DataBind();
    }

    protected string getFormattedDate(DateTime dt)
    {
        string strDate = "";
        string strDateFormat = (string)Session["dateFormat"];
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dt.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dt.ToString("MM/dd/yyyy");
        }
        return strDate;
    }

    protected string getMinutesAndSeconds(int intSec)
    {
        int min = intSec / 60;
        int sec = intSec % 60;

        string strDuration = (min + " Minute(s) and " + sec + " Second(s)");
        return strDuration;
    }

    protected void getStationAndMarketNames(out int station_id, out string strStationName, out string strMarketName)
    {
        station_id = 0;
        strStationName = "";
        strMarketName = "";

        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("SELECT station_id,[name] AS 'Station',(SELECT [name] FROM aircheck..Markets WHERE market_id=Stations.market_id) AS 'Market' FROM Aircheck..Stations WHERE [name] = @stationPassed", conn);
        comm.Parameters.AddWithValue("@stationPassed", Request.QueryString["strStationName"].Trim());
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                station_id = int.Parse(dr["station_id"].ToString());
                strStationName = dr["Station"].ToString();
                strMarketName = dr["Market"].ToString();
            }
        }

        conn.Close();
    }


    protected string get_class_for_account_and_parent(string strAccountName, string strParentName)
    {
        string strClass = "";

        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("get_class_for_account_and_parent", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@accountName", strAccountName);
        comm.Parameters.AddWithValue("@parentName", strParentName);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                strClass = dr["corpclassdesc"].ToString();
            }
        }

        conn.Close();
        return strClass;
    }


    protected void datagrid_disp_details_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "12px";

        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Cells[0].Text = getFormattedDate(DateTime.Parse(e.Item.Cells[0].Text));
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }
        }
    }

    protected string undoStringSanitation(string str)
    {
        if (str.IndexOf("123123") >= 0)
        {
            str = str.Replace("123123", "'");
        }
        if (str.IndexOf("456456") >= 0)
        {
            str = str.Replace("456456", "&");
        }

        return str;
    }
}
