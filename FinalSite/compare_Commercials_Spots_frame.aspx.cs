﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class compare_Commercials_Spots_frame : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["dtStartDate"] = Request.QueryString["s_date"];
        Session["dtEndDate"] = Request.QueryString["e_date"];
        Session["stationSelected"] = Request.QueryString["stationSelected"];
        Session["stationToCompare"] = Request.QueryString["stationToCompare"];
    }
}
