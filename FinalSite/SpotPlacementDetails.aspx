﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SpotPlacementDetails.aspx.cs" Inherits="SpotPlacementDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Spot Placement Details</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="css/report_caption.css" rel="stylesheet" text="text/css">
		<script language="javascript">
            function HideIcons() {
                prn.style.display = 'none';
            }
        </script>
	</head>
	<body leftmargin="0" rightmargin="0" topmargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="53" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="top" align="top" width="760" height="53" border="0"><img src="images/bgTop.gif"></td>
					<td vAlign="top" width="100%"><IMG height="28" src="images/bgTopExt.gif" width="100%" border="0"></td>
				</tr>
			</table>
			<span name="prn" ID="prn">
				<table height="0" cellSpacing="0" cellPadding="0" width="100%" border="0">
					<tr>
						<td width="100%" align="right" colspan="2"><a href="#" onClick="prn.style.display='none';print();"><IMG src="images/icon_print.gif" border="0"></a>&nbsp;&nbsp;</td>
					</tr>
				</table>
			</span>
			<table width="100%">
				<tr>
					<td class="date" width="12%">Date Range</td>
					<td class="date" width="1%">:</td>
					<td width="85%"><asp:Label ID="label_Date_Range" Runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr>
					<td class="date" width="12%">Search</td>
					<td class="date" width="1%">:</td>
					<td width="85%"><asp:Label ID="label_search" Runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr>
					<td class="date" width="12%">Position</td>
					<td class="date" width="1%">:</td>
					<td width="85%"><asp:Label ID="label_position" Runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr>
					<td class="date" width="12%">Break Start Time</td>
					<td class="date" width="1%">:</td>
					<td width="85%"><asp:Label ID="label_break_start_time" Runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr>
					<td class="date" width="100%" colspan="3">&nbsp;</td>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<td width="100%">
						<asp:DataGrid ID="datagrid_disp_details" Runat="server" Width="100%" 
                            onitemdatabound="datagrid_disp_details_ItemDataBound">
							<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</asp:DataGrid>
					</td>
				</tr>
				<tr>
					<td width="100%">&nbsp;</td>
				</tr>
			</table>
		</form>
	</body>
</html>
