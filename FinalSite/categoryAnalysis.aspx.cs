﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using Telerik.Web.UI;

public partial class categoryAnalysis : System.Web.UI.Page
{
    Boolean shouldAddNewColumn = true;
    bool exportToExcel = false;

    protected void Page_Load(object sender, EventArgs e)
    {

        StringBuilder onloadScript = new StringBuilder();
        //onloadScript.Append("<script type='text/javascript'>");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("ShowReport();");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("</script>");
        //this.ClientScript.RegisterStartupScript(this.GetType(), "onLoadCall", onloadScript.ToString());

        //((Label)this.Master.FindControl("lblReportName")).Text = "Category Analysis";


        getDateFormat();
        string strDateFormat = (string)Session["dateFormat"];
        if (strDateFormat == "DD/MM/YYYY")
        {
            dtStart.DateInput.DateFormat = "dd/MM/yyyy";
            dtEnd.DateInput.DateFormat = "dd/MM/yyyy";

            dtStart.DateInput.DisplayDateFormat = "dd/MM/yyyy";
            dtEnd.DateInput.DisplayDateFormat = "dd/MM/yyyy";
        }


        if (!IsPostBack)
        {
            getUserPreferencesForCurrentUser();
            litExportTo.Visible = false;
            btnExportToExcel.Visible = false;
            setDate();
            GenerateTreeView();
            if (Session["dataReadOnly"].ToString() == "True")
            {
                btnExportToExcel.Enabled = false;

            }
            else
            {
                btnExportToExcel.Enabled = true;

            }
        }
        Pg_Title.Text = "Category Analysis";
    }

    protected void getUserPreferencesForCurrentUser()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getUserPreferenceForAUserAndReport", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@user", (string)Session["user"]);
        da.SelectCommand.Parameters.AddWithValue("@reportName", "Category Analysis");

        DataSet ds = new DataSet();
        da.Fill(ds);

        Session["headerFontName"] = ds.Tables[0].Rows[0]["headerFontName"].ToString();
        Session["headerFontSize"] = ds.Tables[0].Rows[0]["headerFontSize"].ToString();
        Session["headerFontColor"] = ds.Tables[0].Rows[0]["headerFontColor"].ToString();
        Session["headerRowColor"] = ds.Tables[0].Rows[0]["headerRowColor"].ToString();
        Session["headerFontStyle"] = ds.Tables[0].Rows[0]["headerFontStyle"].ToString();
        Session["dataFontName"] = ds.Tables[0].Rows[0]["dataFontName"].ToString();
        Session["dataFontSize"] = ds.Tables[0].Rows[0]["dataFontSize"].ToString();
        Session["dataFontColor"] = ds.Tables[0].Rows[0]["dataFontColor"].ToString();
        Session["alternateRowColor"] = ds.Tables[0].Rows[0]["alternateRowColor"].ToString();
        Session["dataFontStyle"] = ds.Tables[0].Rows[0]["dataFontStyle"].ToString();

    }


    protected void GenerateTreeView()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getStationsForUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);

        var radTreeCities = (RadTreeView)CmbTreeCities.Items[0].FindControl("radTreeCities");
        radTreeCities.DataFieldID = "name";
        radTreeCities.DataFieldParentID = "m_name";
        radTreeCities.DataTextField = "name";

        radTreeCities.DataSource = ds.Tables[0];
        radTreeCities.DataBind();
    }

    protected void setDate()
    {
        DateTime lockDataBefore = DateTime.Parse(Session["lockDataBefore"].ToString());
        DateTime lockDataAfter = DateTime.Parse(Session["lockDataAfter"].ToString());
        dtStart.MinDate = lockDataBefore;
        dtEnd.MinDate = lockDataBefore;

        dtStart.MaxDate = lockDataAfter;
        dtEnd.MaxDate = lockDataAfter;

        dtStart.SelectedDate = System.DateTime.Now;
        dtEnd.SelectedDate = System.DateTime.Now;
    }

    protected void getDateFormat()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("select * from Country_dateFormat where isSelected=1", conn);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                Session["dateFormat"] = dr["DateFormat"].ToString();
            }
        }

        conn.Close();
    }

    protected string getStationIDs()
    {
        string strStations = string.Empty;
        var radTreeCities = (RadTreeView)CmbTreeCities.Items[0].FindControl("radTreeCities");
        //ArrayList nodeCollection = radTreeCities.CheckedNodes;
        int stationID = 0;
        string strStationNames = "";

        foreach (RadTreeNode node in radTreeCities.CheckedNodes)
        {
            stationID = getStationIDOfStation(node.Text);
            if (stationID != 0)
            {
                strStations += stationID.ToString() + ",";
                strStationNames += node.Text + ",";
            }
        }

        return strStations.Substring(0, strStations.Length - 1);
    }

    protected int getStationIDOfStation(string txtStation)
    {
        int stationID = 0;
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("getStationIDOfStation", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@stationName", txtStation);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                stationID = (int)dr["station_id"];
            }
        }
        else
        {
            stationID = 0;
        }

        conn.Close();
        return stationID;
    }

    protected void addAuditLog(DateTime startDate, DateTime endDate, string station,
Boolean exportedToExcel, Boolean exportedToPdf, Boolean exportedToText)
    {
        auditTrail at = new auditTrail();
        at.connectionString = ConfigurationSettings.AppSettings["connectionString"].ToString();
        at.userID = (int)Session["userID"];
        at.IPAddress = Request.ServerVariables["REMOTE_ADDR"];
        at.pageViewed = "Category Analysis";
        at.startDate = startDate;
        at.endDate = endDate;
        at.station = station;
        at.exportedToExcel = exportedToExcel;
        at.exportedToPdf = exportedToPdf;
        at.exportedToText = exportedToText;
        at.addUserActivityLogPassingStationIDs();
        at = null;
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        pnlDate.Visible = true;
        ViewState["info"] = null;
        string strStationIDs = getStationIDs();
        exportToExcel = false;

        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());

        string strStartDate = dtStartDate.ToString("d");
        string strEndDate = dtEndDate.ToString("d");

        Session["searchString"] = txtSearch.Text.Trim();


        addAuditLog(dtStartDate, dtEndDate, strStationIDs, false, false, false);


        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("categoryAnalysisRevised", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@stationIDs", strStationIDs);
        da.SelectCommand.Parameters.AddWithValue("@startDate", strStartDate);
        da.SelectCommand.Parameters.AddWithValue("@endDate", strEndDate);
        da.SelectCommand.Parameters.AddWithValue("@searchText", txtSearch.Text.Trim().Replace("'", "''"));
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

        Session["searchText"] = txtSearch.Text.Trim();

        DataSet ds = new DataSet();
        da.Fill(ds);
        gvData.FooterStyle.CssClass = "FF";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
        gvData.AlternatingRowStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);

        //gvData.AlternatingRowStyle.BackColor = System.Drawing.Color.FromArgb(149, 179, 215);
        try
        {
            if (ds.Tables.Count == 0)
            {
                litExportTo.Visible = false;
                btnExportToExcel.Visible = false;
                //imgChart.Visible = false;
                gvData.Visible = false;
                lblError.Visible = true;
                lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
                return;
            }
            else if (ds.Tables[0].Rows.Count == 0)
            {
                litExportTo.Visible = false;
                btnExportToExcel.Visible = false;
                //imgChart.Visible = false;
                gvData.Visible = false;
                lblError.Visible = true;
                lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
                return;
            }
            else
            {
                DataTable dt = dtAddTotalRow(ds.Tables[0]);

                Session["station_selected"] = ds.Tables[1].Rows[0][1].ToString();


                Session["data"] = dt;
                litExportTo.Visible = false;
                //imgChart.Visible = true;
                btnExportToExcel.Visible = false;
                lblError.Text = "";
                lblError.Visible = false;
                gvData.Visible = true;

                try
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        int[] intArray = commaStrToArray(dr[0].ToString());
                        info.AddMergedColumns(intArray, dr[1].ToString());
                    }
                }
                catch
                {
                    shouldAddNewColumn = false;
                }

                Session["myDataTableExport"] = dt;

                litExportTo.Visible = true;
                btnExportToExcel.Visible = true;

                gvData.DataSource = dt;
                gvData.DataBind();


            }
        }
        catch
        {
            litExportTo.Visible = false;
            btnExportToExcel.Visible = false;
            gvData.Visible = false;
            lblError.Visible = true;
            lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
            return;
        }

    }

    protected int getTotalForAColumn(DataTable dt, DataColumn dc)
    {
        int totalOfColumn = 0;
        try
        {
            totalOfColumn = int.Parse(dt.Compute("sum([" + dc.ColumnName + "])", "[Category Name] <> 'Total'").ToString());
        }
        catch
        {

        }
        return totalOfColumn;
    }


    protected int getCountForAColumn(DataTable dt, DataColumn dc)
    {
        int totalOfColumn = 0;
        try
        {
            totalOfColumn = int.Parse(dt.Compute("count([" + dc.ColumnName + "])", "[Category Name] <> 'Total'").ToString());
        }
        catch
        {

        }
        return totalOfColumn;
    }

    protected int getSumForAColumn(DataTable dt, DataColumn dc)
    {
        int totalOfColumn = 0;
        int avgOfRow = 0;
        int colIndex = 0;

        foreach (DataRow dr in dt.Rows)
        {
            if (dr[0] != "Total")
            {
                colIndex = 0;
                foreach (DataColumn dc1 in dt.Columns)
                {

                    if (dc1 == dc)
                    {
                        try
                        {
                            avgOfRow = int.Parse(dr[colIndex].ToString()) / int.Parse(dr[colIndex - 1].ToString());
                        }
                        catch
                        {
                            avgOfRow = 0;
                        }
                        totalOfColumn = totalOfColumn + avgOfRow;
                    }
                    colIndex = colIndex + 1;
                }
            }
        }

        //try
        //{
        //    totalOfColumn = int.Parse(dt.Compute("sum([" + dc.ColumnName + "])", "[Category Name] <> 'Total'").ToString());
        //}
        //catch
        //{

        //}
        return totalOfColumn;
    }
    protected DataTable dtAddTotalRow(DataTable dt)
    {
        DataRow drFooter = dt.NewRow();
        foreach (DataColumn dc in dt.Columns)
        {
            if (dc.ColumnName.IndexOf("(#Plays)") > 0)
            {
                drFooter[dc] = getCountForAColumn(dt, dc);
            }
            if (dc.ColumnName.IndexOf("(#Seconds)") > 0)
            {
                drFooter[dc] = getSumForAColumn(dt, dc);
            }
            if (dc.ColumnName == "Category Name")
            {
                drFooter[dc] = "Total";
            }
        }
        dt.Rows.Add(drFooter);
        dt.AcceptChanges();

        return dt;
    }

    private int[] commaStrToArray(string strIntComma)
    {
        string[] strArray;
        strArray = strIntComma.Split(new char[] { ',' });
        int[] intArray = new int[strArray.Length];
        for (int i = 0; i < strArray.Length; i++)
            intArray[i] = int.Parse(strArray[i]);
        return intArray;
    }

    protected void img_btn_export_to_excel_Click(object sender, EventArgs e)
    {
        string strStationIDs = getStationIDs();
        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());
        addAuditLog(dtStartDate, dtEndDate, strStationIDs, true, false, false);

        exportToExcel = true;
        ExportToExcel();
    }

    protected DataTable getDataTableToExport()
    {
        string strDateFormat = (string)Session["dateFormat"];
        DataTable dt = new DataTable();
        dt = (DataTable)Session["myDataTableExport"];

        return dt;
    }

    public void ExportToExcel()
    {
        string strDateFormat = (string)Session["dateFormat"];
        DataTable dt = getDataTableToExport();


        string str_station_name = "";

        string str_market_name = "";// getMarketOfStation(str_station_name);

        String strStationName = "<BR><BR>Station: " + str_station_name;
        String strMarket = "<BR><BR>Market: " + str_market_name;

        DateTime dt1 = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dt2 = DateTime.Parse(dtEnd.SelectedDate.ToString());

        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }

        String strDateRange = "<BR><BR>From: " + sDt1 + "  to  " + sDt2;

        ExportToExcel(dt, Response, strStationName, strMarket, strDateRange);
    }

    public void ExportToExcel(DataTable dt, HttpResponse response, String strStationName, string strMarket, String strDateRange)
    {

        //first let's clean up the response.object
        response.Clear();
        response.Charset = "";
        //set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel";
        //create a string writer
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //create an htmltextwriter which uses the stringwriter
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //instantiate a datagrid
        DataGrid dg = new DataGrid();
        //set the datagrid datasource to the dataset passed in
        if (dt == null)
        {
            return;
        }
        else if (dt.Rows.Count == 0)
        {
            return;
        }
        dg.DataSource = dt;
        //bind the datagrid
        dg.DataBind();

        dg.RenderControl(htmlWrite);


        String str = stringWrite.ToString();

        int start, end;
        start = 0;
        end = 0;

        int colspan = 0;

        colspan = dt.Columns.Count;


        String strReportName = "Aircheck Category Analysis Report";



        string strFinal = "<tr><td align=center colspan=" + colspan + " style='background-color:#FFFF66;'>" + strReportName + "</td></tr>";
        //strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strStationName + "</td></tr>";
        //strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strMarket + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strDateRange + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + ">&nbsp;</td></tr>";



        while (str.IndexOf("<select name=") > 0)
        {
            start = str.IndexOf("<select name=");
            end = str.IndexOf("</select>") + 9;
            str = str.Replace(str.Substring(start, end - start), "");
        }
        //str = str.Replace("<tr>\r\n\t\t<td>&nbsp;</td><td>&nbsp;</td>", "<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td>");

        string strAirTime = "";
        string strPlayTime = "";
        string strNewPlayTime = "";

        int intCount = 0;

        foreach (DataRow dr in dt.Rows)
        {
            strAirTime = dr[0].ToString();
            if (strAirTime == "")
            {
                strPlayTime = dr[2].ToString();
                strNewPlayTime = dr[2].ToString() + " ";

                if ((string)Session["category"] == "Song")
                {
                    str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=4 align='Center'>" + strNewPlayTime + "</td></tr>");
                }
                else
                {
                    str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=5 align='Center'>" + strNewPlayTime + "</td></tr>");
                }
            }
            else
            {
                intCount = intCount + 1;
            }
        }


        str = strFinal + "<BR><BR>" + str;


        string strDateFormat = (string)Session["dateFormat"];

        DateTime dtDate = System.DateTime.Now;
        string strDate = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dtDate.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dtDate.ToString("MM/dd/yyyy");
        }


        string strTime = System.DateTime.Now.ToString();
        strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


        string strCountText = "<b>" + intCount + " rows </b>";

        string strFooterText = "";

        strFooterText = "<BR><table>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>This data was generated and exported on " + strDate + " at " + strTime + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>" + strCountText + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><BR></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.</td></tr>";
        strFooterText += "</table>";

        str = "<table>" + str + strFooterText;



        response.Write(str);
        response.End();
    }




    //property for storing of information about merged columns
    private MergedColumnsInfo info
    {
        get
        {
            if (ViewState["info"] == null)
                ViewState["info"] = new MergedColumnsInfo();
            return (MergedColumnsInfo)ViewState["info"];
        }
    }

    protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
    {
        //call the method for custom rendering the columns headers	
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.SetRenderMethodDelegate(RenderHeader);
        }
    }

    private System.Drawing.Color getColor(int i)
    {
        System.Drawing.Color xCol = System.Drawing.Color.FromArgb(0, 112, 192);

        if (i == 0)
        {
            xCol = System.Drawing.Color.FromArgb(0, 112, 192);
        }
        else
        {
            if (i % 2 == 1)
            {
                xCol = System.Drawing.Color.FromArgb(31, 73, 125);
            }
        }

        return xCol;
    }

    private string getColor1(int i)
    {
        System.Drawing.Color c;
        string xCol = "";
        if (i == 0)
        {
            xCol = "#000066";
        }
        else if (i == 1)
        {
            xCol = "#0066FF";
        }
        else if (i == 2)
        {
            xCol = "#003366";
        }
        else if (i == 3)
        {
            xCol = "#996666";
        }
        else if (i == 4)
        {
            xCol = "#CCFF99";
        }
        else if (i == 5)
        {
            xCol = "#FFFF00";
        }
        else if (i == 6)
        {
            xCol = "#00FF66";
        }
        else
        {
            xCol = "#660066";
        }



        c = System.Drawing.ColorTranslator.FromHtml(xCol);
        return xCol;
    }

    //method for rendering the columns headers	
    private void RenderHeader(HtmlTextWriter output, Control container)
    {
        int j = 0;
        System.Drawing.Color c = System.Drawing.Color.SkyBlue;// System.Drawing.ColorTranslator.FromHtml("#99CCCC");
        string strColWidth = "";
        string strColor = "";
        for (int i = 0; i < container.Controls.Count; i++)
        {
            TableCell cell = (TableCell)container.Controls[i];
            //stretch non merged columns for two rows
            if (!info.MergedColumns.Contains(i))
            {
                cell.Attributes["rowspan"] = "2";
                System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
                cell.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);

                //cell.Width = Unit.Pixel(300);
                //cell.ForeColor = System.Drawing.Color.White;
                cell.RenderControl(output);
            }
            else //render merged columns common title
                if (info.StartColumns.Contains(i))
                {
                    if (j % 2 == 0)
                    {
                        strColor = (string)Session["headerRowColor"];
                    }
                    else
                    {
                        strColor = (string)Session["headerRowColor"];
                    }

                    strColWidth = (int.Parse(info.StartColumns[i].ToString()) * 50).ToString() + "px";
                    output.Write(string.Format("<th bgcolor='" + strColor + "' align='center' colspan='{0}' style='color:" + (string)Session["headerFontColor"] + ";font-size:" + Session["headerFontSize"].ToString().Replace("Px", "pt") + ";font-weight:" + (string)Session["headerFontStyle"] + ";font-family:" + (string)Session["headerFontName"] + ";'>{1}</th>",
                             info.StartColumns[i], info.Titles[i]));
                    j++;
                }
        }

        //close the first row	
        output.RenderEndTag();
        //set attributes for the second row
        gvData.HeaderStyle.AddAttributesToRender(output);
        //start the second row
        output.RenderBeginTag("tr");

        //render the second row (only the merged columns)
        for (int i = 0; i < info.MergedColumns.Count; i++)
        {
            TableCell cell = (TableCell)container.Controls[info.MergedColumns[i]];
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            cell.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);

            cell.RenderControl(output);
        }
    }

    private string InsertLineBreaks(string _emailString)
    {
        int MaxStringLength = 40;

        if (_emailString.Length > MaxStringLength)
        {
            int indexOfSpace = _emailString.IndexOf(" ", MaxStringLength - 1);

            if ((indexOfSpace != -1) && (indexOfSpace != _emailString.Length - 1))
            {
                string firstString = _emailString.Substring(0, indexOfSpace);
                string secondString = _emailString.Substring(indexOfSpace);

                return firstString + "<br>" + InsertLineBreaks(secondString);
            }
            else
            {
                return _emailString;
            }
        }
        else
        {
            return _emailString;
        }
    }

    protected string doStringSanitation(string str)
    {
        if (str.IndexOf("'") >= 0)
        {
            str = str.Replace("'", "123123");
        }
        if (str.IndexOf("&") >= 0)
        {
            str = str.Replace("&amp;", "456456");
        }

        return str;
    }


    int stCount;
    protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "12px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "12px";

        string footerFontName = "Helvetica";//ConfigurationSettings.AppSettings["footerFontNameMarketShareOfEachStation"].ToString();
        string footerFontSize = "14px";//ConfigurationSetting       


        int maxColIndex = 0;       



        DataTable dt = (DataTable)Session["data"];

        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());

        string strStartDate = dtStartDate.ToString("d");
        string strEndDate = dtEndDate.ToString("d");

        int indexOfBlank = 0;


        if (e.Row.RowType == DataControlRowType.Header)
        {
            int colIndex = 0;
            stCount = 0;

            foreach (DataColumn dc in dt.Columns)
            {
                if (dc.ColumnName.IndexOf("(#Plays)") > 0 && dc.ColumnName.IndexOf("Total") == -1)
                {
                    stCount = stCount + 1;
                }


                
                if (colIndex <= maxColIndex)
                {                    
                    e.Row.Cells[colIndex].Style.Add("white-space", "nowrap");
                    e.Row.Cells[colIndex].Style.Add("font-family", headerFontName);
                    e.Row.Cells[colIndex].Style.Add("font-size", "16px");
                    if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                    {
                       e.Row.Cells[colIndex].Font.Bold = true;
                    }
                    else
                    {
                        e.Row.Cells[colIndex].Font.Bold = false;
                    }                    
                    e.Row.Cells[colIndex].Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
                }
                else
                {
                    indexOfBlank = e.Row.Cells[colIndex].Text.LastIndexOf(" ");
                    if (e.Row.Cells[colIndex].Text.IndexOf("(#Plays)") > 0)
                    {
                        e.Row.Cells[colIndex].Text = e.Row.Cells[colIndex].Text.Substring(0, indexOfBlank) + "<BR>(# Plays)";
                    }
                    else
                    {
                        e.Row.Cells[colIndex].Text = e.Row.Cells[colIndex].Text.Substring(0, indexOfBlank) + "<BR>(# Seconds)";
                    }
                    e.Row.Cells[colIndex].Style.Add("white-space", "nowrap");
                    e.Row.Cells[colIndex].Style.Add("Width", "50px");
                    e.Row.Cells[colIndex].Style.Add("font-family", headerFontName);
                    e.Row.Cells[colIndex].Style.Add("font-size", headerFontSize);
                    if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                    {
                        //e.Item.Style.Add("font-weight", "bold");
                        e.Row.Cells[colIndex].Font.Bold = true;
                    }
                    else
                    {
                        e.Row.Cells[colIndex].Font.Bold = false;
                    }
                    //e.Row.Cells[colIndex].Style.Add("font-size", headerFontSize);
                    e.Row.Cells[colIndex].Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
                }
                System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
                e.Row.Cells[colIndex].ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);                
                colIndex++;
            }


        }


        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int j = 0;
            int intNoOfPlays = 0;

            int songParentStartID = 0;


            string strCategory = doStringSanitation(e.Row.Cells[0].Text);

            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            e.Row.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);


            string strStationName = "";
            bool isFooter = false;

            foreach (DataColumn dc in dt.Columns)
            {
                if (dc.ColumnName.IndexOf("(#Plays)") > 0)
                {
                    strStationName = dc.ColumnName.Replace("(#Plays)", "");
                }
                else
                {
                    strStationName = dc.ColumnName.Replace("(#Seconds)", "");
                }
                e.Row.Cells[j].Style.Add("font-family", actualDataFontName);
                //e.Row.Cells[j].Style.Add("font-size", actualDataFontSize);
                e.Row.Cells[j].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
                //e.Row.Cells[j].Style.Add("font-weight", "bold");
                if (Session["dataFontStyle"].ToString().ToLower() == "bold")
                {
                    //e.Item.Style.Add("font-weight", "bold");
                    e.Row.Cells[j].Font.Bold = true;
                }
                else
                {
                    e.Row.Cells[j].Font.Bold = false;
                }
                //if (j == 0 || j == 1 || j == 2)
                if (j <= maxColIndex)
                {
                    e.Row.Cells[j].Text = InsertLineBreaks(e.Row.Cells[j].Text);

                    e.Row.Cells[j].Style.Add("white-space", "nowrap");
                }
                //if (j > 2)
                if (j > maxColIndex)
                {
                    e.Row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                    e.Row.Cells[j].Style.Add("white-space", "nowrap");
                    bool isANumber = false;
                    int number = 0;
                    isANumber = int.TryParse(e.Row.Cells[j].Text.Trim(), out number);
                    if (isANumber)
                    {
                        e.Row.Cells[j].Text = "<a href='' onclick=\"javascript:return disp_details('" + strStartDate + "','" + strEndDate + "','" + strCategory + "','" + strStationName + "')\"  alt='Click here to see the details'>" + e.Row.Cells[j].Text + "</a>";
                    }
                }
                j++;
                if (dc.ColumnName == "Category Name")
                {
                    if (e.Row.Cells[j].Text.Trim() == "Total")
                    {
                        isFooter = true;
                    }
                }


            }



            if (e.Row.Cells[maxColIndex].Text == "Total")
            {
                if (!exportToExcel)
                {
                    int colIndex = 0;
                    int arrTotalIndex = 0;
                    int arrSecondsIndex = 0;
                    foreach (DataColumn dc in dt.Columns)
                    {
                        e.Row.Cells[colIndex].Style.Add("font-family", headerFontName);
                        if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                        {
                            e.Row.Cells[colIndex].Font.Bold = true;
                        }
                        else
                        {
                            e.Row.Cells[colIndex].Font.Bold = false;
                        }
                        e.Row.Cells[colIndex].Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());

                        if (dc.ColumnName.IndexOf("(#Plays)") > 0 && dc.ColumnName.IndexOf("Total") == -1)
                        {
                            if (e.Row.Cells[colIndex].Text.Trim() != "")
                            {
                                //arr_selected_station_names[arrTotalIndex] = dc.ColumnName.Replace("(#Plays)", "");
                                //arr_total[arrTotalIndex] = int.Parse(e.Row.Cells[colIndex].Text.Trim());
                                arrTotalIndex = arrTotalIndex + 1;
                            }
                        }
                        else if (dc.ColumnName.IndexOf("(#Seconds)") > 0 && dc.ColumnName.IndexOf("Total") == -1)
                        {
                            //arr_total_exclusive[arrSecondsIndex] = int.Parse(e.Row.Cells[colIndex].Text.Trim());
                            arrSecondsIndex = arrSecondsIndex + 1;
                        }
                        colIndex = colIndex + 1;
                    }
                    e.Row.Visible = false;
                }
                else
                {
                    e.Row.Visible = true;
                }
            }

        }

        if (e.Row.RowType == DataControlRowType.Footer)
        {
            //System.Drawing.Color c = getColor(0);//.Drawing.Color.SkyBlue;// System.Drawing.ColorTranslator.FromHtml("#99CCCC");
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            System.Drawing.Color c = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);

            e.Row.Cells[0].ColumnSpan = maxColIndex + 1;
            e.Row.Cells[0].Style.Add("white-space", "nowrap");
            e.Row.Cells[0].BackColor = c;
            e.Row.Cells[0].Text = "Total (Click to see exclusive report)";
            e.Row.Cells[0].Attributes.Add("align", "center");
            //e.Row.Cells[0].Style.Add("font-weight", "bold");
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                //e.Item.Style.Add("font-weight", "bold");
                e.Row.Cells[0].Font.Bold = true;
            }
            else
            {
                e.Row.Cells[0].Font.Bold = false;
            }
            e.Row.Cells[0].Style.Add("font-family", footerFontName);
            e.Row.Cells[0].Font.Size = FontUnit.Parse(footerFontSize.Replace("Px", "").Trim());
            string strStationIDs = getStationIDs();
            e.Row.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

            int i = 0;
            string strStationName = "";

            foreach (DataColumn dc in dt.Columns)
            {
                if (i > 0)
                {
                    e.Row.Cells[i].Style.Add("white-space", "nowrap");
                    e.Row.Cells[i].BackColor = c;
                    e.Row.Cells[i].Style.Add("font-weight", "bold");
                    e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                    if (dc.ColumnName.IndexOf("(#Plays)") > 0 || dc.ColumnName.IndexOf("(#Seconds)") > 0)
                    {
                        if (dc.ColumnName.IndexOf("(#Plays)") > 0)
                        {
                            e.Row.Cells[i].Text = getCountForAColumn(dt, dc).ToString();
                        }
                        if (dc.ColumnName.IndexOf("(#Seconds)") > 0)
                        {
                            e.Row.Cells[i].Text = getSumForAColumn(dt, dc).ToString();
                        }

                        bool isANumber = false;
                        int number = 0;
                        isANumber = int.TryParse(e.Row.Cells[i].Text.Trim(), out number);
                        if (isANumber)
                        {
                            if (dc.ColumnName.IndexOf("(#Plays)") > 0)
                            {
                                strStationName = dc.ColumnName.Replace("(#Plays)", "");
                            }
                            else if (dc.ColumnName.IndexOf("(#Seconds)") > 0)
                            {
                                strStationName = dc.ColumnName.Replace("(#Seconds)", "");
                            }

                            if (e.Row.Cells[i].Text == "0")
                            {

                            }
                            else
                            {
                                e.Row.Cells[i].Text = e.Row.Cells[i - maxColIndex].Text + "  <a href='' onclick=\"javascript:return disp_exclusiveReport('" + strStartDate + "','" + strEndDate + "','" + strStationName + "','" + strStationIDs + "')\"  alt='Click here to see exclusive report'><img src='images/btn_exclusive.gif' border=0/></a>";
                            }
                        }
                    }
                }
                i++;
            }

            //Populate arrays for Chart
            Session["stCount"] = stCount;

        }
    }
}
