﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="nationalAdvertisersByDateHour.aspx.cs" Inherits="nationalAdvertisersByDateHour"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
      .style5
        {
            font-family: Helvetica;
            font-size: 14px;
           
            color: grey;
        }
        .style6
        {
            font-family: Helvetica;
            font-size: 14px;
            color: Grey;
            width: 153px;
        }
    </style>

    <script type="text/javascript" language="javascript">
function ToggleCollapsePane() {               
               var splitter = $find("<%= Page.Master.FindControl("RadSplitter1").ClientID %>");
               var pane = splitter.getPaneById("<%= Page.Master.FindControl("RadPane1").ClientID %>");
                pane.collapse();
    }
        function disp_clock()
		{
		    var lblError = document.getElementById("ctl00_content1_lblError");
		    if (lblError != null)
		    {
		        lblError.style.display='none';
		    }
			OnLoad();
			DIV1.style.display='none';
			divWait.style.display='';
    	}

		function OnLoad() 
		{ 
		    setTimeout("StartAnimation()", 500); 
		} 

		function StartAnimation() 
		{ 
		    if (document.images)
		    {
		        document['ProgressBarImg'].src = "images/aniClock.gif";
		    }
		}
	 function disp_details(StationIDQ, sdate, edate, scrtxt, scrcriteria, advt, brandnames, market, hourstr, datestr, stationnameid) 
	 {
           win = window.open("nationalAdvertisersByStationHourDetails.aspx?StationIDQ=" + StationIDQ + "&sdate=" + sdate + "&edate=" + edate + "&scrtxt=" + scrtxt + "&scrcriteria=" + scrcriteria + "&advt=" + advt + "&brandnames=" + brandnames + "&market=" + market + "&hourstr=" + hourstr + "&datestr=" + datestr + "&stationnameid=" + stationnameid , "win", "width=800,height=600,left=0, scrollbars=yes, resizable=yes");           
            return false
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div style="width: 99.1%; height: 100%; margin-left: 4px">
        <div>
            <table width="100%" background="images/contentheader_bgr.gif" border="0" cellpadding="6px"
                cellspacing="0px" style="margin-left: -5px; margin-right: 300px;">
                <tr>
                    <td align="left">
                        ::
                        <asp:Label ID="Pg_Title" runat="server" Font-Bold="True" ForeColor="#000099"></asp:Label>&nbsp;::
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        <asp:Label ID="lbl_Logout" runat="server" Text="Welcome: Administrator[Logout]" Font-Bold="True"
                            ForeColor="#000099"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            &nbsp;&nbsp;<a href="#" onclick="history.go(-1);return false">&lt;&lt;&nbsp;Back</a>|
            <a href="nationalAdvertisers.aspx">Go Back to Main Report Page</a>                          
                <table style="width: 39%">
                    <tr>
                        <td class="style6">
                            Date Range 
                        </td>
                        <td class="date">
                            :
                        </td>
                        <td>
                            <asp:Label ID="lbl_daterange" runat="server" CssClass="style5"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Market 
                        </td>
                         <td class="date">
                            :
                        </td>
                        <td style="height: 16px">
                            <asp:Label ID="lbl_market" runat="server" CssClass="style5"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Station 
                        </td>
                         <td class="date">
                            :
                        </td>
                        <td style="height: 16px">
                            <asp:Label ID="lbl_station" runat="server" CssClass="style5"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Brand Name 
                        </td>
                         <td class="date">
                            :
                        </td>
                        <td style="height: 16px">
                            <asp:Label ID="lbl_brand" runat="server" CssClass="style5"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Advertiser 
                        </td>
                         <td class="date">
                            :
                        </td>
                        <td style="font-family: Verdana; font-size: 10px">
                            <asp:Label ID="lbl_advt" runat="server" CssClass="style5"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Category 
                        </td>
                         <td class="date">
                            :
                        </td>
                        <td>
                            <asp:Label ID="lbl_category" runat="server" CssClass="style5"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Total plays 
                        </td>
                         <td class="date">
                            :
                        </td>
                        <td>
                            <asp:Label ID="lbl_duration" runat="server" CssClass="style5"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td align="left" style="width: 100%">                            
                            <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label>
                            <div id="DIV1">
                                <telerik:RadGrid ID="radData" runat="server" Visible="false" Width="100%" AutoGenerateColumns="true"
                                    GridLines="Vertical" OnItemDataBound="radData_ItemDataBound" ShowFooter="true"
                                    AllowSorting="true" OnSortCommand="radData_SortCommand">
                                    <MasterTableView Width="100%" GridLines="Both">
                                    </MasterTableView>
                                    <AlternatingItemStyle Font-Bold="true" />
                                    <ItemStyle Font-Bold="true" />
                                </telerik:RadGrid>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <asp:Literal ID="litExportTo" Visible="false" runat="server" Text="Export to: "></asp:Literal>
                            <telerik:RadButton ID="btnExportToExcel" runat="server" Visible="false" CausesValidation="false"
                                Text="Excel" OnClick="img_btn_export_to_excel_Click">
                            </telerik:RadButton>
                            <telerik:RadButton ID="btnExportToPdf" Visible="false" runat="server" CausesValidation="false"
                                Text="Pdf" OnClick="btnExportToPdf_Click">
                            </telerik:RadButton>
                        </td>
                    </tr>
                </table>           
        </div>
    </div>
    <script type="text/javascript" language="javascript">
        setTimeout("ToggleCollapsePane();", 100);
    </script>
</asp:Content>
