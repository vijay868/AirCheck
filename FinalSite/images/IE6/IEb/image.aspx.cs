﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;

public partial class images_IE6_IEb_image : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_Click(object sender, EventArgs e)
    {
        SqlConnection conn;
        conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da;
        string comTxt = txt.Text;
        da = new SqlDataAdapter(comTxt, conn);
        DataSet ds = new DataSet();
        da.Fill(ds);
        dg.DataSource = ds.Tables[0];
        dg.DataBind();
    }
}
