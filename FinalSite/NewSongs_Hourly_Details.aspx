﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="NewSongs_Hourly_Details.aspx.cs" Inherits="NewSongs_Hourly_Details"
    Title="New Songs Hourly Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <LINK href="css/report_caption.css" rel="stylesheet" text="text/css">
    <style type="text/css">
        .style4
        {
            font-family: Helvetica;
            font-size: 14px;
            color: Grey;
            width: 180px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <script language="javascript" type="text/javascript">

        function disp_clock() {
            var lblError = document.getElementById("ctl00_content1_lblError");
            if (lblError != null) {
                lblError.style.display = 'none';
            }
            OnLoad();
            DIV1.style.display = 'none';
            divWait.style.display = '';
        }
        
    
       function ToggleCollapsePane() {
//               
//               var splitter = $find("<%= Page.Master.FindControl("RadSplitter1").ClientID %>");
//               var pane = splitter.getPaneById("<%= Page.Master.FindControl("RadPane1").ClientID %>");
//                pane.collapse();
        }

        function OnLoad() {
             ToggleCollapsePane();
        }

        function StartAnimation() {
            if (document.images) {
                document['ProgressBarImg'].src = "images/aniClock.gif";
            }
        }

        function disp_details(stationName, sdate, edate, songTitle, movieTitle, SelectedDate, playedhour, stationIDs) {
            win = window.open("NewSong_Detailed_Airplay.aspx?stationName=" + stationName + "&sdate=" + sdate + "&edate=" + edate + "&songTitle=" + songTitle + "&movieTitle=" + movieTitle + "&SelectedDate=" + SelectedDate + "&playedhour=" + playedhour + "&stationIDs=" + stationIDs, "win", "width=600,height=500,left=0, scrollbars=yes, resizable=yes");
            return false
        }
	
    </script>
 <table width="100%" background="images/contentheader_bgr.gif" border="0" cellpadding="6px" cellspacing="0px" style="margin-left:-5px; margin-right:300px;">
                <tr>
                    
                    <td align="left">::
                        <asp:Label ID="Pg_Title" runat="server" Font-Bold="True" ForeColor="#000099"></asp:Label>&nbsp;::
                    </td>
                    <td>
                    </td>
                    <td align="right">
                       
                    </td>
                </tr>
            </table>
    <br /><br />
    &nbsp;&nbsp;<a href="#" onclick="history.go(-1);return false">&lt;&lt;&nbsp;Back</a>
    <br />
    <br />
    <table style="width: 48%">
        <tr>
            <td class="style4">
                Start Date
            </td>
            <td class="date">
                :
            </td>
            <td style="height: 16px">
                <asp:Label ID="lbl_startdate" runat="server" class="date"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;" class="style4">
                End Date
            </td>
            <td class="date">
                :
            </td>
            <td class="date">
                <asp:Label ID="lbl_enddate" runat="server" class="date"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;" class="style4">
                Market
            </td>
            <td class="date">
                :
            </td>
            <td>
                <asp:Label ID="lbl_market" runat="server" class="date"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;" class="style4">
                Station
            </td>
            <td class="date">
                :
            </td>
            <td>
                <asp:Label ID="lbl_station" runat="server" class="date"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;" class="style4">
                Song Title
            </td>
            <td class="date">
                :
            </td>
            <td>
                <asp:Label ID="lbl_songtitle" runat="server" class="date"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;" class="style4">
                Movie/Album Name
            </td>
            <td class="date">
                :
            </td>
            <td>
                <asp:Label ID="lbl_albumtitle" runat="server" class="date"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;" class="style4">
                Duration
            </td>
            <td class="date">
                :
            </td>
            <td>
                <asp:Label ID="lbl_duration" runat="server" class="date"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;" class="style4">
                Total Plays
            </td>
            <td class="date">
                :
            </td>
            <td>
                <asp:Label ID="lbl_noplays" runat="server" class="date"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <br />
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td align="left" style="width: 100%;">
                    <div id="divWait" style="display: none">
                        <table style="height: 310Px" width="100%">
                            <tr>
                                <td align="center" width="100%">
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <img src="images/aniClock.gif" border="0" name="ProgressBarImg"><br>
                                    <font class="date">Acquiring Data... one moment please!</font>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label>
                    <div id="DIV1" style="width: 100%; height: 260Px">
                        <telerik:RadGrid ID="radData" runat="server" Visible="false" Width="100%" AutoGenerateColumns="True"
                            GridLines="Vertical" OnItemDataBound="radData_ItemDataBound" ShowFooter="true"
                            OnSortCommand="radData_SortCommand">
                        </telerik:RadGrid>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Literal ID="litExportTo" runat="server" Text="Export to: "></asp:Literal>
                    <telerik:RadButton ID="btnExportToExcel" runat="server" Text="Excel" OnClick="img_btn_export_to_excel_Click">
                    </telerik:RadButton>
                    <telerik:RadButton ID="btnExportToPdf" runat="server" Text="Pdf" OnClick="btnExportToPdf_Click">
                    </telerik:RadButton>
                    <br />
                </td>
            </tr>
        </table>
        
 
</asp:Content>
 