﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using Telerik.Web.UI;

public partial class DownloadPlayListByMarketStation : System.Web.UI.Page
{
    bool isPdfExport = false;

    protected void bindTypes()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getDatewisePlaylistTypesForUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@user", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);

        ddType.DataSource = ds;
        ddType.DataTextField = "Types";
        ddType.DataValueField = "Type";
        ddType.DataBind();
        ddType.SortItems();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        //StringBuilder onloadScript = new StringBuilder();
        //onloadScript.Append("<script type='text/javascript'>");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("ShowReport();");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("</script>");
        //this.ClientScript.RegisterStartupScript(this.GetType(), "onLoadCall", onloadScript.ToString());

        ((Label)this.Master.FindControl("lblReportName")).Text = "Download - PlayList By Market / Station";

        //Literal lit = new Literal();
        //lit.Text = "<script language='javascript'>tdhide('leftMenu', 'ctl00_showhide', 'hide');</script>";
        //Page.Controls.Add(lit);

        string txtFontName = ConfigurationSettings.AppSettings["textFontNameDPBMS"].ToString();
        string txtFontSize = ConfigurationSettings.AppSettings["textFontSizeDPBMS"].ToString();
        int intTxtFontSize = int.Parse(txtFontSize.Replace("Px", "").Trim());
        lblTxt.Font.Name = txtFontName;
        lblTxt.Font.Size = FontUnit.Point(intTxtFontSize);


        lblTxt.Text = "The Report will be downloaded to the specified location in the following format as per your selection of Spots/ Links/ Songs.";


        getDateFormat();
        string strDateFormat = (string)Session["dateFormat"];

        if (strDateFormat == "DD/MM/YYYY")
        {
            dtStart.DateInput.DateFormat = "dd/MM/yyyy";
            dtEnd.DateInput.DateFormat = "dd/MM/yyyy";

            dtStart.DateInput.DisplayDateFormat = "dd/MM/yyyy";
            dtEnd.DateInput.DisplayDateFormat = "dd/MM/yyyy";
        }
        applyGridFormatting();
        if (!IsPostBack)
        {

            Session["totalRecordCount"] = 0;
            Session["category"] = "";

            hideButtons();
            setDate();
            GenerateTreeView();
            bindTypes();
            if (Session["dataReadOnly"].ToString() == "True")
            {
                btnExportToExcel.Enabled = false;
                btnGo.Enabled = false;
                Button1.Enabled = false;
            }
            else
            {
                btnExportToExcel.Enabled = true;
                btnGo.Enabled = true;
                Button1.Enabled = true;

            }
            Pg_Title.Text = "Download - PlayList By Market / Station";
        }

        //Session["StationLeft"] = txtStationSelect.Text;
        Session["s_date"] = dtStart.SelectedDate;
        Session["e_date"] = dtEnd.SelectedDate;


    }

    protected void applyGridFormatting()
    {
        dgResult.ClientSettings.Scrolling.AllowScroll = true;
        dgResult.ClientSettings.Scrolling.UseStaticHeaders = true;
        dgResult.ClientSettings.Scrolling.ScrollHeight = Unit.Pixel(350);

    }

    protected void GenerateTreeView()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getStationsForUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);

        var radTreeCities = (RadTreeView)txtMarketSelect.Items[0].FindControl("radTreeCities");
        radTreeCities.DataFieldID = "name";
        radTreeCities.DataFieldParentID = "m_name";
        radTreeCities.DataTextField = "name";

        radTreeCities.DataSource = ds.Tables[0];
        radTreeCities.DataBind();
    }

    protected void setDate()
    {
        DateTime lockDataBefore = DateTime.Parse(Session["lockDataBefore"].ToString());
        DateTime lockDataAfter = DateTime.Parse(Session["lockDataAfter"].ToString());
        dtStart.MinDate = lockDataBefore;
        dtEnd.MinDate = lockDataBefore;

        dtStart.MaxDate = lockDataAfter;
        dtEnd.MaxDate = lockDataAfter;

        dtStart.SelectedDate = System.DateTime.Now;
        dtEnd.SelectedDate = System.DateTime.Now;
    }

    protected void getDateFormat()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("select * from India_Report_New..Country_dateFormat where isSelected=1", conn);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                Session["dateFormat"] = dr["DateFormat"].ToString();
            }
        }

        conn.Close();
    }

    protected void btnGo_Click(object sender, EventArgs e)
    {
        lit.Text = "";
        //bindGrid();
        Session["category"] = ddType.SelectedItem.Value;
    }


    protected int getCount(DataTable dt, string type)
    {
        DataRow[] foundRows = null;

        if (type == "M")
        {
            foundRows = dt.Select("Type='M'");
        }
        else if (type == "L")
        {
            foundRows = dt.Select("Type='L'");
        }
        else if (type == "S")
        {
            foundRows = dt.Select("Type='S'");
        }

        return foundRows.Length;
    }

    protected bool bindGrid()
    {
        bool hasData = false;
        pnlBlank.Visible = false;
        if (dtStart.SelectedDate.ToString() == "" || dtEnd.SelectedDate.ToString() == "")
        {
            return hasData;
        }


        DateTime startTime = (DateTime)dtStart.SelectedDate;
        DateTime endTime = (DateTime)dtEnd.SelectedDate;


        string strStationIDs = getStationIDs();

        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("downloadWeeklyData", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@stationIDs", strStationIDs);
        da.SelectCommand.Parameters.AddWithValue("@element", ddType.SelectedItem.Text);
        da.SelectCommand.Parameters.AddWithValue("@startDate", startTime);
        da.SelectCommand.Parameters.AddWithValue("@endDate", endTime);
        da.SelectCommand.Parameters.AddWithValue("@user", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);

        if (ddType.SelectedItem.Text.ToLower() == "all")
        {
            DataTable dtNew = ds.Tables[0];
            Session["countSongs"] = getCount(dtNew, "M");
            Session["countLinks"] = getCount(dtNew, "L");
            Session["countSpots"] = getCount(dtNew, "S");
        }

        try
        {
            if (ds.Tables[0].Rows.Count == 0)
            {
                dgResult.Visible = false;
                lblError.Visible = true;
                lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
                hasData = false;
            }
            else
            {
                lblError.Text = "";
                lblError.Visible = false;
                dgResult.Visible = false;
                dgResult.DataSource = ds.Tables[0];
                dgResult.DataBind();
                Session["data"] = ds.Tables[0];
                Session["myDataTableExport"] = ds.Tables[0];
                hasData = true;
            }
        }
        catch
        {
            dgResult.Visible = false;
            lblError.Visible = true;
            lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
            hasData = false;
        }

        return hasData;
    }

    protected string getStationIDs()
    {
        string strStations = string.Empty;
        var radTreeCities = (RadTreeView)txtMarketSelect.Items[0].FindControl("radTreeCities");
        //ArrayList nodeCollection = radTreeCities.CheckedNodes;
        int stationID = 0;
        string strStationNames = "";
        foreach (RadTreeNode node in radTreeCities.CheckedNodes)
        {
            stationID = getStationIDOfStation(node.Text);
            if (stationID != 0)
            {
                strStations += stationID.ToString() + ",";
                strStationNames += node.Text + ",";
            }
        }

        return strStations.Substring(0, strStations.Length - 1);
    }

    protected int getStationIDOfStation(string txtStation)
    {
        int stationID = 0;
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("getStationIDOfStation", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@stationName", txtStation);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                stationID = (int)dr["station_id"];
            }
        }
        else
        {
            stationID = 0;
        }

        conn.Close();
        return stationID;
    }


    protected void btnCompare_Click(object sender, EventArgs e)
    {

    }

    protected void disableButtons()
    {
        btnGo.Enabled = false;
    }

    //protected void bindStations()
    //{

    //    SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
    //    SqlCommand comm;
    //    SqlDataReader dr;

    //    comm = new SqlCommand("select s.station_id, s.name, m.name as m_name , s.name + ' / ' + m.name as s_name  from " + ConfigurationSettings.AppSettings["market"] + "..stations s, " + ConfigurationSettings.AppSettings["market"] + "..formats f, " + ConfigurationSettings.AppSettings["market"] + "..markets m where s.allow_report = 1 and s.format_id = f.format_id and s.market_id = m.market_id and m.market_id in (select marketPermission from user_market_permission where userID =(select userID from users where UserName = @username)) order by m.name asc, s.name asc", conn);
    //    comm.Parameters.AddWithValue("@username", (string)Session["user"]);
    //    conn.Open();
    //    dr = comm.ExecuteReader();
    //    myStationSelect.DataSource = dr;
    //    myStationSelect.DataTextField = "s_name";
    //    myStationSelect.DataValueField = "station_id";
    //    myStationSelect.DataBind();

    //    conn.Close();
    //    conn.Open();
    //    dr = comm.ExecuteReader();
    //    myStationCompare.DataSource = dr;
    //    myStationCompare.DataTextField = "s_name";
    //    myStationCompare.DataValueField = "station_id";
    //    myStationCompare.DataBind();

    //    conn.Close();
    //    myStationSelect.Items.Insert(0, new ListItem("Select Station", "-1"));
    //    myStationCompare.Items.Insert(0, new ListItem("Select Station to compare", "-1"));
    //}

    protected bool isValidUser(string userName)
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("select * from India_Report_New..Users where username = '" + userName + "'", conn);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            return true;
        }
        else
        {
            return false;
        }
        conn.Close();
    }

    public static string DecryptText(String pass)
    {
        return Decrypt(pass, "&amp;%#@?,:*");
    }

    public static string Decrypt(string stringToDecrypt, string sEncryptionKey)
    {
        byte[] key = { };
        byte[] IV = { 10, 20, 30, 40, 50, 60, 70, 80 };
        byte[] inputByteArray = new byte[stringToDecrypt.Length];
        try
        {
            key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            inputByteArray = Convert.FromBase64String(stringToDecrypt);
            //MemoryStream ms = new MemoryStream(); 
            Encoding encoding = Encoding.UTF8;
            return encoding.GetString(inputByteArray);
        }
        catch (System.Exception ex)
        {
            return ("");
        }
    }

    protected void hideButtons()
    {
        litExportTo.Visible = false;
        btnExportToExcel.Visible = false;
        btnExportToCsv.Visible = false;
    }

    protected void showButtons()
    {
        litExportTo.Visible = true;
        btnExportToExcel.Visible = true;
        btnExportToCsv.Visible = true;
    }

    protected void dgResult_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {


        string headerFontName = ConfigurationSettings.AppSettings["headerFontNameDP"].ToString();
        string headerFontSize = ConfigurationSettings.AppSettings["headerFontSizeDP"].ToString();
        int intHeaderFontSize = int.Parse(headerFontSize.Replace("Px", "").Trim());


        if (isPdfExport && e.Item is GridFooterItem)
        {
            GridFooterItem footerItem = (GridFooterItem)e.Item;

            string strDateFormat = (string)Session["dateFormat"];

            DateTime dtDate = System.DateTime.Now;
            string strDate = "";
            if (strDateFormat == "DD/MM/YYYY")
            {
                strDate = dtDate.ToString("dd/MM/yyyy");
            }
            else
            {
                strDate = dtDate.ToString("MM/dd/yyyy");
            }


            string strTime = System.DateTime.Now.ToString();
            strTime = strTime.Substring(strTime.IndexOf(" "), strTime.Length - 9);


            string strCountText = "<b>" + Session["totalRecordCount"].ToString() + " rows </b>";
            if (ddType.SelectedItem.Value == "All")
            {
                strCountText = strCountText + " (" + Session["countSongs"].ToString() + " song instances, " + Session["countLinks"].ToString() + " link instances , " + Session["countSpots"].ToString() + " spot instances)";
            }

            string strFooterText = "";

            strFooterText = "This data was generated and exported on " + strDate + " at " + strTime + ".";
            strFooterText += " (" + strCountText + ") ";

            strFooterText += "AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved. ";
            strFooterText += "AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India. ";
            strFooterText += "The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.";

            footerItem.Cells[2].ColumnSpan = 2;
            footerItem.Cells[2].Text = strFooterText;
            footerItem.Cells[2].Style["color"] = "red";


        }

        if (e.Item.ItemType == Telerik.Web.UI.GridItemType.Header)
        {
            Telerik.Web.UI.GridHeaderItem headerItem = dgResult.MasterTableView.GetItems(Telerik.Web.UI.GridItemType.Header)[0] as Telerik.Web.UI.GridHeaderItem;

            dgResult.HeaderStyle.Font.Name = headerFontName;
            dgResult.HeaderStyle.Font.Size = FontUnit.Point(intHeaderFontSize);


            if (!isPdfExport)
            {
                headerItem["Hour"].Visible = false;
                headerItem["Type"].Visible = false;
            }
            Boolean hasClass = true;
            try
            {
                string ss = headerItem["Category"].Text;
            }
            catch
            {
                hasClass = false;
            }

            #region Width
            if (hasClass)
            {
                Telerik.Web.UI.GridColumn col;
                GridHeaderItem gridHeaderItem = e.Item as GridHeaderItem;
                col = ((Telerik.Web.UI.GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[0];
                col.HeaderStyle.Width = Unit.Percentage(15);

                col = ((Telerik.Web.UI.GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[1];
                col.HeaderStyle.Width = Unit.Percentage(10);

                col = ((Telerik.Web.UI.GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[2];
                col.HeaderStyle.Width = Unit.Percentage(30);

                col = ((Telerik.Web.UI.GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[3];
                col.HeaderStyle.Width = Unit.Percentage(30);

                col = ((Telerik.Web.UI.GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[4];
                col.HeaderStyle.Width = Unit.Percentage(15);
            }
            else
            {
                Telerik.Web.UI.GridColumn col;
                GridHeaderItem gridHeaderItem = e.Item as GridHeaderItem;
                col = ((Telerik.Web.UI.GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[0];
                col.HeaderStyle.Width = Unit.Percentage(15);

                col = ((Telerik.Web.UI.GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[1];
                col.HeaderStyle.Width = Unit.Percentage(10);

                col = ((Telerik.Web.UI.GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[2];
                col.HeaderStyle.Width = Unit.Percentage(40);

                col = ((Telerik.Web.UI.GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[3];
                col.HeaderStyle.Width = Unit.Percentage(35);
            }
            #endregion

            if (!isPdfExport)
            {
                headerItem.Style["font-size"] = "100px";
            }
        }


        if (e.Item is Telerik.Web.UI.GridGroupHeaderItem)
        {
            Telerik.Web.UI.GridGroupHeaderItem item = (Telerik.Web.UI.GridGroupHeaderItem)e.Item;

            item.DataCell.Text = item.DataCell.Text.Replace(" 13:00:00 PM", " 01:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 14:00:00 PM", " 02:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 15:00:00 PM", " 03:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 16:00:00 PM", " 04:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 17:00:00 PM", " 05:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 18:00:00 PM", " 06:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 19:00:00 PM", " 07:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 20:00:00 PM", " 08:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 21:00:00 PM", " 09:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 22:00:00 PM", " 10:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 23:00:00 PM", " 11:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 00:00:00 AM", " 12:00:00 AM");


        }

        if (e.Item.ItemType == Telerik.Web.UI.GridItemType.Item || e.Item.ItemType == Telerik.Web.UI.GridItemType.AlternatingItem)
        {
            Telerik.Web.UI.GridDataItem dataItem = e.Item as Telerik.Web.UI.GridDataItem;


            if (isPdfExport)
            {
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 00:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 01:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 02:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 03:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 04:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 05:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 06:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 07:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 08:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 09:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 10:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 11:00:00 AM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 12:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 13:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 14:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 15:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 16:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 17:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 18:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 19:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 20:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 21:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 22:00:00 PM", "");
                dataItem["Date"].Text = dataItem["Date"].Text.Replace(" 23:00:00 PM", "");
            }
            string strAirTime = dataItem["Air Time"].Text;


            try
            {
                dataItem["Account"].HorizontalAlign = HorizontalAlign.Left;
            }
            catch { }

            try
            {
                dataItem["Song Title"].HorizontalAlign = HorizontalAlign.Left;
            }
            catch { }


            try
            {
                dataItem["Account/Song Title"].HorizontalAlign = HorizontalAlign.Left;
            }
            catch { }


            try
            {
                dataItem["Parent"].HorizontalAlign = HorizontalAlign.Left;
            }
            catch { }

            try
            {
                dataItem["Category"].HorizontalAlign = HorizontalAlign.Left;
            }
            catch { }

            try
            {
                dataItem["Movie/Artist"].HorizontalAlign = HorizontalAlign.Left;
            }
            catch { }

            try
            {
                dataItem["Parent/Movie/Artist"].HorizontalAlign = HorizontalAlign.Left;
            }
            catch { }

            if (strAirTime == "*")
            {

                try
                {
                    dataItem["Account"].HorizontalAlign = HorizontalAlign.Center;
                }
                catch { }

                try
                {
                    dataItem["Song Title"].HorizontalAlign = HorizontalAlign.Center;
                }
                catch { }

                try
                {
                    dataItem["Account/Song Title"].HorizontalAlign = HorizontalAlign.Center;
                }
                catch { }


                try
                {
                    dataItem["Parent"].HorizontalAlign = HorizontalAlign.Center;
                }
                catch { }



                try
                {
                    dataItem["Movie/Artist"].HorizontalAlign = HorizontalAlign.Center;
                }
                catch { }

                try
                {
                    dataItem["Parent/Movie/Artist"].HorizontalAlign = HorizontalAlign.Center;
                }
                catch { }

                dataItem["Air Time"].Text = "";
                dataItem["Duration (in sec)"].Text = "";
                dataItem.BackColor = System.Drawing.Color.Black;
                dataItem.ForeColor = System.Drawing.Color.White;


                try
                {
                    dataItem["Account"].Text = getHourHeaderFormatted(dataItem["Account"].Text);
                    dataItem["Account"].HorizontalAlign = HorizontalAlign.Center;
                }
                catch
                {
                    try
                    {
                        dataItem["Song Title"].Text = getHourHeaderFormatted(dataItem["Song Title"].Text);
                        dataItem["Song Title"].HorizontalAlign = HorizontalAlign.Center;
                    }
                    catch
                    {
                        try
                        {
                            dataItem["Account/Song Title"].Text = getHourHeaderFormatted(dataItem["Account/Song Title"].Text);
                            dataItem["Account/Song Title"].HorizontalAlign = HorizontalAlign.Center;
                        }
                        catch
                        {

                        }
                    }
                }

            }


            try
            {
                if (!isPdfExport)
                {
                    dataItem["Hour"].Visible = false;
                    dataItem["Type"].Visible = false;
                }
            }
            catch { }


            string xCol = "#CCCCFF";
            System.Drawing.Color cM = System.Drawing.ColorTranslator.FromHtml(xCol);

            xCol = "#FFCCCC";
            System.Drawing.Color cL = System.Drawing.ColorTranslator.FromHtml(xCol);

            xCol = "#FFFFFF";
            System.Drawing.Color cS = System.Drawing.ColorTranslator.FromHtml(xCol);

            if (!isPdfExport)
            {
                if (dataItem["Type"].Text == "M")
                {
                    dataItem.BackColor = System.Drawing.Color.Aqua;
                }
                if (dataItem["Type"].Text == "L")
                {
                    dataItem.BackColor = System.Drawing.Color.Yellow;
                }
                if (dataItem["Type"].Text == "S")
                {
                    dataItem.BackColor = System.Drawing.Color.LightGreen;
                }
            }
        }
    }

    protected string getHourHeaderFormatted(string hourHeader)
    {
        string strDateFormat = (string)Session["dateFormat"];
        if (strDateFormat == "DD/MM/YYYY")
        {
            String dt = hourHeader.Substring(0, 10);
            hourHeader = hourHeader.Replace(dt, changeDateFormat(dt));
        }

        hourHeader = hourHeader.Replace("13:00:00 PM", "01:00:00 PM");
        hourHeader = hourHeader.Replace("14:00:00 PM", "02:00:00 PM");
        hourHeader = hourHeader.Replace("15:00:00 PM", "03:00:00 PM");
        hourHeader = hourHeader.Replace("16:00:00 PM", "04:00:00 PM");
        hourHeader = hourHeader.Replace("17:00:00 PM", "05:00:00 PM");
        hourHeader = hourHeader.Replace("18:00:00 PM", "06:00:00 PM");
        hourHeader = hourHeader.Replace("19:00:00 PM", "07:00:00 PM");
        hourHeader = hourHeader.Replace("20:00:00 PM", "08:00:00 PM");
        hourHeader = hourHeader.Replace("21:00:00 PM", "09:00:00 PM");
        hourHeader = hourHeader.Replace("22:00:00 PM", "10:00:00 PM");
        hourHeader = hourHeader.Replace("23:00:00 PM", "11:00:00 PM");
        hourHeader = hourHeader.Replace("24:00:00 AM", "12:00:00 AM");

        return hourHeader;
    }


    protected string changeDateFormat(string strDate)
    {
        string dd;
        string mm;
        string yyyy;

        dd = strDate.Substring(3, 2);
        mm = strDate.Substring(0, 2);
        yyyy = strDate.Substring(6, 4);

        strDate = dd + "/" + mm + "/" + yyyy;

        return strDate;
    }



    protected string getMarketOfStation(string strStation)
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("getMarketOfStation", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@stationName", strStation);
        conn.Open();
        dr = comm.ExecuteReader();
        string strMarketName = "";
        while (dr.Read())
        {
            strMarketName = dr[0].ToString();
        }

        dr.Close();
        conn.Close();

        return strMarketName;
    }

    protected void addAuditLog(DateTime startDate, DateTime endDate, string station,
Boolean exportedToExcel, Boolean exportedToPdf, Boolean exportedToText)
    {
        auditTrail at = new auditTrail();
        at.connectionString = ConfigurationSettings.AppSettings["connectionString"].ToString();
        at.userID = (int)Session["userID"];
        at.IPAddress = Request.ServerVariables["REMOTE_ADDR"];
        at.pageViewed = "Download - PlayList By Market / Station";
        at.startDate = startDate;
        at.endDate = endDate;
        at.station = station;
        at.exportedToExcel = exportedToExcel;
        at.exportedToPdf = exportedToPdf;
        at.exportedToText = exportedToText;
        at.addUserActivityLogPassingStationIDs();
        at = null;
    }

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        string strStationIDs = getStationIDs();

        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());

        addAuditLog(dtStartDate, dtEndDate, strStationIDs, true, false, false);

        bool hasData = bindGrid();
        if (hasData)
        {
            ExportToExcel();
        }
    }

    protected void btnExportToCsv_Click(object sender, EventArgs e)
    {
        string strStationIDs = getStationIDs();

        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());

        addAuditLog(dtStartDate, dtEndDate, strStationIDs, false, false, true);

        bool hasData = bindGrid();
        if (hasData)
        {
            ExportToCSV();
        }
    }


    public void ExportToCSV()
    {
        DataTable dt = getDataTableToExport();

        string strData = Export(dt);
        Byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(strData);

        Response.Clear();
        Response.AddHeader("Content-Type", "application/Excel");
        Response.AddHeader("Content-Disposition", "inline;filename=DownloadPlayListByMarketStation.csv");
        Response.BinaryWrite(data);
        Response.End();
    }
    protected string CsvEscape(string value)
    {
        if (value.Contains(","))
        {
            //return "\"" + value.Replace("\"", "\"\"") + "\"";
            return value.Replace(",", " - ");
        }
        return value;
    }
    protected string Export(DataTable dt)
    {

        String Start_Date, End_Date;
        Int32 i, j;
        StringBuilder sb = new StringBuilder();

        DateTime dt1 = (DateTime)Session["s_date"];
        DateTime dt2 = (DateTime)Session["e_date"];
        string strDateFormat = (string)Session["dateFormat"];
        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }

        Start_Date = sDt1;
        End_Date = sDt2;

        string strType = "";
        if (ddType.SelectedItem.Value == "Song")
        {
            strType = "Songs";
        }
        else if (ddType.SelectedItem.Value == "Links")
        {
            strType = "Links";
        }
        else if (ddType.SelectedItem.Value == "Spot")
        {
            strType = "Spots";
        }
        else if (ddType.SelectedItem.Value == "All")
        {
            strType = "All";
        }

        sb.Append("Aircheck Download - PlayList by Market / Station Report (" + strType + ")");
        sb.Append("\r\n");
        sb.Append("\r\n");

        string str_station_name = "";

        string str_market_name = getMarketOfStation(str_station_name);


        sb.Append("Date");
        sb.Append(":");
        sb.Append(Start_Date + " to " + End_Date);
        sb.Append("\r\n");
        sb.Append("\r\n");


        for (j = 1; j <= dt.Columns.Count; j++)
        {
            //if (j == 2)
            //{
            //    sb.Append("Market");
            //    sb.Append("|");
            //}
            sb.Append(dt.Columns[j - 1].ColumnName.ToString());
            sb.Append("|");

        }


        sb.Replace("|", "", sb.Length - 1, 1);
        sb.Append("\r\n");


        Int32 intNumberOfRows = dt.Rows.Count;
        Int32 intNumberOfColumns = dt.Columns.Count;
        for (i = 1; i <= intNumberOfRows; i++)
        {
            for (j = 1; j <= intNumberOfColumns; j++)
            {
                sb.Append(CsvEscape(dt.Rows[i - 1][j - 1].ToString()));
                sb.Append("|");
            }
            sb.Replace("|", "", sb.Length - 1, 1);
            sb.Append("\r\n");
        }
        sb.Append("\r\n");
        sb.Append("\r\n");




        DateTime dtDate = System.DateTime.Now;
        string strDate = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dtDate.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dtDate.ToString("MM/dd/yyyy");
        }


        string strTime = System.DateTime.Now.ToString();
        strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


        string strCountText = intNumberOfRows.ToString() + " rows ";
        if (ddType.SelectedItem.Value == "All")
        {
            strCountText = strCountText + " (" + Session["countSongs"].ToString() + " song instances " + Session["countLinks"].ToString() + " link instances " + Session["countSpots"].ToString() + " spot instances)";
        }




        sb.Append("This data was generated and exported on " + strDate + " at " + strTime + "");
        sb.Append("\r\n");
        sb.Append(strCountText);
        sb.Append("\r\n");
        sb.Append("\r\n");
        sb.Append("\r\n");
        sb.Append("AirCheck India services are Copyright 2002-2009 AirCheck India. All Rights Reserved.");
        sb.Append("\r\n");
        sb.Append("AirCheck India its logo and \'The New Broadcast Monitoring\' are registered trademarks of AirCheck India.");
        sb.Append("\r\n");
        sb.Append("The AirCheck India broadcast content recognition process is protected by U.S. Patents 5437050 and 7386047 with additional patents pending.");


        return sb.ToString();
    }

    protected DataTable getDataTableToExport()
    {
        string strDateFormat = (string)Session["dateFormat"];
        DataTable dt = new DataTable();
        dt = (DataTable)Session["myDataTableExport"];
        try
        {
            dt.Columns.Remove("Type");
        }
        catch { }

        try
        {
            dt.Columns["Hour"].ColumnName = "Date";
            //dt.Columns.Remove("Hour");
        }
        catch { }

        int i = 0;
        if (dt == null)
        {
            return null;
        }
        foreach (DataRow dr in dt.Rows)
        {
            try
            {
                String strDate = (string)dr["Date"];
                if (strDate.IndexOf(" ") > 0)
                {
                    strDate = strDate.Substring(0, strDate.IndexOf(" "));
                }

                DateTime dtDate = DateTime.Parse(strDate);

                if (strDateFormat == "DD/MM/YYYY")
                {
                    strDate = dtDate.ToString("dd/MM/yyyy");
                }
                else
                {
                    strDate = dtDate.ToString("MM/dd/yyyy");
                }
                dr["Date"] = strDate;
            }
            catch { }

            string strAirTime = (string)dr["Air Time"];
            if (strAirTime == "*")
            {
                dt.Rows[i][0] = "";
                dt.Rows[i][1] = DBNull.Value;
                dt.Rows[i].Delete();
            }
            i++;
        }


        dt.AcceptChanges();
        return dt;
    }

    public void ExportToExcel()
    {
        string strDateFormat = (string)Session["dateFormat"];
        DataTable dt = getDataTableToExport();



        DateTime dt1 = (DateTime)Session["s_date"];
        DateTime dt2 = (DateTime)Session["e_date"];

        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }

        String strDateRange = "<BR><BR>From: " + sDt1 + "  to  " + sDt2;

        ExportToExcel(dt, Response, strDateRange);
    }

    public void ExportToExcel(DataTable dt, HttpResponse response, String strDateRange)
    {

        //first let's clean up the response.object
        response.Clear();
        response.Charset = "";
        //set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel";
        //create a string writer
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //create an htmltextwriter which uses the stringwriter
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //instantiate a datagrid
        DataGrid dg = new DataGrid();
        //set the datagrid datasource to the dataset passed in
        if (dt == null)
        {
            return;
        }
        else if (dt.Rows.Count == 0)
        {
            return;
        }
        dg.DataSource = dt;
        //bind the datagrid
        dg.DataBind();

        dg.RenderControl(htmlWrite);


        String str = stringWrite.ToString();

        int start, end;
        start = 0;
        end = 0;

        int colspan = 0;

        colspan = dt.Columns.Count;

        string strType = "";
        if (ddType.SelectedItem.Value == "Song")
        {
            strType = "Songs";
        }
        else if (ddType.SelectedItem.Value == "Links")
        {
            strType = "Links";
        }
        else if (ddType.SelectedItem.Value == "Spot")
        {
            strType = "Spots";
        }
        else if (ddType.SelectedItem.Value == "All")
        {
            strType = "All";
        }


        String strReportName = "Aircheck Download - PlayList by Market / Station Report (" + strType + ")";



        string strFinal = "<tr><td align=center colspan=" + colspan + " style='background-color:#FFFF66;'>" + strReportName + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strDateRange + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + ">&nbsp;</td></tr>";



        while (str.IndexOf("<select name=") > 0)
        {
            start = str.IndexOf("<select name=");
            end = str.IndexOf("</select>") + 9;
            str = str.Replace(str.Substring(start, end - start), "");
        }
        //str = str.Replace("<tr>\r\n\t\t<td>&nbsp;</td><td>&nbsp;</td>", "<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td>");

        string strAirTime = "";
        string strPlayTime = "";
        string strNewPlayTime = "";

        int intCount = 0;

        foreach (DataRow dr in dt.Rows)
        {
            strAirTime = dr[0].ToString();
            if (strAirTime == "")
            {
                strPlayTime = dr[2].ToString();
                strNewPlayTime = dr[2].ToString() + " ";

                if (ddType.SelectedItem.Value == "Song")
                {
                    str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=4 align='Center'>" + strNewPlayTime + "</td></tr>");
                }
                else
                {
                    str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=5 align='Center'>" + strNewPlayTime + "</td></tr>");
                }
            }
            else
            {
                intCount = intCount + 1;
            }
        }


        str = strFinal + "<BR><BR>" + str;


        string strDateFormat = (string)Session["dateFormat"];

        DateTime dtDate = System.DateTime.Now;
        string strDate = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dtDate.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dtDate.ToString("MM/dd/yyyy");
        }


        string strTime = System.DateTime.Now.ToString();
        strTime = strTime.Substring(strTime.IndexOf(" "), strTime.Length - 10);


        string strCountText = "<b>" + intCount + " rows </b>";
        if (ddType.SelectedItem.Value == "All")
        {
            strCountText = strCountText + " (" + Session["countSongs"].ToString() + " song instances, " + Session["countLinks"].ToString() + " link instances , " + Session["countSpots"].ToString() + " spot instances)";
        }

        string strFooterText = "";

        strFooterText = "<BR><table>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>This data was generated and exported on " + strDate + " at " + strTime + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>" + strCountText + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><BR></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.</td></tr>";
        strFooterText += "</table>";

        str = "<table>" + str + strFooterText;



        response.Write(str);
        response.End();
    }
}
