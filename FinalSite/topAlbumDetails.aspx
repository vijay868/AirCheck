﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="topAlbumDetails.aspx.cs" Inherits="topAlbumDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head id="Head1" runat="server">
    <title>Top Album (Movie/Music) Details</title>
    <meta name="DownloadOptions" content="noopen" />
    <%--<link href="css/style_risk.css" rel="stylesheet" type="text/css" />  --%>
    <style type="text/css">
        .style5
        {
            font-family: Helvetica;
            font-size: 14px;
           
            color: grey;
        }
        .style6
        {
            font-family: Helvetica;
            font-size: 14px;
            color: Grey;
            width: 153px;
        }
    </style>
    <LINK href="css/report_caption.css" rel="stylesheet" text="text/css">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0">
    <form id="form1" runat="server">
    <div>
    <table height="53" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="top" align="top" width="760" height="53" border="0"><img src="images/bgTop.gif"></td>
					<td vAlign="top" width="100%"><IMG height="28" src="images/bgTopExt.gif" width="100%" border="0"></td>
				</tr>
			</table>
			<table width="100%" background="images/contentheader_bgr.gif" border="0" cellpadding="6px" cellspacing="0px" style="margin-left:-5px; margin-right:300px;">
                <tr>
                    
                    <td align="left" 
                        style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #0066CC">::
                        Top Album (Movie/Music) Details&nbsp;::
                    </td>
                    <td>
                    </td>
                    <td align="right">
                       
                    </td>
                </tr>
            </table><br />
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td class="style6">
                    Date Range
                </td>
                <td class="date">
                    :
                </td>
                <td class="date">
                   &nbsp;&nbsp; <asp:Label ID="lblDateRange" runat="server" CssClass="style5"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style6" nowrap>
                    Song Title
                </td>
                <td class="date">
                    :
                </td>
                <td class="date">
                   &nbsp;&nbsp; <asp:Label ID="lblSongTitle" runat="server" CssClass="style5"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style6" nowrap>
                    Movie / Artist
                </td>
                <td class="date">
                    :
                </td>
                <td class="date">
                    &nbsp;&nbsp; <asp:Label ID="lblMovieArtist" runat="server" CssClass="style5"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style6">
                    Duration
                </td>
                <td class="date">
                    :
                </td>
                <td class="date">
                   &nbsp;&nbsp; <asp:Label ID="lblDuration" runat="server" CssClass="style5"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style6" nowrap>
                    Total No. Of Plays
                </td>
                <td class="date">
                    :
                </td>
                <td class="date">
                   &nbsp;&nbsp; <asp:Label ID="lblTotalNoOfPlays" runat="server" CssClass="style5"></asp:Label>
                </td>
            </tr>
        </table>
        <br /><br />
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td colspan="3" align="left" style="width: 100%">
                    <div id="DIV1">
                        <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label>
                        <asp:GridView UseAccessibleHeader="true" EnableViewState="false" ID="gvData" ShowFooter="true"
                            runat="server" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound">
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="left">
                    <br />
                    <asp:Panel ID="pnlExportTo" runat="server">
                        <span style="font-family:Helvetica; font-size: 12px;"> Export to:    </span>

                        <asp:Button ID="btnExportToExcel" runat="server" Visible="True" CausesValidation="False"
                            Text="Excel" ForeColor="white" Font-Names="Arial" Font-Bold="true" BackColor="#336699"
                            Font-Size="8" OnClick="img_btn_export_to_excel_Click"></asp:Button>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html> 