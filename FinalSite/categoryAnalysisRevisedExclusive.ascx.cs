﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class categoryAnalysisRevisedExclusive : System.Web.UI.UserControl
{
    DataTable dtInner;
    protected void Page_Load(object sender, EventArgs e)
    {

       
    }
    protected void DG1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string strBrandName = "";
        string strParentName = "";
        string strCategory = "";

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.HorizontalAlign = HorizontalAlign.Center;
        }

        string headerFontName = "Helvetica";
        string headerFontSize = "12px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "10px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int i = 0;
            foreach(DataColumn dc in dtInner.Columns)
            {
                bool isANumber = false;
                int intNum;
                if (dc.ColumnName != "brandID" && dc.ColumnName != "parentID")
                {
                    if (dc.ColumnName == "Brand Name")
                    {
                        strBrandName = doStringSanitation(e.Item.Cells[i].Text);
                    }
                    if (dc.ColumnName == "Advertiser")
                    {
                        strParentName = doStringSanitation(e.Item.Cells[i].Text);
                    }

                    if (dc.ColumnName == "Category")
                    {
                        strCategory = doStringSanitation(e.Item.Cells[i].Text);
                    }

                    if (dc.ColumnName == "No Of Plays" || dc.ColumnName == "Total Seconds")
                    {
                        isANumber = int.TryParse(e.Item.Cells[i].Text, out intNum);
                        if (isANumber)
                        {
                            e.Item.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                        }

                        if (isANumber)
                        {
                            //e.Item.Cells[i].Text = "<a href='' onclick=\"javascript:return disp_details('" + strBrandName + "','" + strParentName + "','" + strCategory + "')\"  alt='Click here to see the details'>" + e.Item.Cells[i].Text + "</a>";
                        }                    
                    }

                }
                i++;
            }



            
        }

    }

    protected string doStringSanitation(string str)
    {
        if (str.IndexOf("'") >= 0)
        {
            str = str.Replace("'", "123123");
        }
        if (str.IndexOf("&") >= 0)
        {
            str = str.Replace("&amp;", "456456");
        }

        return str;
    }

    protected void DG1_DataBinding(object sender, EventArgs e)
    {

    }


    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    ///		Required method for Designer support - do not modify
    ///		the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new System.EventHandler(this.Page_Load);
        this.DataBinding += new System.EventHandler(this.SalesList_DataBinding);

    }
    #endregion

    private void SalesList_DataBinding(object sender, System.EventArgs e)
    {
        //if an InvalidCastException occurs in either of the next two lines, 
        //please make sure that you've set the TemplateDataMode to Table (because you want nested grids)
        DataGridItem dgi = (DataGridItem)this.BindingContainer;
        if (!(dgi.DataItem is DataSet))
            throw new ArgumentException("Please change the TemplateDataMode attribute to 'Table' in the HierarGrid declaration");
        DataSet ds = (DataSet)dgi.DataItem;
        dtInner = ds.Tables[1];
        DG1.DataSource = ds;
        DG1.DataMember = ds.Tables[1].TableName;
        DG1.DataBind();
    }
}
