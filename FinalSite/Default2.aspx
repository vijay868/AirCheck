﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Default2.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .gvHeaderRow
        {
            background-image: url("images/dhrudde.png");
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: White;
            text-decoration: none;
            line-height: 20px;
            padding-left: 10px;
            border-left: solid 1px white;
            border-right: solid 1px white;
            border-bottom: solid 1px white;
            position: relative;
        }
    </style>

    <script type="text/javascript">
        function StopPropagation(e) {
            if (!e) {
                e = window.event;
            }

            e.cancelBubble = true;
        }

        function OnClientDropDownOpenedHandler(sender, eventArgs) {



            var tree = sender.get_items().getItem(0).findControl("radTreeCities");
            var tree1 = sender.get_items().getItem(0).findControl("radStationCompare");

            if (!!tree) {
                var selectedNode = tree.get_selectedNode();
                if (selectedNode) {
                    selectedNode.scrollIntoView();
                }
            }

            if (!!tree1) {
                var selectedNode1 = tree1.get_selectedNode();
                if (selectedNode1) {
                    selectedNode1.scrollIntoView();
                }
            }
        }


        function nodeClicking(sender, args) {
            var comboBox = $find("<%= txtMarketSelect.ClientID %>");

            var node = args.get_node();

            if (node.get_parent() == node.get_treeView()) {
                alert("Please select a station. Market selection not allowed.");

            }
            else {
                comboBox.set_text(node.get_text());
                comboBox.get_items().getItem(0).set_value(node.get_value());
            }

        }

        function nodeClicked(sender, args) {
            var node = args.get_node();
            if (node.get_checked()) {
                node.uncheck();
            } else {
                node.check();
            }
            nodeChecked(sender, args)


        }

        function getrootcount(sender, args) {

            //        var tree = sender.get_items().getItem(0).findControl("radTreeCities");
            //        var allNodes = tree.get_nodes().getNode(0).get_allNodes();
            //        for (var i = 0; i < allNodes.length; i++) {
            //            var node = allNodes[i];
            //            alert(node.get_text());
            //        }
        }
        function disp_details(startDate, endDate, strSongName, strArtistName, strStationName) {
            win = window.open("ListOfCurrentSongsRepetition_details.aspx?startDate=" + startDate + "&endDate=" + endDate + "&strSongName=" + strSongName + "&strArtistName=" + strArtistName + "&strStationName=" + strStationName, "win", "width=800,height=400,left=0, scrollbars=yes, resizable=yes");
            return false
        }
        function disp_exclusiveReport(startDate, endDate, strStationName, strStationIDs) {
            win = window.open("ListOfCurrentSongsRepetition_exclusive.aspx?startDate=" + startDate + "&endDate=" + endDate + "&strStationName=" + strStationName + "&strStationIDs=" + strStationIDs, "win", "width=800,height=400,left=0, scrollbars=yes, resizable=yes");
            return false
        }

        function nodeChecked(sender, args) {

            var comboBox = $find("<%= txtMarketSelect.ClientID %>");



            //check if 'Select All' node has been checked/unchecked
            var tempNode = args.get_node();
            if (tempNode.get_text().toString() == "(Select All)") {
                // check or uncheck all the nodes
            } else {
                var nodes = new Array();
                nodes = sender.get_checkedNodes();
                var vals = "";
                var i = 0;
                var icount = 0;

                for (i = 0; i < nodes.length; i++) {
                    var n = nodes[i];
                    var nodeText = n.get_text().toString();

                    if (nodeText != "(Select All)") {
                        if (n._hasChildren() == true) {
                            //                        icount = icount + 1;
                            //                        alert(icount);
                        } else {
                            vals = vals + n.get_text().toString() + ",";
                        }
                    }
                }

                //prevent  combo from closing
                //  supressDropDownClosing = true;
                comboBox.set_text(vals);

                getrootcount(sender, args);
            }
        }
		
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div style="width: 100%; height: 100%">
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default"
            IsSticky="True" Width="100%" Height="100%">
        </telerik:RadAjaxLoadingPanel>
        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="100%" Width="100%"
            HorizontalAlign="NotSet" LoadingPanelID="RadAjaxLoadingPanel1">
            <div>
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="Pg_Title" runat="server"></asp:Label>
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            <asp:Label ID="lbl_Logout" runat="server" Text="Label">Welcome: Administrator[Logout]</asp:Label>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td class="style1">
                        </td>
                        <td class="style2">
                        </td>
                        <td class="style3">
                            &nbsp;
                        </td>
                        <td>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            Start Date:
                        </td>
                        <td class="style2">
                            End Date:
                        </td>
                        <td class="style3">
                            &nbsp;
                        </td>
                        <td>
                            Select Station:
                        </td>
                        <td>
                            Search Text
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style1" style="margin-left: 40px">
                            <telerik:RadDatePicker ID="dtStart" runat="server" Culture="English (United States)"
                                MinDate="2004-01-01">
                                <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                </Calendar>
                                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                                </DateInput>
                            </telerik:RadDatePicker>
                        </td>
                        <td class="style2">
                            <telerik:RadDatePicker ID="dtEnd" runat="server" Culture="English (United States)"
                                MinDate="2004-01-01">
                                <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                </Calendar>
                                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                                </DateInput>
                            </telerik:RadDatePicker>
                        </td>
                        <td class="style3">
                        </td>
                        <td style="margin-left: 40px">
                            <telerik:RadComboBox ID="txtMarketSelect" runat="server" AfterClientCheck="AfterCheckHandler"
                                CollapseAnimation-Type="None" EmptyMessage="Choose a destination" ExpandAnimation-Type="None"
                                OnClientDropDownOpened="OnClientDropDownOpenedHandler" ShowToggleImage="True"
                                Style="vertical-align: middle;" Width="250px">
                                <ItemTemplate>
                                    <div id="div2">
                                        <telerik:RadTreeView ID="radTreeCities" runat="server" Height="200px" OnClientNodeChecked="nodeChecked"
                                            OnClientNodeClicked="nodeClicking" CheckBoxes="true" Width="100%" TriStateCheckBoxes="true">
                                        </telerik:RadTreeView>
                                    </div>
                                </ItemTemplate>
                                <Items>
                                    <telerik:RadComboBoxItem Text="" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtSearch" runat="server">
                                <PasswordStrengthSettings ShowIndicator="False" CalculationWeightings="50;15;15;20"
                                    PreferredPasswordLength="10" MinimumNumericCharacters="2" RequiresUpperAndLowerCaseCharacters="True"
                                    MinimumLowerCaseCharacters="2" MinimumUpperCaseCharacters="2" MinimumSymbolCharacters="2"
                                    OnClientPasswordStrengthCalculating="" TextStrengthDescriptions="Very Weak;Weak;Medium;Strong;Very Strong"
                                    TextStrengthDescriptionStyles="riStrengthBarL0;riStrengthBarL1;riStrengthBarL2;riStrengthBarL3;riStrengthBarL4;riStrengthBarL5;"
                                    IndicatorElementBaseStyle="riStrengthBar" IndicatorElementID=""></PasswordStrengthSettings>
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <telerik:RadButton ID="btnGo" runat="server" Skin="Web20" Text="GO" ToolTip="Go"
                                OnClick="btnGo_Click">                                
                                <Icon PrimaryIconCssClass="rbSearch" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
                            </telerik:RadButton>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                        </td>
                        <td class="style2">
                        </td>
                        <td class="style3">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="panelContainer" runat="server" Height="300px" Width="100%">
                    <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label>
                    <asp:GridView UseAccessibleHeader="true" EnableViewState="false" ID="gvData" ShowFooter="true"
                        runat="server" Width="100%" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                        HeaderStyle-CssClass="gvHeaderRow">
                    </asp:GridView>
                    <div id="divExport" runat="server">
                        <asp:Literal ID="litExportTo" runat="server" Text="Export to: " Visible="false"></asp:Literal>
                        <telerik:RadButton ID="btnExportToExcel" runat="server" Text="Excel" Visible="false"
                            OnClick="img_btn_export_to_excel_Click">
                        </telerik:RadButton>
                    </div>
                </asp:Panel>
            </div>
        </telerik:RadAjaxPanel>
    </div>
</asp:Content>
