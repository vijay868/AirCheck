﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string selectedItemValue = Request.QueryString["ln"];
        RadPanelItem selectedItem = RadPanelBar1.FindItemByValue(selectedItemValue);
        if (selectedItem != null)
        {
            if (selectedItem.Items.Count > 0)
            {
                selectedItem.Expanded = true;
            }
            else
            {
                selectedItem.Selected = true;

                while ((selectedItem != null) &&
                       (selectedItem.Parent.GetType() == typeof(RadPanelItem)))
                {
                    selectedItem = (RadPanelItem)selectedItem.Parent;
                    selectedItem.Expanded = true;
                }
            }
        }


        //Radpane2.Width = new Unit((string)Session["MWidth"]);
        //Radpane2.Width = new Unit("1200px");

        //if (RadPane1.Collapsed == true)
        //{
          
        //}
        //else
        //{
        //    Radpane2.Width = new Unit("1350px");
        //}

    } 
}
