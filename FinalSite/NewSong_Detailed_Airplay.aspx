﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewSong_Detailed_Airplay.aspx.cs" Inherits="NewSong_Detailed_Airplay" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>New Songs Detailed Airplay</title>
    <meta name="DownloadOptions" content="noopen" />
    <LINK href="css/report_caption.css" rel="stylesheet" text="text/css">
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <table height="53" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="top" align="top" width="760" height="53" border="0"><img src="images/bgTop.gif"></td>
					<td vAlign="top" width="100%"><IMG height="28" src="images/bgTopExt.gif" width="100%" border="0"></td>
				</tr>
			</table>
			<table width="100%" background="images/contentheader_bgr.gif" border="0" cellpadding="6px" cellspacing="0px" style="margin-left:-5px; margin-right:300px;">
                <tr>
                    
                    <td align="left" 
                        style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #0066CC">::
                        New Songs Detailed Airplay&nbsp;::
                    </td>
                    <td>
                    </td>
                    <td align="right">
                       
                    </td>
                </tr>
            </table>
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
<tr>
    <td align="left" class="style1">
        <div id="divWait" style="DISPLAY: none">
	        <table style="height:310Px" width="100%">
		        <tr>
			        <td align="center" width="100%" class="style2"><br><br><br><br><br><br><br><br><IMG src="images/aniClock.gif" border="0" name="ProgressBarImg"><br>
				        <font class="date">Acquiring Data... one moment please!</font>
			        </td>
		        </tr>
	        </table>
        </div>  
        <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label> 	
        <div id="DIV1">
            <telerik:RadGrid ID="radData" runat="server" Visible="false" Width="57%" GridLines="None"
            OnItemDataBound="radData_ItemDataBound" ShowFooter="true" ShowHeader="false">
            <MasterTableView Width="100%" GridLines="Horizontal">
                        <RowIndicatorColumn Visible="False">
                            <HeaderStyle Width="20px"></HeaderStyle>
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Visible="False" Resizable="False">
                            <HeaderStyle Width="20px"></HeaderStyle>
                        </ExpandCollapseColumn>
                    </MasterTableView>
		            <AlternatingItemStyle Font-Bold="true" />
     	            <ItemStyle Font-Bold="true" />
            </telerik:RadGrid>                    
        </div>          
    </td>
</tr>
<tr>
<td class="date"><br />
<table>
<tr>
<td class="Exportfont">
 <asp:Literal ID="litExportTo" Visible="false" runat="server" Text="Export to: " ></asp:Literal>     
</td>
<td>
  <telerik:RadButton ID="btnExportToExcel" runat="server" Visible="false" CausesValidation="false"
        Text="Excel" OnClick="img_btn_export_to_excel_Click"></telerik:RadButton>   
    <telerik:RadButton ID="btnExportToPdf" runat="server" CausesValidation="False" Visible="false" Text="Pdf"
                OnClick="btnExportToPdf_Click">
    </telerik:RadButton> 
</td>
</tr>
</table>
   
                         
                                                          

    <br />
    <br />
    <br />

</td></tr>
<tr><td class="style2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
    <a href="#" onclick="javascript:window.close();">Close</a>&nbsp;</td></tr>
</table>
    </form>
</body>
</html>
