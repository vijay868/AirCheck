﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for auditTrail
/// </summary>
public class auditTrail
{
    private string _connectionString;
    private string _userName;
    private string _IPAddress;
    private string _pageViewed;
    private string _market;
    private string _station;
    private int _userID;
    private DateTime? _startDate;
    private DateTime? _endDate;
    private bool _exportedToExcel;
    private bool _exportedToPdf;
    private bool _exportedToText;


    public int userID
    {
        get
        {
            return _userID;
        }
        set
        {
            _userID = value;
        }
    }


    public DateTime? startDate
    {
        get
        {
            return _startDate;
        }
        set
        {
            _startDate = value;
        }
    }


    public DateTime? endDate
    {
        get
        {
            return _endDate;
        }
        set
        {
            _endDate = value;
        }
    }


    public Boolean exportedToExcel
    {
        get
        {
            return _exportedToExcel;
        }
        set
        {
            _exportedToExcel = value;
        }
    }

    public Boolean exportedToPdf
    {
        get
        {
            return _exportedToPdf;
        }
        set
        {
            _exportedToPdf = value;
        }
    }

    public Boolean exportedToText
    {
        get
        {
            return _exportedToText;
        }
        set
        {
            _exportedToText = value;
        }
    }


    public string connectionString
    {
        get
        {
            return _connectionString;
        }
        set
        {
            _connectionString = value;
        }
    }

    public string userName
    {
        get
        {
            return _userName;
        }
        set
        {
            _userName = value;
        }
    }

    public string IPAddress
    {
        get
        {
            return _IPAddress;
        }
        set
        {
            _IPAddress = value;
        }
    }

    public string pageViewed
    {
        get
        {
            return _pageViewed;
        }
        set
        {
            _pageViewed = value;
        }
    }

    public string market
    {
        get
        {
            return _market;
        }
        set
        {
            _market = value;
        }
    }

    public string station
    {
        get
        {
            return _station;
        }
        set
        {
            _station = value;
        }
    }


	public auditTrail()
	{
		//
		// TODO: Add constructor logic here
		//
	}



    public void addUserActivityLog()
    {
        SqlConnection conn;
        SqlCommand comm;

        conn = new SqlConnection(connectionString);
        comm = new SqlCommand("addUserActivityLog", conn);
        comm.CommandType = CommandType.StoredProcedure;
       
        comm.Parameters.AddWithValue("@dateViewed", System.DateTime.Now);
        comm.Parameters.AddWithValue("@userID", userID);
        comm.Parameters.AddWithValue("@ipAddress", IPAddress);
        comm.Parameters.AddWithValue("@pageViewed", pageViewed);
        comm.Parameters.AddWithValue("@startDate", startDate);
        comm.Parameters.AddWithValue("@endDate", endDate);
        comm.Parameters.AddWithValue("@market", market);
        comm.Parameters.AddWithValue("@station", station);
        comm.Parameters.AddWithValue("@exportedToExcel", exportedToExcel);
        comm.Parameters.AddWithValue("@exportedToPdf", exportedToPdf);
        comm.Parameters.AddWithValue("@exportedToText", exportedToText);

        conn.Open();
        comm.ExecuteNonQuery();
        conn.Close();
    }

    public void addUserActivityLogForMarket()
    {
        SqlConnection conn;
        SqlCommand comm;

        conn = new SqlConnection(connectionString);
        comm = new SqlCommand("addUserActivityLogForMarket", conn);
        comm.CommandType = CommandType.StoredProcedure;

        comm.Parameters.AddWithValue("@dateViewed", System.DateTime.Now);
        comm.Parameters.AddWithValue("@userID", userID);
        comm.Parameters.AddWithValue("@ipAddress", IPAddress);
        comm.Parameters.AddWithValue("@pageViewed", pageViewed);
        comm.Parameters.AddWithValue("@startDate", startDate);
        comm.Parameters.AddWithValue("@endDate", endDate);
        comm.Parameters.AddWithValue("@market", market);
        comm.Parameters.AddWithValue("@exportedToExcel", exportedToExcel);
        comm.Parameters.AddWithValue("@exportedToPdf", exportedToPdf);
        comm.Parameters.AddWithValue("@exportedToText", exportedToText);

        conn.Open();
        comm.ExecuteNonQuery();
        conn.Close();
    }

    public void addUserActivityLogPassingStationIDs()
    {
        SqlConnection conn;
        SqlCommand comm;

        conn = new SqlConnection(connectionString);
        comm = new SqlCommand("addUserActivityLogPassingStationIDs", conn);
        comm.CommandType = CommandType.StoredProcedure;

        comm.Parameters.AddWithValue("@dateViewed", System.DateTime.Now);
        comm.Parameters.AddWithValue("@userID", userID);
        comm.Parameters.AddWithValue("@ipAddress", IPAddress);
        comm.Parameters.AddWithValue("@pageViewed", pageViewed);
        comm.Parameters.AddWithValue("@startDate", startDate);
        comm.Parameters.AddWithValue("@endDate", endDate);
        comm.Parameters.AddWithValue("@stationIDs", station);
        comm.Parameters.AddWithValue("@exportedToExcel", exportedToExcel);
        comm.Parameters.AddWithValue("@exportedToPdf", exportedToPdf);
        comm.Parameters.AddWithValue("@exportedToText", exportedToText);

        conn.Open();
        comm.ExecuteNonQuery();
        conn.Close();
    }

}
