﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class topAlbumDetails : System.Web.UI.Page
{
    Boolean shouldAddNewColumn = true;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillHeaderDetailPart1();
            generateReport();

            if (Session["dataReadOnly"].ToString() == "True")
            {
                btnExportToExcel.Enabled = false;

            }
            else
            {
                btnExportToExcel.Enabled = true;

            }
        }
    }


    protected void fillHeaderDetailPart1()
    {
        DateTime dt1 = DateTime.Parse(Session["strStartDate"].ToString());
        DateTime dt2 = DateTime.Parse(Session["strEndDate"].ToString());
        string strDateFormat = (string)Session["dateFormat"];
        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }

        String strDateRange = sDt1 + "  to  " + sDt2;

        lblDateRange.Text = strDateRange;
        lblSongTitle.Text = undoStringSanitation(Request.QueryString["strSong"]);
        lblMovieArtist.Text = undoStringSanitation(Request.QueryString["strMovie"]);
    }

    protected void fillHeaderDetailPart2(DataTable dt)
    {
        lblDuration.Text = dt.Rows[0][1].ToString();
        lblTotalNoOfPlays.Text = dt.Rows[0][0].ToString();
    }

    protected string getPlayCountForMarket(DataTable dt, string strMarket)
    {
        // Presuming the DataTable has a column named Date.
        string expression;
        expression = "Market='" + strMarket + "'";
        DataRow[] foundRows;

        // Use the Select method to find all rows matching the filter.
        foundRows = dt.Select(expression);
        string strCount = "";
        // Print column 0 of each returned row.
        for (int i = 0; i < foundRows.Length; i++)
        {
            strCount = foundRows[i][1].ToString();
        }
        return strCount;
    }

    protected string undoStringSanitation(string str)
    {
        if (str.IndexOf("123123") >= 0)
        {
            str = str.Replace("123123", "'");
        }
        if (str.IndexOf("456456") >= 0)
        {
            str = str.Replace("456456", "&");
        }

        return str;
    }

    protected void generateReport()
    {
        string strStationIDs = Session["strStationIDs"].ToString();

        DateTime dtStartDate = DateTime.Parse(Session["strStartDate"].ToString());
        DateTime dtEndDate = DateTime.Parse(Session["strEndDate"].ToString());

        string strStartDate = dtStartDate.ToString("d");
        string strEndDate = dtEndDate.ToString("d");


        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("topSongDetails", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@stationIDs", strStationIDs);
        da.SelectCommand.Parameters.AddWithValue("@startDate", strStartDate);
        da.SelectCommand.Parameters.AddWithValue("@endDate", strEndDate);
        da.SelectCommand.Parameters.AddWithValue("@searchSong", undoStringSanitation(Request.QueryString["strSong"]).Replace("'", "''"));
        da.SelectCommand.Parameters.AddWithValue("@searchMovie", undoStringSanitation(Request.QueryString["strMovie"]).Replace("'", "''"));


        DataSet ds = new DataSet();
        da.Fill(ds);
        gvData.FooterStyle.CssClass = "FF";
        gvData.AlternatingRowStyle.BackColor = System.Drawing.Color.SkyBlue;
        try
        {
            if (ds.Tables[0].Rows.Count == 0)
            {
                pnlExportTo.Visible = false;
                btnExportToExcel.Visible = false;
                gvData.Visible = false;
                lblError.Visible = true;
                lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
                return;
            }
            else
            {
                DataTable dt = ds.Tables[0];
                Session["topSongsDetailsData"] = dt;
                pnlExportTo.Visible = false;
                btnExportToExcel.Visible = false;
                lblError.Text = "";
                lblError.Visible = false;
                gvData.Visible = true;

                try
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        int[] intArray = commaStrToArray(dr[0].ToString());
                        info.AddMergedColumns(intArray, dr[1].ToString());
                    }
                }
                catch
                {
                    shouldAddNewColumn = false;
                }

                Session["topSongsDetailsDataForExport"] = dt;

                DataTable dt2 = ds.Tables[2];
                Session["dt2"] = dt2;

                fillHeaderDetailPart2(ds.Tables[3]);
                pnlExportTo.Visible = true;
                btnExportToExcel.Visible = true;

                gvData.DataSource = dt;
                gvData.DataBind();


            }
        }
        catch
        {
            pnlExportTo.Visible = false;
            btnExportToExcel.Visible = false;
            gvData.Visible = false;
            lblError.Visible = true;
            lblError.Text = "<BR><BR><BR><BR><BR><BR><BR>No record found";
            return;
        }

    }

    private int[] commaStrToArray(string strIntComma)
    {
        string[] strArray;
        strArray = strIntComma.Split(new char[] { ',' });
        int[] intArray = new int[strArray.Length];
        for (int i = 0; i < strArray.Length; i++)
            intArray[i] = int.Parse(strArray[i]);
        return intArray;
    }

    //property for storing of information about merged columns
    private MergedColumnsInfo info
    {
        get
        {
            if (ViewState["info"] == null)
                ViewState["info"] = new MergedColumnsInfo();
            return (MergedColumnsInfo)ViewState["info"];
        }
    }

    protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
    {
        //call the method for custom rendering the columns headers	
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.SetRenderMethodDelegate(RenderHeader);
        }

        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.SetRenderMethodDelegate(RenderFooter);
        }
    }

    private System.Drawing.Color getColor(int i)
    {
        System.Drawing.Color xCol = System.Drawing.Color.FromArgb(0, 112, 192);

        if (i == 0)
        {
            xCol = System.Drawing.Color.FromArgb(0, 112, 192);
        }
        else
        {
            if (i % 2 == 1)
            {
                xCol = System.Drawing.Color.FromArgb(31, 73, 125);
            }
        }

        return xCol;
    }

    //method for rendering the columns headers	
    private void RenderHeader(HtmlTextWriter output, Control container)
    {
        int j = 0;
        string strColWidth = "";
        string strColor = "";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();

        for (int i = 0; i < container.Controls.Count; i++)
        {
            TableCell cell = (TableCell)container.Controls[i];
            //stretch non merged columns for two rows
            if (!info.MergedColumns.Contains(i))
            {
                cell.Attributes["rowspan"] = "2";
                cell.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);

                cell.RenderControl(output);
            }
            else //render merged columns common title
                if (info.StartColumns.Contains(i))
                {
                    //if (j % 2 == 0)
                    //{
                    //    strColor = "MidnightBlue";
                    //}
                    //else
                    //{
                    //    strColor = "RoyalBlue";
                    //}
                    //strColWidth = (int.Parse(info.StartColumns[i].ToString()) * 100).ToString() + "px";
                    //output.Write(string.Format("<th bgcolor='" + strColor + "' align='center' colspan='{0}' style='color:white;font-size:" + (string)Session["headerFontSize"] + ";font-weight:" + (string)Session["headerFontStyle"] + ";font-family:" + (string)Session["headerFontName"] + ";'>{1}</th>",
                    //         info.StartColumns[i], info.Titles[i]));
                    //j++;
                    if (j % 2 == 0)
                    {
                        strColor = (string)Session["headerRowColor"];
                    }
                    else
                    {
                        strColor = (string)Session["headerRowColor"];
                    }

                    strColWidth = (int.Parse(info.StartColumns[i].ToString()) * 100).ToString() + "px";
                    output.Write(string.Format("<th bgcolor='" + strColor + "' align='center' colspan='{0}' style='color:" + (string)Session["headerFontColor"] + ";font-size:14px;font-family:Helvetica;'>{1}</th>",

                             info.StartColumns[i], info.Titles[i]));
                    j++;
                }
        }

        //close the first row	
        output.RenderEndTag();
        //set attributes for the second row
        gvData.HeaderStyle.AddAttributesToRender(output);
        //start the second row
        output.RenderBeginTag("tr");

        //render the second row (only the merged columns)
        for (int i = 0; i < info.MergedColumns.Count; i++)
        {
            TableCell cell = (TableCell)container.Controls[info.MergedColumns[i]];
            cell.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);

            cell.RenderControl(output);
        }
    }

    //method for rendering the columns footers	
    private void RenderFooter(HtmlTextWriter output, Control container)
    {
        int j = 0;

        DataTable dt2 = (DataTable)Session["dt2"];
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        string strColWidth = "";
        for (int i = 0; i < container.Controls.Count; i++)
        {
            TableCell cell = (TableCell)container.Controls[i];
            //stretch non merged columns for two rows
            if (!info.MergedColumns.Contains(i))
            {
                if (i == 0)
                {
                    cell.Attributes["colspan"] = "2";
                    cell.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);

                    cell.RenderControl(output);
                }
            }
            else //render merged columns common title
                if (info.StartColumns.Contains(i))
                {
                    strColWidth = (int.Parse(info.StartColumns[i].ToString()) * 100).ToString() + "px";
                    output.Write(string.Format("<th align='center' colspan='{0}' style='color:'" + (string)Session["headerRowColor"] + "';font-size:14px;font-family:Helvetica;'>{1}</th>",
                             info.StartColumns[i], getPlayCountForMarket(dt2, info.Titles[i].ToString().Trim())));
                    j++;
                }
        }

        //close the first row	
        output.RenderEndTag();
        //set attributes for the second row
        gvData.HeaderStyle.AddAttributesToRender(output);
        //start the second row
        output.RenderBeginTag("tr");

    }

    private string InsertLineBreaks(string _emailString)
    {
        int MaxStringLength = 40;

        if (_emailString.Length > MaxStringLength)
        {
            int indexOfSpace = _emailString.IndexOf(" ", MaxStringLength - 1);

            if ((indexOfSpace != -1) && (indexOfSpace != _emailString.Length - 1))
            {
                string firstString = _emailString.Substring(0, indexOfSpace);
                string secondString = _emailString.Substring(indexOfSpace);

                return firstString + "<br>" + InsertLineBreaks(secondString);
            }
            else
            {
                return _emailString;
            }
        }
        else
        {
            return _emailString;
        }
    }

    protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //e.Row.Cells[0].CssClass = "locked"; 
        //e.Row.Cells[1].CssClass = "locked";

        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();




        DataTable dt = (DataTable)Session["topSongsDetailsData"];

        DateTime dtStartDate = DateTime.Parse(Session["strStartDate"].ToString());
        DateTime dtEndDate = DateTime.Parse(Session["strEndDate"].ToString());

        string strStartDate = dtStartDate.ToString("d");
        string strEndDate = dtEndDate.ToString("d");

        int indexOfBlank = 0;

        if (e.Row.RowType == DataControlRowType.Header)
        {
            int colIndex = 0;
            foreach (DataColumn dc in dt.Columns)
            {
                if (colIndex == 0 || colIndex == 1)
                {
                    e.Row.Cells[colIndex].Style.Add("white-space", "nowrap");
                    e.Row.Cells[colIndex].BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
                    e.Row.Cells[colIndex].Style.Add("font-family", headerFontName);
                    //if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                    //{
                    //    e.Row.Cells[colIndex].Font.Bold = true;
                    //}
                    //else
                    //{
                    //    e.Row.Cells[colIndex].Font.Bold = false;
                    //}
                    e.Row.Cells[colIndex].Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
                    e.Row.Cells[colIndex].ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

                }
                else
                {
                    indexOfBlank = e.Row.Cells[colIndex].Text.LastIndexOf(" ");
                    e.Row.Cells[colIndex].Text = e.Row.Cells[colIndex].Text.Substring(0, indexOfBlank) + "<BR>(# Plays)";
                    e.Row.Cells[colIndex].Style.Add("white-space", "nowrap");
                    e.Row.Cells[colIndex].BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
                    e.Row.Cells[colIndex].Style.Add("font-family", headerFontName);
                    //if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                    //{
                    //    e.Row.Cells[colIndex].Font.Bold = true;
                    //}
                    //else
                    //{
                    //    e.Row.Cells[colIndex].Font.Bold = false;
                    //}
                    e.Row.Cells[colIndex].Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
                    e.Row.Cells[colIndex].ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

                }
                colIndex++;
            }

        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int j = 0;
            int intNoOfPlays = 0;
            string strSongName = e.Row.Cells[0].Text.Replace("'", "###");
            string strArtistName = e.Row.Cells[1].Text.Replace("'", "###");
            string strStationName = "";
            foreach (DataColumn dc in dt.Columns)
            {
                strStationName = dc.ColumnName.Replace("(#Plays)", "");
                e.Row.Cells[j].Style.Add("font-family", actualDataFontName);
                if (Session["dataFontStyle"].ToString().ToLower() == "bold")
                {
                    e.Row.Cells[j].Font.Bold = true;
                }
                else
                {
                    e.Row.Cells[j].Font.Bold = false;
                }
                e.Row.Cells[j].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
                e.Row.Cells[j].ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

                if (j == 0 || j == 1)
                {
                    e.Row.Cells[j].Text = InsertLineBreaks(e.Row.Cells[j].Text);

                    e.Row.Cells[j].Style.Add("white-space", "nowrap");
                }
                if (j > 1)
                {
                    e.Row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                    intNoOfPlays = int.Parse(e.Row.Cells[j].Text);
                    e.Row.Cells[j].Style.Add("white-space", "nowrap");

                }
                j++;
            }
        }

        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[0].Style.Add("white-space", "nowrap");
            e.Row.Cells[0].Text = "Total Plays on Each Market";
            e.Row.Cells[0].Attributes.Add("align", "center");
            e.Row.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Row.Style.Add("font-family", headerFontName);
            //if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            //{
            //    e.Row.Font.Bold = true;
            //}
            //else
            //{
            //    e.Row.Font.Bold = false;
            //}
            e.Row.Font.Size = FontUnit.Parse("14px");
            e.Row.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
    }

    protected void img_btn_export_to_excel_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    public void ExportToExcel()
    {
        string strDateFormat = (string)Session["dateFormat"];
        DataTable dt = (DataTable)Session["topSongsDetailsDataForExport"];


        string str_station_name = "";

        string str_market_name = "";// getMarketOfStation(str_station_name);

        String strStationName = "<BR><BR>Station: " + str_station_name;
        String strMarket = "<BR><BR>Market: " + str_market_name;

        DateTime dt1 = DateTime.Parse(Session["strStartDate"].ToString());
        DateTime dt2 = DateTime.Parse(Session["strEndDate"].ToString());

        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }

        String strDateRange = "<BR><BR>From: " + sDt1 + "  to  " + sDt2;

        ExportToExcel(dt, Response, strStationName, strMarket, strDateRange);
    }

    public void ExportToExcel(DataTable dt, HttpResponse response, String strStationName, string strMarket, String strDateRange)
    {

        //first let's clean up the response.object
        response.Clear();
        response.Charset = "";
        //set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel";
        //create a string writer
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //create an htmltextwriter which uses the stringwriter
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //instantiate a datagrid
        DataGrid dg = new DataGrid();
        //set the datagrid datasource to the dataset passed in
        if (dt == null)
        {
            return;
        }
        else if (dt.Rows.Count == 0)
        {
            return;
        }
        dg.DataSource = dt;
        //bind the datagrid
        dg.DataBind();

        dg.RenderControl(htmlWrite);


        String str = stringWrite.ToString();

        int start, end;
        start = 0;
        end = 0;

        int colspan = 0;

        colspan = dt.Columns.Count;


        String strReportName = "Aircheck Top Album Detail Report";



        string strFinal = "<tr><td align=center colspan=" + colspan + " style='background-color:#FFFF66;'>" + strReportName + "</td></tr>";
        //strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strStationName + "</td></tr>";
        //strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strMarket + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strDateRange + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + ">&nbsp;</td></tr>";



        while (str.IndexOf("<select name=") > 0)
        {
            start = str.IndexOf("<select name=");
            end = str.IndexOf("</select>") + 9;
            str = str.Replace(str.Substring(start, end - start), "");
        }
        //str = str.Replace("<tr>\r\n\t\t<td>&nbsp;</td><td>&nbsp;</td>", "<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td>");

        string strAirTime = "";
        string strPlayTime = "";
        string strNewPlayTime = "";

        int intCount = 0;

        foreach (DataRow dr in dt.Rows)
        {
            strAirTime = dr[0].ToString();
            if (strAirTime == "")
            {
                strPlayTime = dr[2].ToString();
                strNewPlayTime = dr[2].ToString() + " ";

                if ((string)Session["category"] == "Song")
                {
                    str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=4 align='Center'>" + strNewPlayTime + "</td></tr>");
                }
                else
                {
                    str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=5 align='Center'>" + strNewPlayTime + "</td></tr>");
                }
            }
            else
            {
                intCount = intCount + 1;
            }
        }


        str = strFinal + "<BR><BR>" + str;


        string strDateFormat = (string)Session["dateFormat"];

        DateTime dtDate = System.DateTime.Now;
        string strDate = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dtDate.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dtDate.ToString("MM/dd/yyyy");
        }


        string strTime = System.DateTime.Now.ToString();
        strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


        string strCountText = "<b>" + intCount + " rows </b>";

        string strFooterText = "";

        strFooterText = "<BR><table>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>This data was generated and exported on " + strDate + " at " + strTime + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>" + strCountText + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><BR></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.</td></tr>";
        strFooterText += "</table>";

        str = "<table>" + str + strFooterText;



        response.Write(str);
        response.End();
    }
}
