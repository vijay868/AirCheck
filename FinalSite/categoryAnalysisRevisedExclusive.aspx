﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="categoryAnalysisRevisedExclusive.aspx.cs"
    Inherits="categoryAnalysisRevisedExclusive" %>
    <%@ Register Assembly="DBauer.Web.UI.WebControls.HierarGrid" Namespace="DBauer.Web.UI.WebControls"
    TagPrefix="DBWC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Exclusive Category Analysis</title>
    <link href="css/report_caption.css" rel="stylesheet" >    

    <script language="javascript" type="text/javascript">

			    function HideIcons() {
			        prn.style.display = 'none';
			    }

var message="Sorry, this feature is disabled.";

function click(e) 
{
	if (document.all) 
	{
		if (event.button == 2) 
		{
			alert(message);
			return false;
		}
	}
	if (document.layers) 
	{
		if (e.which == 3) 
		{
			alert(message);
			return false;
		}
	}
}

if (document.layers) 
{
	document.captureEvents(Event.MOUSEDOWN);
}
document.onmousedown=click;

    </script>

</head>
<body >
    <form id="Form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <table height="53" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="top" align="top" width="760" height="53" border="0">
                <img src="images/bgTop.gif">
            </td>
            <td valign="top" width="100%">
                <img height="28" src="images/bgTopExt.gif" width="100%" border="0">
            </td>
        </tr>
    </table>
    <span name="prn" id="prn">
        <table height="0" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td width="100%" align="right" colspan="2">
                    <a href="#" onclick="prn.style.display='none';print();">
                        <img src="images/icon_print.gif" border="0"></a>&nbsp;&nbsp;
                </td>
            </tr>
        </table>
    </span>
    <table width="100%">
        <tr valign="top">
            <td align="middle" width="100%" colspan="3" valign="top" class="date" height="18"
                bgcolor="cornsilk">
                Category Analysis Exclusive
            </td>
        </tr>
        <tr class="date">
            <td width="10%" height="15">
                &nbsp;
            </td>
            <td width="1%" height="15">
                &nbsp;
            </td>
            <td align="left" height="15">
                &nbsp;
            </td>
        </tr>
        <tr class="date">
            <td width="10%" height="15">
                Market
            </td>
            <td width="1%" height="15">
                :
            </td>
            <td align="left" height="15">
                <asp:Label ID="lblMarket" runat="server" CssClass="date"></asp:Label>
            </td>
        </tr>
        <tr class="date">
            <td width="10%" height="15">
                Station
            </td>
            <td width="1%" height="15">
                :
            </td>
            <td align="left" height="15">
                <asp:Label ID="lblStation" runat="server" CssClass="date"></asp:Label>
            </td>
        </tr>
        <tr class="date">
            <td width="10%" height="15">
                Start Date
            </td>
            <td width="1%" height="15">
                :
            </td>
            <td align="left" height="15">
                <asp:Label ID="lblStartDate" runat="server" CssClass="date"></asp:Label>
            </td>
        </tr>
        <tr class="date">
            <td width="10%" height="15">
                End Date
            </td>
            <td width="1%" height="15">
                :
            </td>
            <td align="left" height="15">
                <asp:Label ID="lblEndDate" runat="server" CssClass="date"></asp:Label>
            </td>
        </tr>
        <tr class="date">
            <td width="10%" height="15">
                &nbsp;
            </td>
            <td width="1%" height="15">
                &nbsp;
            </td>
            <td align="left" height="15">
                &nbsp;
            </td>
        </tr>
        <tr class="date">
            <td width="100%" colspan="3">
                <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label>
                <dbwc:hierargrid id="dgHierarchical" runat="server" templatedatamode="Table" loadcontrolmode="Template"
                    templatecachingbase="Tablename" showheader="True" autogeneratecolumns="True"
                    enableviewstate="False" width="100%" ontemplateselection="dgHierarchical_TemplateSelection"
                    onitemdatabound="dgHierarchical_ItemDataBound" visible="false">
	                <HeaderStyle CssClass="DataGridFixedHeader" HorizontalAlign="Center"/>
                </dbwc:hierargrid>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <asp:Literal ID="litExportTo" Visible="false" runat="server" Text="Export to: "></asp:Literal>                
                <telerik:RadButton ID="btnExportToExcel" runat="server" Text="Excel" OnClick="img_btn_export_to_excel_Click">
                </telerik:RadButton>  
                    
            </td>
        </tr>
    </table>
    <br>
    </form>
</body>
</html>
