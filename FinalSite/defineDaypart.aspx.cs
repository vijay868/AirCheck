﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class defineDaypart : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string strDayPart = "";
            if (Session["strDayPart"] == null || Session["strDayPart"].ToString() == "")
            {
                selectDayPart(0);
            }
            else
            {
                strDayPart = Session["strDayPart"].ToString();
                preSelectDayPart(strDayPart);
            }

            
        }
    }

    protected void preSelectDayPart(string strDayPart)
    {
        string str = strDayPart;


        string[] arrDayParts = str.Split('/');
        string[] arrDayPartsFromTo = new string[2];
        
        DropDownList ddFrom;
        DropDownList ddTo;

        for (int i = 1; i <= 12; i++)
        {
            ddFrom = (DropDownList)Page.FindControl("ddDayPart" + i.ToString() + "From");
            ddTo = (DropDownList)Page.FindControl("ddDayPart" + i.ToString() + "To");
            string strSelect = "btnSelect" + i.ToString();


            Button btn = (Button)Page.FindControl(strSelect);

            ddFrom.Enabled = false;
            ddTo.Enabled = false;

            ddFrom.SelectedIndex = -1;
            ddTo.SelectedIndex = -1;

            btn.Enabled = false;
        }

        for (int i = 0; i < arrDayParts.Length; i++)
        {
            arrDayPartsFromTo = arrDayParts[i].Split(',');
   
            ddFrom = (DropDownList)Page.FindControl("ddDayPart" + (i+1).ToString() + "From");
            ddTo = (DropDownList)Page.FindControl("ddDayPart" + (i + 1).ToString() + "To");

            string strSelect = "btnSelect" + (i+1).ToString();
            Button btn = (Button)Page.FindControl(strSelect);

            ddFrom.Enabled = true;
            ddTo.Enabled = true;
            btn.Enabled = true;

            ddFrom.SelectedIndex = int.Parse(arrDayPartsFromTo[0]);
            ddTo.SelectedIndex = int.Parse(arrDayPartsFromTo[1]);

        }
    }

    protected void btnSelect1_Click(object sender, EventArgs e)
    {
        selectDayPart(1);
    }
    protected void btnSelect2_Click(object sender, EventArgs e)
    {
        selectDayPart(2);
    }
    protected void btnSelect3_Click(object sender, EventArgs e)
    {
        selectDayPart(3);
    }
    protected void btnSelect4_Click(object sender, EventArgs e)
    {
        selectDayPart(4);
    }
    protected void btnSelect5_Click(object sender, EventArgs e)
    {
        selectDayPart(5);
    }
    protected void btnSelect6_Click(object sender, EventArgs e)
    {
        selectDayPart(6);
    }
    protected void btnSelect7_Click(object sender, EventArgs e)
    {
        selectDayPart(7);
    }
    protected void btnSelect8_Click(object sender, EventArgs e)
    {
        selectDayPart(8);
    }
    protected void btnSelect9_Click(object sender, EventArgs e)
    {
        selectDayPart(9);
    }
    protected void btnSelect10_Click(object sender, EventArgs e)
    {
        selectDayPart(10);
    }
    protected void btnSelect11_Click(object sender, EventArgs e)
    {
        selectDayPart(11);
    }
    protected void btnSelect12_Click(object sender, EventArgs e)
    {
        selectDayPart(12);
    }
    protected void btnOK_Click(object sender, EventArgs e)
    {

    }

    protected void selectDayPart(int rowNumber)
    {

        string strDayPartFrom;
        string strDayPartTo;
        string strSelect;

        DropDownList ddFrom;
        DropDownList ddTo;
        Button btn;

        int i = rowNumber;

        strDayPartFrom = "ddDayPart" + i.ToString() + "From";
        strDayPartTo = "ddDayPart" + i.ToString() + "To";

        ddFrom = (DropDownList)Page.FindControl(strDayPartFrom);
        ddTo = (DropDownList)Page.FindControl(strDayPartTo);

        try
        {
            if (ddFrom.SelectedItem.Text == "" || ddTo.SelectedItem.Text == "")
            {
                showError("Please select valid Daypart.");
                return;
            }
        }
        catch { }

        if (i > 0)
        {
            string strPreviousDayPartTo;

            if (i > 1)
            {
                strPreviousDayPartTo = "ddDayPart" + (i - 1).ToString() + "To";
                DropDownList ddPreviousDayPartTo = (DropDownList)Page.FindControl(strPreviousDayPartTo);
                if (int.Parse(ddFrom.SelectedItem.Value) < int.Parse(ddPreviousDayPartTo.SelectedItem.Value))
                {
                    showError("Dayparts cannot be overlapped.");
                    return;
                }
            }
            if (int.Parse(ddTo.SelectedItem.Value) <= int.Parse(ddFrom.SelectedItem.Value))
            {
                showError("'To Daypart' must be greater than 'From Daypart'.");
                return;
            }
        }
        

        lblError.Text = "";
        lblError.Visible = false;

        if (rowNumber == 12)
        {
            return;
        }

        
        i = rowNumber + 1;
        strDayPartFrom = "ddDayPart" + i.ToString() + "From";
        strDayPartTo = "ddDayPart" + i.ToString() + "To";
        strSelect = "btnSelect" + i.ToString();


        ddFrom = (DropDownList)Page.FindControl(strDayPartFrom);
        ddTo = (DropDownList)Page.FindControl(strDayPartTo);
        btn = (Button)Page.FindControl(strSelect);

        ddFrom.SelectedIndex = -1;
        ddFrom.Enabled = true;
        ddTo.SelectedIndex = -1;
        ddTo.Enabled = true;
        btn.Enabled = true;

        //Uncheck all lower rows and disable their Select buttons
        for (i = rowNumber + 2; i <= 12; i++)
        {
            strDayPartFrom = "ddDayPart"+i.ToString()+"From";
            strDayPartTo = "ddDayPart"+i.ToString()+"To";
            strSelect = "btnSelect"+i.ToString();
                        

            ddFrom = (DropDownList)Page.FindControl(strDayPartFrom);
            ddTo = (DropDownList)Page.FindControl(strDayPartTo);
            btn = (Button)Page.FindControl(strSelect);

            ddFrom.SelectedIndex = -1;
            ddFrom.Enabled = false;
            ddTo.SelectedIndex = -1;
            ddTo.Enabled = false;
            btn.Enabled = false;
        }

        storeDayPart(rowNumber);
    }

    protected void showError(string strError)
    {
        lblError.Visible = true;
        lblError.Text = strError;
    }

    private void storeDayPart(int rowNumber)
    {
        string strDayPartFrom;
        string strDayPartTo;
        
        DropDownList ddFrom;
        DropDownList ddTo;
        
        string strDayPart = "";
        for (int i = 1; i <= rowNumber; i++)
        {
            strDayPartFrom = "ddDayPart"+i.ToString()+"From";
            strDayPartTo = "ddDayPart"+i.ToString()+"To";
                        

            ddFrom = (DropDownList)Page.FindControl(strDayPartFrom);
            ddTo = (DropDownList)Page.FindControl(strDayPartTo);

            if (i == 1)
            {
                strDayPart = ddFrom.SelectedItem.Value.ToString()+","+ddTo.SelectedItem.Value.ToString();
            }
            else
            {
                strDayPart = strDayPart + "/" + ddFrom.SelectedItem.Value.ToString()+","+ddTo.SelectedItem.Value.ToString();
            }

        }
        Session["strDayPart"] = strDayPart;
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        Session["strDayPart"] = "";
        Response.Redirect("defineDaypart.aspx");
    }
}
