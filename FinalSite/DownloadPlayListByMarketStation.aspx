﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DownloadPlayListByMarketStation.aspx.cs" Inherits="DownloadPlayListByMarketStation"
    Title="Download PlayList By Market/Station" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript" language="javascript">
         function ToggleCollapsePane() {
               
               var splitter = $find("<%= Page.Master.FindControl("RadSplitter1").ClientID %>");
               var pane = splitter.getPaneById("<%= Page.Master.FindControl("RadPane1").ClientID %>");
                pane.collapse();
//               if (!pane) return;
// 
//               if (pane.get_collapsed()) {
//                    pane.expand();
//               }
//               else {
//                    pane.collapse();
//               }
    }
    
        function calcDays()
        {
            ToggleCollapsePane();
            var startDate =$find("<%=dtStart.ClientID %>");
            var endDate =  $find("<%=dtEnd.ClientID %>");
            
            var sDate = startDate.get_selectedDate();
            var eDate = endDate.get_selectedDate();
            
           var lockDataBefore="<%=Session["lockDataBefore"]%>";
		    var lockDataAfter="<%=Session["lockDataAfter"]%>";
            if(sDate == null)
            {
                lockDataBefore = new Date(lockDataBefore);
                var curr_date = lockDataBefore.getDate();
                var curr_month = lockDataBefore.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataBefore.getFullYear();
                lockDataBefore= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights only from " + lockDataBefore);
                return false;
            }
            
            if(eDate == null)
            {
                lockDataAfter = new Date(lockDataAfter);
                var curr_date = lockDataAfter.getDate();
                var curr_month = lockDataAfter.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataAfter.getFullYear();
                lockDataAfter= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights till " + lockDataAfter);
                return false;
            }
                    
//            tdhide('leftMenu', 'ctl00_showHide','hide');            
            
            
            var comboBox = $find("<%= txtMarketSelect.ClientID %>");    
            var treeS = comboBox.get_items().getItem(0).findControl("radTreeCities");
            
            var rootNodesS = treeS.SelectedNode;
            var nodesS = treeS.get_allNodes();
            
            var i;
            var isChecked = 0;
             for(i=0; i<nodesS.length; i++)
             {
                if(!nodesS[i].get_checked())
                {
                
                }
                else
                {
                    isChecked = 1;
                }
             }            
            if (isChecked == 0)
            {
                alert("You have not selected any Market/Station.");
                return false;
            }
            else if(days_between(sDate,eDate) > 30)
            {
                alert("Maximum allowed date range is one month.");
                return false;
            }
            else
            {
                //disp_clock();
                return true;
            }
         
        } 
        
        function days_between(date1, date2) {

            // The number of milliseconds in one day
            var ONE_DAY = 1000 * 60 * 60 * 24

            // Convert both dates to milliseconds
            var date1_ms = date1.getTime()
            var date2_ms = date2.getTime()

            // Calculate the difference in milliseconds
            var difference_ms = Math.abs(date1_ms - date2_ms)
            
            // Convert back to days and return
            return Math.round(difference_ms/ONE_DAY)

        }
        
        function disp_clock()
		{
			OnLoad();
			divDataGrid.style.display='none';
			divWait.style.display='';
						
    	}

		function OnLoad() 
		{ 
		    setTimeout("StartAnimation()", 500); 
		} 

		function StartAnimation() 
		{ 
		    if (document.images)
		    {
		        document['ProgressBarImg'].src = "images/aniClock.gif";
		    }
		}
		
		function excelExport(type) 
        {
            window.open('ExcelExportDatewise.aspx?type='+type,'')
            return false;
        }  
		
		function CallPrint(strid) {
		    var prtContent = document.getElementById(strid);
		    var WinPrint = window.open('', '', 'letf=0,top=0,width=1,height=1,toolbar=0,scrollbars=0,status=0');
		    WinPrint.document.write(prtContent.innerHTML);
		    WinPrint.document.close();
		    WinPrint.focus();
		    WinPrint.print();
		    WinPrint.close();
		    prtContent.innerHTML = strOldOne;
		}
		
		function nodeChecked(sender, args) {
var comboBox = $find("<%= txtMarketSelect.ClientID %>");            
            document.getElementById('<%=txtMarketSelectHidden.ClientID%>').value = "";
            var tree = comboBox.get_items().getItem(0).findControl("radTreeCities");
            var node = args.get_node();
            var rootNodeSelCount = 0;
            var nodesJ=tree.get_nodes(); 
            var marketsval="";
              for (i=0; i<nodesJ.get_count(); i++) 
               {                
                   if (nodesJ.getNode(i).get_checked()) 
                   { 
                       //alert(nodesJ.getNode(i).get_text());
                       marketsval = marketsval + nodesJ.getNode(i).get_text().toString() + ",";
                       rootNodeSelCount =  rootNodeSelCount + 1;
                   } 
               }
               
               if(rootNodeSelCount > 1)
               {
                   alert('Cannot select more than 1 markets');
                   node.uncheck();
                   return false;
               }
 
            var tempNode = args.get_node();
            if (tempNode.get_text().toString() == "(Select All)") 
            {
                // check or uncheck all the nodes
            } else {
                var nodes = new Array();
                nodes = sender.get_checkedNodes();
                var vals = "";
                var i = 0;
                var icount = 0;

                for (i = 0; i < nodes.length; i++) {
                    var n = nodes[i];
                    var nodeText = n.get_text().toString();
                    if (nodeText != "(Select All)") {
                        if (n._hasChildren() == true) {
                        } else {
                            vals = vals + n.get_text().toString() + ",";
                        }
                    }
                }
                document.getElementById('<%=txtMarketSelectHidden.ClientID%>').value = vals;
                comboBox.set_text(marketsval);
            }
        }
        
        function nodeClicking(sender, args) {
            var comboBox = $find("<%= txtMarketSelect.ClientID %>");

            var node = args.get_node();

            if (node.get_parent() == node.get_treeView()) {
                alert("Please select a station. Market selection not allowed.");

            }
            else {
                comboBox.set_text(node.get_text());
                comboBox.get_items().getItem(0).set_value(node.get_value());
            }

        }
        
        function OnClientDropDownOpenedHandler(sender, eventArgs) {

            var tree = sender.get_items().getItem(0).findControl("radTreeCities");            

            if (!!tree) {
                var selectedNode = tree.get_selectedNode();
                if (selectedNode) {
                    selectedNode.scrollIntoView();
                }
            }            
        }
        function getrootcount(sender, args) {

            //        var tree = sender.get_items().getItem(0).findControl("radTreeCities");
            //        var allNodes = tree.get_nodes().getNode(0).get_allNodes();
            //        for (var i = 0; i < allNodes.length; i++) {
            //            var node = allNodes[i];
            //            alert(node.get_text());
            //        }
        }
        function OnClientClicked(button, args)
        {
           if (calcDays())
           {
                button.set_autoPostBack(true); 
           }                
            else
            {                              
                button.set_autoPostBack(false);                        
            }
        }
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div style="width: 99.7%; height: 100%; margin-left:4px">
        <div>
            <table width="100%" background="images/contentheader_bgr.gif" border="0" cellpadding="6px" cellspacing="0px" style="margin-left:-5px; margin-right:300px;">
                <tr>
                    
                    <td align="left">::
                        <asp:Label ID="Pg_Title" runat="server" Font-Bold="True" ForeColor="#000099"></asp:Label>&nbsp;::
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        <asp:Label ID="lbl_Logout" runat="server" Text="Welcome: Administrator[Logout]" 
                            Font-Bold="True" ForeColor="#000099"></asp:Label>
                    </td>
                </tr>
            </table>
    <table border="0">
        <tr>
            <td style="margin-left: 40px" align="left">
                &nbsp;Start Date:
            </td>
            <td style="margin-left: 40px" align="left">
                End Date:
            </td>
            <td style="margin-left: 40px" align="left">
                Select Market/Station
            </td>
            <td style="margin-left: 40px" align="left">
            </td>
            <td  style="margin-left: 40px" align="left">
            </td>
            <td style="margin-left: 40px" align="left">
            </td>
            <td style="margin-left: 40px" align="left">
            </td>
        </tr>
        <tr>
            <td style="margin-left: 40px" align="left">
                &nbsp;        
         <telerik:RadDatePicker ID="dtStart" runat="server" Culture="English (United States)"
                        MinDate="2004-01-01">
                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                        </Calendar>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
        
            </td>
            <td style="margin-left: 40px" align="left">
            <telerik:RadDatePicker ID="dtEnd" runat="server" Culture="English (United States)"
                        MinDate="2004-01-01">
                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                        </Calendar>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>      
        
                <asp:CompareValidator Display="Dynamic" ID="dateCompareValidator" runat="server"
                    ControlToValidate="dtEnd" ControlToCompare="dtStart" Operator="GreaterThanEqual"
                    Type="Date" ErrorMessage="The End date must be after the Start date." Style="font-size: x-small"></asp:CompareValidator>
            </td>
            <td style="margin-left: 40px" align="left">
                         <telerik:RadComboBox ID="txtMarketSelect" runat="server" AfterClientCheck="AfterCheckHandler"
                        CollapseAnimation-Type="None" EmptyMessage="Choose Markets/Stations" ExpandAnimation-Type="None"
                        OnClientDropDownOpened="OnClientDropDownOpenedHandler" ShowToggleImage="True"
                        Style="vertical-align: middle;" Width="200px">
                        <ItemTemplate>
                            <div id="div2">
                                <telerik:RadTreeView ID="radTreeCities" runat="server" Height="200px" OnClientNodeChecked="nodeChecked"
                                    CheckBoxes="true" Width="100%" TriStateCheckBoxes="true">
                                </telerik:RadTreeView>
                            </div>
                        </ItemTemplate>
                        <Items>
                            <telerik:RadComboBoxItem Text="" />
                        </Items>
                    </telerik:RadComboBox>
                     <asp:HiddenField ID="txtMarketSelectHidden" runat="server" />
            </td>
            <td style="margin-left: 40px" align="left">               
                
                <telerik:RadComboBox ID="ddType" runat="server" Width="75px" Sort="Ascending" SortCaseSensitive="true">
                        </telerik:RadComboBox>
            </td>
            <td style="margin-left: 40px" align="left">
                Export to:
            </td>
            <td style="margin-left: 40px" align="left">
                <telerik:RadButton ID="btnGo" runat="server" Text="Excel" OnClick="btnExportToExcel_Click" OnClientClicked="OnClientClicked">
                </telerik:RadButton>
            </td>
            <td style="margin-left: 40px" align="left"> 
                <telerik:RadButton ID="Button1" runat="server" Text="Text" OnClick="btnExportToCsv_Click" OnClientClicked="OnClientClicked" >
                </telerik:RadButton>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td align="left" style="width: 100%" colspan="8">
                <asp:Label ID="lblTxt" runat="server" Font-Names="helvetica" Font-Size="12px"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <table style="width: 100%;background-color:#95B3D7">
                <tr>
                            <td style="font-family: Helvetica; font-size: 14px; font-weight:bold;color:White;">
                                Station
                            </td>
                            <td style="font-family: Helvetica; font-size: 14px; font-weight:bold;color:White;">
                               Market
                            </td>
                            <td style="font-family: Helvetica; font-size: 14px; font-weight:bold;color:White;">
                                Date
                            </td>
                            <td style="font-family: Helvetica; font-size: 14px; font-weight:bold;color:White;">
                               Air Time
                            </td>
                            <td style="font-family: Helvetica; font-size: 14px; font-weight:bold;color:White;">
                               Duration (in Sec)
                            </td>
                            <td style="font-family: Helvetica; font-size: 14px; font-weight:bold;color:White;">
                               Account / Song Title
                            </td>
                            <td style="font-family: Helvetica; font-size: 14px; font-weight:bold;color:White;">
                               Parent / Movie / Artist
                            </td>
                            <td style="font-family: Helvetica; font-size: 14px; font-weight:bold;color:White;">
                               Category
                            </td>
                        </tr>
                    </table>
    <table width="100%">
        <tr>
            <td align="left" style="width: 100%; font-family: Helvetica; font-size: 14px;">
                
                    
                   <br />
                    <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label>                    
            
             <telerik:RadGrid ID="dgResult" runat="server" Visible="false" GridLines="Vertical" >
                <MasterTableView TableLayout="Fixed"></MasterTableView>
                <AlternatingItemStyle Font-Size="13Px" Font-Bold="true" Font-Names="Arial" HorizontalAlign="Center" />
                <ItemStyle Font-Size="13Px" Font-Bold="true" Font-Names="Arial" HorizontalAlign="Center" />
                <HeaderStyle Font-Size="11Px" Font-Names="Verdana" BackColor="#336699" ForeColor="White" HorizontalAlign="Center" />
                <FooterStyle Font-Size="4Px" Font-Names="Verdana" ForeColor="Black" HorizontalAlign="Left" />
             </telerik:RadGrid>
              
              
              
                <div id="divWait" style="display: none">
                    <table style="height: 310Px" width="100%">
                        <tr>
                            <td align="center" width="100%">
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <img src="images/aniClock.gif" border="0" name="ProgressBarImg"><br>
                                <font class="date">Acquiring Data... one moment please!</font>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr height="1">
            <td align="left" valign="top" width="50%">
                <asp:Literal ID="litExportTo" runat="server" Text="Export to: "></asp:Literal>               
                    
                <telerik:RadButton ID="btnExportToExcel" runat="server" Visible="true" Text="Excel" OnClick="btnExportToExcel_Click">
                </telerik:RadButton>
                
                <telerik:RadButton ID="btnExportToCsv" runat="server" Visible="true" Text="Text" OnClick="btnExportToCsv_Click">
                </telerik:RadButton>   
            </td>
            <td align="right" valign="top" width="50%">
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtArray" runat="server" Style="visibility: hidden; height: 0Px"></asp:TextBox>
    <asp:Panel ID="pnlBlank" runat="server" Height="390Px" Visible="true">
    </asp:Panel>
    <asp:Literal ID="lit" runat="server"></asp:Literal>
    </div>
</asp:Content>
