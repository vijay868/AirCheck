﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using Telerik.Web.UI;
using System.Collections.Generic;
using System.Globalization;

public partial class DatewisePlaylist_Compare : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            string strType = (string)Session["category"];

            bindGrid(strType, "");
            ShowGirdContainerButtons.Visible = true;
        }
    }

    protected void bindGrid(string type, string sortColumn)
    {
        if (Session["s_date"] == null || Session["e_date"] == null || (string)Session["StationLeft"] == "")
        {
            return;
        }


        string str_station_name = "";

        if (Request.QueryString["location"] == "Left")
        {
            str_station_name = (String)Session["StationLeft"];
        }
        else
        {
            str_station_name = (String)Session["StationRight"];
        }

        //str_station_name = str_station_name.Substring(0, str_station_name.IndexOf("/") - 1);
        lblTitle.Text = str_station_name;

        var t1 = Session["s_date"].ToString();
        var t2 = Session["e_date"].ToString();

        DateTime startTime = DateTime.Parse(t1);
        DateTime endTime = DateTime.Parse(t2);

        //DateTime startTime2 = DateTime.ParseExact((String)Session["s_date"], "dd/MM/yyyy", null);
        //DateTime endTime = DateTime.ParseExact(Session["s_date"].ToString(), "ddd MMM dd HH:mm:ss", CultureInfo.InvariantCulture);

        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("DatewisePlaylist", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;

        da.SelectCommand.Parameters.AddWithValue("@stationName", str_station_name);
        da.SelectCommand.Parameters.AddWithValue("@element", type);
        da.SelectCommand.Parameters.AddWithValue("@startNoEarlierThan", startTime);
        da.SelectCommand.Parameters.AddWithValue("@endNoLaterThan", endTime);
        da.SelectCommand.Parameters.AddWithValue("@untitledSuspect", "Don't show");
        da.SelectCommand.Parameters.AddWithValue("@titledUnknown", "Show");
        da.SelectCommand.Parameters.AddWithValue("@subsequentUnknown", "Show");
        da.SelectCommand.Parameters.AddWithValue("@repeatedTitle", "Don't show");
        da.SelectCommand.Parameters.AddWithValue("@OrderByExpression", "StartTime");
        da.SelectCommand.Parameters.AddWithValue("@user", (string)Session["user"]);

        da.SelectCommand.CommandTimeout = 0;
        DataSet ds = new DataSet();
        da.Fill(ds);

        dgResult.Visible = true;

        DataView dv = ds.Tables[0].DefaultView;

        dgResult.DataSource = dv;
        dgResult.DataBind();

        if (ds.Tables[0].Rows.Count > 0)
        {
            //lblError.Visible = false;
            //showHideTypeButtons();
            showHideTypeButtons();
          //  showButtons();
            NoRecordsDiv.Visible = false;
            NoRecordsLiteral.Visible = false;
        }
        else
        {
            dgResult.Visible = false;
            //lblError.Visible = true;
            dgResult.Visible = false;
            ShowGirdContainerButtons.Visible = false;
            NoRecordsDiv.Visible = true;
            NoRecordsLiteral.Visible = true;
            //lblError.Visible = true;
            //lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";

          //  hideButtons();
            //lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;No Record Found";

        }
    }
   
    protected bool isValidUser(string userName)
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("select * from India_Report_New..Users where username = '" + userName + "'", conn);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            return true;
        }
        else
        {
            return false;
        }
        conn.Close();
    }

    public static string DecryptText(String pass)
    {
        return Decrypt(pass, "&amp;%#@?,:*");
    }

    public static string Decrypt(string stringToDecrypt, string sEncryptionKey)
    {
        byte[] key = { };
        byte[] IV = { 10, 20, 30, 40, 50, 60, 70, 80 };
        byte[] inputByteArray = new byte[stringToDecrypt.Length];
        try
        {
            key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            inputByteArray = Convert.FromBase64String(stringToDecrypt);
            //MemoryStream ms = new MemoryStream(); 
            Encoding encoding = Encoding.UTF8;
            return encoding.GetString(inputByteArray);
        }
        catch (System.Exception ex)
        {
            return ("");
        }
    }

    protected void showHideTypeButtons()
    {
        if (Session["strTypes"] != null)
        {
            string strTypes = Session["strTypes"].ToString();
            string[] arrTypes = strTypes.Split(',');
            List<string> Types = new List<string>(arrTypes);

            if (Types.Contains("Song"))
            {
                btn1.Visible = true;
            }
            else
            {
                btn1.Visible = false;
            }

            if (Types.Contains("Spot"))
            {
                btn2.Visible = true;
            }
            else
            {
                btn2.Visible = false;
            }

            if (Types.Contains("Links"))
            {
                btn4.Visible = true;
            }
            else
            {
                btn4.Visible = false;
            }

            if (Types.Contains("All"))
            {
                btn3.Visible = true;
            }
            else
            {
                btn3.Visible = false;
            }
        }
    }
    protected string changeDateFormat(string strDate)
    {
        string str;
        string dd;
        string mm;
        string yyyy;

        dd = strDate.Substring(3, 2);
        mm = strDate.Substring(0, 2);
        yyyy = strDate.Substring(6, 4);
        //str = strDate.Substring(strDate.IndexOf("/") + 1,strDate.Length);
        //mm = str.Substring(1, str.IndexOf("/") - 1);
        //yyyy = str.Substring(str.IndexOf("/") + 1, str.Length);

        strDate = dd + "/" + mm + "/" + yyyy;

        return strDate;
    }

    protected void dgResult_ItemDataBound(object sender, GridItemEventArgs e)
    {

        if (e.Item.ItemType == GridItemType.Header)
        {
            GridHeaderItem headerItem = dgResult.MasterTableView.GetItems(GridItemType.Header)[0] as GridHeaderItem;
            headerItem["Hour"].Visible = false;
            headerItem["Type"].Visible = false;

            Boolean hasClass = true;
            try
            {
                string ss = headerItem["Category"].Text;
            }
            catch
            {
                hasClass = false;
            }

            #region Width
            if (hasClass)
            {
                GridColumn col;
                GridHeaderItem gridHeaderItem = e.Item as GridHeaderItem;
                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[0];
                col.HeaderStyle.Width = Unit.Percentage(15);

                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[1];
                col.HeaderStyle.Width = Unit.Percentage(10);

                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[2];
                col.HeaderStyle.Width = Unit.Percentage(30);

                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[3];
                col.HeaderStyle.Width = Unit.Percentage(30);

                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[4];
                col.HeaderStyle.Width = Unit.Percentage(15);
            }
            else
            {
                GridColumn col;
                GridHeaderItem gridHeaderItem = e.Item as GridHeaderItem;
                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[0];
                col.HeaderStyle.Width = Unit.Percentage(15);

                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[1];
                col.HeaderStyle.Width = Unit.Percentage(10);

                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[2];
                col.HeaderStyle.Width = Unit.Percentage(40);

                col = ((GridItem)(gridHeaderItem)).OwnerTableView.AutoGeneratedColumns[3];
                col.HeaderStyle.Width = Unit.Percentage(35);
            }
            #endregion
        }


        if (e.Item is GridGroupHeaderItem)
        {
            GridGroupHeaderItem item = (GridGroupHeaderItem)e.Item;

            item.DataCell.Text = item.DataCell.Text.Replace(" 13:00:00 PM", " 01:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 14:00:00 PM", " 02:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 15:00:00 PM", " 03:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 16:00:00 PM", " 04:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 17:00:00 PM", " 05:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 18:00:00 PM", " 06:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 19:00:00 PM", " 07:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 20:00:00 PM", " 08:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 21:00:00 PM", " 09:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 22:00:00 PM", " 10:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 23:00:00 PM", " 11:00:00 PM");
            item.DataCell.Text = item.DataCell.Text.Replace(" 00:00:00 AM", " 12:00:00 AM");
        }

        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            GridDataItem dataItem = e.Item as GridDataItem;

            string strAirTime = dataItem["Air Time"].Text;
            if (strAirTime == "*")
            {
                dataItem["Air Time"].Text = "";
                dataItem["Duration (in sec)"].Text = "";
                dataItem.BackColor = System.Drawing.Color.Black;
                dataItem.ForeColor = System.Drawing.Color.White;


                try
                {
                    dataItem["Account"].Text = getHourHeaderFormatted(dataItem["Account"].Text);
                    dataItem["Account"].HorizontalAlign = HorizontalAlign.Center;
                }
                catch
                {
                    try
                    {
                        dataItem["Song Title"].Text = getHourHeaderFormatted(dataItem["Song Title"].Text);
                        dataItem["Song Title"].HorizontalAlign = HorizontalAlign.Center;
                    }
                    catch
                    {
                        try
                        {
                            dataItem["Account/Song Title"].Text = getHourHeaderFormatted(dataItem["Account/Song Title"].Text);
                            dataItem["Account/Song Title"].HorizontalAlign = HorizontalAlign.Center;
                        }
                        catch
                        {

                        }
                    }
                }

            }

            dataItem["Hour"].Visible = false;
            try
            {
                dataItem["Type"].Visible = false;
            }
            catch { }

            string xCol = "#CCCCFF";
            System.Drawing.Color cM = System.Drawing.ColorTranslator.FromHtml(xCol);

            xCol = "#FFCCCC";
            System.Drawing.Color cL = System.Drawing.ColorTranslator.FromHtml(xCol);

            xCol = "#FFFFFF";
            System.Drawing.Color cS = System.Drawing.ColorTranslator.FromHtml(xCol);

            if (dataItem["Type"].Text == "M")
            {
                //dataItem.BackColor = cM;
                dataItem.BackColor = System.Drawing.Color.Aqua;
            }
            if (dataItem["Type"].Text == "L")
            {
                //dataItem.BackColor = cL;
                dataItem.BackColor = System.Drawing.Color.Yellow;
            }
            if (dataItem["Type"].Text == "S")
            {
                //dataItem.BackColor = cS;
                dataItem.BackColor = System.Drawing.Color.LightGreen;
            }
            //dataItem["Air Time"].Width = Unit.Percentage(1);
        }
    }

    protected string getHourHeaderFormatted(string hourHeader)
    {
        string strDateFormat = (string)Session["dateFormat"];
        if (strDateFormat == "DD/MM/YYYY")
        {
            String dt = hourHeader.Substring(0, 10);
            hourHeader = hourHeader.Replace(dt, changeDateFormat(dt));
        }

        hourHeader = hourHeader.Replace("13:00:00 PM", "01:00:00 PM");
        hourHeader = hourHeader.Replace("14:00:00 PM", "02:00:00 PM");
        hourHeader = hourHeader.Replace("15:00:00 PM", "03:00:00 PM");
        hourHeader = hourHeader.Replace("16:00:00 PM", "04:00:00 PM");
        hourHeader = hourHeader.Replace("17:00:00 PM", "05:00:00 PM");
        hourHeader = hourHeader.Replace("18:00:00 PM", "06:00:00 PM");
        hourHeader = hourHeader.Replace("19:00:00 PM", "07:00:00 PM");
        hourHeader = hourHeader.Replace("20:00:00 PM", "08:00:00 PM");
        hourHeader = hourHeader.Replace("21:00:00 PM", "09:00:00 PM");
        hourHeader = hourHeader.Replace("22:00:00 PM", "10:00:00 PM");
        hourHeader = hourHeader.Replace("23:00:00 PM", "11:00:00 PM");
        hourHeader = hourHeader.Replace("24:00:00 AM", "12:00:00 AM");

        return hourHeader;
    }
   
    protected void btn1_onclick(object sender, EventArgs e)
    {
        bindGrid("Song", "");
    }

    protected void btn2_onclick(object sender, EventArgs e)
    {
        bindGrid("Spot", "");
    }
    protected void btn3_onclick(object sender, EventArgs e)
    {
        bindGrid("All", "");
    }
    protected void btn4_onclick(object sender, EventArgs e)
    {
        bindGrid("Links", "");
    }
}
