﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="topAdvertisersByBrandDetails.aspx.cs" Inherits="topAdvertisersByBrandDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Spot Placement Details</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0"/>
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0"/>
		<meta name="vs_defaultClientScript" content="JavaScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<link href="css/report_caption.css" rel="stylesheet" text="text/css"/>
	    <style type="text/css">
            .style1
            {
                font-family: verdana, arial, sans-serif, Helvetica;
                font-size: 11px;
                font-weight: bold;
                color: #595F47;
                width: 17%;
            }
        </style>
        <script language="javascript" type="text/javascript">
            function HideIcons() {
                prn.style.display = 'none';
            }
        </script>
	</head>
	<body leftmargin="0" rightmargin="0" topmargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="53" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="top" align="top" width="760" height="53" border="0"><img src="images/bgTop.gif"></td>
					<td vAlign="top" width="100%"><IMG height="28" src="images/bgTopExt.gif" width="100%" border="0"></td>
				</tr>
			</table>
			<span name="prn" ID="prn">
				<table height="0" cellSpacing="0" cellPadding="0" width="100%" border="0">
					<tr>
						<td width="100%" align="right" colspan="2"><a href="#" onClick="prn.style.display='none';print();"><IMG src="images/icon_print.gif" border="0"></a>&nbsp;&nbsp;</td>
					</tr>
				</table>
			</span>
			<table width="100%">
				<tr>
					<td class="style1">Date Range</td>
					<td class="date" width="1%">:</td>
					<td width="85%"><asp:Label ID="label_Date_Range" Runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr>
					<td class="style1">Brand</td>
					<td class="date" width="1%">:</td>
					<td width="85%"><asp:Label ID="lblBrand" Runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr>
					<td class="style1">Advertiser</td>
					<td class="date" width="1%">:</td>
					<td width="85%"><asp:Label ID="lblAdvertiser" Runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr>
					<td class="style1">Category</td>
					<td class="date" width="1%">:</td>
					<td width="85%"><asp:Label ID="lblCategory" Runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr>
					<td class="style1">Duration</td>
					<td class="date" width="1%">:</td>
					<td width="85%"><asp:Label ID="lblDuration" Runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr>
					<td class="style1">Total No. Of Plays</td>
					<td class="date" width="1%">:</td>
					<td width="85%"><asp:Label ID="lblTotalNoOfPlays" Runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr>
					<td class="style1">Total No. Of Seconds</td>
					<td class="date" width="1%">:</td>
					<td width="85%"><asp:Label ID="lblTotalNoOfSeconds" Runat="server" CssClass="date"></asp:Label></td>
				</tr>											
				<tr>
					<td class="date" width="100%" colspan="3">&nbsp;</td>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<td width="100%">
<br>
						<asp:Panel ID="Panel1" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station1" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station1" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station1" Runat="server" 
                                    onitemdatabound="datagrid_station1_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel><br>
						<asp:Panel ID="Panel2" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station2" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station2" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station2" Runat="server" 
                                    onitemdatabound="datagrid_station2_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>										
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel><br>
						<asp:Panel ID="Panel3" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station3" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station3" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station3" Runat="server" 
                                    onitemdatabound="datagrid_station3_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>	
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel><br>
						<asp:Panel ID="Panel4" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station4" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station4" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station4" Runat="server" 
                                    onitemdatabound="datagrid_station4_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel><br>
						<asp:Panel ID="Panel5" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station5" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station5" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station5" Runat="server" 
                                    onitemdatabound="datagrid_station5_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel><br>
						<asp:Panel ID="Panel6" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station6" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station6" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station6" Runat="server" 
                                    onitemdatabound="datagrid_station6_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel><br>
						<asp:Panel ID="Panel7" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station7" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station7" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station7" Runat="server" 
                                    onitemdatabound="datagrid_station7_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel><br>
						<asp:Panel ID="Panel8" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station8" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station8" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station8" Runat="server" 
                                    onitemdatabound="datagrid_station8_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel><br>
						<asp:Panel ID="Panel9" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station9" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station9" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station9" Runat="server" 
                                    onitemdatabound="datagrid_station9_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel><br>
						<asp:Panel ID="Panel10" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station10" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station10" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station10" Runat="server" 
                                    onitemdatabound="datagrid_station10_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel><br>
						<asp:Panel ID="Panel11" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station11" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station11" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station11" Runat="server" 
                                    onitemdatabound="datagrid_station11_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel>
						<br>
						<asp:Panel ID="Panel12" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station12" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station12" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station12" Runat="server" 
                                    onitemdatabound="datagrid_station12_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel>
						<br>
						<asp:Panel ID="Panel13" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station13" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station13" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station13" Runat="server" 
                                    onitemdatabound="datagrid_station13_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel>
						<br>
						<asp:Panel ID="Panel14" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station14" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station14" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station14" Runat="server" 
                                    onitemdatabound="datagrid_station14_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel>
						<br>
						<asp:Panel ID="Panel15" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station15" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station15" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station15" Runat="server" 
                                    onitemdatabound="datagrid_station15_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel>
						<br>
						<asp:Panel ID="Panel16" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station16" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station16" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station16" Runat="server" 
                                    onitemdatabound="datagrid_station16_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel>
						<br>
						<asp:Panel ID="Panel17" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station17" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station17" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station17" Runat="server" 
                                    onitemdatabound="datagrid_station17_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel>
						<br>
						<asp:Panel ID="Panel18" Runat="server" Visible="False">
							<table>
								<TR>
									<TD>
										<asp:label id="label_station18" Runat="server" CssClass="date"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="label_no_station18" Runat="server" CssClass="date" Visible="False" BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:label>
										<asp:datagrid id="datagrid_station18" Runat="server" 
                                    onitemdatabound="datagrid_station18_ItemDataBound" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market"></asp:BoundColumn>
                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market"></asp:BoundColumn>
                                        </Columns>
										</asp:datagrid></TD>
								</TR>
							</table>
						</asp:Panel>
																																										
					</td>
				</tr>
				<tr>
					<td width="100%">&nbsp;</td>
				</tr>
			</table>
		</form>
	</body>
</html>