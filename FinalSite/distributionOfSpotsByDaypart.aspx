﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="distributionOfSpotsByDaypart.aspx.cs" Inherits="distributionOfSpotsByDaypart"
    Title="Distribution Of Spots By Day Part" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<script type="text/javascript" language="javascript">
        function OnClientDropDownOpenedHandler1(sender, eventArgs) {
            var tree = sender.get_items().getItem(0).findControl("radTreeStations");

            if (!!tree) {
                var selectedNode = tree.get_selectedNode();
                if (selectedNode) {
                    selectedNode.scrollIntoView();
                }
            }
        }
        function nodeCheckedTreeStations(sender, args) {

            var comboBox = $find("<%= CmbTreeStations.ClientID %>");
        var tree = comboBox.get_items().getItem(0).findControl("radTreeStations");
            var node = args.get_node();
            var rootNodeSelCount = 0;
            var nodesJ=tree.get_nodes(); 
            var marketsval="";
              for (i=0; i<nodesJ.get_count(); i++) 
               {                
                   if (nodesJ.getNode(i).get_checked()) 
                   { 
                       //alert(nodesJ.getNode(i).get_text());
                       marketsval = marketsval + nodesJ.getNode(i).get_text().toString() + ",";
                       rootNodeSelCount =  rootNodeSelCount + 1;
                   } 
               }
               
               if(rootNodeSelCount > 1)
               {
                   alert('Cannot select more than 1 markets');
                   node.uncheck();
                   return false;
               }
 
            var tempNode = args.get_node();
            if (tempNode.get_text().toString() == "(Select All)") 
            {
                // check or uncheck all the nodes
            } else {
                var nodes = new Array();
                nodes = sender.get_checkedNodes();
                var vals = "";
                var i = 0;
                var icount = 0;

                for (i = 0; i < nodes.length; i++) {
                    var n = nodes[i];
                    var nodeText = n.get_text().toString();
                    if (nodeText != "(Select All)") {
                        if (n._hasChildren() == true) {
                        } else {
                            vals = vals + n.get_text().toString() + ",";
                        }
                    }
                }
             
                comboBox.set_text(marketsval);
            }
        }
        function OnClientClicked(button, args) {
            if (calcDays()) {
                button.set_autoPostBack(true);
            }
            else {
                button.set_autoPostBack(false);
            }
        }
        function calcDays()
        {
            ToggleCollapsePane();
            var startDate =$find("<%=dtStart.ClientID %>");
            var endDate =  $find("<%=dtEnd.ClientID %>");
            
            var sDate = startDate.get_selectedDate();
            var eDate = endDate.get_selectedDate();
            
             var lockDataBefore="<%=Session["lockDataBefore"]%>";
		    var lockDataAfter="<%=Session["lockDataAfter"]%>";
            if(sDate == null)
//            if(sDate == null || eDate == null)
            {
                lockDataBefore = new Date(lockDataBefore);
                var curr_date = lockDataBefore.getDate();
                var curr_month = lockDataBefore.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataBefore.getFullYear();
                lockDataBefore= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights only from " + lockDataBefore);
                return false;
            }
            
            if(eDate == null)
            {
                lockDataAfter = new Date(lockDataAfter);
                var curr_date = lockDataAfter.getDate();
                var curr_month = lockDataAfter.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataAfter.getFullYear();
                lockDataAfter= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights till " + lockDataAfter);
                return false;
            }
        
            if(days_between(sDate,eDate) > 30)
            {
                alert("Maximum allowed date range is 1 month.");
                return false;
            }
                    
            //tdhide('leftMenu', 'ctl00_showHide','hide');
            
            var comboBox = $find("<%= CmbTreeStations.ClientID %>");    
            var treeS = comboBox.get_items().getItem(0).findControl("radTreeStations");
            
            var rootNodesS = treeS.SelectedNode;
            var nodesS = treeS.get_allNodes();

            var iS;
            var isStationSelected = 0;
             for(iS=0; iS<nodesS.length; iS++)
             {
                if(!nodesS[iS].get_checked())
                {
                
                }
                else
                {
                    isStationSelected = 1;
                }
             }   

            if (isStationSelected == 0 && valSelectBy == "Station")
            {
                alert("You have not selected any station/s");
                return false;
            }
            else
            {
                    disp_clock();
                    return true;
            }         
        }  
		function disp_clock()
		{
		    var lblError = document.getElementById("ctl00_content1_lblError");
		    if (lblError != null)
		    {
		        lblError.style.display='none';
		    }
			OnLoad();
			DIV1.style.display='none';
			divWait.style.display='';
    	}
    	function OnLoad() 
		{ 
		    setTimeout("StartAnimation()", 500); 
		} 

		function StartAnimation() 
		{ 
		    if (document.images)
		    {
		        document['ProgressBarImg'].src = "images/aniClock.gif";
		    }
		}
		function ToggleCollapsePane() {               
               var splitter = $find("<%= Page.Master.FindControl("RadSplitter1").ClientID %>");
               var pane = splitter.getPaneById("<%= Page.Master.FindControl("RadPane1").ClientID %>");
                pane.collapse();
    }
        function openWindow()
        {
                var win = window.open("defineDaypart.aspx",'win','height=370,width=430,left=200,top=250;resizable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
                return false    
        }
    
        function disp_chart() 
        {
            window.open("DayPartsWiseDistributionOfSpots_Charts.aspx",'','height=600,width=1400,resizable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
            return false
        } 
        function OnbtnDefineDayPartClicked()
        {
            openWindow();
        }
        function days_between(date1, date2) {

            // The number of milliseconds in one day
            var ONE_DAY = 1000 * 60 * 60 * 24

            // Convert both dates to milliseconds
            var date1_ms = date1.getTime()
            var date2_ms = date2.getTime()

            // Calculate the difference in milliseconds
            var difference_ms = Math.abs(date1_ms - date2_ms)
            
            // Convert back to days and return
            return Math.round(difference_ms/ONE_DAY)

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div style="width: 99.7%; height: 100%; margin-left: 4px">
        <div>
            <table width="100%" background="images/contentheader_bgr.gif" border="0" cellpadding="6px"
                cellspacing="0px" style="margin-left: -5px; margin-right: 300px;">
                <tr>
                    <td align="left">
                        ::
                        <asp:Label ID="Pg_Title" runat="server" Font-Bold="True" ForeColor="#000099"></asp:Label>&nbsp;::
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        <asp:Label ID="lbl_Logout" runat="server" Text="Welcome: Administrator[Logout]" Font-Bold="True"
                            ForeColor="#000099"></asp:Label>
                    </td>
                </tr>
            </table>
    <table >
        <tr>
            <td class="style1">
                &nbsp;Start Date
            </td>
            <td class="style1">
                &nbsp;End Date
            </td>
            <td class="style1">
                &nbsp;Select Market/Station
            </td>
            <td class="style1">
            </td>
            <td class="style1">
            </td>
            <td class="style1">
                &nbsp;&nbsp;
            </td>
            <td class="style1">
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td class="style1">
                &nbsp;<telerik:RadDatePicker ID="dtStart" runat="server" Culture="English (United States)"
                        MinDate="2004-01-01">
                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                        </Calendar>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
            </td>
            <td class="style1">
                <telerik:RadDatePicker ID="dtEnd" runat="server" Culture="English (United States)"
                        MinDate="2004-01-01">
                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                        </Calendar>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
                <asp:CompareValidator Display="Dynamic" ID="dateCompareValidator" runat="server"
                    ControlToValidate="dtEnd" ControlToCompare="dtStart" Operator="GreaterThanEqual"
                    Type="Date" ErrorMessage="The End date must be after the Start date." Style="font-size: x-small"></asp:CompareValidator>
            </td>
            <td class="style1">
                <telerik:RadComboBox ID="CmbTreeStations" runat="server" AfterClientCheck="AfterCheckHandler"
                        CollapseAnimation-Type="None" EmptyMessage="Choose Stations" ExpandAnimation-Type="None"
                        OnClientDropDownOpened="OnClientDropDownOpenedHandler1" ShowToggleImage="True"
                        Style="vertical-align: middle;" Width="200px">
                        <ItemTemplate>
                            <div id="div2">
                                <telerik:RadTreeView ID="radTreeStations" runat="server" Height="200px" OnClientNodeChecked="nodeCheckedTreeStations"
                                    CheckBoxes="true" Width="100%"
                                    TriStateCheckBoxes="true">
                                </telerik:RadTreeView>
                            </div>
                        </ItemTemplate>
                        <Items>
                            <telerik:RadComboBoxItem Text="" />
                        </Items>
                    </telerik:RadComboBox>
            </td>
            <td class="style1">
                <telerik:RadButton ID="btnDefineDaypart" runat="server" Text="Customize Daypart" OnClientClicked="OnbtnDefineDayPartClicked">
                </telerik:RadButton>
            </td>
            <td class="style1">
                <asp:RadioButtonList RepeatDirection="Horizontal" ID="radioWeek" runat="server" Style="font-family: Verdana">
                    <asp:ListItem Value="all" Text="All days" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="weekday" Text="Weekday"></asp:ListItem>
                    <asp:ListItem Value="weekend" Text="Weekend"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td class="style1">
                &nbsp;&nbsp;
                <telerik:RadButton ID="btnGenerateReport" runat="server" Text="Generate Report" OnClientClicked="OnClientClicked"
                        OnClick="btnGenerateReport_Click">
                    </telerik:RadButton>
            </td>
            <td class="style1">
                &nbsp;<a href="" onclick="javascript: return disp_chart();"><img style="border: 0"
                    alt="View pie chart" src="images/newImages/bar_graph.png" visible="false" runat="server"
                    id="imgChart" /></a>
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlDate" runat="server" Visible="true" Width="100%">
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 100%">
                    <div id="divWait" style="display: none">
                        <table style="height: 310Px" width="100%">
                            <tr>
                                <td align="center" width="100%">
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <img src="images/aniClock.gif" border="0" name="ProgressBarImg"><br>
                                    <font class="date">Acquiring Data... one moment please!</font>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="DIV1">
                        <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label>
                        <asp:GridView UseAccessibleHeader="true" EnableViewState="false" ID="gvData" ShowFooter="true"
                            runat="server" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                            Height="200px">
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="litExportTo" runat="server" Text="Export to: "></asp:Literal>
                    <telerik:RadButton ID="btnExportToExcel" runat="server" Visible="false" CausesValidation="false"
                            Text="Excel" OnClick="img_btn_export_to_excel_Click">
                        </telerik:RadButton>
                </td>
            </tr>
        </table>
        <asp:TextBox ID="txtArray" runat="server" Style="visibility: hidden; height: 0Px"></asp:TextBox>
    </asp:Panel>
    </div>
    </div>
</asp:Content>
