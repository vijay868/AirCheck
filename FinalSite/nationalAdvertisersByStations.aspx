﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="nationalAdvertisersByStations.aspx.cs" Inherits="nationalAdvertisersByStations"
    Title="National Advertisers By Stations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
       
        .style5
        {
            font-family: Helvetica;
            font-size: 14px;
            color: grey;
        }
        .style6
        {
            font-family: Helvetica;
            font-size: 14px;
            color: Grey;
            width: 153px;
        }
        .style7
        {
            width: 27px;
        }
    </style>

    <script type="text/javascript" language="javascript">
function ToggleCollapsePane() {               
               var splitter = $find("<%= Page.Master.FindControl("RadSplitter1").ClientID %>");
               var pane = splitter.getPaneById("<%= Page.Master.FindControl("RadPane1").ClientID %>");
                pane.collapse();
    }
        
		function disp_clock()
		{
		    var lblError = document.getElementById("ctl00_content1_lblError");
		    if (lblError != null)
		    {
		        lblError.style.display='none';
		    }
			OnLoad();
			DIV1.style.display='none';
			divWait.style.display='';
    	}

		function OnLoad() 
		{ 
		    setTimeout("StartAnimation()", 500);
		    
		} 

		function StartAnimation() 
		{ 
		    if (document.images)
		    {
		        document['ProgressBarImg'].src = "images/aniClock.gif";
		    }
		}
	
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div style="width: 99.1%; height: 100%; margin-left: 4px">
        <div>
            <table width="100%" background="images/contentheader_bgr.gif" border="0" cellpadding="6px"
                cellspacing="0px" style="margin-left: -5px; margin-right: 300px;">
                <tr>
                    <td align="left">
                        ::
                        <asp:Label ID="Pg_Title" runat="server" Font-Bold="True" ForeColor="#000099"></asp:Label>&nbsp;::
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        <asp:Label ID="lbl_Logout" runat="server" Text="Welcome: Administrator[Logout]" Font-Bold="True"
                            ForeColor="#000099"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            &nbsp;&nbsp;<a href="#" onclick="history.go(-1);return false">&lt;&lt;&nbsp;Back</a>|
            <a href="nationalAdvertisers.aspx">Go Back to Main Report Page</a>
            <br />
            <br />
            <table style="width: 60%">
                <tr>
                    <td class="style6">
                        Brand Name
                    </td>
                    <td class="style7">
                        :
                    </td>
                    <td class="date">
                        <asp:Label ID="lbl_brand" runat="server" CssClass="style5"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        Advertiser
                    </td>
                    <td class="style7">
                        :
                    </td>
                    <td class="date">
                        <asp:Label ID="lbl_advt" runat="server" CssClass="style5"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        Category
                    </td>
                    <td class="style7">
                        :
                    </td>
                    <td class="date">
                        <asp:Label ID="lbl_category" runat="server" CssClass="style5"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        Date Range
                    </td>
                    <td class="style7">
                        :
                    </td>
                    <td class="date">
                        <asp:Label ID="lbl_daterange" runat="server" CssClass="style5"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td>
                        <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label>
                        <div id="DIV1" style="width: 100%">
                            <telerik:RadGrid ID="radData" runat="server" Visible="False" Width="55%"
                                GridLines="Vertical" 
                                AllowSorting="false" OnSortCommand="radData_SortCommand" OnItemDataBound="radData_ItemDataBound" 
                                Font-Names="Helvetica" CellSpacing="0">
                                <MasterTableView Width="100%" GridLines="Both">
                                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                    </ExpandCollapseColumn>
                                    <EditFormSettings>
                                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                        </EditColumn>
                                    </EditFormSettings>
                                </MasterTableView>                                
                                <AlternatingItemStyle Font-Bold="true" />
                                <ItemStyle  HorizontalAlign="Center" Font-Names="Helvetica" Font-Size="12pt" />
                                <HeaderStyle HorizontalAlign="Center" Font-Names="Helvetica" Font-Size="12pt" />
                                <FilterMenu EnableImageSprites="False">
                                </FilterMenu>
                            </telerik:RadGrid>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <asp:Literal ID="litExportTo" runat="server" Text="Export to: "></asp:Literal>
                        <telerik:RadButton ID="btnExportToExcel" runat="server" CausesValidation="False"
                            Text="Excel" OnClick="img_btn_export_to_excel_Click">
                        </telerik:RadButton>
                        <telerik:RadButton ID="btnExportToPdf" runat="server" CausesValidation="False" Text="Pdf"
                            OnClick="btnExportToPdf_Click">
                        </telerik:RadButton>
                        <br />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
