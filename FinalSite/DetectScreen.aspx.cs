﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DetectScreen : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["XVal"] != null)
        {
            Session["SWidth"] = Request.QueryString["XVal"].ToString();
            Session["MWidth"] =  (int.Parse(Request.QueryString["XVal"]) - 263) + "px";
            Session["SHeight"] = Request.QueryString["YVal"].ToString();
            Response.Redirect("WelcomePage.aspx");
        }
        
    }
}
