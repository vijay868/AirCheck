<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ListOfCurrentSongsRepetition.aspx.cs" Inherits="ListOfCurrentSongsRepetition" %>
<%@ Register assembly="IdeaSparx.CoolControls.Web" namespace="IdeaSparx.CoolControls.Web" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
      
        .gvHeaderRow
        {
            background-image: url("images/dhrudde.png");
            font-family: Helvetica;
            font-size: 12px;
            color: White;
            text-decoration: none;
            line-height: 20px;
            padding-left: 10px;
            border-left: solid 1px white;
            border-right: solid 1px white;
            border-bottom: solid 1px white;

        }
        
        .gvHeadersub
        {
            font-family: Helvetica;
            font-size: 9px;
            text-decoration: none;
            line-height: 13px;
        }
        
    </style>

   
    
    <script type="text/javascript">
     function ToggleCollapsePane() {
               
               var splitter = $find("<%= Page.Master.FindControl("RadSplitter1").ClientID %>");
               var pane = splitter.getPaneById("<%= Page.Master.FindControl("RadPane1").ClientID %>");
                pane.collapse();
//               if (!pane) return;
// 
//               if (pane.get_collapsed()) {
//                    pane.expand();
//               }
//               else {
//                    pane.collapse();
//               }
    }
    
        function StopPropagation(e) {
            if (!e) {
                e = window.event;
            }

            e.cancelBubble = true;
        }

        function OnClientDropDownOpenedHandler(sender, eventArgs) {
            var tree = sender.get_items().getItem(0).findControl("radTreeCities");
            var tree1 = sender.get_items().getItem(0).findControl("radStationCompare");

            if (!!tree) {
                var selectedNode = tree.get_selectedNode();
                if (selectedNode) {
                    selectedNode.scrollIntoView();
                }
            }

            if (!!tree1) {
                var selectedNode1 = tree1.get_selectedNode();
                if (selectedNode1) {
                    selectedNode1.scrollIntoView();
                }
            }
        }
        function disp_details(startDate, endDate, strSongName, strArtistName, strStationName) {
            win = window.open("ListOfCurrentSongsRepetition_details.aspx?startDate=" + startDate + "&endDate=" + endDate + "&strSongName=" + strSongName + "&strArtistName=" + strArtistName + "&strStationName=" + strStationName, "win", "width=800,height=400,left=0, scrollbars=yes, resizable=yes");
            return false
        }
        function disp_exclusiveReport(startDate, endDate, strStationName, strStationIDs) {
            win = window.open("ListOfCurrentSongsRepetition_exclusive.aspx?startDate=" + startDate + "&endDate=" + endDate + "&strStationName=" + strStationName + "&strStationIDs=" + strStationIDs, "win", "width=800,height=400,left=0, scrollbars=yes, resizable=yes");
            return false
        }

        function nodeChecked(sender, args) {
            var comboBox = $find("<%= txtMarketSelect.ClientID %>");            
            document.getElementById('<%=txtMarketSelectHidden.ClientID%>').value = "";
            var tree = comboBox.get_items().getItem(0).findControl("radTreeCities");
            var node = args.get_node();
            var rootNodeSelCount = 0;
            var nodesJ=tree.get_nodes(); 
            var marketsval="";
              for (i=0; i<nodesJ.get_count(); i++) 
               {                
                   if (nodesJ.getNode(i).get_checked()) 
                   { 
                       //alert(nodesJ.getNode(i).get_text());
                       marketsval = marketsval + nodesJ.getNode(i).get_text().toString() + ",";
                       rootNodeSelCount =  rootNodeSelCount + 1;
                   } 
               }
               
               if(rootNodeSelCount > 3)
               {
                   alert('Cannot select more than 3 markets');
                   node.uncheck();
                   return false;
               }
 
            var tempNode = args.get_node();
            if (tempNode.get_text().toString() == "(Select All)") 
            {
                // check or uncheck all the nodes
            } else {
                var nodes = new Array();
                nodes = sender.get_checkedNodes();
                var vals = "";
                var i = 0;
                var icount = 0;

                for (i = 0; i < nodes.length; i++) {
                    var n = nodes[i];
                    var nodeText = n.get_text().toString();
                    if (nodeText != "(Select All)") {
                        if (n._hasChildren() == true) {
                        } else {
                            vals = vals + n.get_text().toString() + ",";
                        }
                    }
                }
                document.getElementById('<%=txtMarketSelectHidden.ClientID%>').value = vals;
                comboBox.set_text(marketsval);
            }
        }
        function OnClientClicked(button, args) {
           if(calcDays())
           {                       
                button.set_autoPostBack(true); 
           }                
            else
            {                              
                button.set_autoPostBack(false);                        
            }
        }
        
        
      function OnKeyPress(sender, eventArgs) 
        { 
            var c = eventArgs.get_keyCode(); 
            if (c == 13) 
            { 
               
//               if(calcDays())
//                {                       
//                    button.set_autoPostBack(true); 
//                }                
//                else
//                {                              
//                    button.set_autoPostBack(false);                        
//                }
            }else{
               
            }        
        } 
        
        
        function disp_clock() {
            var lblError = document.getElementById("ctl00_content1_lblError");
            if (lblError != null) {
                lblError.style.display = 'none';
            }
            OnLoad();
            DIV1.style.display = 'none';
            divWait.style.display = '';
        }

        function OnLoad() {
            setTimeout("StartAnimation()", 500);
        }

        function StartAnimation() {
            if (document.images) {
                document['ProgressBarImg'].src = "images/aniClock.gif";
            }
        }
        function ShowExportDiv() {
            document.getElementById("divExport").style.display = "block";
        }
        function calcDays()
        {
             ToggleCollapsePane();
            var startDate =$find("<%=dtStart.ClientID %>");
            var endDate =  $find("<%=dtEnd.ClientID %>");
            
            var sDate = startDate.get_selectedDate();
            var eDate = endDate.get_selectedDate();
            
             var lockDataBefore="<%=Session["lockDataBefore"]%>";
		    var lockDataAfter="<%=Session["lockDataAfter"]%>";
            if(sDate == null)
            {
                lockDataBefore = new Date(lockDataBefore);
                var curr_date = lockDataBefore.getDate();
                var curr_month = lockDataBefore.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataBefore.getFullYear();
                lockDataBefore= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights only from " + lockDataBefore);
                return false;
            }
            
            if(eDate == null)
            {
                lockDataAfter = new Date(lockDataAfter);
                var curr_date = lockDataAfter.getDate();
                var curr_month = lockDataAfter.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataAfter.getFullYear();
                lockDataAfter= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights till " + lockDataAfter);
                return false;
            }
        
            if(days_between(sDate,eDate) > 30)
            {
                alert("Maximum allowed date range is 1 month.");
                return false;
            }      
            

          

           
            var isMarketSelected = 0;  
            
            //var tree = sender.get_items().getItem(0).findControl("radTreeMarkets");
            
            var comboBox = $find("<%= txtMarketSelect.ClientID %>");    
            var tree = comboBox.get_items().getItem(0).findControl("radTreeCities");
            
            var rootNodes = tree.SelectedNode;
            var nodes = tree.get_allNodes(); 
            
            var i;
            
             for(i=0; i<nodes.length; i++)
             {                               
                if(!nodes[i].get_checked())
                {
                    
                }
                else
                {
                    isMarketSelected = 1;
                }
             }             
            if (isMarketSelected == 0)
            {
                alert("You have not selected any  Market/s");
                return false;
            }            

           
          //  tdhide('leftMenu', 'ctl00_showHide','hide');
                        
            disp_clock();
            return true;

                
        } 
        function days_between(date1, date2) {

            // The number of milliseconds in one day
            var ONE_DAY = 1000 * 60 * 60 * 24

            // Convert both dates to milliseconds
            var date1_ms = date1.getTime()
            var date2_ms = date2.getTime()

            // Calculate the difference in milliseconds
            var difference_ms = Math.abs(date1_ms - date2_ms)
            
            // Convert back to days and return
            return Math.round(difference_ms/ONE_DAY)

        }
		
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div style="width: 99.7%; height: 100%; margin-left:4px">
        <div>
            <table width="100%" background="images/contentheader_bgr.gif" border="0" cellpadding="6px" cellspacing="0px" style="margin-left:-5px; margin-right:300px;">
                <tr>
                    
                    <td align="left">::
                        <asp:Label ID="Pg_Title" runat="server" Font-Bold="True" ForeColor="#000099"></asp:Label>&nbsp;::
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        <asp:Label ID="lbl_Logout" runat="server" Text="Welcome: Administrator[Logout]" 
                            Font-Bold="True" ForeColor="#000099"></asp:Label>
                    </td>
                </tr>
            </table>
        <table>
            <tr>
                <td class="style1">
                    Start Date:
                </td>
                <td class="style2">
                    End Date:
                </td>
                <td class="style3">
                    &nbsp;
                </td>
                <td>
                    Select Station:
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="style1" style="margin-left: 40px">
                    <telerik:RadDatePicker ID="dtStart" runat="server" Culture="English (United States)"
                        MinDate="2004-01-01">
                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                        </Calendar>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
                </td>
                <td class="style2">
                    <telerik:RadDatePicker ID="dtEnd" runat="server" Culture="English (United States)"
                        MinDate="2004-01-01">
                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                        </Calendar>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
                </td>
                <td class="style3">
                </td>
                <td style="margin-left: 40px">
                    <telerik:RadComboBox ID="txtMarketSelect" runat="server" AfterClientCheck="AfterCheckHandler"
                        CollapseAnimation-Type="None" EmptyMessage="Choose Markets/Stations" ExpandAnimation-Type="None"
                        OnClientDropDownOpened="OnClientDropDownOpenedHandler" ShowToggleImage="True"
                        Style="vertical-align: middle;" Width="200px">
                        <ItemTemplate>
                            <div id="div2">
                                <telerik:RadTreeView ID="radTreeCities" runat="server" Height="200px" OnClientNodeChecked="nodeChecked"
                                    CheckBoxes="true" Width="100%" TriStateCheckBoxes="true">
                                </telerik:RadTreeView>
                            </div>
                        </ItemTemplate>
                        <Items>
                            <telerik:RadComboBoxItem Text="" />
                        </Items>
                    </telerik:RadComboBox>
                     <asp:HiddenField ID="txtMarketSelectHidden" runat="server" />
                </td>
                <td>
                    <telerik:RadTextBox ID="txtSearch" runat="server">
                        <ClientEvents OnKeyPress="OnKeyPress" />
                    </telerik:RadTextBox>
                   
                </td>
                <td>
                    <telerik:RadButton ID="btnGo" runat="server" Skin="Web20" Text="GO" ToolTip="Go"
                        OnClick="btnGo_Click" OnClientClicked="OnClientClicked">
                        <Icon PrimaryIconCssClass="rbSearch" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
                    </telerik:RadButton>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            </table>
        <div id="divWait" style="display: none">
            <table style="height: 310Px" width="100%">
                <tr>
                    <td align="center" width="100%">
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <img src="images/aniClock.gif" border="0" name="ProgressBarImg" alt=""><br />
                        <font class="date">Acquiring Data... one moment please!</font>
                    </td>
                </tr>
            </table>
        </div>
        <div id="DIV1">
             <cc1:CoolGridView UseAccessibleHeader="true" EnableViewState="false" ID="gvData" runat="server" Height="420px"
                        FixHeaders="True" ShowFooter="True" width="100%"
        OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" 
        BorderStyle="None" BorderWidth="0px" AutoGenerateColumns="true" ColumnNosWidth="300px" FixedColumnsNos="2">
    <HeaderStyle CssClass="gvHeaderRow" />
    <RowStyle CssClass="gvHeadersub" />
<BoundaryStyle BorderColor="Gray" BorderWidth="0px" BorderStyle="None"></BoundaryStyle>

                        </cc1:CoolGridView>
           
             </br>
        </div>
        <div id="divExport" style="display: none;">
            <asp:Literal ID="litExportTo" runat="server" Text="Export to: " Visible="false"></asp:Literal>
            <telerik:RadButton ID="btnExportToExcel" runat="server" Text="Excel" OnClick="img_btn_export_to_excel_Click"
                Visible="false">
            </telerik:RadButton>
        </div>
    </div>
   
</asp:Content>
