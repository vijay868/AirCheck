﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using Telerik.Web.UI;

public partial class SpotPlacement : System.Web.UI.Page
{
    bool isPdfExport = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder onloadScript = new StringBuilder();
        //onloadScript.Append("<script type='text/javascript'>");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("ShowReport();");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("</script>");
        //this.ClientScript.RegisterStartupScript(this.GetType(), "onLoadCall", onloadScript.ToString());

        //((Label)this.Master.FindControl("lblReportName")).Text = "Spot Placement";

        //Literal lit = new Literal();
        //lit.Text = "<script language='javascript'>tdhide('leftMenu', 'ctl00_showhide', 'hide');</script>";
        //Page.Controls.Add(lit);

        if (!IsPostBack)
        {
            getUserPreferencesForCurrentUser();
        }

        formatsearchGrid();

        getDateFormat();
        string strDateFormat = (string)Session["dateFormat"];

        if (strDateFormat == "DD/MM/YYYY")
        {
            dtStart.DateInput.DateFormat = "dd/MM/yyyy";
            dtStart.DateInput.DisplayDateFormat = "dd/MM/yyyy";

            dtEnd.DateInput.DateFormat = "dd/MM/yyyy";
            dtEnd.DateInput.DisplayDateFormat = "dd/MM/yyyy";
        }


        if (!IsPostBack)
        {
            Session["ds"] = null;

            setDate();
            //getStations(ddStations);
            //getStations(ddStationsToCompare);
            GenerateTreeView();
            if (Session["dataReadOnly"].ToString() == "True")
            {
                btnExportToExcel.Enabled = false;
                btnExportToPdf.Enabled = false;
            }
            else
            {
                btnExportToExcel.Enabled = true;
                btnExportToPdf.Enabled = true;
            }
        }
        Pg_Title.Text = "Spot Placement";
    }

    protected void getUserPreferencesForCurrentUser()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getUserPreferenceForAUserAndReport", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@user", (string)Session["user"]);
        da.SelectCommand.Parameters.AddWithValue("@reportName", "Spot Placement");

        DataSet ds = new DataSet();
        da.Fill(ds);

        Session["headerFontName"] = ds.Tables[0].Rows[0]["headerFontName"].ToString();
        Session["headerFontSize"] = ds.Tables[0].Rows[0]["headerFontSize"].ToString();
        Session["headerFontColor"] = ds.Tables[0].Rows[0]["headerFontColor"].ToString();
        Session["headerRowColor"] = ds.Tables[0].Rows[0]["headerRowColor"].ToString();
        Session["headerFontStyle"] = ds.Tables[0].Rows[0]["headerFontStyle"].ToString();
        Session["dataFontName"] = ds.Tables[0].Rows[0]["dataFontName"].ToString();
        Session["dataFontSize"] = ds.Tables[0].Rows[0]["dataFontSize"].ToString();
        Session["dataFontColor"] = ds.Tables[0].Rows[0]["dataFontColor"].ToString();
        Session["alternateRowColor"] = ds.Tables[0].Rows[0]["alternateRowColor"].ToString();
        Session["dataFontStyle"] = ds.Tables[0].Rows[0]["dataFontStyle"].ToString();

    }


    protected void GenerateTreeView()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getStationsForUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);

        var txtStationsTree = (RadTreeView)txtStationSelect.Items[0].FindControl("radStationSelect");
        txtStationsTree.DataFieldID = "name";
        txtStationsTree.DataFieldParentID = "m_name";
        txtStationsTree.DataTextField = "name";

        txtStationsTree.DataSource = ds.Tables[0];
        txtStationsTree.DataBind();

    }

    protected void setDate()
    {
        DateTime lockDataBefore = DateTime.Parse(Session["lockDataBefore"].ToString());
        dtStart.MinDate = lockDataBefore;
        dtEnd.MinDate = lockDataBefore;

        DateTime lockDataAfter = DateTime.Parse(Session["lockDataAfter"].ToString());
        dtStart.MaxDate = lockDataAfter;
        dtEnd.MaxDate = lockDataAfter;

        dtStart.SelectedDate = System.DateTime.Now;
        dtEnd.SelectedDate = System.DateTime.Now;
    }

    protected void getDateFormat()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("select * from India_Report_New..Country_dateFormat where isSelected=1", conn);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                Session["dateFormat"] = dr["DateFormat"].ToString();
            }
        }

        conn.Close();
    }

    protected void getStations(DropDownList dd)
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);

        SqlCommand comm = default(SqlCommand);
        conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);

        comm = new SqlCommand("getOnlyStationsForUser", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@userName", (string)Session["user"]);
        conn.Open();
        SqlDataReader dr = comm.ExecuteReader();
        dd.DataSource = dr;
        dd.DataTextField = "s_name";
        dd.DataValueField = "station_id";
        dd.DataBind();
        dr.Close();
        conn.Close();
    }

    protected int getStationID(string strStation)
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);

        SqlCommand comm = default(SqlCommand);
        conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);

        comm = new SqlCommand("getStationIDOfStation", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@stationName", strStation);
        conn.Open();
        SqlDataReader dr = comm.ExecuteReader();
        int strStartionID = 0;
        while (dr.Read())
        {
            strStartionID = int.Parse(dr[0].ToString());
        }
        dr.Close();
        conn.Close();
        return strStartionID;
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        int stationID = getStationID(txtStationSelect.Text.Trim());

        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());

        addAuditLog(dtStartDate, dtEndDate, stationID.ToString(), false, false, false);

        isPdfExport = false;
        //bindGrid();
        bindSearchGrid();
    }

    protected void addAuditLog(DateTime startDate, DateTime endDate, string station,
Boolean exportedToExcel, Boolean exportedToPdf, Boolean exportedToText)
    {
        auditTrail at = new auditTrail();
        at.connectionString = ConfigurationSettings.AppSettings["connectionString"].ToString();
        at.userID = (int)Session["userID"];
        at.IPAddress = Request.ServerVariables["REMOTE_ADDR"];
        at.pageViewed = "Spot Placement";
        at.startDate = startDate;
        at.endDate = endDate;
        at.station = station;
        at.exportedToExcel = exportedToExcel;
        at.exportedToPdf = exportedToPdf;
        at.exportedToText = exportedToText;
        at.addUserActivityLogPassingStationIDs();
        at = null;
    }

    protected void bindSearchGrid()
    {
        pnlSearchedCriteria.Visible = true;
        pnlback.Visible = false;

        int stationID = getStationID(txtStationSelect.Text.Trim());

        if (stationID == 0)
        {
            pnlText.Visible = false;
            gvSearch.Visible = false;
            lblNoSearchFound.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
            return;
        }
        pnlText.Visible = true;
        lblNoSearchFound.Text = "";
        lblNoSearchFound.Visible = false;

        gvSearch.Visible = true;
        radData.Visible = false;

        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());


        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("TopAndTailSearch", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@stationID", stationID);
        da.SelectCommand.Parameters.AddWithValue("@element", "spot");
        da.SelectCommand.Parameters.AddWithValue("@startNoEarlierThan", dtStartDate.ToString());
        da.SelectCommand.Parameters.AddWithValue("@endNoLaterThan", dtEndDate.ToString());
        da.SelectCommand.Parameters.AddWithValue("@untitledSuspect", "Don't show");
        da.SelectCommand.Parameters.AddWithValue("@titledUnknown", "Show");
        da.SelectCommand.Parameters.AddWithValue("@subsequentUnknown", "Show");
        da.SelectCommand.Parameters.AddWithValue("@repeatedTitle", "Don't show");
        da.SelectCommand.Parameters.AddWithValue("@OrderByExpression", "StartTime");
        da.SelectCommand.Parameters.AddWithValue("@SearchStr", txtSearchText.Text.Trim().Replace("'", "''"));

        DataSet ds = new DataSet();
        da.Fill(ds);

        if (ds.Tables[0].Rows.Count == 0)
        {
            gvSearch.DataSource = new DataTable();
            gvSearch.DataBind();
            lblNoSearchFound.Visible = true;
            pnlText.Visible = false;
            lblNoSearchFound.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
            gvSearch.Visible = false;
            litNote.Visible = false;
            litExportTo.Visible = false;
            btnExportToExcel.Visible = false;
            btnExportToPdf.Visible = false;
            return;
        }
        else
        {
            lblNoSearchFound.Text = "";
            lblNoSearchFound.Visible = false;

            gvSearch.DataSource = ds.Tables[0];
            gvSearch.DataBind();
            litNote.Visible = false;
            litExportTo.Visible = false;
            btnExportToExcel.Visible = false;
            btnExportToPdf.Visible = false;
        }
    }

    protected string changeDateFormat(string strDate)
    {
        int intTimePart = strDate.IndexOf(" ");
        string strTimePart = strDate.Substring(intTimePart, strDate.Length - intTimePart);

        string dd;
        string mm;
        string yyyy;

        int intIndexOfFirstHash = strDate.IndexOf("/");
        mm = strDate.Substring(0, intIndexOfFirstHash);
        strDate = strDate.Substring(intIndexOfFirstHash + 1, strDate.Length - (intIndexOfFirstHash + 1));

        intIndexOfFirstHash = strDate.IndexOf("/");
        dd = strDate.Substring(0, intIndexOfFirstHash);
        strDate = strDate.Substring(intIndexOfFirstHash + 1, strDate.Length - (intIndexOfFirstHash + 1));

        yyyy = strDate.Substring(0, strDate.IndexOf(" "));

        strDate = dd + "/" + mm + "/" + yyyy + " " + strTimePart;

        return strDate;
    }

    protected string changeDateFormatDDMMYYYY(string strDate)
    {
        int intTimePart = strDate.IndexOf(" ");
        string strTimePart = strDate.Substring(intTimePart, strDate.Length - intTimePart);

        string dd;
        string mm;
        string yyyy;

        int intIndexOfFirstHash = strDate.IndexOf("/");
        dd = strDate.Substring(0, intIndexOfFirstHash);
        strDate = strDate.Substring(intIndexOfFirstHash + 1, strDate.Length - (intIndexOfFirstHash + 1));

        intIndexOfFirstHash = strDate.IndexOf("/");
        mm = strDate.Substring(0, intIndexOfFirstHash);
        strDate = strDate.Substring(intIndexOfFirstHash + 1, strDate.Length - (intIndexOfFirstHash + 1));

        yyyy = strDate.Substring(0, strDate.IndexOf(" "));

        strDate = mm.Trim() + "/" + dd.Trim() + "/" + yyyy.Trim() + " " + strTimePart.Trim();

        return strDate;
    }

    protected void bindGrid(string strCategory, string strSearch)
    {

        litNote.Text = "*A commercial break is defined as recorded commercials played within 90 seconds of each other";
        int stationID = getStationID(txtStationSelect.Text.Trim());


        if (stationID == 0)
        {
            lblNoSearchFound.Text = "<BR><BR><BR><BR><BR><BR><BR>No record found";
            litNote.Visible = false;
            return;
        }

        lblNoSearchFound.Text = "";
        lblNoSearchFound.Visible = false;

        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());

        Session["dtStartDate"] = dtStartDate;
        Session["dtEndDate"] = dtEndDate;
        Session["strSearch"] = strSearch;
        Session["strCategory"] = strCategory;
        Session["stationID"] = stationID;

        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("SpotPlacementReport", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@stationID", stationID);
        da.SelectCommand.Parameters.AddWithValue("@startDate", dtStartDate.ToString());
        da.SelectCommand.Parameters.AddWithValue("@endDate", dtEndDate.ToString());
        da.SelectCommand.Parameters.AddWithValue("@strSearch", strSearch);
        da.SelectCommand.Parameters.AddWithValue("@category", strCategory);

        pnlback.Visible = true;

        DataSet ds = new DataSet();
        da.Fill(ds);
        gvSearch.Visible = false;
        radData.Visible = true;
        if (ds.Tables[0].Rows.Count > 0)
        {
            Session["ds"] = ds;
            Session["data"] = ds.Tables[0];
            Session["tblExport"] = ds.Tables[0];
            Session["totalRecordCount"] = ds.Tables[0].Rows.Count;
            DefineGridStructure();
            radData.DataSource = ds;
            radData.DataBind();
            litNote.Visible = true;
            litExportTo.Visible = true;
            btnExportToExcel.Visible = true;
            btnExportToPdf.Visible = true;
        }
        else
        {
            Session["data"] = null;
            Session["totalRecordCount"] = 0;
            radData.DataSource = null;
            radData.DataBind();
            litNote.Visible = false;
            displayNoRecordFound();
        }

    }


    protected string getFooterValueForAColumn(DataTable dt, DataColumn dc)
    {
        decimal totalAll = decimal.Parse(dt.Compute("sum([Break Length])", "").ToString());
        string footerText = "";
        try
        {
            decimal totalOfColumn = decimal.Parse(dt.Compute("sum([" + dc.ColumnName + "])", "").ToString());
            decimal perc = ((totalOfColumn / totalAll) * 100);
            footerText = totalOfColumn.ToString() + "(" + perc.ToString("n2") + "%)";
        }
        catch
        {

        }
        return footerText;
    }

    protected void dgResult_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        DataTable dt = (DataTable)Session["data"];
        if (!isPdfExport && e.Item is GridFooterItem)
        {
            GridFooterItem footerItem = e.Item as GridFooterItem;

            string footerFontName = (string)Session["headerFontName"];//ConfigurationSettings.AppSettings["footerFontName"].ToString();
            string footerFontSize = (string)Session["headerFontName"];//ConfigurationSettings.AppSettings["footerFontSize"].ToString();


            int i = 0;
            foreach (DataColumn dc in dt.Columns)
            {
                footerItem[dc.ColumnName].Style.Add("font-weight", "bold");

                footerItem[dc.ColumnName].Style.Add("font-family", footerFontName);
                footerItem[dc.ColumnName].Style.Add("font-size", footerFontSize);
                footerItem[dc.ColumnName].Style.Add("font-weight", "bold");

                if (i == 0)
                {
                    footerItem[dc.ColumnName].Text = "Total";
                }
                else
                {
                    footerItem[dc.ColumnName].Text = getFooterValueForAColumn(dt, dc);

                    if (dc.ColumnName == "Break Start Time *")
                    {

                    }
                    else if (dc.ColumnName == "First & Last" || dc.ColumnName == "First Or Last" || dc.ColumnName == "First" || dc.ColumnName == "Last" || dc.ColumnName == "Break Length")
                    {

                    }
                    else
                    {
                        footerItem[dc.ColumnName].Text = "<a href='' onclick=\"javascript:return disp_details('All','" + dc.ColumnName + "')\"  alt='Click here to see the details'><font color=white>" + footerItem[dc.ColumnName].Text + "</font></a>";
                    }
                }
                footerItem[dc.ColumnName].HorizontalAlign = HorizontalAlign.Center;
                System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
                footerItem.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
                footerItem.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

                i++;
            }
        }

        if (isPdfExport && e.Item is GridFooterItem)
        {
            GridFooterItem footerItem = (GridFooterItem)e.Item;

            string strDateFormat = (string)Session["dateFormat"];

            DateTime dtDate = System.DateTime.Now;
            string strDate = "";
            if (strDateFormat == "DD/MM/YYYY")
            {
                strDate = dtDate.ToString("dd/MM/yyyy");
            }
            else
            {
                strDate = dtDate.ToString("MM/dd/yyyy");
            }


            string strTime = System.DateTime.Now.ToString();
            strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


            string strCountText = "<b>" + Session["totalRecordCount"].ToString() + " rows </b>";

            string strFooterText = "";

            strFooterText = "This data was generated and exported on " + strDate + " at " + strTime + ".";
            strFooterText += " (" + strCountText + ") ";

            strFooterText += "AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved. ";
            strFooterText += "AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India. ";
            strFooterText += "The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.";


            e.Item.Cells[2].ColumnSpan = 2;
            e.Item.Cells[2].Text = strFooterText;
            e.Item.Cells[2].Style["color"] = "red";
            e.Item.Font.Size = FontUnit.Point(5);
            e.Item.Font.Bold = false;

        }

        if (e.Item.ItemType == GridItemType.Header)
        {
            string headerFontName = (string)Session["headerFontName"];//ConfigurationSettings.AppSettings["headerFontNameMarketShareOfEachStation"].ToString();
            string headerFontSize = (string)Session["headerFontSize"];//ConfigurationSettings.AppSettings["headerFontSizeMarketShareOfEachStation"].ToString();
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", headerFontName);
            for (int i = 0; i < e.Item.Cells.Count; i++)
            {
                if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                {
                    e.Item.Cells[i].Font.Bold = true;
                }
                else
                {
                    e.Item.Cells[i].Font.Bold = false;
                }
                e.Item.Cells[i].Font.Size = FontUnit.Parse(Session["headerFontSize"].ToString().Replace("Px", "").Trim());
            }
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        }

        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            string actualDataFontName = (string)Session["dataFontName"];//ConfigurationSettings.AppSettings["actualDataFontName"].ToString();
            string actualDataFontSize = (string)Session["dataFontName"];//ConfigurationSettings.AppSettings["actualDataFontSize"].ToString();

            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            e.Item.Cells[2].Text = changeDateFormat(e.Item.Cells[2].Text);

            if (!isPdfExport)
            {
                for (int ii = 0; ii < e.Item.Cells.Count; ii++)
                {
                    e.Item.Cells[ii].Style.Add("font-family", actualDataFontName);
                    if (Session["dataFontStyle"].ToString().ToLower() == "bold")
                    {
                        e.Item.Cells[ii].Font.Bold = true;
                    }
                    else
                    {
                        e.Item.Cells[ii].Font.Bold = false;
                    }
                    e.Item.Cells[ii].Font.Size = FontUnit.Parse(Session["dataFontSize"].ToString().Replace("Px", "").Trim());

                }
            }
            else
            {
                e.Item.Font.Size = FontUnit.Point(8);
                e.Item.Font.Bold = false;
            }

            GridDataItem dataItem = e.Item as GridDataItem;
            int j = 0;
            string strBreakStartTime = "";

            foreach (DataColumn dc in dt.Columns)
            {
                if (j == 0)
                {
                    strBreakStartTime = dataItem[dc.ColumnName].Text;
                }
                else
                {
                    if (!isPdfExport)
                    {
                        dataItem[dc.ColumnName].Text = "<a href='' onclick=\"javascript:return disp_details('" + strBreakStartTime + "','" + dc.ColumnName + "')\"  alt='Click here to see the details'><font color=black>" + dataItem[dc.ColumnName].Text + "</font></a>";
                    }
                }

                if (dc.ColumnName == "First" || dc.ColumnName == "Last" || dc.ColumnName == "First & Last" || dc.ColumnName == "First Or Last")
                {
                    dataItem[dc.ColumnName].BackColor = System.Drawing.Color.FromArgb(219, 229, 241);
                }

                j++;
            }
        }
    }



    protected void displayNoRecordFound()
    {
        lblError.Visible = true;
        lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
        radData.Visible = false;
        litExportTo.Visible = false;
        btnExportToExcel.Visible = false;
        btnExportToPdf.Visible = false;
    }

    protected void Page_Init(object source, System.EventArgs e)
    {

    }

    protected void formatsearchGrid()
    {
        string headerFontName = (string)Session["headerFontName"];// ConfigurationSettings.AppSettings["headerFontName"].ToString();
        string headerFontSize = (string)Session["headerFontSize"];//ConfigurationSettings.AppSettings["headerFontSize"].ToString();

        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();

        gvSearch.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
        gvSearch.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        gvSearch.HeaderStyle.Font.Name = headerFontName;
        gvSearch.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());

        string actualDataFontName = (string)Session["dataFontName"];//ConfigurationSettings.AppSettings["actualDataFontName"].ToString();
        string actualDataFontSize = (string)Session["dataFontSize"];//ConfigurationSettings.AppSettings["actualDataFontSize"].ToString();

        gvSearch.ItemStyle.Font.Name = actualDataFontName;
        gvSearch.ItemStyle.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
        gvSearch.AlternatingItemStyle.Font.Name = actualDataFontName;
        gvSearch.AlternatingItemStyle.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
        gvSearch.AlternatingItemStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
        gvSearch.ItemStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);
        gvSearch.AlternatingItemStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

        gvSearch.ClientSettings.Scrolling.AllowScroll = true;
        gvSearch.ClientSettings.Scrolling.ScrollHeight = Unit.Pixel(300);
        gvSearch.ClientSettings.Scrolling.UseStaticHeaders = true;
        gvSearch.GridLines = GridLines.Both;
    }

    protected void DefineGridStructure()
    {
        string headerFontName = (string)Session["headerFontName"];// ConfigurationSettings.AppSettings["headerFontName"].ToString();
        string headerFontSize = (string)Session["headerFontSize"];//ConfigurationSettings.AppSettings["headerFontSize"].ToString();


        radData.ClientSettings.Scrolling.AllowScroll = true;
        radData.ClientSettings.Scrolling.ScrollHeight = Unit.Pixel(300);
        radData.ClientSettings.Scrolling.UseStaticHeaders = true;
        radData.GridLines = GridLines.Both;

        radData.MasterTableView.EnableColumnsViewState = false;
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();

        GridBoundColumn boundColumn;
        DataTable dt = (DataTable)Session["data"];
        string strColName = "";
        int colWidth = 0;
        foreach (DataColumn dc in dt.Columns)
        {

            strColName = dc.ColumnName;
            if (strColName == "Break Start Time *")
            {
                colWidth = 200;
            }
            else if (strColName == "First & Last" || strColName == "First Or Last" || strColName == "First" || strColName == "Last" || strColName == "Break Length")
            {
                colWidth = 100;
            }
            else
            {
                colWidth = 75;
            }

            boundColumn = new GridBoundColumn();
            boundColumn.DataField = strColName;
            boundColumn.HeaderText = strColName;
            boundColumn.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            boundColumn.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
            boundColumn.HeaderStyle.Font.Name = headerFontName;
            if (!isPdfExport)
            {
                boundColumn.HeaderStyle.Width = Unit.Pixel(colWidth);
            }
            boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            boundColumn.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            radData.MasterTableView.Columns.Add(boundColumn);


        }

        radData.ItemStyle.BackColor = System.Drawing.Color.White;

        radData.ShowFooter = true;

    }

    protected void img_btn_export_to_excel_Click(object sender, EventArgs e)
    {
        int stationID = getStationID(txtStationSelect.Text.Trim());

        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());

        addAuditLog(dtStartDate, dtEndDate, stationID.ToString(), true, false, false);

        ExportToExcel();
    }

    public void ExportToExcel()
    {
        string strDateFormat = (string)Session["dateFormat"];

        DataSet ds = (DataSet)Session["ds"];
        DataTable dt = new DataTable("tblExport");
        dt = ds.Tables[0].Copy();



        string str_station_name = "";

        String strStationName = "<BR><BR>Station: " + str_station_name;

        DateTime dt1 = DateTime.Parse(Session["dtStartDate"].ToString());
        DateTime dt2 = DateTime.Parse(Session["dtEndDate"].ToString());

        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }

        String strDateRange = "<BR><BR>From: " + sDt1 + "  to  " + sDt2;

        ExportToExcel(dt, Response, strStationName, strDateRange);
    }

    protected DataTable getDataTableWithChangedDateFormat(DataTable dt)
    {
        DataTable cloneTable;
        cloneTable = dt.Clone();
        cloneTable.Columns[0].DataType = typeof(string);

        for (int i = 0; i <= dt.Rows.Count - 1; ++i)
        {
            cloneTable.ImportRow(dt.Rows[i]);
        }

        foreach (DataRow dr in cloneTable.Rows)
        {
            foreach (DataColumn dc in cloneTable.Columns)
            {
                if (dc.ColumnName == "Break Start Time *")
                {
                    dr[dc] = changeDateFormatDDMMYYYY(dr[dc].ToString());
                }
            }
        }
        cloneTable.AcceptChanges();
        return cloneTable;
    }

    public void ExportToExcel(DataTable dt, HttpResponse response, String strStationName, String strDateRange)
    {

        //first let's clean up the response.object
        response.Clear();
        response.Charset = "";
        //set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel";
        //create a string writer
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //create an htmltextwriter which uses the stringwriter
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //instantiate a datagrid
        DataGrid dg = new DataGrid();
        //set the datagrid datasource to the dataset passed in
        if (dt == null)
        {
            return;
        }
        else if (dt.Rows.Count == 0)
        {
            return;
        }
        dg.DataSource = getDataTableWithChangedDateFormat(dt);
        //bind the datagrid
        dg.DataBind();

        dg.RenderControl(htmlWrite);


        String str = stringWrite.ToString();

        int start, end;
        start = 0;
        end = 0;

        int colspan = 0;

        colspan = dt.Columns.Count;

        string strFooterTR = "<tr>";
        string strFooterTD = "";
        int i = 0;
        foreach (DataColumn dc in dt.Columns)
        {
            if (i == 0)
            {
                strFooterTD = "<td>Total</td>";
            }
            else
            {
                strFooterTD = strFooterTD + "<td>" + getFooterValueForAColumn(dt, dc) + "</td>";
            }
            i++;
        }
        strFooterTR = strFooterTR + strFooterTD + "</tr></table>";
        str = str.Replace("</table>", strFooterTR);

        String strReportName = "";
        strReportName = txtStationSelect.Text + " AirCheck India Spot Placement Report.";



        string strFinal = "<tr><td align=center colspan=" + colspan + " style='background-color:#FFFF66;'>" + strReportName + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strDateRange + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + ">&nbsp;</td></tr>";



        while (str.IndexOf("<select name=") > 0)
        {
            start = str.IndexOf("<select name=");
            end = str.IndexOf("</select>") + 9;
            str = str.Replace(str.Substring(start, end - start), "");
        }

        string strAirTime = "";
        string strPlayTime = "";
        string strNewPlayTime = "";

        int intCount = 0;

        foreach (DataRow dr in dt.Rows)
        {
            strAirTime = dr[0].ToString();
            if (strAirTime == "")
            {
                strPlayTime = dr[2].ToString();
                strNewPlayTime = dr[2].ToString() + " ";

                str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=5 align='Center'>" + strNewPlayTime + "</td></tr>");

            }
            else
            {
                intCount = intCount + 1;
            }
        }



        str = strFinal + "<BR><BR>" + str;


        string strDateFormat = (string)Session["dateFormat"];

        DateTime dtDate = System.DateTime.Now;
        string strDate = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dtDate.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dtDate.ToString("MM/dd/yyyy");
        }


        string strTime = System.DateTime.Now.ToString();
        strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


        string strCountText = "<b>" + intCount + " rows </b>";

        string strFooterText = "";

        strFooterText = "<BR><table>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>This data was generated and exported on " + strDate + " at " + strTime + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>" + strCountText + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><BR></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.</td></tr>";
        strFooterText += "</table>";

        str = "<table>" + str + strFooterText;



        response.Write(str);
        response.End();
    }


    protected void btnExportToPdf_Click(object sender, EventArgs e)
    {
        isPdfExport = true;

        int stationID = getStationID(txtStationSelect.Text.Trim());

        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());

        addAuditLog(dtStartDate, dtEndDate, stationID.ToString(), false, true, false);

        string columnClicked = Session["columnClicked"].ToString();
        string strValueSelected = Session["strValueSelected"].ToString();

        if (columnClicked == "accountName")
        {
            bindGrid("Account", strValueSelected);
        }
        else if (columnClicked == "parentName")
        {
            bindGrid("Parent", strValueSelected);
        }


        string strDateFormat = (string)Session["dateFormat"];
        DateTime dt1 = DateTime.Parse(Session["dtStartDate"].ToString());
        DateTime dt2 = DateTime.Parse(Session["dtEndDate"].ToString());

        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }

        String strDateRange = "From: " + sDt1 + "  -  " + sDt2;

        radData.ExportSettings.FileName = "Spot Placement Report";
        radData.ExportSettings.IgnorePaging = true;
        radData.ExportSettings.OpenInNewWindow = true;
        radData.ExportSettings.Pdf.PaperSize = GridPaperSize.Letter;
        radData.ExportSettings.Pdf.PageLeftMargin = Unit.Pixel(10);
        radData.ExportSettings.Pdf.PageRightMargin = Unit.Pixel(10);

        String strReportName = "";
        strReportName = txtStationSelect.Text + " AirCheck India Spot Placement Report";

        radData.ExportSettings.Pdf.PageTitle = strReportName + " " + strDateRange;
        radData.ExportSettings.ExportOnlyData = false;
        radData.MasterTableView.ExportToPdf();

    }

    protected void gvSearch_RowDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item.ItemType == GridItemType.Header)
        {
            string headerFontName = (string)Session["headerFontName"];//ConfigurationSettings.AppSettings["headerFontNameMarketShareOfEachStation"].ToString();
            string headerFontSize = (string)Session["headerFontSize"];//ConfigurationSettings.AppSettings["headerFontSizeMarketShareOfEachStation"].ToString();
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", headerFontName);
            for (int i = 0; i < e.Item.Cells.Count; i++)
            {
                if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                {
                    e.Item.Cells[i].Font.Bold = true;
                }
                else
                {
                    e.Item.Cells[i].Font.Bold = false;
                }
                e.Item.Cells[i].Font.Size = FontUnit.Parse(Session["headerFontSize"].ToString().Replace("Px", "").Trim());
            }
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        }

        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            string actualDataFontName = (string)Session["dataFontName"];//ConfigurationSettings.AppSettings["actualDataFontNameMarketShareOfEachStation"].ToString();
            string actualDataFontSize = (string)Session["dataFontSize"];//ConfigurationSettings.AppSettings["actualDataFontSizeMarketShareOfEachStation"].ToString();


            LinkButton lnlAccount = (LinkButton)e.Item.FindControl("lnlAccount");
            lnlAccount.Font.Name = actualDataFontName;
            lnlAccount.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());

            LinkButton lnlParent = (LinkButton)e.Item.FindControl("lnlParent");
            lnlParent.Font.Name = actualDataFontName;
            lnlParent.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
        }
    }
    protected void gvSearch_RowCommand(object source, GridCommandEventArgs e)
    {
        pnlSearchedCriteria.Visible = false;


        string columnClicked = e.CommandName.ToString();
        string strValueSelected = e.CommandArgument.ToString();
        txtSearchText.Text = strValueSelected;
        Session["columnClicked"] = columnClicked;
        Session["strValueSelected"] = strValueSelected;
        if (columnClicked == "accountName")
        {
            bindGrid("Account", strValueSelected);
        }
        else if (columnClicked == "parentName")
        {
            bindGrid("Parent", strValueSelected);
        }
    }
}
