﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using Telerik.Web.UI;

public partial class Commercials_Spots_compare : System.Web.UI.Page
{
    bool isPdfExport = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        bindGrid();
    }

    protected int getStationID(string strStation)
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);

        SqlCommand comm = default(SqlCommand);
        conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);

        comm = new SqlCommand("getStationIDOfStation", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@stationName", strStation);
        conn.Open();
        SqlDataReader dr = comm.ExecuteReader();
        int strStartionID = 0;
        while (dr.Read())
        {
            strStartionID = int.Parse(dr[0].ToString());
        }
        dr.Close();
        conn.Close();
        return strStartionID;
    }

    protected void bindGrid()
    {
        DefineGridStructure();

        string strStationID = "";

        if (Request.QueryString["location"] == "Left")
        {
            strStationID = (String)Session["stationSelected"];
        }
        else
        {
            strStationID = (String)Session["stationToCompare"];
        }


        int intStationID = getStationID(strStationID);

        DateTime dtStartDate = DateTime.Parse(Session["dtStartDate"].ToString());
        string strStartDate = dtStartDate.ToString("d");

        DateTime dtEndDate = DateTime.Parse(Session["dtEndDate"].ToString());
        string strEndDate = dtEndDate.ToString("d");

        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("commercialsSpots", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@stationID", intStationID);
        da.SelectCommand.Parameters.AddWithValue("@startDate", strStartDate);
        da.SelectCommand.Parameters.AddWithValue("@endDate", strEndDate);

        DataSet ds = new DataSet();
        da.Fill(ds);

        if (ds.Tables[0].Rows.Count == 0)
        {
            lblError.Visible = true;
            lblError.Text = getStationNameForID(intStationID);
            lblError.Text += "<BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/>No Record Found";
            radData.Visible = false;
        }
        else
        {
            radData.Visible = true;
            lblError.Visible = true;
            lblError.Text = getStationNameForID(intStationID);
            Session["ds"] = ds;
            Session["sumDuration"] = ds.Tables[1].Rows[0][0].ToString();
            radData.DataSource = ds.Tables[0];
            radData.DataBind();
        }
    }

    protected string getStationNameForID(int stationID)
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getStationNameForStationID", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@stationID", stationID);

        DataSet ds = new DataSet();
        da.Fill(ds);

        return ds.Tables[0].Rows[0][0].ToString();

    }

    int iRowIndex;
    int iTimeBandIndex;
    string strTimeBand;
    protected void dgResult_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
        string headerFontName = "Helvetica";// ConfigurationSettings.AppSettings["headerFontNameTA"].ToString();
        string headerFontSize = "12px";//ConfigurationSettings.AppSettings["headerFontSizeTA"].ToString();

        if (e.Item.ItemType == GridItemType.Header)
        {
            iRowIndex = 0;
            strTimeBand = "";
            iTimeBandIndex = 0;

            e.Item.Style.Add("font-family", headerFontName);



            //e.Item.Style.Add("font-size", headerFontSize);
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
            for (int i = 0; i < e.Item.Cells.Count; i++)
            {
                e.Item.Cells[i].Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
                if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                {
                    e.Item.Cells[i].Font.Bold = true;
                }
                else
                {
                    e.Item.Cells[i].Font.Bold = false;
                }
            }
        }
        if (e.Item.ItemType == GridItemType.Footer)
        {
            e.Item.Visible = true;
            //e.Item.Cells[7].BackColor = System.Drawing.Color.FromArgb(0, 112, 192);
            //e.Item.Cells[7].ForeColor = System.Drawing.Color.White;
            //e.Item.Cells[8].BackColor = System.Drawing.Color.FromArgb(0, 112, 192);
            //e.Item.Cells[8].ForeColor = System.Drawing.Color.White;
            //e.Item.Cells[7].HorizontalAlign = HorizontalAlign.Center;
            //e.Item.Cells[8].HorizontalAlign = HorizontalAlign.Center;
            //e.Item.Cells[7].Text = "Total Seconds";
            //e.Item.Cells[8].Text = Session["sumDuration"].ToString();

            e.Item.Cells[7].BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]); //System.Drawing.Color.FromArgb(0, 112, 192);
            e.Item.Cells[7].ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]); //System.Drawing.Color.White;
            e.Item.Cells[8].BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]); //System.Drawing.Color.FromArgb(0, 112, 192);
            e.Item.Cells[8].ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]); //System.Drawing.Color.White;
            e.Item.Cells[7].Text = "Total Seconds";
            e.Item.Cells[7].Style.Add("font-family", headerFontName);
            e.Item.Cells[8].Style.Add("font-family", headerFontName);
            e.Item.Cells[7].HorizontalAlign = HorizontalAlign.Center;
            e.Item.Cells[8].HorizontalAlign = HorizontalAlign.Center;
            e.Item.Cells[8].Text = Session["sumDuration"].ToString();

            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Cells[7].Font.Bold = true;
                e.Item.Cells[8].Font.Bold = true;
            }
            else
            {
                e.Item.Cells[7].Font.Bold = false;
                e.Item.Cells[8].Font.Bold = false;
            }
            e.Item.Cells[7].Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.Cells[8].Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());

        }

        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            string actualDataFontName = (string)Session["dataFontName"];//ConfigurationSettings.AppSettings["actualDataFontNameCS"].ToString();
            string actualDataFontSize = (string)Session["dataFontSize"];//ConfigurationSettings.AppSettings["actualDataFontSizeCS"].ToString();

            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]); //System.Drawing.Color.White;


            for (int ii = 0; ii < e.Item.Cells.Count; ii++)
            {
                //e.Item.Cells[ii].Style.Add("font-family", actualDataFontName);
                //e.Item.Cells[ii].Style.Add("font-size", actualDataFontSize);
                //e.Item.Cells[ii].Style.Add("font-weight", "bold");
                e.Item.Cells[ii].Style.Add("font-family", actualDataFontName);
                e.Item.Cells[ii].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
                if (Session["dataFontStyle"].ToString().ToLower() == "bold")
                {
                    e.Item.Cells[ii].Font.Bold = true;
                }
                else
                {
                    e.Item.Cells[ii].Font.Bold = false;
                }
            }
        }

        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            string actualDataFontName = (string)Session["dataFontName"];//ConfigurationSettings.AppSettings["actualDataFontNameCS"].ToString();
            string actualDataFontSize = (string)Session["dataFontSize"];//ConfigurationSettings.AppSettings["actualDataFontSizeCS"].ToString();

            if (!isPdfExport)
            {
                for (int ii = 0; ii < e.Item.Cells.Count; ii++)
                {
                    e.Item.Cells[ii].Style.Add("font-family", actualDataFontName);
                    e.Item.Cells[ii].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
                    if (Session["dataFontStyle"].ToString().ToLower() == "bold")
                    {
                        e.Item.Cells[ii].Font.Bold = true;
                    }
                    else
                    {
                        e.Item.Cells[ii].Font.Bold = false;
                    }
                }

                if (iRowIndex == 0)
                {
                    strTimeBand = e.Item.Cells[3].Text;
                    iTimeBandIndex = 0;
                }
                else
                {
                    if (strTimeBand == e.Item.Cells[3].Text)
                    {

                    }
                    else
                    {
                        strTimeBand = e.Item.Cells[3].Text;
                        iTimeBandIndex++;
                    }
                    if (iTimeBandIndex % 2 != 0)
                    {
                        e.Item.BackColor = System.Drawing.Color.FromArgb(176, 196, 222);
                       // e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
                    }
                    else
                    {
                        e.Item.BackColor = System.Drawing.Color.White;
                    }
                }
                iRowIndex++;
            }
            else
            {
                e.Item.Font.Size = FontUnit.Point(8);
                e.Item.Font.Bold = false;
            }
        }
    }

    protected void DefineGridStructure()
    {
        string headerFontName = (string)Session["headerFontName"];// ConfigurationSettings.AppSettings["headerFontNameDP"].ToString();
        string headerFontSize = (string)Session["headerFontSize"];//ConfigurationSettings.AppSettings["headerFontSizeDP"].ToString();

        radData.ClientSettings.Scrolling.AllowScroll = true;
        radData.ClientSettings.Scrolling.ScrollHeight = Unit.Pixel(300);
        radData.ClientSettings.Scrolling.UseStaticHeaders = true;
        radData.GridLines = GridLines.Both;

        radData.MasterTableView.EnableColumnsViewState = false;

        GridBoundColumn boundColumn;
        boundColumn = new GridBoundColumn();
        boundColumn.DataField = "Date";
        boundColumn.HeaderText = "Date";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
        boundColumn.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);

        //boundColumn.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(0, 112, 192);
        boundColumn.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        boundColumn.HeaderStyle.Font.Name = headerFontName;

        if (!isPdfExport)
        {
            boundColumn.HeaderStyle.Width = Unit.Pixel(100);
        }
        boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        boundColumn.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
        radData.MasterTableView.Columns.Add(boundColumn);


        boundColumn = new GridBoundColumn();
        boundColumn.DataField = "Time Band";
        boundColumn.HeaderText = "Time Band";
        boundColumn.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
        boundColumn.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        boundColumn.HeaderStyle.Font.Name = headerFontName;
        if (!isPdfExport)
        {
            boundColumn.HeaderStyle.Width = Unit.Pixel(100);
        }
        boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        boundColumn.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
        radData.MasterTableView.Columns.Add(boundColumn);

        boundColumn = new GridBoundColumn();
        boundColumn.DataField = "Spot Time";
        boundColumn.HeaderText = "Spot Time";
        boundColumn.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
        boundColumn.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        boundColumn.HeaderStyle.Font.Name = headerFontName;
        if (!isPdfExport)
        {
            boundColumn.HeaderStyle.Width = Unit.Pixel(100);
        }
        boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        boundColumn.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
        radData.MasterTableView.Columns.Add(boundColumn);

        boundColumn = new GridBoundColumn();
        boundColumn.DataField = "Spot Name";
        boundColumn.HeaderText = "Spot Name";
        boundColumn.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
        boundColumn.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        boundColumn.HeaderStyle.Font.Name = headerFontName;
        if (!isPdfExport)
        {
            boundColumn.HeaderStyle.Width = Unit.Pixel(250);
        }
        boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
        boundColumn.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
        radData.MasterTableView.Columns.Add(boundColumn);

        boundColumn = new GridBoundColumn();
        boundColumn.DataField = "Parent";
        boundColumn.HeaderText = "Parent";
        boundColumn.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
        boundColumn.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        boundColumn.HeaderStyle.Font.Name = headerFontName;
        if (!isPdfExport)
        {
            boundColumn.HeaderStyle.Width = Unit.Pixel(250);
        }
        boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
        boundColumn.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
        radData.MasterTableView.Columns.Add(boundColumn);


        boundColumn = new GridBoundColumn();
        boundColumn.DataField = "Spot Category";
        boundColumn.HeaderText = "Spot Category";
        boundColumn.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
        boundColumn.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        boundColumn.HeaderStyle.Font.Name = headerFontName;
        if (!isPdfExport)
        {
            boundColumn.HeaderStyle.Width = Unit.Pixel(200);
        }
        boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
        boundColumn.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
        radData.MasterTableView.Columns.Add(boundColumn);


        boundColumn = new GridBoundColumn();
        boundColumn.DataField = "Spot Duration (in sec)";
        boundColumn.HeaderText = "Spot Duration (in sec)";
        boundColumn.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
        boundColumn.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        boundColumn.HeaderStyle.Font.Name = headerFontName;
        if (!isPdfExport)
        {
            boundColumn.HeaderStyle.Width = Unit.Pixel(100);
        }
        boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        boundColumn.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
        radData.MasterTableView.Columns.Add(boundColumn);

        //radData.AlternatingItemStyle.BackColor = System.Drawing.Color.FromArgb(149, 179, 215);
        radData.AlternatingItemStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);

        radData.ItemStyle.BackColor = System.Drawing.Color.White;

        radData.ShowFooter = true;

    }
}
