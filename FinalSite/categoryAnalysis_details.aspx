﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="categoryAnalysis_details.aspx.cs" Inherits="categoryAnalysis_details" %>

<html>
	<head>
		<title>Category Analysis Details</title>
		<link href="css/report_caption.css" rel="stylesheet" />
	    <script language="JavaScript" type="text/javascript">

var message="Sorry, this feature is disabled.";
function HideIcons() {
    prn.style.display = 'none';
}
function click(e) 
{
	if (document.all) 
	{
		if (event.button == 2) 
		{
			alert(message);
			return false;
		}
	}
	if (document.layers) 
	{
		if (e.which == 3) 
		{
			alert(message);
			return false;
		}
	}
}

if (document.layers) 
{
	document.captureEvents(Event.MOUSEDOWN);
}
document.onmousedown=click;

			</script>
	</head>
	<body>
		<form id="Form1" runat="server">
			<table height="53" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="top" align="top" width="760" height="53" border="0"><img src="images/bgTop.gif"></td>
					<td vAlign="top" width="100%"><IMG height="28" src="images/bgTopExt.gif" width="100%" border="0"></td>
				</tr>
			</table>
			<span name="prn" ID="prn">
				<table height="0" cellSpacing="0" cellPadding="0" width="100%" border="0">
					<tr>
						<td width="100%" align="right" colspan="2"><a href="#" onClick="prn.style.display='none';print();"><IMG src="images/icon_print.gif" border="0"></a>&nbsp;&nbsp;</td>
					</tr>
				</table>
			</span>
			<table width="100%">
				<tr valign="top">
					<td align="middle" width="100%" colspan="3" valign="top" class="date" height="18" bgcolor="cornsilk">Category 
						Analysis Details</td>
				</tr>
				<tr class="date">
					<td width="10%" height="15">&nbsp;</td>
					<td width="1%" height="15">&nbsp;</td>
					<td align="left" height="15">&nbsp;</td>
				</tr>
				<tr class="date">
					<td width="10%" height="15">Market</td>
					<td width="1%" height="15">:</td>
					<td align="left" height="15"><asp:Label ID="lblMarket" runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr class="date">
					<td width="10%" height="15">Station</td>
					<td width="1%" height="15">:</td>
					<td align="left" height="15"><asp:Label ID="lblStation" runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr class="date">
					<td width="10%" height="15">Start Date</td>
					<td width="1%" height="15">:</td>
					<td align="left" height="15"><asp:Label ID="lblStartDate" runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr class="date">
					<td width="10%" height="15">End Date</td>
					<td width="1%" height="15">:</td>
					<td align="left" height="15"><asp:Label ID="lblEndDate" runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr class="date">
					<td width="10%" height="15">Category</td>
					<td width="1%" height="15">:</td>
					<td align="left" height="15"><asp:Label ID="lblCategory" runat="server" CssClass="date"></asp:Label></td>
				</tr>
				<tr class="date">
					<td width="10%" height="15">&nbsp;</td>
					<td width="1%" height="15">&nbsp;</td>
					<td align="left" height="15">&nbsp;</td>
				</tr>
				<tr class="date">
					<td width="100%" colspan="3">
						<asp:DataGrid ID="datagrid_disp_details" Runat="server" Width="100%" OnItemDataBound="datagrid_disp_details_ItemDataBound">
							<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</asp:DataGrid>
					</td>
				</tr>
			</table>
			<br>
		</form>
	</body>
</html>
