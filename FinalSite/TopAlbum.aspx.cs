﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using Telerik.Web.UI;
using System.Data.SqlClient;
using DBauer.Web.UI.WebControls;

public partial class TopAlbum : System.Web.UI.Page
{
    bool isPdfExport = false;    
    protected void Page_Load(object sender, EventArgs e)
    {
        DBauer.Web.UI.WebControls.HierarGrid dgHierarchical;

        //StringBuilder onloadScript = new StringBuilder();
        //onloadScript.Append("<script type='text/javascript'>");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("ShowReport();");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("</script>");
        //this.ClientScript.RegisterStartupScript(this.GetType(), "onLoadCall", onloadScript.ToString());

        ((Label)this.Master.FindControl("lblReportName")).Text = "Top Album (Movie/Music)";

        //Literal lit = new Literal();
        //lit.Text = "<script language='javascript'>tdhide('leftMenu', 'ctl00_showhide', 'hide');</script>";
        //Page.Controls.Add(lit);

        getDateFormat();
        string strDateFormat = (string)Session["dateFormat"];

        if (strDateFormat == "DD/MM/YYYY")
        {
            dtStart.DateInput.DateFormat = "dd/MM/yyyy";
            dtEnd.DateInput.DateFormat = "dd/MM/yyyy";

            dtStart.DateInput.DisplayDateFormat = "dd/MM/yyyy";
            dtEnd.DateInput.DisplayDateFormat = "dd/MM/yyyy";
        }

        applyGridFormatting();
        if (!IsPostBack)
        {
            Session["ds"] = null;



            setDate();
            getCities();
            GenerateTreeViewStations();
            GenerateTreeViewNetworks();
            GenerateTreeViewMarkets();

            //ddSelectBy.Attributes.Add("OnChange", "showHideNextColumn(this);");


            if (Session["dataReadOnly"].ToString() == "True")
            {
                btnExportToExcel.Enabled = false;
                //btnExportToPdf.Enabled = false;
            }
            else
            {
                btnExportToExcel.Enabled = true;
                //btnExportToPdf.Enabled = true;
            }

        }
        Pg_Title.Text = "Top Album (Movie/Music)";
    }
    protected void applyGridFormatting()
    {
        //int ScreenHeight;

        //ScreenHeight = Convert.ToInt32(Session["SHeight"]) - 235;

        ////radData.ClientSettings.Scrolling.AllowScroll = true;
        ////radData.ClientSettings.Scrolling.UseStaticHeaders = true;
        ////radData.ClientSettings.Scrolling.ScrollWidth = Unit.Pixel(1500);
        ////radData.ClientSettings.Scrolling.ScrollHeight = Unit.Pixel(ScreenHeight);

        //radData.ClientSettings.Scrolling.ScrollHeight = Unit.Pixel(ScreenHeight);
    }

    protected void getUserPreferencesForCurrentUser()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getUserPreferenceForAUserAndReport", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@user", (string)Session["user"]);
        da.SelectCommand.Parameters.AddWithValue("@reportName", "Top Songs");

        DataSet ds = new DataSet();
        da.Fill(ds);

        Session["headerFontName"] = ds.Tables[0].Rows[0]["headerFontName"].ToString();
        Session["headerFontSize"] = ds.Tables[0].Rows[0]["headerFontSize"].ToString();
        Session["headerFontColor"] = ds.Tables[0].Rows[0]["headerFontColor"].ToString();
        Session["headerRowColor"] = ds.Tables[0].Rows[0]["headerRowColor"].ToString();
        Session["headerFontStyle"] = ds.Tables[0].Rows[0]["headerFontStyle"].ToString();
        Session["dataFontName"] = ds.Tables[0].Rows[0]["dataFontName"].ToString();
        Session["dataFontSize"] = ds.Tables[0].Rows[0]["dataFontSize"].ToString();
        Session["dataFontColor"] = ds.Tables[0].Rows[0]["dataFontColor"].ToString();
        Session["alternateRowColor"] = ds.Tables[0].Rows[0]["alternateRowColor"].ToString();
        Session["dataFontStyle"] = ds.Tables[0].Rows[0]["dataFontStyle"].ToString();

    }

    protected void GenerateTreeViewStations()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getStationsForUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);

        var radTreeStations = (RadTreeView)CmbTreeStations.Items[0].FindControl("radTreeStations");


        radTreeStations.DataFieldID = "name";
        radTreeStations.DataFieldParentID = "m_name";
        radTreeStations.DataTextField = "name";

        radTreeStations.DataSource = ds.Tables[0];
        radTreeStations.DataBind();
    }

    protected void GenerateTreeViewNetworks()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getNetworksForUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);

        ddNetworks.DataSource = ds.Tables[0];
        ddNetworks.DataTextField = "s_name";
        ddNetworks.DataValueField = "s_name";
        ddNetworks.DataBind();

        //radTreeNetworks.DataFieldID = "s_name";
        //radTreeNetworks.DataFieldParentID = "p_name";
        //radTreeNetworks.DataTextField = "s_name";
        //radTreeNetworks.ExpandAllNodes();

        //radTreeNetworks.DataSource = ds.Tables[0];
        //radTreeNetworks.DataBind();
    }

    protected void ddNetworks_SelectedIndexChanged(object sender, EventArgs e)
    {
        var radTreeMarketsForSelectedNetwork = (RadTreeView)CmbTreeMarketsForSelectedNetwork.Items[0].FindControl("radTreeMarketsForSelectedNetwork");
        if (ddNetworks.SelectedItem.Text != "")
        {
            GenerateTreeViewMarketsForSelectedNetwork();
            foreach (RadTreeNode node in radTreeMarketsForSelectedNetwork.GetAllNodes())
            {
                node.Expanded = true;
                node.Checked = true;
            }
        }
        else
        {
            radTreeMarketsForSelectedNetwork.Nodes.Clear();
        }

        litExportTo.Visible = false;
        btnExportToExcel.Visible = false;
        dgHierarchical.Visible = false;
    }

    protected void GenerateTreeViewMarketsForSelectedNetwork()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getMarketsForSelectedNetworkAndUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@station", ddNetworks.SelectedItem.Text);
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);

        var radTreeMarketsForSelectedNetwork = (RadTreeView)CmbTreeMarketsForSelectedNetwork.Items[0].FindControl("radTreeMarketsForSelectedNetwork");

        radTreeMarketsForSelectedNetwork.DataFieldID = "m_name";
        radTreeMarketsForSelectedNetwork.DataFieldParentID = "p_name";
        radTreeMarketsForSelectedNetwork.DataTextField = "m_name";
        radTreeMarketsForSelectedNetwork.ExpandAllNodes();
        radTreeMarketsForSelectedNetwork.DataSource = ds.Tables[0];
        radTreeMarketsForSelectedNetwork.DataBind();
    }

    protected void GenerateTreeViewMarkets()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getMarketsForUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);

        var radTreeMarkets = (RadTreeView)CmbTreeMarkets.Items[0].FindControl("radTreeMarkets");

        radTreeMarkets.DataFieldID = "m_name";
        radTreeMarkets.DataFieldParentID = "p_name";
        radTreeMarkets.DataTextField = "m_name";
        radTreeMarkets.ExpandAllNodes();
        radTreeMarkets.DataSource = ds.Tables[0];
        radTreeMarkets.DataBind();
        radTreeMarkets.ExpandAllNodes();
        foreach (RadTreeNode node in radTreeMarkets.GetAllNodes())
        {
            node.Expanded = true;
            node.Checked = true;
        }
    }

    protected void Page_Init(object source, System.EventArgs e)
    {
        getUserPreferencesForCurrentUser();
       // DefineGridStructure();
    }
    

    protected void setDate()
    {
        DateTime lockDataBefore = DateTime.Parse(Session["lockDataBefore"].ToString());
        DateTime lockDataAfter = DateTime.Parse(Session["lockDataAfter"].ToString());
        dtStart.MinDate = lockDataBefore;
        dtEnd.MinDate = lockDataBefore;

        dtStart.MaxDate = lockDataAfter;
        dtEnd.MaxDate = lockDataAfter;

        dtStart.SelectedDate = System.DateTime.Now;
        dtEnd.SelectedDate = System.DateTime.Now;
    }

    protected void getDateFormat()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("select * from India_Report_New..Country_dateFormat where isSelected=1", conn);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                Session["dateFormat"] = dr["DateFormat"].ToString();
            }
        }

        conn.Close();
    }

    protected void getCities()
    {
        //SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);

        //SqlCommand comm = default(SqlCommand);
        //conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);

        //comm = new SqlCommand("select * from " + ConfigurationSettings.AppSettings["market"] + "..Markets where market_id in (select marketPermission from user_market_permission where userID =(select userID from users where UserName = '" + (string)Session["user"] + "'))", conn);
        //conn.Open();
        //ddCity.DataSource = comm.ExecuteReader();
        //ddCity.DataTextField = "name";
        //ddCity.DataValueField = "market_id";
        //ddCity.DataBind();
        //conn.Close();

        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getMarketIDandNameOfMarketsForUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);

        ddCity.DataSource = ds;
        ddCity.DataTextField = "name";
        ddCity.DataValueField = "market_id";
        ddCity.DataBind();
    }

    protected string getStationIDs_now()
    {
        string strStations = string.Empty;
        var radTreeStations = (RadTreeView)CmbTreeStations.Items[0].FindControl("radTreeStations");
        //ArrayList nodeCollection = radTreeStations.CheckedNodes;
        string strStationNames = "";
        int stationID = 0;
        foreach (RadTreeNode node in radTreeStations.CheckedNodes)
        {
            stationID = getStationIDOfStation(node.Text);
            if (stationID != 0)
            {
                strStations += stationID.ToString() + ",";
                strStationNames += node.Text + ",";
            }
        }

        return strStations.Substring(0, strStations.Length - 1);
    }

    protected int getStationIDOfStation(string txtStation)
    {
        int stationID = 0;
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("getStationIDOfStation", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.CommandTimeout = 0;
        comm.Parameters.AddWithValue("@stationName", txtStation);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                stationID = (int)dr["station_id"];
            }
        }
        else
        {
            stationID = 0;
        }

        conn.Close();
        return stationID;
    }


    protected string getStationIDsOfMarket()
    {
        string strStations = string.Empty;
        var radTreeMarkets = (RadTreeView)CmbTreeMarkets.Items[0].FindControl("radTreeMarkets");
        //ArrayList nodeCollection = radTreeMarkets.CheckedNodes;
        string strStationID = "";
        string strStationNames = "";
        foreach (RadTreeNode node in radTreeMarkets.CheckedNodes)
        {
            strStationID = getStationsOfMarket(node.Text);
            if (strStationID != "")
            {
                strStations += strStationID.ToString() + ",";
                strStationNames += node.Text + ",";
            }
        }

        return strStations.Substring(0, strStations.Length - 1);
    }

    protected string getStationsOfMarket(string txtMarket)
    {
        string strStationID = "";
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("getStationIDOfMarket", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.CommandTimeout = 0;
        comm.Parameters.AddWithValue("@marketName", txtMarket);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                strStationID += dr["station_id"] + ",";
            }
        }
        else
        {
            strStationID = "";
        }

        conn.Close();

        if (strStationID != "")
        {
            strStationID = strStationID.Substring(0, strStationID.Length - 1);
        }

        return strStationID;

    }


    protected string getStationIDsOfNetwork()
    {
        string strStations = string.Empty;
        var radTreeMarketsForSelectedNetwork = (RadTreeView)CmbTreeMarketsForSelectedNetwork.Items[0].FindControl("radTreeMarketsForSelectedNetwork");
        //ArrayList nodeCollectionMarkets = radTreeMarketsForSelectedNetwork.CheckedNodes;
        string strStationID = "";
        string strStationNames = "";
        string strTempStationID = "";
        foreach (RadTreeNode market in radTreeMarketsForSelectedNetwork.CheckedNodes)
        {
            strTempStationID = getStationsOfNetwork(ddNetworks.SelectedItem.Text, market.Text);
            if (strTempStationID != "")
            {
                strStationNames += market.Text + ",";
                strStationID += strTempStationID + ",";
            }
        }


        return strStationID.Substring(0, strStationID.Length - 1);
    }

    protected string getStationsOfNetwork(string txtNetwork, string txtMarket)
    {
        string strStationID = "";
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("getStationIDOfNetworkAndMarket", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.CommandTimeout = 0;
        comm.Parameters.AddWithValue("@networkName", txtNetwork);
        comm.Parameters.AddWithValue("@marketName", txtMarket);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                strStationID += dr["station_id"].ToString() + ",";
            }
        }
        else
        {
            strStationID = "";
        }

        conn.Close();

        string strReturn = "";
        if (strStationID == "")
        {
            strReturn = strStationID;
        }
        else
        {
            strReturn = strStationID.Substring(0, strStationID.Length - 1);
        }
        return strReturn;

    }

    protected string getStationIDs()
    {
        string strStationIDs = "";

        string strMarkets = "";
        string strNetworks = "";


        bool isNetwork = false;
        bool isMarketOrNetwork = false;

        if (ddSelectBy.Text == "Station")
        {
            strStationIDs = getStationIDs_now();
            isMarketOrNetwork = false;
        }
        else if (ddSelectBy.Text == "Market")
        {
            strStationIDs = getStationIDsOfMarket();
            isMarketOrNetwork = true;
            isNetwork = false;

            strMarkets = "";
        }
        else if (ddSelectBy.Text == "Network")
        {
            isMarketOrNetwork = true;
            isNetwork = true;

            strMarkets = "";
            strNetworks = "";

            strStationIDs = getStationIDsOfNetwork();

        }

        return strStationIDs;
    }

    protected void displayNoRecordFound()
    {
        lblError.Visible = true;
        lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
        dgHierarchical.Visible = false;
        litExportTo.Visible = false;
        btnExportToExcel.Visible = false;
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        var radTreeMarketsForSelectedNetwork = (RadTreeView)CmbTreeMarketsForSelectedNetwork.Items[0].FindControl("radTreeMarketsForSelectedNetwork");
        var radTreeStations = (RadTreeView)CmbTreeStations.Items[0].FindControl("radTreeStations");
        if (ddSelectBy.SelectedItem.Text != "Network")
        {
            ddNetworks.SelectedIndex = -1;
            radTreeMarketsForSelectedNetwork.Nodes.Clear();
        }

        if (ddSelectBy.SelectedItem.Text == "Market" || ddSelectBy.SelectedItem.Text == "Network")
        {
            radTreeStations.CollapseAllNodes();
        }

        bindGrid("[Total Seconds] desc");
    }

    protected void addAuditLog(DateTime startDate, DateTime endDate, string station,
        Boolean exportedToExcel, Boolean exportedToPdf, Boolean exportedToText)
    {
        auditTrail at = new auditTrail();
        at.connectionString = ConfigurationSettings.AppSettings["connectionString"].ToString();
        at.userID = (int)Session["userID"];
        at.IPAddress = Request.ServerVariables["REMOTE_ADDR"];
        at.pageViewed = "Top Songs";
        at.startDate = startDate;
        at.endDate = endDate;
        at.station = station;
        at.exportedToExcel = exportedToExcel;
        at.exportedToPdf = exportedToPdf;
        at.exportedToText = exportedToText;
        at.addUserActivityLogPassingStationIDs();
        at = null;
    }

    protected void bindGrid(string strSortOrder)
    {
        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());

        string strStartDate = dtStartDate.ToString("d");
        string strEndDate = dtEndDate.ToString("d");

        int topNumber;
        if (txt_top.Text.Trim() == "")
        {
            topNumber = 0;
        }
        else
        {
            topNumber = int.Parse(txt_top.Text.Trim());
        }




        try
        {
            string strStationIDs = getStationIDs();

            addAuditLog(dtStartDate, dtEndDate, strStationIDs, false, false, false);

            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
            SqlDataAdapter da = new SqlDataAdapter("topAlbums", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.CommandTimeout = 0;

            da.SelectCommand.Parameters.AddWithValue("@stationIDs", strStationIDs);
            da.SelectCommand.Parameters.AddWithValue("@startDate", strStartDate);
            da.SelectCommand.Parameters.AddWithValue("@endDate", strEndDate);
            da.SelectCommand.Parameters.AddWithValue("@topCount", topNumber);
            da.SelectCommand.Parameters.AddWithValue("@strSortOrder", strSortOrder);
            da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);


            Session["strStationIDs"] = strStationIDs;
            Session["strStartDate"] = strStartDate;
            Session["strEndDate"] = strEndDate;

            DataSet ds = new DataSet();
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count == 0)
            {
                displayNoRecordFound();
                Session["totalRecordCount"] = 0;
            }
            else
            {
                DataRelation dataRel = new DataRelation("album_song", ds.Tables[0].Columns["Movie/Music Album"], ds.Tables[1].Columns["Movie/Music Album"], false);
                ds.Relations.Add(dataRel);

                //DBauer.Web.UI.WebControls.HierarGrid dgHierarchical1 = new DBauer.Web.UI.WebControls.HierarGrid();
                //dgHierarchical.AutoGenerateHierarchy = true;

                //ds.Tables[0].TableName = "Album";
                //ds.Tables[1].TableName = "Songs";

                dgHierarchical.DataSource = ds;             
                dgHierarchical.DataBind();

                dgHierarchical.RowExpanded.CollapseAll();

  
                litExportTo.Visible = true;
                btnExportToExcel.Visible = true;
                dgHierarchical.Visible = true;
                lblError.Visible = false;
                Session["ds"] = ds;
                Session["totalRecordCount"] = ds.Tables[0].Rows.Count;

              //  dgHierarchical.RowExpanded.CollapseAll();
            }
        }
        catch
        {
            displayNoRecordFound();
        }
    }

    protected void dgHierarchical_TemplateSelection(object sender, DBauer.Web.UI.WebControls.HierarGridTemplateSelectionEventArgs e)
    {
        e.TemplateFilename = "TopAlbumSongs.ascx";
    }    

    protected string getHeaderIfMarketsSelected()
    {
        string strMarkets = string.Empty;
        var radTreeMarkets = (RadTreeView)CmbTreeMarkets.Items[0].FindControl("radTreeMarkets");
        //ArrayList nodeCollection = radTreeMarkets.CheckedNodes;

        foreach (RadTreeNode node in radTreeMarkets.CheckedNodes)
        {
            if (node.Parent != null)
            {
                strMarkets += node.Text + ",";
            }
        }
        return strMarkets.Substring(0, strMarkets.Length - 1) + " market/s";
    }

    protected string getHeaderIfNetworkSelected()
    {
        string str = "";

        string strNetworks = string.Empty;
        string strMarkets = string.Empty;

        var radTreeMarketsForSelectedNetwork = (RadTreeView)CmbTreeMarketsForSelectedNetwork.Items[0].FindControl("radTreeMarketsForSelectedNetwork");
        //ArrayList nodeCollectionMarkets = radTreeMarketsForSelectedNetwork.CheckedNodes;

        strNetworks = ddNetworks.SelectedItem.Text + ",";

        str = strNetworks.Substring(0, strNetworks.Length - 1); ;
        str = str + " network";

        foreach (RadTreeNode node in radTreeMarketsForSelectedNetwork.CheckedNodes)
        {
            if (node.Parent != null)
            {
                strMarkets += node.Text + ",";
            }
        }

        if (strMarkets != "")
        {
            str = str + " for " + strMarkets.Substring(0, strMarkets.Length - 1) + " market/s";
        }

        return str;
    }

    protected void img_btn_export_to_excel_Click(object sender, EventArgs e)
    {
        //Response.ContentType = "application/vnd.ms-excel";
        //Response.AddHeader("Content-Disposition", "attachment; filename=topSongs.xls");
        //Response.Charset = "";
        //this.EnableViewState = false;
        //System.IO.StringWriter stringWriter = new System.IO.StringWriter();
        //System.Web.UI.HtmlTextWriter htmlWriter = new System.Web.UI.HtmlTextWriter(stringWriter);

        //DataGrid dgExport = new DataGrid();
        //DataSet ds = (DataSet)Session["ds"];
        //DataTable dt = new DataTable("tblExport");
        //dt = ds.Tables[0].Copy();
        //dgExport.DataSource = dt;
        //dgExport.DataBind();
        //dgExport.RenderControl(htmlWriter);
        //Response.Write(stringWriter.ToString());
        //Response.End();

        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());
        string strStationIDs = getStationIDs();
        addAuditLog(dtStartDate, dtEndDate, strStationIDs, true, false, false);

        ExportToExcel();
    }

    public void ExportToExcel()
    {
        string strDateFormat = (string)Session["dateFormat"];

        DataSet ds = (DataSet)Session["ds"];
        DataTable dt = new DataTable("tblExport");
        dt = ds.Tables[0].Copy();



        string str_station_name = "";

        string str_market_name = "";// getMarketOfStation(str_station_name);

        String strStationName = "<BR><BR>Station: " + str_station_name;
        String strMarket = "<BR><BR>Market: " + str_market_name;

        DateTime dt1 = DateTime.Parse(Session["strStartDate"].ToString());
        DateTime dt2 = DateTime.Parse(Session["strEndDate"].ToString());

        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }

        String strDateRange = "<BR><BR>From: " + sDt1 + "  to  " + sDt2;

        ExportToExcel(dt, Response, strStationName, strMarket, strDateRange);
    }

    public void ExportToExcel(DataTable dt, HttpResponse response, String strStationName, string strMarket, String strDateRange)
    {

        //first let's clean up the response.object
        response.Clear();
        response.Charset = "";
        //set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel";
        //create a string writer
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //create an htmltextwriter which uses the stringwriter
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //instantiate a datagrid
        DataGrid dg = new DataGrid();
        //set the datagrid datasource to the dataset passed in
        if (dt == null)
        {
            return;
        }
        else if (dt.Rows.Count == 0)
        {
            return;
        }
        dg.DataSource = dt;
        //bind the datagrid
        dg.DataBind();

        dg.RenderControl(htmlWrite);


        String str = stringWrite.ToString();

        int start, end;
        start = 0;
        end = 0;

        int colspan = 0;

        colspan = dt.Columns.Count;


        String strReportName = "";
        if (ddSelectBy.Text == "Station")
        {
            strReportName = getHeaderIfStationsSelected() + " AirCheck India Top Songs Report.";
        }
        else if (ddSelectBy.Text == "Market")
        {
            strReportName = "AirCheck India Top Songs Report " + getHeaderIfMarketsSelected();
        }
        else if (ddSelectBy.Text == "Network")
        {
            strReportName = "AirCheck India Top Songs Report " + getHeaderIfNetworkSelected();
        }


        string strFinal = "<tr><td align=center colspan=" + colspan + " style='background-color:#FFFF66;'>" + strReportName + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strDateRange + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + ">&nbsp;</td></tr>";



        while (str.IndexOf("<select name=") > 0)
        {
            start = str.IndexOf("<select name=");
            end = str.IndexOf("</select>") + 9;
            str = str.Replace(str.Substring(start, end - start), "");
        }

        string strAirTime = "";
        string strPlayTime = "";
        string strNewPlayTime = "";

        int intCount = 0;

        foreach (DataRow dr in dt.Rows)
        {
            strAirTime = dr[0].ToString();
            if (strAirTime == "")
            {
                strPlayTime = dr[2].ToString();
                strNewPlayTime = dr[2].ToString() + " ";

                str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=5 align='Center'>" + strNewPlayTime + "</td></tr>");

            }
            else
            {
                intCount = intCount + 1;
            }
        }


        str = strFinal + "<BR><BR>" + str;


        string strDateFormat = (string)Session["dateFormat"];

        DateTime dtDate = System.DateTime.Now;
        string strDate = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dtDate.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dtDate.ToString("MM/dd/yyyy");
        }


        string strTime = System.DateTime.Now.ToString();
        strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


        string strCountText = "<b>" + intCount + " rows </b>";

        string strFooterText = "";

        strFooterText = "<BR><table>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>This data was generated and exported on " + strDate + " at " + strTime + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>" + strCountText + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><BR></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.</td></tr>";
        strFooterText += "</table>";

        str = "<table>" + str + strFooterText;



        response.Write(str);
        response.End();
    }

    protected string doStringSanitation(string str)
    {
        if (str.IndexOf("'") >= 0)
        {
            str = str.Replace("'", "123123");
        }
        if (str.IndexOf("&") >= 0)
        {
            str = str.Replace("&", "456456");
        }

        return str;
    }

    protected string getHeaderIfStationsSelected()
    {
        string strStations = string.Empty;
        var radTreeStations = (RadTreeView)CmbTreeStations.Items[0].FindControl("radTreeStations");
        //ArrayList nodeCollection = radTreeStations.CheckedNodes;

        foreach (RadTreeNode node in radTreeStations.CheckedNodes)
        {
            if (node.Parent != null)
            {
                strStations += node.Text + ",";
            }
        }
        return strStations.Substring(0, strStations.Length - 1) + " station/s";
    }

    protected void dgHierarchical_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        //string headerFontName = (string)Session["headerFontName"];// ConfigurationSettings.AppSettings["headerFontNameTA"].ToString();
        //string headerFontSize = (string)Session["headerFontSize"];//ConfigurationSettings.AppSettings["headerFontSizeTA"].ToString();

        //string actualDataFontName = (string)Session["dataFontName"];//ConfigurationSettings.AppSettings["actualDataFontNameTA"].ToString();
        //string actualDataFontSize = (string)Session["dataFontSize"];//ConfigurationSettings.AppSettings["actualDataFontSizeTA"].ToString();

        string headerFontName =  ConfigurationSettings.AppSettings["headerFontNameTA"].ToString();
        string headerFontSize = ConfigurationSettings.AppSettings["headerFontSizeTA"].ToString();

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "12px";

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");

            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                //e.Item.Style.Add("font-weight", "bold");
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }


            //e.Item.Style.Add("font-size", headerFontSize);
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
            for (int i = 0; i < e.Item.Cells.Count; i++)
            {
                e.Item.Cells[i].Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
                e.Item.Cells[i].Style.Add("font-family", headerFontName);
            }
        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            //e.Item.Style.Add("font-weight", "bold");
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                //e.Item.Style.Add("font-weight", "bold");
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            //e.Item.Style.Add("font-size", actualDataFontSize);
            for (int ii = 0; ii < e.Item.Cells.Count; ii++)
            {
                e.Item.Cells[ii].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            }
            for (int i = 0; i < e.Item.Cells.Count; i++)
            {
                bool isANumber = false;
                int intNum;
                isANumber = int.TryParse(e.Item.Cells[i].Text, out intNum);
                if (isANumber)
                {
                    e.Item.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                }
            }
        }

        if (e.Item.ItemType == ListItemType.AlternatingItem)
        {
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);

            //e.Item.BackColor = System.Drawing.Color.FromArgb(149, 179, 215);
        }
    }
    protected void dgHierarchical_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        bindGrid("[" + e.SortExpression + "] asc");
    }
    
}
