﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class ListOfCurrentSongsRepetition_exclusive : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DateTime dtStartDate = DateTime.Parse(Request.QueryString["startDate"]);
        DateTime dtEndDate = DateTime.Parse(Request.QueryString["endDate"]);

        string strStartDate = getFormattedDate(dtStartDate);
        string strEndDate = getFormattedDate(dtEndDate);


        label_Date_Range.Text = strStartDate + " to " + strEndDate;

        string strStationName, strMarketName;
        int station_id;
        getStationAndMarketNames(out station_id, out strStationName, out strMarketName);
        label_station.Text = strStationName;
        label_market.Text = strMarketName;

        if (Session["dataReadOnly"].ToString() == "True")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "HideOutIcons", "HideIcons();", true);
        }

        bindGrid();
    }

    protected string getFormattedDate(DateTime dt)
    {
        string strDate = "";
        string strDateFormat = (string)Session["dateFormat"];
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dt.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dt.ToString("MM/dd/yyyy");
        }
        return strDate;
    }

    protected void bindGrid()
    {
        string searchText = Session["searchText"].ToString();

        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("ListOfCurrentSongsRepetitionExclusive", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@stationIDs", Request.QueryString["strStationIDs"]);
        da.SelectCommand.Parameters.AddWithValue("@startDate", Request.QueryString["startDate"]);
        da.SelectCommand.Parameters.AddWithValue("@endDate", Request.QueryString["endDate"]);
        da.SelectCommand.Parameters.AddWithValue("@stationName", Request.QueryString["strStationName"]);
        da.SelectCommand.Parameters.AddWithValue("@searchText", searchText);

        DataSet ds = new DataSet();
        da.Fill(ds);

        Session["myDataTableExportExclusive"] = ds.Tables[0];

        datagrid_disp_details.DataSource = ds;
        datagrid_disp_details.DataBind();
    }

    protected void getStationAndMarketNames(out int station_id, out string strStationName, out string strMarketName)
    {
        station_id = 0;
        strStationName = "";
        strMarketName = "";

        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("SELECT station_id,[name] AS 'Station',(SELECT [name] FROM aircheck..Markets WHERE market_id=Stations.market_id) AS 'Market' FROM Aircheck..Stations WHERE name = @stationPassed", conn);
        comm.Parameters.AddWithValue("@stationPassed", Request.QueryString["strStationName"].Trim());
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                station_id = int.Parse(dr["station_id"].ToString());
                strStationName = dr["Station"].ToString();
                strMarketName = dr["Market"].ToString();
            }
        }
        strStationName = strStationName.Replace(strMarketName.ToUpper(), "");
        conn.Close();
    }

    protected void datagrid_disp_details_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "16px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "12px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Cells[0].HorizontalAlign = HorizontalAlign.Left;
            e.Item.Cells[1].HorizontalAlign = HorizontalAlign.Left;
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }
        }


    }

    protected void img_btn_export_to_excel_Click(object sender, ImageClickEventArgs e)
    {
        //Response.ContentType = "application/vnd.ms-excel";
        //Response.AddHeader("Content-Disposition", "attachment; filename=ListOfCurrentSongsRepetition.xls");
        //Response.Charset = "";
        //this.EnableViewState = false;
        //System.IO.StringWriter stringWriter = new System.IO.StringWriter();
        //System.Web.UI.HtmlTextWriter htmlWriter = new System.Web.UI.HtmlTextWriter(stringWriter);

        //DataGrid dgExport = new DataGrid();
        //dgExport.DataSource = (DataTable)Session["data"];
        //dgExport.DataBind();

        //dgExport.RenderControl(htmlWriter);
        //Response.Write(stringWriter.ToString());
        //Response.End();
        ExportToExcel();
    }

    protected DataTable getDataTableToExport()
    {
        string strDateFormat = (string)Session["dateFormat"];
        DataTable dt = new DataTable();
        dt = (DataTable)Session["myDataTableExportExclusive"];


        return dt;
    }

    public void ExportToExcel()
    {
        string strDateFormat = (string)Session["dateFormat"];
        DataTable dt = getDataTableToExport();


        string str_station_name = label_station.Text;

        string str_market_name = label_market.Text;

        String strStationName = "<BR><BR>Station: " + str_station_name;
        String strMarket = "<BR><BR>Market: " + str_market_name;

        DateTime dt1 = DateTime.Parse(Request.QueryString["startDate"]);
        DateTime dt2 = DateTime.Parse(Request.QueryString["endDate"]);

        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }

        String strDateRange = "<BR><BR>From: " + sDt1 + "  to  " + sDt2;

        ExportToExcel(dt, Response, strStationName, strMarket, strDateRange);
    }

    public void ExportToExcel(DataTable dt, HttpResponse response, String strStationName, string strMarket, String strDateRange)
    {

        //first let's clean up the response.object
        response.Clear();
        response.Charset = "";
        //set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel";

        //create a string writer
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //create an htmltextwriter which uses the stringwriter
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //instantiate a datagrid
        DataGrid dg = new DataGrid();
        //set the datagrid datasource to the dataset passed in
        if (dt == null)
        {
            return;
        }
        else if (dt.Rows.Count == 0)
        {
            return;
        }
        dg.DataSource = dt;
        //bind the datagrid
        dg.DataBind();

        dg.RenderControl(htmlWrite);


        String str = stringWrite.ToString();

        int start, end;
        start = 0;
        end = 0;

        int colspan = 0;

        colspan = dt.Columns.Count;


        String strReportName = "Aircheck List Of Current Songs and Repetition Exclusive Report";



        string strFinal = "<tr><td align=center colspan=" + colspan + " style='background-color:#FFFF66;'>" + strReportName + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strStationName + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strMarket + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strDateRange + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + ">&nbsp;</td></tr>";



        while (str.IndexOf("<select name=") > 0)
        {
            start = str.IndexOf("<select name=");
            end = str.IndexOf("</select>") + 9;
            str = str.Replace(str.Substring(start, end - start), "");
        }
        //str = str.Replace("<tr>\r\n\t\t<td>&nbsp;</td><td>&nbsp;</td>", "<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td>");

        string strAirTime = "";
        string strPlayTime = "";
        string strNewPlayTime = "";

        int intCount = 0;

        foreach (DataRow dr in dt.Rows)
        {
            strAirTime = dr[0].ToString();
            if (strAirTime == "")
            {
                strPlayTime = dr[2].ToString();
                strNewPlayTime = dr[2].ToString() + " ";

                if ((string)Session["category"] == "Song")
                {
                    str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=4 align='Center'>" + strNewPlayTime + "</td></tr>");
                }
                else
                {
                    str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=5 align='Center'>" + strNewPlayTime + "</td></tr>");
                }
            }
            else
            {
                intCount = intCount + 1;
            }
        }


        str = strFinal + "<BR><BR>" + str;


        string strDateFormat = (string)Session["dateFormat"];

        DateTime dtDate = System.DateTime.Now;
        string strDate = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dtDate.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dtDate.ToString("MM/dd/yyyy");
        }


        string strTime = System.DateTime.Now.ToString();
        strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


        string strCountText = "<b>" + intCount + " rows </b>";

        string strFooterText = "";

        strFooterText = "<BR><table>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>This data was generated and exported on " + strDate + " at " + strTime + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>" + strCountText + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><BR></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.</td></tr>";
        strFooterText += "</table>";

        str = "<table>" + str + strFooterText;



        response.Write(str);
        response.End();
    }
}
