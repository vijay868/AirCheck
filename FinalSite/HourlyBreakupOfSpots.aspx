﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="HourlyBreakupOfSpots.aspx.cs" Inherits="HourlyBreakupOfSpots" Title="Hourly Breakup Of Spots" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        
    </style>
    <script type="text/javascript" language="javascript">
        function OnClientDropDownOpenedHandler1(sender, eventArgs) {
            var tree = sender.get_items().getItem(0).findControl("radTreeStations");

            if (!!tree) {
                var selectedNode = tree.get_selectedNode();
                if (selectedNode) {
                    selectedNode.scrollIntoView();
                }
            }
        }
        function nodeCheckedTreeStations(sender, args) {

            var comboBox = $find("<%= CmbTreeStations.ClientID %>");


            var tree = comboBox.get_items().getItem(0).findControl("radTreeStations");
            var node = args.get_node();
            var rootNodeSelCount = 0;
            var nodesJ = tree.get_nodes();
            var marketsval = "";
            for (i = 0; i < nodesJ.get_count(); i++) {
                if (nodesJ.getNode(i).get_checked()) {
                    //alert(nodesJ.getNode(i).get_text());
                    marketsval = marketsval + nodesJ.getNode(i).get_text().toString() + ",";
                    rootNodeSelCount = rootNodeSelCount + 1;
                }
            }


            //check if 'Select All' node has been checked/unchecked
            var tempNode = args.get_node();
            if (tempNode.get_text().toString() == "(Select All)") {
                // check or uncheck all the nodes
            } else {
                var nodes = new Array();
                nodes = sender.get_checkedNodes();
                var vals = "";
                var i = 0;
                var icount = 0;

                for (i = 0; i < nodes.length; i++) {
                    var n = nodes[i];
                    var nodeText = n.get_text().toString();
                    if (nodeText != "(Select All)") {
                        if (n._hasChildren() == true) {
                        } else {
                            vals = vals + n.get_text().toString() + ",";
                        }
                    }
                }

                //prevent  combo from closing
                //  supressDropDownClosing = true;
                comboBox.set_text(marketsval);

                // getrootcount(sender, args);
            }
        }
        function OnClientClicked(button, args) {
            if (calcDays()) {                
                button.set_autoPostBack(true);
            }
            else {
                button.set_autoPostBack(false);
            }
        }
        function calcDays()
        {
            ToggleCollapsePane();
            var startDate =$find("<%=dtStart.ClientID %>");
            var sDate = startDate.get_selectedDate();

            
            var lockDataBefore="<%=Session["lockDataBefore"]%>";
            if(sDate == null)
            {
                lockDataBefore = new Date(lockDataBefore);
                var curr_date = lockDataBefore.getDate();
                var curr_month = lockDataBefore.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataBefore.getFullYear();
                lockDataBefore= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights only from " + lockDataBefore);
                return false;
            }
                    
            //tdhide('leftMenu', 'ctl00_showHide','hide');
            
            var comboBox = $find("<%= CmbTreeStations.ClientID %>");    
            var treeS = comboBox.get_items().getItem(0).findControl("radTreeStations");
            
            var rootNodesS = treeS.SelectedNode;
            var nodesS = treeS.get_allNodes();

            var iS;
            var isStationSelected = 0;
             for(iS=0; iS<nodesS.length; iS++)
             {
                if(!nodesS[iS].get_checked())
                {
                
                }
                else
                {
                    isStationSelected = 1;
                }
             }   

            if (isStationSelected == 0 && valSelectBy == "Station")
            {
                alert("You have not selected any station/s");
                return false;
            }
            else
            {
                    disp_clock();
                    return true;
            }         
        }  
		function disp_clock()
		{
		    var lblError = document.getElementById("ctl00_content1_lblError");
		    if (lblError != null)
		    {
		        lblError.style.display='none';
		    }
			OnLoad();
			DIV1.style.display='none';
			divWait.style.display='';
    	}
    	function OnLoad() 
		{ 
		    setTimeout("StartAnimation()", 500); 
		} 

		function StartAnimation() 
		{ 
		    if (document.images)
		    {
		        document['ProgressBarImg'].src = "images/aniClock.gif";
		    }
		}
		function ToggleCollapsePane() {               
               var splitter = $find("<%= Page.Master.FindControl("RadSplitter1").ClientID %>");
               var pane = splitter.getPaneById("<%= Page.Master.FindControl("RadPane1").ClientID %>");
                pane.collapse();
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">    
<div style="width: 99.7%; height: 100%; margin-left: 4px">
        <div>
            <table width="100%" background="images/contentheader_bgr.gif" border="0" cellpadding="6px"
                cellspacing="0px" style="margin-left: -5px; margin-right: 300px;">
                <tr>
                    <td align="left">
                        ::
                        <asp:Label ID="Pg_Title" runat="server" Font-Bold="True" ForeColor="#000099"></asp:Label>&nbsp;::
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        <asp:Label ID="lbl_Logout" runat="server" Text="Welcome: Administrator[Logout]" Font-Bold="True"
                            ForeColor="#000099"></asp:Label>
                    </td>
                </tr>
            </table>
    <table cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td class="style1">
                Select Date
            </td>
            <td class="style1">
                Select station
            </td>
            <td class="style1">
                &nbsp;
            </td>
        </tr>
        <tr valign="top">
            <td class="style1">
                &nbsp;<telerik:RadDatePicker ID="dtStart" runat="server" Culture="English (United States)"
                            MinDate="2004-01-01">
                            <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                            </Calendar>
                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                            <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                            </DateInput>
                        </telerik:RadDatePicker>
            </td>
            <td class="style1">                
                <telerik:RadComboBox ID="CmbTreeStations" runat="server" AfterClientCheck="AfterCheckHandler"
                        CollapseAnimation-Type="None" EmptyMessage="Choose Stations" ExpandAnimation-Type="None"
                        OnClientDropDownOpened="OnClientDropDownOpenedHandler1" ShowToggleImage="True"
                        Style="vertical-align: middle;" Width="200px">
                        <ItemTemplate>
                            <div id="div2">
                                <telerik:RadTreeView ID="radTreeStations" runat="server" Height="200px" OnClientNodeChecked="nodeCheckedTreeStations"
                                    CheckBoxes="true" Width="100%"
                                    TriStateCheckBoxes="true">
                                </telerik:RadTreeView>
                            </div>
                        </ItemTemplate>
                        <Items>
                            <telerik:RadComboBoxItem Text="" />
                        </Items>
                    </telerik:RadComboBox>
            </td>
            <td class="style1">   &nbsp;           
              <telerik:RadButton ID="RadButton1" runat="server" Text="Generate Report" OnClientClicked="OnClientClicked"
                        OnClick="btnGenerateReport_Click">
                    </telerik:RadButton>
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlDate" runat="server" Visible="true" Width="100%">
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 100%">
                    <div id="divWait" style="display: none">
                        <table style="height: 310Px" width="100%">
                            <tr>
                                <td align="center" width="100%">
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <img src="images/aniClock.gif" border="0" name="ProgressBarImg"><br>
                                    <font class="date">Acquiring Data... one moment please!</font>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="DIV1" style="width: 100%">
                        <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label>
                        <asp:GridView UseAccessibleHeader="true" EnableViewState="false" ID="gvData" ShowFooter="false"
                            runat="server" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                            Height="200px">
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Literal ID="litExportTo" runat="server" Text="Export to: "></asp:Literal>
                    <asp:Button ID="btnExportToExcel" runat="server" Visible="True" CausesValidation="False"
                        Text="Excel" ForeColor="white" Font-Names="Arial" Font-Bold="true" BackColor="#336699"
                        Font-Size="8" OnClick="img_btn_export_to_excel_Click"></asp:Button>
                </td>
            </tr>
        </table>
        <asp:TextBox ID="txtArray" runat="server" Style="visibility: hidden; height: 0Px"></asp:TextBox>
    </asp:Panel>
    </div>
    </div>
</asp:Content>
