﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="nationalAdvertisersByStationHourDetails.aspx.cs" Inherits="nationalAdvertisersByStationHourDetails"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .gvHeaderRow
        {
            background-image: url("images/dhrudde.png");
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: White;
            text-decoration: none;
            line-height: 12px;
            padding-left: 10px;
            border-left: solid 1px white;
            border-right: solid 1px white;
            border-bottom: solid 1px white;
            position: relative;
        }
    </style>

    <script type="text/javascript" language="javascript">
function ToggleCollapsePane() {               
               var splitter = $find("<%= Page.Master.FindControl("RadSplitter1").ClientID %>");
               var pane = splitter.getPaneById("<%= Page.Master.FindControl("RadPane1").ClientID %>");
                pane.collapse();
    }
        function disp_clock() {
            var lblError = document.getElementById("ctl00_content1_lblError");
            if (lblError != null) {
                lblError.style.display = 'none';
            }
            OnLoad();
            DIV1.style.display = 'none';
            divWait.style.display = '';
        }

        function OnLoad() {
            setTimeout("StartAnimation()", 500);
        }

        function StartAnimation() {
            if (document.images) {
                document['ProgressBarImg'].src = "images/aniClock.gif";
            }
        }
	
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div style="width: 99.1%; height: 100%; margin-left: 4px">
        <div>
            <table width="100%" background="images/contentheader_bgr.gif" border="0" cellpadding="6px"
                cellspacing="0px" style="margin-left: -5px; margin-right: 300px;">
                <tr>
                    <td align="left">
                        ::
                        <asp:Label ID="Pg_Title" runat="server" Font-Bold="True" ForeColor="#000099"></asp:Label>&nbsp;::
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        <asp:Label ID="lbl_Logout" runat="server" Text="Welcome: Administrator[Logout]" Font-Bold="True"
                            ForeColor="#000099"></asp:Label>
                    </td>
                </tr>
            </table>            
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <br />
            </table>
            <br />
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="left" style="width: 100%">                        
                        <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label>
                        <div id="DIV1">
                            <telerik:RadGrid ID="radData" runat="server" Visible="false" Width="55%" AutoGenerateColumns="true"
                                CellSpacing="0" GridLines="None" Skin="Web20" 
                                OnSortCommand="radData_SortCommand">
                                <MasterTableView Width="100%" GridLines="Horizontal">
                                    <RowIndicatorColumn Visible="False">
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn Visible="False" Resizable="False">
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </ExpandCollapseColumn>
                                </MasterTableView>
                                <AlternatingItemStyle Font-Bold="true" Font-Names="Helvetica" />
                                <HeaderStyle Font-Names="Helvetica" Font-Size="12pt" />
                                <ItemStyle Font-Bold="true" />
                            </telerik:RadGrid>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <asp:Literal ID="litExportTo" Visible="false" runat="server" Text="Export to: "></asp:Literal>
                        <telerik:RadButton ID="btnExportToExcel" runat="server" CausesValidation="False"
                            Text="Excel" OnClick="img_btn_export_to_excel_Click">
                        </telerik:RadButton>
                        <telerik:RadButton ID="btnExportToPdf" runat="server" CausesValidation="False" Text="Pdf"
                            OnClick="btnExportToPdf_Click">
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>          

        </div>
    </div>
</asp:Content>
