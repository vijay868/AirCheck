﻿using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;
using System.Web.Security;
using System.Web.Caching;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //FormsAuthentication.SignOut();
        //try
        //{
        //    string strConCat = (String)Session["UserDetails"];
        //    string strUser = (string)Cache[strConCat];
        //    Cache.Remove(strUser);
        //}
        //catch { }
    }

    protected void addAuditLog()
    {
        auditTrail at = new auditTrail();
        at.connectionString = ConfigurationSettings.AppSettings["connectionString"].ToString();
        at.userID = (int)Session["userID"];
        at.IPAddress = Request.ServerVariables["REMOTE_ADDR"];
        at.pageViewed = "Logged In";
        at.startDate = DateTime.Parse("1/1/1900");
        at.endDate = DateTime.Parse("1/1/1900");
        at.market = "";
        at.station = "";
        at.exportedToExcel = false;
        at.exportedToPdf = false;
        at.exportedToText = false;
        at.addUserActivityLog();
        at = null;
    }


    protected void btnHome_Click(object sender, EventArgs e)
    {
        string strHome = ConfigurationSettings.AppSettings["Server"].ToString();
        Response.Redirect(strHome);
    }

    protected void btnForgotPassword_Click(object sender, EventArgs e)
    {
        if (txtUsername.Text == "")
        {
            lblMessage.Text = "Enter the User Name to change the password";
        }
        else
        {
            lblMessage.Text = "";
            string strTO = "aircheck@rcs.in";
            string strCC = "";
            forgotPasswordCCForUser(txtUsername.Text, out strCC);
            if (strCC == "")
            {
                lblMessage.Text = "No such Username found.";
            }
            else
            {
                string str = "mailto:" + strTO + "?CC=" + strCC +"&subject=Forgot password: " + txtUsername.Text;
                str = "<script language='javascript' type='text/javascript'>window.open('" + str + "' );</script>";
                Page.RegisterStartupScript("email", str);
            }
        }
    }

    protected void forgotPasswordCCForUser(string strUserName, out string strCC)
    {
        strCC = "";

        using (SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]))
        {
            using (SqlCommand comm = new SqlCommand("forgotPasswordCCForUser", conn))
            {
                comm.CommandType = CommandType.StoredProcedure;

                comm.Parameters.AddWithValue("@strUserName", strUserName);

                try
                {
                    conn.Open();
                    SqlDataReader dr = comm.ExecuteReader();
                    while (dr.Read())
                    {
                        string primaryContactPersonEmailID = dr["primaryContactPersonEmailID"].ToString();
                        string secondaryContactPersonEmailID = dr["secondaryContactPersonEmailID"].ToString();

                        if (primaryContactPersonEmailID == "" && secondaryContactPersonEmailID == "")
                        {
                        }
                        else if (primaryContactPersonEmailID != "" && secondaryContactPersonEmailID == "")
                        {
                            strCC = primaryContactPersonEmailID;
                        }
                        else if (primaryContactPersonEmailID == "" && secondaryContactPersonEmailID != "")
                        {
                            strCC = secondaryContactPersonEmailID;
                        }
                        else if (primaryContactPersonEmailID != "" && secondaryContactPersonEmailID != "")
                        {
                            strCC = primaryContactPersonEmailID + ";" + secondaryContactPersonEmailID;
                        }
                    }
                    conn.Close();
                }
                catch (Exception ex)
                {
                    lblMessage.Text  = ex.Message;
                }
            }
        }
    }

    protected void setAuthenticationTicket(string strUserData, DateTime expiry_date)
    {
        //FormsAuthenticationTicket objTicket = default(FormsAuthenticationTicket);
        //HttpCookie objCookie = default(HttpCookie);

        //objTicket = new FormsAuthenticationTicket(1, txtUsername.Text, System.DateTime.Now, System.DateTime.Now.AddYears(100), chkRemember.Checked, strUserDate);
        //objCookie = new HttpCookie("myCookie");
        //objCookie.Value = FormsAuthentication.Encrypt(objTicket);
        //if (chkRemember.Checked)
        //{
        //    objCookie.Expires = objTicket.Expiration;
        //}
        //Response.Cookies.Add(objCookie);

        FormsAuthenticationTicket Authticket = new FormsAuthenticationTicket(
                                                        1,
                                                        txtUsername.Text,
                                                        DateTime.Now,
                                                        DateTime.Now.AddMinutes(30),
                                                        true,
                                                        strUserData,
                                                        FormsAuthentication.FormsCookiePath);

        string hash = FormsAuthentication.Encrypt(Authticket);
        HttpCookie Authcookie = new HttpCookie(FormsAuthentication.FormsCookieName, hash);
        //if (Authticket.IsPersistent)
        //{
        Authcookie.Expires = Authticket.Expiration;
        //}
        //else
        //{
        //    Authcookie.Expires = DateTime.Now.AddMinutes(30);
        //}
        Response.Cookies.Add(Authcookie);

    }

   protected void Button1_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtPassword.Text.Trim()))
        {
            Session["userDetails"] = null;
            lblMessage.Text = "Enter your password.";
            return;
        }

    if (IsValid) {
        
        SqlConnection conn = default(SqlConnection);
        SqlCommand comm = default(SqlCommand);
        SqlDataReader dr = default(SqlDataReader);
              
        conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["connectionString"]);
        conn.Open();
        //comm = new SqlCommand("select * from CompanyUsers where username='" + txtUsername.Text.Trim() + "' and password='" + txtPassword.Text.Trim() + "'", conn);
        //comm = new SqlCommand("select *,UserType=(SELECT UserType=case when count(*)>0 then 'Admin' else 'User' end FROM user_company_report_permission WHERE userCompanyID=c.userid AND isUser = 1 and reportmapid in(select reportmapid from reportmap where reportid >(select reportid from reports where reportname like 'National%'))) from CompanyUsers c where username='" + txtUsername.Text.Trim() + "' and password='" + txtPassword.Text.Trim() + "'", conn);
        comm = new SqlCommand("select *,UserType=(SELECT UserType=case when count(*)>0 then 'Admin' else 'User' end FROM user_company_report_permission WHERE userCompanyID=c.userid AND isUser = 1 and reportmapid in(select reportmapid from reportmap where reportid >(select reportid from reports where reportname like 'Access to Only View Data%'))),ReadOnly=(select ReadOnly=case when count(*)>0 then 'True' else 'False' end FROM user_company_report_permission WHERE userCompanyID=c.userid AND isUser = 1 and reportmapid in(select reportmapid from reportmap where reportid =(select reportid from reports where reportname like 'Access to Only View Data'))) from CompanyUsers c where username='" + txtUsername.Text.Trim() + "' and password='" + txtPassword.Text.Trim() + "'", conn);
        
        dr = comm.ExecuteReader();
        if (dr.Read()) {
            
            DateTime today_date = System.DateTime.Now.Date;
            DateTime expiry_date = DateTime.Parse(dr["loginExpiryDate"].ToString());
            DateTime lockDataBefore = DateTime.Parse(dr["lockDataBefore"].ToString());
            DateTime lockDataAfter = DateTime.Parse(dr["lockDataAfter"].ToString());
            Session["userType"] = dr["UserType"].ToString();
            Session["dataReadOnly"] = dr["ReadOnly"].ToString();

            if (DateTime.Compare(expiry_date, today_date) < 0) {
                lblMessage.Text = "Your account has expired. Please contact " + System.Configuration.ConfigurationManager.AppSettings["expiration1"] + " for assistance.<BR><BR>Email: <a href='mailto:" + System.Configuration.ConfigurationManager.AppSettings["expiration2"] + "'><font color=red>" + System.Configuration.ConfigurationManager.AppSettings["expiration2"] + "</font></a>";
            }
            else {

                string strConCat = null;
                string strUser = null;
                
                strConCat = txtUsername.Text;
                strUser = (string)Cache[strConCat];


                //if (string.IsNullOrEmpty(strUser))
                //{

                    string strUserDate = null;

                    TimeSpan SessTimeOut = default(TimeSpan);
                    int minutes = int.Parse(System.Configuration.ConfigurationManager.AppSettings["minutes"].ToString());
                    SessTimeOut = new TimeSpan(0, 0, minutes, 0, 0);
                    Cache.Insert(strConCat, strConCat, null, DateTime.MaxValue, SessTimeOut, CacheItemPriority.NotRemovable, null);

                    int userID = 0;

                    if (int.TryParse(dr["userID"].ToString(), out userID))
                    {
                        Session["userID"] = userID;
                        
                    }

                    Session["UserDetails"] = strConCat;
                    if (dr["firstName"] == System.DBNull.Value) {
                        strUserDate = txtUsername.Text;
                    }
                    else if (string.IsNullOrEmpty(dr["firstName"].ToString())) {
                        strUserDate = txtUsername.Text;
                    }
                    else {
                        strUserDate = dr["firstName"] + " " + dr["lastName"];
                    }

                    //setAuthenticationTicket(strUserDate, expiry_date);

                    Session["user"] = txtUsername.Text.Trim();
                    Session["lockDataBefore"] = lockDataBefore;
                    Session["lockDataAfter"] = lockDataAfter;

                    addAuditLog();

                    FormsAuthentication.RedirectFromLoginPage(txtUsername.Text, false);
                    Response.Redirect("DetectScreen.aspx");
               //}

               //else
               //{
               //    lblMessage.Text = "Duplicate login not allowed! User " + strConCat + " is already logged in.";
               //}
            }
        }
        else {
            Session["userDetails"] = null;
            lblMessage.Text = "Bad username/password!";
        }
    }
 
    }
}
