﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="topAdvertisersDetails.aspx.cs" Inherits="topAdvertisersDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Top Advertisers - Spot Placement Details</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0" />
    <meta name="CODE_LANGUAGE" content="Visual Basic 7.0" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="css/report_caption.css" rel="stylesheet" />
    <style type="text/css">
        .style1
        {
            font-family: verdana, arial, sans-serif, Helvetica;
            font-size: 11px;
            font-weight: bold;
            color: #595F47;
            width: 17%;
        }
    </style>
    <style type="text/css">
      .style5
        {
            font-family: Helvetica;
            font-size: 14px;
           
            color: grey;
        }
        .style6
        {
            font-family: Helvetica;
            font-size: 14px;
            color: Grey;
            width: 70px;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function HideIcons() {
            prn.style.display = 'none';
        }
    </script>

</head>
<body leftmargin="0" rightmargin="0" topmargin="0">
    <form id="Form1" method="post" runat="server">
    <table height="53" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="top" align="top" width="760" height="53" border="0">
                <img src="images/bgTop.gif">
            </td>
            <td valign="top" width="100%">
                <img height="28" src="images/bgTopExt.gif" width="100%" border="0">
            </td>
        </tr>
    </table>
    <span name="prn" id="prn">
        <table height="0" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td width="100%" align="right" colspan="2">
                    <a href="#" onclick="prn.style.display='none';print();">
                        <img src="images/icon_print.gif" border="0"></a>&nbsp;&nbsp;
                </td>
            </tr>
        </table>
    </span>
    <table width="100%">
        <tr>
            <td class="style6">
                Date Range
            </td>
            <td class="date">
                :
            </td>
            <td >
                <asp:Label ID="label_Date_Range" runat="server" CssClass="style5"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style6">
                Brand
            </td>
            <td class="date">
                :
            </td>
            <td >
                <asp:Label ID="lblBrand" runat="server" CssClass="style5"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style6">
                Advertiser
            </td>
            <td class="date">
                :
            </td>
            <td >
                <asp:Label ID="lblAdvertiser" runat="server" CssClass="style5"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style6">
                Category
            </td>
            <td class="date">
                :
            </td>
            <td >
                <asp:Label ID="lblCategory" runat="server" CssClass="style5"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style6">
                Duration
            </td>
            <td class="date">
                :
            </td>
            <td >
                <asp:Label ID="lblDuration" runat="server" CssClass="style5"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style6">
                Total No. Of Plays
            </td>
            <td class="date">
                :
            </td>
            <td >
                <asp:Label ID="lblTotalNoOfPlays" runat="server" CssClass="style5"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style6">
                Total No. Of Seconds
            </td>
            <td class="date">
                :
            </td>
            <td >
                <asp:Label ID="lblTotalNoOfSeconds" runat="server" CssClass="style5"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="date" width="100%" colspan="3">
                &nbsp;
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td width="100%">
                <br>
                <asp:Panel ID="Panel1" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station1" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station1" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station1" runat="server" OnItemDataBound="datagrid_station1_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel2" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station2" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station2" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station2" runat="server" OnItemDataBound="datagrid_station2_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel3" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station3" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station3" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station3" runat="server" OnItemDataBound="datagrid_station3_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel4" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station4" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station4" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station4" runat="server" OnItemDataBound="datagrid_station4_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel5" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station5" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station5" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station5" runat="server" OnItemDataBound="datagrid_station5_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel6" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station6" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station6" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station6" runat="server" OnItemDataBound="datagrid_station6_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel7" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station7" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station7" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station7" runat="server" OnItemDataBound="datagrid_station7_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel8" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station8" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station8" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station8" runat="server" OnItemDataBound="datagrid_station8_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel9" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station9" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station9" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station9" runat="server" OnItemDataBound="datagrid_station9_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel10" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station10" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station10" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station10" runat="server" OnItemDataBound="datagrid_station10_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel11" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station11" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station11" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station11" runat="server" OnItemDataBound="datagrid_station11_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel12" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station12" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station12" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station12" runat="server" OnItemDataBound="datagrid_station12_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel13" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station13" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station13" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station13" runat="server" OnItemDataBound="datagrid_station13_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel14" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station14" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station14" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station14" runat="server" OnItemDataBound="datagrid_station14_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel15" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station15" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station15" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station15" runat="server" OnItemDataBound="datagrid_station15_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel16" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station16" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station16" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station16" runat="server" OnItemDataBound="datagrid_station16_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel17" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station17" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station17" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station17" runat="server" OnItemDataBound="datagrid_station17_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br>
                <asp:Panel ID="Panel18" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="label_station18" runat="server" CssClass="date"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="label_no_station18" runat="server" CssClass="date" Visible="False"
                                    BackColor="#ffffcc" Width="200Px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">
												No Matches Found</font></asp:Label>
                                <asp:DataGrid ID="datagrid_station18" runat="server" OnItemDataBound="datagrid_station18_ItemDataBound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                            DataField="Station" HeaderText="Station"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="No of Plays" HeaderText="No of Plays"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds" HeaderText="Total Seconds"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Plays on Each Market" HeaderText="Total Plays on Each Market">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Total Seconds on Each Market" HeaderText="Total Seconds on Each Market">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td width="100%">
                &nbsp;
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
