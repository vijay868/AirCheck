﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;


public partial class categoryAnalysisRevisedChartInner : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["stCount"] == null)
        {
            return;
        }

        if (Session["stCount"].ToString() == "")
        {
            return;
        }

        fillArray();

        int intLen = int.Parse(Session["stCount"].ToString());
        int[] arrValues = new int[intLen];
        string[] arrValueNames = new string[intLen];

        int[] arr_total = new int[intLen];
        string[] arr_selected_station_names = new string[intLen];

        if (Request.QueryString["isExclusive"] == "0")
        {
            arr_total = (int[])Session["arr_total"];
        }
        else
        {
            arr_total = (int[])Session["arr_total_exclusive"];
        }

        arr_selected_station_names = (string[])Session["arr_selected_station_names"];
        int i;

        for (i = 0; i < intLen; i++)
        {
            arrValues[i] = arr_total[i];
            arrValueNames[i] = arr_selected_station_names[i];
        }


        DrawPieChart(arrValues, arrValueNames);
    }

    protected void fillArray()
    {
        DataTable dt = (DataTable)Session["data"];

        int[] arr_total, arr_total_exclusive;
        string[] arr_selected_station_names;
        int stCount = (int)Session["stCount"];
        arr_total = new int[stCount];
        arr_total_exclusive = new int[stCount];
        arr_selected_station_names = new string[stCount];

        int i = 0;

        foreach (DataColumn dc in dt.Columns)
        {
            if (dc.ColumnName.IndexOf("Plays") >= 0)
            {
                arr_selected_station_names[i] = dc.ColumnName;
                if (Request.QueryString["isExclusive"] == "0")
                {
                    arr_total[i] = getCountForAColumn(dt, dc);
                }
                else
                {
                    arr_total_exclusive[i] = getExclusiveCountForAColumn(dt, dc);//getExclusiveTotalForAColumn(dt, dc);
                }
                i++;
            }
        }


        Session["arr_total"] = arr_total;
        Session["arr_total_exclusive"] = arr_total_exclusive;
        Session["arr_selected_station_names"] = arr_selected_station_names;
    }

    protected int getTotalForAColumn(DataTable dt, DataColumn dc)
    {
        int totalOfColumn = 0;
        try
        {
            totalOfColumn = int.Parse(dt.Compute("sum([" + dc.ColumnName + "])", "[Category Name] <> 'Total'").ToString());
        }
        catch
        {

        }
        return totalOfColumn;
    }

    //protected int getExclusiveTotalForAColumn(DataTable dt, DataColumn dc)
    protected int getExclusiveCountForAColumn(DataTable dt, DataColumn dc)
    {
        int totalOfColumn = 0;
        int countValue = 0;
        string colValue = "";
        int i = 0;
        foreach (DataRow dr in dt.Rows)
        {
            colValue = "";
            countValue = 0;
            i = 0;
            colValue = dr[dc].ToString();
            if (dr["Category Name"].ToString() != "Total")
            {
                if (colValue == "")
                {
                    //No record
                }
                else
                {
                    foreach (DataColumn dc1 in dt.Columns)
                    {
                        if (dc1.ColumnName.IndexOf("Plays") > 0)
                        {
                            if (dr[dc1].ToString() != "")
                            {
                                countValue = countValue + 1;
                            }

                            i = i + 1;
                        }
                    }
                }
                if (countValue == 1)
                {
                    try
                    {
                        totalOfColumn = totalOfColumn + 1;// int.Parse(colValue);
                    }
                    catch { }
                }
            }

        }
        return totalOfColumn;
    }

    protected int getTotalSumForAColumn(DataTable dt, DataColumn dc)
    {
        int totalOfColumn = 0;
        foreach (DataRow dr in dt.Rows)
        {
            try
            {
                if (dr["Category Name"].ToString() != "Total")
                {
                    totalOfColumn = totalOfColumn + int.Parse(dr[dc].ToString());
                }
            }
            catch { }
        }
        return totalOfColumn;
    }

    protected int getCountForAColumn(DataTable dt, DataColumn dc)
    {
        int totalOfColumn = 0;
        try
        {
            totalOfColumn = int.Parse(dt.Compute("count([" + dc.ColumnName + "])", "[Category Name] <> 'Total'").ToString());
        }
        catch
        {

        }
        return totalOfColumn;
    }

    protected void DrawPieChart(int[] arrValues, string[] arrValueNames)
    {
        int i = 0;
        Bitmap objBitMap = new Bitmap(500, 250);

        Graphics objGraphics = default(Graphics);

        objGraphics = Graphics.FromImage(objBitMap);

        objGraphics.Clear(Color.White);

        objGraphics.DrawRectangle(Pens.Black, 0, 0, 498, 230);
        Pen blackPen = new Pen(Color.Black, 3);

        string chart_title = null;

        if (Request.QueryString["isExclusive"] == "0")
        {
            chart_title = "Category Analysis (" + Session["station_selected"] + "): Total no. of categories on air on specific stations";
        }
        else
        {
            chart_title = "Category Analysis (" + Session["station_selected"] + "): Exclusive categories on air on specific stations";
        }

        objGraphics.DrawString(chart_title, new Font("Tahoma", 10), Brushes.Blue, new PointF(5, 5));

        PointF symbolLeg = new PointF(180, 40);

        PointF descLeg = new PointF(210, 36);

        string station_name = null;

        int total = 0;
        double perc = 0;

        for (i = 0; i <= arrValues.Length - 1; i++)
        {
            total = total + arrValues[i];
        }

        for (i = 0; i <= arrValues.Length - 1; i++)
        {
            descLeg.X = 210;
            perc = double.Parse(arrValues[i].ToString()) / double.Parse(total.ToString()) * 100;
            perc = double.Parse(perc.ToString("N2"));

            objGraphics.FillRectangle(new SolidBrush(GetColor(i)), symbolLeg.X, symbolLeg.Y, 20, 10);

            objGraphics.DrawRectangle(Pens.Black, symbolLeg.X, symbolLeg.Y, 20, 10);

            station_name = arrValueNames[i];
            station_name = station_name.Substring(0, station_name.LastIndexOf(Session["station_selected"].ToString().ToUpper()) - 1);

            objGraphics.DrawString(station_name, new Font("Tahoma", 10), Brushes.Black, descLeg);
            descLeg.X += 165;
            station_name = arrValues[i].ToString();
            objGraphics.DrawString(station_name, new Font("Tahoma", 10), Brushes.Black, descLeg);
            descLeg.X += 60;
            station_name = perc + "%";
            objGraphics.DrawString(station_name, new Font("Tahoma", 10), Brushes.Black, descLeg);

            symbolLeg.Y += 15;


            descLeg.Y += 15;
        }
        descLeg.X = 210;
        objGraphics.DrawLine(Pens.Black, symbolLeg.X + 30, symbolLeg.Y, symbolLeg.X + 305, symbolLeg.Y);
        objGraphics.DrawString("Market Total:", new Font("Tahoma", 10), Brushes.Black, descLeg.X, descLeg.Y + 5);
        objGraphics.DrawString(total.ToString(), new Font("Tahoma", 10), Brushes.Black, descLeg.X + 165, descLeg.Y + 5);
        objGraphics.DrawString("100.00%", new Font("Tahoma", 10), Brushes.Black, descLeg.X + 225, descLeg.Y + 5);

        float sglCurrentAngle = 0;

        float sglTotalAngle = 0;

        i = 0;

        Pen rotatePen = new Pen(Color.Black, 0);
        rotatePen.ScaleTransform(2, 1);

        Pen objPen = default(Pen);
        Brush objBrush = default(Brush);
        Font objFont = default(Font);

        PointF position_per = new PointF(130, 86);

        int loop_pie = 0;

        objPen = new Pen(Color.White);
        objBrush = new SolidBrush(Color.White);
        objFont = new Font("Lucida Sans Unicode", 8);

        Random rand = new Random();

        objGraphics.SmoothingMode = SmoothingMode.AntiAlias;

        for (i = 0; i <= arrValues.Length - 1; i++)
        {
            perc = double.Parse(arrValues[i].ToString()) / double.Parse(total.ToString()) * 100;
            perc = double.Parse(perc.ToString("N2"));

            sglCurrentAngle = (float)(int.Parse(arrValues[i].ToString())) / float.Parse(total.ToString()) * 360;

            for (loop_pie = 0; loop_pie <= 5; loop_pie++)
            {
                objGraphics.FillPie(new HatchBrush(HatchStyle.Percent50, GetColor(i)), 10, 55 + loop_pie, 160, 130, sglTotalAngle, sglCurrentAngle);
            }
            objGraphics.FillPie(new SolidBrush(GetColor(i)), 10, 55, 160, 130, sglTotalAngle, sglCurrentAngle);

            sglTotalAngle += sglCurrentAngle;
        }

        objBitMap.Save(Response.OutputStream, ImageFormat.Gif);
    }

    private System.Drawing.Color GetColor(int itemIndex)
    {
        System.Drawing.Color objColor = default(System.Drawing.Color);
        switch (itemIndex)
        {
            case 0:
                objColor = System.Drawing.Color.Red;
                break;
            case 1:
                objColor = System.Drawing.Color.Blue;
                break;
            case 2:
                objColor = System.Drawing.Color.Brown;
                break;
            case 3:
                objColor = System.Drawing.Color.Black;
                break;
            case 4:
                objColor = System.Drawing.Color.DarkBlue;
                break;
            case 5:
                objColor = System.Drawing.Color.Green;
                break;
            case 6:
                objColor = System.Drawing.Color.Gray;
                break;
            case 7:
                objColor = System.Drawing.Color.DarkKhaki;
                break;
            case 8:
                objColor = System.Drawing.Color.Magenta;
                break;
            case 9:
                objColor = System.Drawing.Color.BlueViolet;
                break;
            case 10:
                objColor = System.Drawing.Color.CadetBlue;
                break;
            case 11:
                objColor = System.Drawing.Color.Chocolate;
                break;
            case 12:
                objColor = System.Drawing.Color.CornflowerBlue;
                break;
            case 13:
                objColor = System.Drawing.Color.Crimson;
                break;
            case 14:
                objColor = System.Drawing.Color.DarkCyan;
                break;
            case 15:
                objColor = System.Drawing.Color.DodgerBlue;
                break;
            case 16:
                objColor = System.Drawing.Color.Indigo;
                break;
            case 17:
                objColor = System.Drawing.Color.LightSlateGray;
                break;
            case 18:
                objColor = System.Drawing.Color.MidnightBlue;
                break;
            default:
                objColor = System.Drawing.Color.Teal;
                break;
        }
        return objColor;
    } 
}
