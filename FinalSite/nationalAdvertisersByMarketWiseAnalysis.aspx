﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="nationalAdvertisersByMarketWiseAnalysis.aspx.cs" Inherits="nationalAdvertisersByMarketWiseAnalysis"
    Title="National Advertisers By Market Wise Analysis" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .gvHeaderRow
        {
            background-image: url("images/dhrudde.png");
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: White;
            text-decoration: none;
            line-height: 12px;
            padding-left: 10px;
            border-left: solid 1px white;
            border-right: solid 1px white;
            border-bottom: solid 1px white;
            position: relative;
        }
        .style5
        {
            font-family: Helvetica;
            font-size: 14px;
           
            color: grey;
        }
        .style6
        {
            font-family: Helvetica;
            font-size: 14px;
            color: Grey;
            width: 153px;
        }
    </style>

    <script type="text/javascript" language="javascript">
function ToggleCollapsePane() {               
               var splitter = $find("<%= Page.Master.FindControl("RadSplitter1").ClientID %>");
               var pane = splitter.getPaneById("<%= Page.Master.FindControl("RadPane1").ClientID %>");
                pane.collapse();
    }
        
		function disp_clock()
		{
		    var lblError = document.getElementById("ctl00_content1_lblError");
		    if (lblError != null)
		    {
		        lblError.style.display='none';
		    }
			OnLoad();
			DIV1.style.display='none';
			divWait.style.display='';
    	}

		function OnLoad() 
		{ 
		    setTimeout("StartAnimation()", 500); 
		} 

		function StartAnimation() 
		{ 
		    if (document.images)
		    {
		        document['ProgressBarImg'].src = "images/aniClock.gif";
		    }
		}
   
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div style="width: 99.7%; height: 100%; margin-left: 4px">
        <div>
            <table width="100%" background="images/contentheader_bgr.gif" border="0" cellpadding="6px"
                cellspacing="0px" style="margin-left: -5px; margin-right: 300px;">
                <tr>
                    <td align="left">
                        ::
                        <asp:Label ID="Pg_Title" runat="server" Font-Bold="True" ForeColor="#000099"></asp:Label>&nbsp;::
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        <asp:Label ID="lbl_Logout" runat="server" Text="Welcome: Administrator[Logout]" Font-Bold="True"
                            ForeColor="#000099"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            &nbsp;&nbsp;<a href="#" onclick="history.go(-1);return false">&lt;&lt;&nbsp;Back</a>|
            <a href="nationalAdvertisers.aspx">Go Back to Main Report Page</a>
            <br />
            <br />
            <table style="width: 39%">
                <tr>
                    <td class="style6">
                        Date Range
                    </td>
                    <td class="date">
                    :
                </td>
                    <td class="date">
                        <asp:Label ID="lbl_daterange" runat="server" CssClass="style5"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        Market
                    </td>
                    <td class="date">
                    :
                </td>
                    <td class="date">
                        <asp:Label ID="lbl_market" runat="server" CssClass="style5"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        Brand Name
                    </td>
                    <td class="date">
                    :
                </td>
                    <td class="date">
                        <asp:Label ID="lbl_brand" runat="server" CssClass="style5"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        Advertiser
                    </td>
                    <td class="date">
                    :
                </td>
                    <td class="date">
                        <asp:Label ID="lbl_advt" runat="server" CssClass="style5"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        Category
                    </td>
                    <td class="date">
                    :
                </td>
                    <td class="date">
                        <asp:Label ID="lbl_category" runat="server" CssClass="style5"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        Total No. Plays
                    </td>
                    <td class="date">
                    :
                </td>
                    <td class="date">
                        <asp:Label ID="lbl_totalnoplays" runat="server" CssClass="style5"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        Duration
                    </td>
                    <td class="date">
                    :
                </td>
                    <td class="date">
                        <asp:Label ID="lbl_duration" runat="server" CssClass="style5"></asp:Label>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="pnlDate" runat="server" Visible="true" Width="100%">
                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 100%">
                            <div id="divWait" style="display: none">
                                <table style="height: 310Px" width="100%">
                                    <tr>
                                        <td align="center" width="100%">
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <img src="images/aniClock.gif" border="0" name="ProgressBarImg"><br>
                                            <font class="date">Acquiring Data... one moment please!</font>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="DIV1" style="width: 100%">
                                <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label>
                                <asp:GridView UseAccessibleHeader="true" EnableViewState="false" ID="gvData" ShowFooter="true"
                                    runat="server" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" Height="100px">
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <asp:Literal ID="litExportTo" runat="server" Text="Export to: "></asp:Literal>
                            <telerik:RadButton ID="btnExportToExcel" runat="server" CausesValidation="False"
                                Text="Excel" OnClick="img_btn_export_to_excel_Click">
                            </telerik:RadButton>
                        </td>
                    </tr>
                </table>
                <asp:TextBox ID="txtArray" runat="server" Style="visibility: hidden; height: 0Px"></asp:TextBox>
            </asp:Panel>
        </div>
    </div>
    <script type="text/javascript" language="javascript">
        setTimeout("ToggleCollapsePane();", 100);
    </script>
</asp:Content>
