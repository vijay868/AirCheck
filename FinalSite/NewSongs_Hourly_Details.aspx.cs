﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text;
using Telerik.Web.UI;

public partial class NewSongs_Hourly_Details : System.Web.UI.Page
{
    bool isPdfExport = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        //StringBuilder onloadScript = new StringBuilder();
        //onloadScript.Append("<script type='text/javascript'>");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("ShowReport();");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("</script>");
        //this.ClientScript.RegisterStartupScript(this.GetType(), "onLoadCall", onloadScript.ToString());

     //   ((Label)this.Master.FindControl("lblReportName")).Text = "New Songs";

        //Literal lit = new Literal();
        //lit.Text = "<script language='javascript'>tdhide('leftMenu', 'ctl00_showhide', 'hide');</script>";
        //Page.Controls.Add(lit);



        if (!IsPostBack)
        {
            Session["STds"] = null;
            //ds1 = null;
            //lbl_brand.Text=doStringCleanup(Request.QueryString["brandnames"]);
            //lbl_advt.Text = doStringCleanup(Request.QueryString["advt"]);
            //lbl_category.Text = doStringCleanup(Request.QueryString["tvbrand"]);
            //lbl_daterange.Text = GetUsDate(Request.QueryString["sdate"]) + " To " + GetUsDate(Request.QueryString["edate"]);

            getUserPreferencesForCurrentUser();
            bindGrid(Request.QueryString["StationIDQ"], Request.QueryString["sdate"], Request.QueryString["edate"], Request.QueryString["songTitle"], Request.QueryString["movieTitle"], Request.QueryString["StationNames"]);
            if (Session["dataReadOnly"].ToString() == "True")
            {
                btnExportToExcel.Enabled = false;
                btnExportToPdf.Enabled = false;
            }
            else
            {
                btnExportToExcel.Enabled = true;
                btnExportToPdf.Enabled = true;
            }
        }
        RadPane mypane = this.Master.FindControl("RadPane1") as RadPane;
        mypane.Collapsed = true;
        Pg_Title.Text = "New Songs Hourly Details";


       
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        //StringBuilder onloadScript = new StringBuilder();
        //onloadScript.Append("<script type='text/javascript'>");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("ToggleCollapsePane();");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("</script>");
        //this.ClientScript.RegisterStartupScript(this.GetType(), "onLoadCall", onloadScript.ToString());

    }
    protected void btnExportToPdf_Click(object sender, EventArgs e)
    {

        isPdfExport = true;
        // bindGrid(Request.QueryString["StationIDQ"], Request.QueryString["sdate"], Request.QueryString["edate"], Request.QueryString["scrtxt"], Request.QueryString["scrcriteria"], Request.QueryString["advt"]);
        bindGrid(Request.QueryString["StationIDQ"], Request.QueryString["sdate"], Request.QueryString["edate"], Request.QueryString["songTitle"], Request.QueryString["movieTitle"], Request.QueryString["StationNames"]);
        string strDateFormat = (string)Session["dateFormat"];
        DateTime dt1 = DateTime.Parse(Session["strStartDate"].ToString());
        DateTime dt2 = DateTime.Parse(Session["strEndDate"].ToString());

        string sDt1 = "";
        string sDt2 = "";
        sDt1 = dt1.ToString("dd/MM/yyyy");
        sDt2 = dt2.ToString("dd/MM/yyyy");

        String strDateRange = "From: " + sDt1 + "  -  " + sDt2;

        radData.ExportSettings.FileName = "New Songs Hourly Report";
        radData.ExportSettings.IgnorePaging = true;
        radData.ExportSettings.OpenInNewWindow = true;

        String strReportName = "";

        strReportName = " AirCheck India New Songs Hourly Report";

        radData.ExportSettings.Pdf.PageTitle = strReportName + " " + strDateRange;
        radData.ExportSettings.ExportOnlyData = false;
        radData.MasterTableView.ExportToPdf();

        //addAuditLog(Convert.ToDateTime(sDt1), Convert.ToDateTime(sDt2), "", false, true, false);


    }
        protected string doStringCleanup(string str)
        {
            if (str == null)
            {
                return "";

            }
            else
            {
                str = str.Replace("456456", "&");
                str = str.Replace("123123", "'");
                return str;
            }



        }

        protected void getUserPreferencesForCurrentUser()
        {
            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
            SqlDataAdapter da = new SqlDataAdapter("getUserPreferenceForAUserAndReport", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@user", (string)Session["user"]);
            da.SelectCommand.Parameters.AddWithValue("@reportName", "Top Advertisers By Brands");

            DataSet ds = new DataSet();
            da.Fill(ds);

            Session["headerFontName"] = ds.Tables[0].Rows[0]["headerFontName"].ToString();
            Session["headerFontSize"] = ds.Tables[0].Rows[0]["headerFontSize"].ToString();
            Session["headerFontColor"] = ds.Tables[0].Rows[0]["headerFontColor"].ToString();
            Session["headerRowColor"] = ds.Tables[0].Rows[0]["headerRowColor"].ToString();
            Session["headerFontStyle"] = ds.Tables[0].Rows[0]["headerFontStyle"].ToString();
            Session["dataFontName"] = ds.Tables[0].Rows[0]["dataFontName"].ToString();
            Session["dataFontSize"] = ds.Tables[0].Rows[0]["dataFontSize"].ToString();
            Session["dataFontColor"] = ds.Tables[0].Rows[0]["dataFontColor"].ToString();
            Session["alternateRowColor"] = ds.Tables[0].Rows[0]["alternateRowColor"].ToString();
            Session["dataFontStyle"] = ds.Tables[0].Rows[0]["dataFontStyle"].ToString();

        }

        protected void displayNoRecordFound()
        {
            lblError.Visible = true;
            lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
            radData.Visible = false;
            litExportTo.Visible = false;
            btnExportToExcel.Visible = false;
            btnExportToPdf.Visible = false;
        }

        protected void radData_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            string hourname;
            if (e.Item.ItemType == GridItemType.Header)
            {
                System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
                for (int i = 0; i < e.Item.Cells.Count; i++)
                {
                    //if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                    //{
                    //    e.Item.Cells[i].Font.Bold = true;
                    //}
                    //else
                    //{
                    //    e.Item.Cells[i].Font.Bold = true;
                    //}
                    e.Item.Cells[i].Font.Size = 9;
                    //i++;
                }
                //e.Item.BackColor = System.Drawing.Color.FromArgb(0, 112, 192);
                e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
            }



            if (e.Item.ItemType == Telerik.Web.UI.GridItemType.Item || e.Item.ItemType == Telerik.Web.UI.GridItemType.AlternatingItem)
            {
                string actualDataFontName = (string)Session["dataFontName"];//ConfigurationSettings.AppSettings["actualDataFontNameMarketShareOfEachStation"].ToString();
                string actualDataFontSize = (string)Session["dataFontSize"];//ConfigurationSettings.AppSettings["actualDataFontSizeMarketShareOfEachStation"].ToString();

                Telerik.Web.UI.GridDataItem dataItem = e.Item as Telerik.Web.UI.GridDataItem;

                System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
                dataItem.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

                //dataItem.Font.Size = FontUnit.Parse(Session["dataFontSize"].ToString().Replace("Px", "").Trim());
                dataItem.Font.Size = FontUnit.Parse("12pt");

                if (Session["dataFontStyle"].ToString().ToLower() == "bold")
                {
                    dataItem.Font.Bold = true;
                }
                else
                {
                    dataItem.Font.Bold = false;
                }

                if (!isPdfExport)
                {
                    for (int i = 3; i < e.Item.Cells.Count; i++)
                    {
                        if (e.Item.Cells[i].Text != "&nbsp;")
                        {
                            string udate;
                            udate = GetUsDate(e.Item.Cells[2].Text);

                            hourname = gethourval(i);
                            e.Item.Cells[i].Text = "<div align=Center><a href='#' onclick=\"javascript:return disp_details('" + Request.QueryString["StationNames"] + "','" + Request.QueryString["sdate"] + "','" + Request.QueryString["edate"] + "','" + Request.QueryString["songTitle"] + "','" + Request.QueryString["movieTitle"] + "','" + udate + "','" + hourname + "','" + Request.QueryString["StationIDQ"] + "')\" style='color:black;font-size:12 px'>" + e.Item.Cells[i].Text + "</a></div>"; //<div align=Center><a href='#' onclick=\"javascript:return disp_details('" + Request.QueryString["StationIDQ"] + "','" + wDate + "','" + wDate + "','" + Request.QueryString["scrtxt"] + "','" + Request.QueryString["scrcriteria"] + "','" + doStringSanitation(Request.QueryString["advt"]) + "','" + doStringSanitation(Request.QueryString["brandnames"]) + "','" + doStringSanitation(Request.QueryString["market"]) + "','" + hourname + "','" + e.Item.Cells[2].Text + "','" + Request.QueryString["stationnameid"] + "')\" style='color:black;font-size:12 px'> " + e.Item.Cells[i].Text + "</a></div>";
                        }
                    }
                }
                if (e.Item.ItemType == GridItemType.AlternatingItem)
                {
                    if (!isPdfExport)
                    {
                        System.Drawing.ColorConverter conv1 = new System.Drawing.ColorConverter();
                        e.Item.BackColor = (System.Drawing.Color)conv1.ConvertFromString((string)Session["alternateRowColor"]);
                    }
                }

            }
            if (e.Item.ItemType == GridItemType.Footer)
            {
                if (!isPdfExport)
                {
                    e.Item.Visible = false;
                }
                else
                {
                    e.Item.Visible = true;

                }
            }

            radData.ClientSettings.Scrolling.ScrollHeight = Unit.Pixel(280);
            radData.ClientSettings.Scrolling.UseStaticHeaders = true;
        }

        private string GetUsDate(string indDate)
        {
            string str = "";
            char[] sep = { '/' };
            string[] vals = indDate.Split(sep);
            str = vals[1] + "/" + vals[0] + "/" + vals[2];
            return str;
        }

        public string gethourval(int cellno)
        {
            string returnval;
            if (cellno == 3)
            {
                returnval = "12 AM";
            }
            else
            {
                if (cellno <= 14)
                {
                    returnval = (cellno - 3) + " AM";
                }
                else if (cellno == 15)
                {
                    returnval = "12 PM";
                }
                else
                {
                    returnval = (cellno - 15) + " PM";
                }

            }
            return returnval;
        }

        protected void bindGrid(string strStationIDs, string strStartDate, string strEndDate, string songTitle, string movieTitle, string stationname)
        {
            string rep_str_advt;

            //  rep_str_advt = advt;
            //rep_str_advt = rep_str_advt.Replace("456456", "&");

            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
                SqlDataAdapter da = new SqlDataAdapter("HourlySongAirplayReport", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.CommandTimeout = 0;

                da.SelectCommand.Parameters.AddWithValue("@stationName", stationname);
                da.SelectCommand.Parameters.AddWithValue("@startDate", strStartDate);
                da.SelectCommand.Parameters.AddWithValue("@endDate", strEndDate);
                da.SelectCommand.Parameters.AddWithValue("@songTitle", songTitle);
                da.SelectCommand.Parameters.AddWithValue("@movieTitle", movieTitle);
                //  da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

                Session["strStationIDs"] = strStationIDs;
                Session["strStartDate"] = strStartDate;
                Session["strEndDate"] = strEndDate;

                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[1].Rows.Count == 0)
                {
                    displayNoRecordFound();
                    Session["totalRecordCount"] = 0;
                }
                else
                {

                    radData.DataSource = ds.Tables[1];
                    radData.DataBind();


                    lbl_startdate.Text = Convert.ToString(ds.Tables[0].Rows[0]["Start Date"]);
                    lbl_enddate.Text = Convert.ToString(ds.Tables[0].Rows[0]["End Date"]);
                    lbl_market.Text = Convert.ToString(ds.Tables[0].Rows[0]["Market"]);
                    lbl_station.Text = Convert.ToString(ds.Tables[0].Rows[0]["Station Name"]);
                    lbl_albumtitle.Text = Convert.ToString(ds.Tables[0].Rows[0]["Movie Title"]);
                    lbl_songtitle.Text = Convert.ToString(ds.Tables[0].Rows[0]["Song Title"]);
                    lbl_duration.Text = Convert.ToString(ds.Tables[0].Rows[0]["Duration"]);
                    lbl_noplays.Text = Convert.ToString(ds.Tables[0].Rows[0]["Total Plays"]);


                    //litExportTo.Visible = true;
                    // btnExportToExcel.Visible = true;
                    // btnExportToPdf.Visible = true;
                    radData.Visible = true;
                    lblError.Visible = false;
                    Session["STds"] = ds;

                    Session["totalRecordCount"] = ds.Tables[1].Rows.Count;

                }

                //addAuditLog(Convert.ToDateTime(strStartDate), Convert.ToDateTime(strEndDate), strStationIDs, false, false, false);
                StringBuilder onloadScript = new StringBuilder();
                onloadScript.Append("<script type='text/javascript'>");
                onloadScript.Append(Environment.NewLine);
                onloadScript.Append("ToggleCollapsePane();");
                onloadScript.Append(Environment.NewLine);
                onloadScript.Append("</script>");
                this.ClientScript.RegisterStartupScript(this.GetType(), "onLoadCall", onloadScript.ToString());
            }
            catch
            {
                displayNoRecordFound();
            }
        }

        protected void img_btn_export_to_excel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }

        public void ExportToExcel()
        {
            string strDateFormat = (string)Session["dateFormat"];

            DataSet ds = (DataSet)Session["STds"];
            DataTable dt = new DataTable("tblExport");
            dt = ds.Tables[1].Copy();



            string str_station_name = "";

            string str_market_name = "";// getMarketOfStation(str_station_name);

            String strStationName = "<BR><BR>Station: " + str_station_name;
            String strMarket = "<BR><BR>Market: " + str_market_name;

            DateTime dt1 = DateTime.Parse(Session["strStartDate"].ToString());
            DateTime dt2 = DateTime.Parse(Session["strEndDate"].ToString());

            string sDt1 = "";
            string sDt2 = "";
            if (strDateFormat == "DD/MM/YYYY")
            {
                sDt1 = dt1.ToString("dd/MM/yyyy");
                sDt2 = dt2.ToString("dd/MM/yyyy");
            }
            else
            {
                sDt1 = dt1.ToString("MM/dd/yyyy");
                sDt2 = dt2.ToString("MM/dd/yyyy");
            }

            String strDateRange = "<BR><BR>From: " + sDt1 + "  to  " + sDt2;

            ExportToExcel(dt, Response, strStationName, strMarket, strDateRange, lbl_startdate.Text, lbl_enddate.Text, lbl_market.Text, lbl_station.Text, lbl_albumtitle.Text, lbl_songtitle.Text, lbl_duration.Text, lbl_noplays.Text);

            //addAuditLog(Convert.ToDateTime(sDt1), Convert.ToDateTime(sDt2), "", true, false, false);


        }

        public void ExportToExcel(DataTable dt, HttpResponse response, String strStationName, string strMarket, String strDateRange, String sdate, String edate, String market, String Stations, String albumtitle, String songtitle, String playduraiton, String tplays)
        {
            string strfinalval;


            //first let's clean up the response.object
            response.Clear();
            response.Charset = "";
            //set the response mime type for excel
            response.ContentType = "application/vnd.ms-excel";
            //create a string writer
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            //create an htmltextwriter which uses the stringwriter
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            //instantiate a datagrid
            DataGrid dg = new DataGrid();
            //set the datagrid datasource to the dataset passed in
            if (dt == null)
            {
                return;
            }
            else if (dt.Rows.Count == 0)
            {
                return;
            }
            dg.DataSource = dt;
            //bind the datagrid
            dg.DataBind();

            dg.RenderControl(htmlWrite);


            String str = stringWrite.ToString();

            int start, end;
            start = 0;
            end = 0;

            int colspan = 0;

            colspan = dt.Columns.Count;


            String strReportName = "";
            strReportName = " AirCheck India New Songs Hourly Report";

            string strFinal = "<tr><td align=center colspan=" + colspan + " style='background-color:#FFFF66;'>" + strReportName + "</td></tr>";
            strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strDateRange + "</td></tr>";
            strFinal = strFinal + "<tr><td colspan=" + colspan + ">&nbsp;</td></tr>";
            strFinal = strFinal + "<tr><td colspan=" + colspan + ">Market           : " + market + "</td></tr>";
            strFinal = strFinal + "<tr><td colspan=" + colspan + ">Station          : " + Stations + "</td></tr>";
            strFinal = strFinal + "<tr><td colspan=" + colspan + ">Movie/Album Name : " + albumtitle + "</td></tr>";
            strFinal = strFinal + "<tr><td colspan=" + colspan + ">Song Title       : " + songtitle + "</td></tr>";
            strFinal = strFinal + "<tr><td colspan=" + colspan + ">Duration         : " + playduraiton + "</td></tr>";
            strFinal = strFinal + "<tr><td colspan=" + colspan + ">Total Plays      : " + tplays + "</td></tr>";
            strFinal = strFinal + "<tr><td colspan=" + colspan + ">&nbsp;</td></tr>";


            while (str.IndexOf("<select name=") > 0)
            {
                start = str.IndexOf("<select name=");
                end = str.IndexOf("</select>") + 9;
                str = str.Replace(str.Substring(start, end - start), "");
            }

            string strAirTime = "";
            string strPlayTime = "";
            string strNewPlayTime = "";

            int intCount = 0;

            foreach (DataRow dr in dt.Rows)
            {
                strAirTime = dr[0].ToString();
                if (strAirTime == "")
                {
                    strPlayTime = dr[2].ToString();
                    strNewPlayTime = dr[2].ToString() + " ";

                    str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=5 align='Center'>" + strNewPlayTime + "</td></tr>");

                }
                else
                {
                    intCount = intCount + 1;
                }
            }


            str = strFinal + "<BR><BR>" + str;


            string strDateFormat = (string)Session["dateFormat"];

            DateTime dtDate = System.DateTime.Now;
            string strDate = "";
            if (strDateFormat == "DD/MM/YYYY")
            {
                strDate = dtDate.ToString("dd/MM/yyyy");
            }
            else
            {
                strDate = dtDate.ToString("MM/dd/yyyy");
            }


            string strTime = System.DateTime.Now.ToString();
            strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


            string strCountText = "<b>" + intCount + " rows </b>";

            string strFooterText = "";

            strFooterText = "<BR><table>";
            strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>This data was generated and exported on " + strDate + " at " + strTime + "</b></td></tr>";
            strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>" + strCountText + "</b></td></tr>";
            strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><BR></td></tr>";
            strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved.</td></tr>";
            strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India.</td></tr>";
            strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.</td></tr>";
            strFooterText += "</table>";

            str = "<table>" + str + strFooterText;



            response.Write(str);
            response.End();
        }

        protected string doStringSanitation(string str)
        {
            if (str == null)
            {
                return "";
            }
            else
            {
                if (str.IndexOf("'") >= 0)
                {
                    str = str.Replace("'", "123123");
                }
                if (str.IndexOf("&") >= 0)
                {
                    str = str.Replace("&", "456456");
                }

                return str;
            }
        }

        protected void radData_SortCommand(object source, GridSortCommandEventArgs e)
        {
            bindGrid(Request.QueryString["StationIDQ"], Request.QueryString["sdate"], Request.QueryString["edate"], Request.QueryString["songTitle"], Request.QueryString["movieTitle"], Request.QueryString["StationNames"]);
        }

}
