<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TestPage.aspx.cs" Inherits="TestPage" %>

<%@ Register assembly="IdeaSparx.CoolControls.Web" namespace="IdeaSparx.CoolControls.Web" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   
     <style type="text/css">
      
        .gvHeaderRow
        {
            background-image: url("images/dhrudde.png");
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: White;
            text-decoration: none;
            line-height: 20px;
            padding-left: 10px;
            border-left: solid 1px white;
            border-right: solid 1px white;
            border-bottom: solid 1px white;
        }
        
    </style>
    <script type="text/javascript">
        function StopPropagation(e) {
            if (!e) {
                e = window.event;
            }

            e.cancelBubble = true;
        }
       


        function OnClientDropDownOpenedHandler(sender, eventArgs) {



            var tree = sender.get_items().getItem(0).findControl("radTreeCities");
            var tree1 = sender.get_items().getItem(0).findControl("radStationCompare");

            if (!!tree) {
                var selectedNode = tree.get_selectedNode();
                if (selectedNode) {
                    selectedNode.scrollIntoView();
                }
            }

            if (!!tree1) {
                var selectedNode1 = tree1.get_selectedNode();
                if (selectedNode1) {
                    selectedNode1.scrollIntoView();
                }
            }
        }


        function nodeClicking(sender, args) {
            var comboBox = $find("<%= txtMarketSelect.ClientID %>");

            var node = args.get_node();

            if (node.get_parent() == node.get_treeView()) {
                alert("Please select a station. Market selection not allowed.");

            }
            else {
                comboBox.set_text(node.get_text());
                comboBox.get_items().getItem(0).set_value(node.get_value());
            }

        }

        function nodeClicked(sender, args) {
            var node = args.get_node();
            if (node.get_checked()) {
                node.uncheck();
            } else {
                node.check();
            }
            nodeChecked(sender, args)


        }

        function getrootcount(sender, args) {

            //        var tree = sender.get_items().getItem(0).findControl("radTreeCities");
            //        var allNodes = tree.get_nodes().getNode(0).get_allNodes();
            //        for (var i = 0; i < allNodes.length; i++) {
            //            var node = allNodes[i];
            //            alert(node.get_text());
            //        }
        }
        function disp_details(startDate, endDate, strSongName, strArtistName, strStationName) {
            win = window.open("ListOfCurrentSongsRepetition_details.aspx?startDate=" + startDate + "&endDate=" + endDate + "&strSongName=" + strSongName + "&strArtistName=" + strArtistName + "&strStationName=" + strStationName, "win", "width=800,height=400,left=0, scrollbars=yes, resizable=yes");
            return false
        }
        function disp_exclusiveReport(startDate, endDate, strStationName, strStationIDs) {
            win = window.open("ListOfCurrentSongsRepetition_exclusive.aspx?startDate=" + startDate + "&endDate=" + endDate + "&strStationName=" + strStationName + "&strStationIDs=" + strStationIDs, "win", "width=800,height=400,left=0, scrollbars=yes, resizable=yes");
            return false
        }

        function nodeChecked(sender, args) {

            var comboBox = $find("<%= txtMarketSelect.ClientID %>");



            //check if 'Select All' node has been checked/unchecked
            var tempNode = args.get_node();
            if (tempNode.get_text().toString() == "(Select All)") {
                // check or uncheck all the nodes
            } else {
                var nodes = new Array();
                nodes = sender.get_checkedNodes();
                var vals = "";
                var i = 0;
                var icount = 0;

                for (i = 0; i < nodes.length; i++) {
                    var n = nodes[i];
                    var nodeText = n.get_text().toString();

                    if (nodeText != "(Select All)") {
                        if (n._hasChildren() == true) {
                            //                        icount = icount + 1;
                            //                        alert(icount);
                        } else {
                            vals = vals + n.get_text().toString() + ",";
                        }
                    }
                }

                //prevent  combo from closing
                //  supressDropDownClosing = true;
                comboBox.set_text(vals);

                getrootcount(sender, args);
            }
        }
        function OnClientClicked(button, args) {
           if(calcDays())
           {                       
//               var divobj = document.getElementById("ShowGirdContainerButtons")
//             divobj.style.display = "block";
                button.set_autoPostBack(true); 
           }                
            else
            {                              
                button.set_autoPostBack(false);                        
            }
        }
        function disp_clock() {
//            var lblError = document.getElementById("ctl00_content1_lblError");
//            if (lblError != null) {
//               // lblError.style.display = 'none';
//            }
//            OnLoad();
         //ivWait.style.display = '';
        }

        function OnLoad() {
//            setTimeout("StartAnimation()", 500);
        }

        function StartAnimation() {
//            if (document.images) {
//                document['ProgressBarImg'].src = "images/aniClock.gif";
//            }
        }
        function ShowExportDiv() {
//            document.getElementById("divExport").style.display = "block";
        }
        function calcDays()
        {
            var startDate =$find("<%=dtStart.ClientID %>");
            var endDate =  $find("<%=dtEnd.ClientID %>");
            
            var sDate = startDate.get_selectedDate();
            var eDate = endDate.get_selectedDate();
            
             var lockDataBefore="<%=Session["lockDataBefore"]%>";
		    var lockDataAfter="<%=Session["lockDataAfter"]%>";
            if(sDate == null)
            {
                lockDataBefore = new Date(lockDataBefore);
                var curr_date = lockDataBefore.getDate();
                var curr_month = lockDataBefore.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataBefore.getFullYear();
                lockDataBefore= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights only from " + lockDataBefore);
                return false;
            }
            
            if(eDate == null)
            {
                lockDataAfter = new Date(lockDataAfter);
                var curr_date = lockDataAfter.getDate();
                var curr_month = lockDataAfter.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataAfter.getFullYear();
                lockDataAfter= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights till " + lockDataAfter);
                return false;
            }
        
            if(days_between(sDate,eDate) > 30)
            {
                alert("Maximum allowed date range is 1 month.");
                return false;
            }      
            

          

           
            var isMarketSelected = 0;  
            
            //var tree = sender.get_items().getItem(0).findControl("radTreeMarkets");
            
            var comboBox = $find("<%= txtMarketSelect.ClientID %>");    
            var tree = comboBox.get_items().getItem(0).findControl("radTreeCities");
            
            var rootNodes = tree.SelectedNode;
            var nodes = tree.get_allNodes(); 
            
            var i;
            
             for(i=0; i<nodes.length; i++)
             {                               
                if(!nodes[i].get_checked())
                {
                    
                }
                else
                {
                    isMarketSelected = 1;
                }
             }             
            if (isMarketSelected == 0)
            {
                alert("You have not selected any  Market/s");
                return false;
            }            

           
            tdhide('leftMenu', 'ctl00_showHide','hide');
                        
            disp_clock();
            return true;

                
        } 
        function days_between(date1, date2) {

            // The number of milliseconds in one day
            var ONE_DAY = 1000 * 60 * 60 * 24

            // Convert both dates to milliseconds
            var date1_ms = date1.getTime()
            var date2_ms = date2.getTime()

            // Calculate the difference in milliseconds
            var difference_ms = Math.abs(date1_ms - date2_ms)
            
            // Convert back to days and return
            return Math.round(difference_ms/ONE_DAY)

        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table width="100%">
            <tr>
                <td align="left">
                    <asp:Label ID="Pg_Title" runat="server"></asp:Label>
                </td>
                <td>
                </td>
                <td align="right">
                    <asp:Label ID="lbl_Logout" runat="server" Text="Label">Welcome: Administrator[Logout]</asp:Label>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td class="style1">
                </td>
                <td class="style2">
                </td>
                <td class="style3">
                    &nbsp;
                </td>
                <td>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Start Date:
                </td>
                <td class="style2">
                    End Date:
                </td>
                <td class="style3">
                    &nbsp;
                </td>
                <td>
                    Select Station:
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="style1" style="margin-left: 40px">
                    <telerik:RadDatePicker ID="dtStart" runat="server" Culture="English (United States)"
                        MinDate="2004-01-01">
                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                        </Calendar>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
                </td>
                <td class="style2">
                    <telerik:RadDatePicker ID="dtEnd" runat="server" Culture="English (United States)"
                        MinDate="2004-01-01">
                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                        </Calendar>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
                </td>
                <td class="style3">
                </td>
                <td style="margin-left: 40px">
                    <telerik:RadComboBox ID="txtMarketSelect" runat="server" AfterClientCheck="AfterCheckHandler"
                        CollapseAnimation-Type="None" EmptyMessage="Choose a destination" ExpandAnimation-Type="None"
                        OnClientDropDownOpened="OnClientDropDownOpenedHandler" ShowToggleImage="True"
                        Style="vertical-align: middle;" Width="250px">
                        <ItemTemplate>
                            <div id="div2">
                                <telerik:RadTreeView ID="radTreeCities" runat="server" Height="200px" OnClientNodeChecked="nodeChecked"
                                    OnClientNodeClicked="nodeClicking" CheckBoxes="true" Width="100%" TriStateCheckBoxes="true">
                                </telerik:RadTreeView>
                            </div>
                        </ItemTemplate>
                        <Items>
                            <telerik:RadComboBoxItem Text="" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtSearch" runat="server">
                        <PasswordStrengthSettings ShowIndicator="False" CalculationWeightings="50;15;15;20"
                            PreferredPasswordLength="10" MinimumNumericCharacters="2" RequiresUpperAndLowerCaseCharacters="True"
                            MinimumLowerCaseCharacters="2" MinimumUpperCaseCharacters="2" MinimumSymbolCharacters="2"
                            OnClientPasswordStrengthCalculating="" TextStrengthDescriptions="Very Weak;Weak;Medium;Strong;Very Strong"
                            TextStrengthDescriptionStyles="riStrengthBarL0;riStrengthBarL1;riStrengthBarL2;riStrengthBarL3;riStrengthBarL4;riStrengthBarL5;"
                            IndicatorElementBaseStyle="riStrengthBar" IndicatorElementID=""></PasswordStrengthSettings>
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadButton ID="btnGo" runat="server" Skin="Web20" Text="GO" ToolTip="Go"
                        OnClick="btnGo_Click" OnClientClicked="OnClientClicked">
                        <Icon PrimaryIconCssClass="rbSearch" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
                    </telerik:RadButton>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="style1">
                </td>
                <td class="style2">
                </td>
                <td class="style3">
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                    
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                </td>
            </tr>
        </table>

<telerik:RadGrid ID="gvData" runat="server" CellSpacing="0" EnableViewState="False" 
        GridLines="None" onitemcreated="gvData_ItemCreated" Width="2000px">
<mastertableview allowfilteringbycolumn="False" Height="95%">
                          </mastertableview>  
                         <SortingSettings  EnableSkinSortStyles="false" />
                          
                          <clientsettings allowdragtogroup="True" allowrowsdragdrop="True"  enablerowhoverstyle="True"  >
                                 <selecting allowrowselect="True" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="true"   >
                                 </Scrolling>
                          </clientsettings>
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        
</telerik:RadGrid>
                
</asp:Content>

