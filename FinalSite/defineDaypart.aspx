﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="defineDaypart.aspx.cs" Inherits="defineDaypart" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Define Daypart</title>

    <style type="text/css">
        .style1
        {
            width: 400px;
        }
        .style2
        {
            width: 99px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
        .style3
        {
            width: 87px;
            text-align: center;
        }
        .style5
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
        .style6
        {
            text-align: center;
            background-color: #FEFFFF;
        }
        .style8
        {
            width: 87px;
            background-color: #C2DBE8;
            text-align: center;
        }
        .style9
        {
            width: 99px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            background-color: #C2DBE8;
        }
        .style10
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: center;
        }
        .style11
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            background-color: #C2DBE8;
            text-align: center;
        }
        .style12
        {
            background-color: #FEFFFF;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
    <div>
<asp:Label ID="lblError" runat="server" 
            style="font-family: Arial, Helvetica, sans-serif; font-size: small; color: #CC3300"></asp:Label>    
        <table class="style1">
            <tr>
                <td class="style9">
                    Daypart 1</td>
                <td class="style8">
                    <asp:DropDownList ID="ddDayPart1From" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style11">
                    To</td>
                <td class="style11">
                    <asp:DropDownList ID="ddDayPart1To" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style6">
                    <asp:Button ID="btnSelect1" runat="server" Forecolor="white" font-names="Arial" font-bold="true" backcolor="#336699" font-size="8" Text="Select" 
                        onclick="btnSelect1_Click" />
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Daypart 2</td>
                <td class="style3">
                    <asp:DropDownList ID="ddDayPart2From" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style10">
                    To</td>
                <td class="style10">
                    <asp:DropDownList ID="ddDayPart2To" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                       
                    </asp:DropDownList>
                </td>
                <td class="style6">
                    <asp:Button  Forecolor="white" font-names="Arial" font-bold="true" backcolor="#336699" font-size="8"  ID="btnSelect2" runat="server" Text="Select" 
                        onclick="btnSelect2_Click" />
                </td>
            </tr>
            <tr>
                <td class="style9">
                    Daypart 3</td>
                <td class="style8">
                    <asp:DropDownList ID="ddDayPart3From" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style11">
                    To</td>
                <td class="style11">
                    <asp:DropDownList ID="ddDayPart3To" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style6">
                    <asp:Button  Forecolor="white" font-names="Arial" font-bold="true" backcolor="#336699" font-size="8"  ID="btnSelect3" runat="server" Text="Select" 
                        onclick="btnSelect3_Click" />
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Daypart 4</td>
                <td class="style3">
                    <asp:DropDownList ID="ddDayPart4From" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style10">
                    To</td>
                <td class="style10">
                    <asp:DropDownList ID="ddDayPart4To" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style6">
                    <asp:Button  Forecolor="white" font-names="Arial" font-bold="true" backcolor="#336699" font-size="8"  ID="btnSelect4" runat="server" Text="Select" 
                        onclick="btnSelect4_Click" />
                </td>
            </tr>
            <tr>
                <td class="style9">
                    Daypart 5</td>
                <td class="style8">
                    <asp:DropDownList ID="ddDayPart5From" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                       
                    </asp:DropDownList>
                </td>
                <td class="style11">
                    To</td>
                <td class="style11">
                    <asp:DropDownList ID="ddDayPart5To" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style6">
                    <asp:Button  Forecolor="white" font-names="Arial" font-bold="true" backcolor="#336699" font-size="8"  ID="btnSelect5" runat="server" Text="Select" 
                        onclick="btnSelect5_Click" />
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Daypart 6</td>
                <td class="style3">
                    <asp:DropDownList ID="ddDayPart6From" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style10">
                    To</td>
                <td class="style10">
                    <asp:DropDownList ID="ddDayPart6To" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style6">
                    <asp:Button  Forecolor="white" font-names="Arial" font-bold="true" backcolor="#336699" font-size="8"  ID="btnSelect6" runat="server" Text="Select" 
                        onclick="btnSelect6_Click" />
                </td>
            </tr>
            <tr>
                <td class="style9">
                    Daypart 7</td>
                <td class="style8">
                    <asp:DropDownList ID="ddDayPart7From" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style11">
                    To</td>
                <td class="style11">
                    <asp:DropDownList ID="ddDayPart7To" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style6">
                    <asp:Button  Forecolor="white" font-names="Arial" font-bold="true" backcolor="#336699" font-size="8"  ID="btnSelect7" runat="server" Text="Select" 
                        onclick="btnSelect7_Click" />
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Daypart 8</td>
                <td class="style3">
                    <asp:DropDownList ID="ddDayPart8From" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style10">
                    To</td>
                <td class="style10">
                    <asp:DropDownList ID="ddDayPart8To" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style6">
                    <asp:Button  Forecolor="white" font-names="Arial" font-bold="true" backcolor="#336699" font-size="8"  ID="btnSelect8" runat="server" Text="Select" 
                        onclick="btnSelect8_Click" />
                </td>
            </tr>
            <tr>
                <td class="style9">
                    Daypart 9</td>
                <td class="style8">
                    <asp:DropDownList ID="ddDayPart9From" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style11">
                    To</td>
                <td class="style11">
                    <asp:DropDownList ID="ddDayPart9To" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                       
                    </asp:DropDownList>
                </td>
                <td class="style6">
                    <asp:Button  Forecolor="white" font-names="Arial" font-bold="true" backcolor="#336699" font-size="8"  ID="btnSelect9" runat="server" Text="Select" 
                        onclick="btnSelect9_Click" />
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Daypart 10</td>
                <td class="style3">
                    <asp:DropDownList ID="ddDayPart10From" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style10">
                    To</td>
                <td class="style10">
                    <asp:DropDownList ID="ddDayPart10To" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style6">
                    <asp:Button  Forecolor="white" font-names="Arial" font-bold="true" backcolor="#336699" font-size="8"  ID="btnSelect10" runat="server" Text="Select" 
                        onclick="btnSelect10_Click" />
                </td>
            </tr>
            <tr>
                <td class="style9">
                    Daypart 11</td>
                <td class="style8">
                    <asp:DropDownList ID="ddDayPart11From" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                       
                    </asp:DropDownList>
                </td>
                <td class="style11">
                    To</td>
                <td class="style11">
                    <asp:DropDownList ID="ddDayPart11To" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style6">
                    <asp:Button  Forecolor="white" font-names="Arial" font-bold="true" backcolor="#336699" font-size="8"  ID="btnSelect11" runat="server" Text="Select" 
                        onclick="btnSelect11_Click" />
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Daypart 12</td>
                <td class="style3">
                    <asp:DropDownList ID="ddDayPart12From" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style10">
                    To</td>
                <td class="style10">
                    <asp:DropDownList ID="ddDayPart12To" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="1">00:00</asp:ListItem>
                        <asp:ListItem Value="2">01:00</asp:ListItem>
                        <asp:ListItem Value="3">02:00</asp:ListItem>                        
                        <asp:ListItem Value="4">03:00</asp:ListItem>                        
                        <asp:ListItem Value="5">04:00</asp:ListItem>                        
                        <asp:ListItem Value="6">05:00</asp:ListItem>                        
                        <asp:ListItem Value="7">06:00</asp:ListItem>                        
                        <asp:ListItem Value="8">07:00</asp:ListItem>                        
                        <asp:ListItem Value="9">08:00</asp:ListItem>                        
                        <asp:ListItem Value="10">09:00</asp:ListItem>                        
                        <asp:ListItem Value="11">10:00</asp:ListItem>                        
                        <asp:ListItem Value="12">11:00</asp:ListItem>                        
                        <asp:ListItem Value="13">12:00</asp:ListItem>                        
                        <asp:ListItem Value="14">13:00</asp:ListItem>                        
                        <asp:ListItem Value="15">14:00</asp:ListItem>                        
                        <asp:ListItem Value="16">15:00</asp:ListItem>                        
                        <asp:ListItem Value="17">16:00</asp:ListItem>                        
                        <asp:ListItem Value="18">17:00</asp:ListItem>                        
                        <asp:ListItem Value="19">18:00</asp:ListItem>                        
                        <asp:ListItem Value="20">19:00</asp:ListItem>                        
                        <asp:ListItem Value="21">20:00</asp:ListItem>                        
                        <asp:ListItem Value="22">21:00</asp:ListItem>                        
                        <asp:ListItem Value="23">22:00</asp:ListItem>                        
                        <asp:ListItem Value="24">23:00</asp:ListItem>                        
                        <asp:ListItem Value="25">24:00</asp:ListItem>                        
                    </asp:DropDownList>
                </td>
                <td class="style6">
                    <asp:Button  Forecolor="white" font-names="Arial" font-bold="true" backcolor="#336699" font-size="8"  ID="btnSelect12" runat="server" Text="Select" 
                        onclick="btnSelect12_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: center; " class="style12">
                    <asp:Button  Forecolor="white" font-names="Arial" font-bold="true" 
                        backcolor="#336699" font-size="8"  ID="btnReset" runat="server" Text="Reset" 
                         CssClass="style5" onclick="btnReset_Click" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:Button  Forecolor="white" font-names="Arial" font-bold="true" 
                        backcolor="#336699" font-size="8"  ID="btnOK0" runat="server" Text="OK" 
                        OnClientClick="javascript: return self.close();" CssClass="style5" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
