﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class TopAdvertiserBrands : System.Web.UI.UserControl
{
    DataTable dtInner;
    protected void Page_Load(object sender, EventArgs e)
    {
        //DataGridItem dgi = (DataGridItem)this.BindingContainer;
        //DataSet ds = (DataSet)((System.Data.DataRowView)(dgi.DataItem)).DataView.DataViewManager.DataSet;
        ////DataSet ds = (DataSet)((DataGridItem)this.BindingContainer).DataItem;

        //DG1.DataSource = ds;
        //DG1.DataMember = ds.Tables[1].TableName;
        //DG1.DataBind();
       
    }

    protected void DG1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string strBrandName = "";
        string strParentName = "";
        string strCategory = "";

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.HorizontalAlign = HorizontalAlign.Center;
        }

        string headerFontName = (string)Session["headerFontName"];//ConfigurationSettings.AppSettings["headerFontNameMarketShareOfEachStation"].ToString();
        string headerFontSize = (string)Session["headerFontSize"];//ConfigurationSettings.AppSettings["headerFontSizeMarketShareOfEachStation"].ToString();

        string actualDataFontName = (string)Session["dataFontName"];//ConfigurationSettings.AppSettings["actualDataFontNameMarketShareOfEachStation"].ToString();
        string actualDataFontSize = (string)Session["dataFontSize"];//ConfigurationSettings.AppSettings["actualDataFontSizeMarketShareOfEachStation"].ToString();

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", headerFontName);
            //e.Item.Style.Add("font-weight", "bold");
            //e.Item.Style.Add("font-size", headerFontSize);
            int i = 0;
            foreach (DataColumn dc in dtInner.Columns)
            {
                if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                {
                    e.Item.Cells[i].Font.Bold = true;
                }
                else
                {
                    e.Item.Cells[i].Font.Bold = false;
                }
                e.Item.Cells[i].Font.Size = FontUnit.Parse(Session["headerFontSize"].ToString().Replace("Px", "").Trim());
                i++;
            }
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            //e.Item.Style.Add("font-weight", "bold");
            //e.Item.Style.Add("font-size", actualDataFontSize);
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            int i = 0;
            foreach(DataColumn dc in dtInner.Columns)
            {
                bool isANumber = false;
                int intNum;
                if (Session["dataFontStyle"].ToString().ToLower() == "bold")
                {
                    e.Item.Cells[i].Font.Bold = true;
                }
                else
                {
                    e.Item.Cells[i].Font.Bold = false;
                }
                e.Item.Cells[i].Font.Size = FontUnit.Parse(Session["dataFontSize"].ToString().Replace("Px", "").Trim());

                if (dc.ColumnName != "brandID" && dc.ColumnName != "parentID")
                {
                    if (dc.ColumnName == "Brand Name")
                    {
                        strBrandName = doStringSanitation(e.Item.Cells[i].Text);
                    }
                    if (dc.ColumnName == "Advertiser")
                    {
                        strParentName = doStringSanitation(e.Item.Cells[i].Text);
                    }

                    if (dc.ColumnName == "Category")
                    {
                        strCategory = doStringSanitation(e.Item.Cells[i].Text);
                    }

                    if (dc.ColumnName == "No Of Plays" || dc.ColumnName == "Total Seconds")
                    {
                        isANumber = int.TryParse(e.Item.Cells[i].Text, out intNum);
                        if (isANumber)
                        {
                            e.Item.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                        }

                        if (isANumber)
                        {
                            e.Item.Cells[i].Text = "<a href='' onclick=\"javascript:return disp_details('" + doStringSanitation(strBrandName) + "','" + doStringSanitation(strParentName) + "','" + doStringSanitation(strCategory) + "')\"  alt='Click here to see the details'>" + e.Item.Cells[i].Text + "</a>";
                        }                    
                    }

                }
                i++;
            }



            
        }

        if (e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //e.Item.BackColor = System.Drawing.Color.FromArgb(149, 179, 215);
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
        }
    }

    protected string doStringSanitation(string str)
    {
        if (str.IndexOf("'") >= 0)
        {
            str = str.Replace("'", "123123");
        }
        if (str.IndexOf("&") >= 0)
        {
            str = str.Replace("&", "456456");
        }

        return str;
    }

    protected void DG1_DataBinding(object sender, EventArgs e)
    {

    }


    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    ///		Required method for Designer support - do not modify
    ///		the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new System.EventHandler(this.Page_Load);
        this.DataBinding += new System.EventHandler(this.SalesList_DataBinding);

    }
    #endregion

    private void SalesList_DataBinding(object sender, System.EventArgs e)
    {
        //if an InvalidCastException occurs in either of the next two lines, 
        //please make sure that you've set the TemplateDataMode to Table (because you want nested grids)
        DataGridItem dgi = (DataGridItem)this.BindingContainer;
        if (!(dgi.DataItem is DataSet))
            throw new ArgumentException("Please change the TemplateDataMode attribute to 'Table' in the HierarGrid declaration");
        DataSet ds = (DataSet)dgi.DataItem;
        dtInner = ds.Tables[1];
        DG1.DataSource = ds;
        DG1.DataMember = ds.Tables[1].TableName;
        DG1.DataBind();
    }
}
