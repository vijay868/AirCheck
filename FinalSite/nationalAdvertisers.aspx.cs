﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using Telerik.Web.UI;

public partial class nationalAdvertisers : System.Web.UI.Page
{
    bool isPdfExport = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder onloadScript = new StringBuilder();
        //onloadScript.Append("<script type='text/javascript'>");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("ShowReport();");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("</script>");
        //this.ClientScript.RegisterStartupScript(this.GetType(), "onLoadCall", onloadScript.ToString());

        //((Label)this.Master.FindControl("lblReportName")).Text = "National Advertiser/National Advertiser By Brand";

        //Literal lit = new Literal();
        //lit.Text = "<script language='javascript'>tdhide('leftMenu', 'ctl00_showhide', 'hide');</script>";
        //Page.Controls.Add(lit);

        getDateFormat();
        string strDateFormat = (string)Session["dateFormat"];

        if (strDateFormat == "DD/MM/YYYY")
        {
            dtStart.DateInput.DateFormat = "dd/MM/yyyy";
            dtEnd.DateInput.DateFormat = "dd/MM/yyyy";

            dtStart.DateInput.DisplayDateFormat = "dd/MM/yyyy";
            dtEnd.DateInput.DisplayDateFormat = "dd/MM/yyyy";
        }


        if (!IsPostBack)
        {
            Session["NAds"] = null;
            getUserPreferencesForCurrentUser();
            setDate();
            getCities();
            GenerateTreeViewStations();
            GenerateTreeViewNetworks();
            GenerateTreeViewMarkets();
            formatsearchGrid();
            //ddSelectBy.Attributes.Add("OnChange", "showHideNextColumn(this);");

            if (Session["dataReadOnly"].ToString() == "True")
            {
                btnExportToExcel.Enabled = false;
                btnExportToPdf.Enabled = false;
            }
            else
            {
                btnExportToExcel.Enabled = true;
                btnExportToPdf.Enabled = true;
            }

        }
        Pg_Title.Text = "National Advertisers";
    }

    protected void getUserPreferencesForCurrentUser()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getUserPreferenceForAUserAndReport", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@user", (string)Session["user"]);
        da.SelectCommand.Parameters.AddWithValue("@reportName", "Top Advertisers By Brands");

        DataSet ds = new DataSet();
        da.Fill(ds);

        Session["headerFontName"] = ds.Tables[0].Rows[0]["headerFontName"].ToString();
        Session["headerFontSize"] = ds.Tables[0].Rows[0]["headerFontSize"].ToString();
        Session["headerFontColor"] = ds.Tables[0].Rows[0]["headerFontColor"].ToString();
        Session["headerRowColor"] = ds.Tables[0].Rows[0]["headerRowColor"].ToString();
        Session["headerFontStyle"] = ds.Tables[0].Rows[0]["headerFontStyle"].ToString();
        Session["dataFontName"] = ds.Tables[0].Rows[0]["dataFontName"].ToString();
        Session["dataFontSize"] = ds.Tables[0].Rows[0]["dataFontSize"].ToString();
        Session["dataFontColor"] = ds.Tables[0].Rows[0]["dataFontColor"].ToString();
        Session["alternateRowColor"] = ds.Tables[0].Rows[0]["alternateRowColor"].ToString();
        Session["dataFontStyle"] = ds.Tables[0].Rows[0]["dataFontStyle"].ToString();

    }


    protected void GenerateTreeViewStations()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getStationsForUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);

        var radTreeStations = (RadTreeView)CmbTreeStations.Items[0].FindControl("radTreeStations");
        radTreeStations.DataFieldID = "name";
        radTreeStations.DataFieldParentID = "m_name";
        radTreeStations.DataTextField = "name";

        radTreeStations.DataSource = ds.Tables[0];
        radTreeStations.DataBind();
    }

    protected void GenerateTreeViewNetworks()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getNetworksForUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);

        ddNetworks.DataSource = ds.Tables[0];
        ddNetworks.DataTextField = "s_name";
        ddNetworks.DataValueField = "s_name";
        ddNetworks.DataBind();

        //radTreeNetworks.DataFieldID = "s_name";
        //radTreeNetworks.DataFieldParentID = "p_name";
        //radTreeNetworks.DataTextField = "s_name";
        //radTreeNetworks.ExpandAllNodes();

        //radTreeNetworks.DataSource = ds.Tables[0];
        //radTreeNetworks.DataBind();
    }

    protected void ddNetworks_SelectedIndexChanged(object sender, EventArgs e)
    {
        var radTreeMarketsForSelectedNetwork = (RadTreeView)CmbTreeMarketsForSelectedNetwork.Items[0].FindControl("radTreeMarketsForSelectedNetwork");
        if (ddNetworks.SelectedItem.Text != "")
        {
            GenerateTreeViewMarketsForSelectedNetwork();
            foreach (RadTreeNode node in radTreeMarketsForSelectedNetwork.GetAllNodes())
            {
                node.Expanded = true;
                node.Checked = true;
            }
        }
        else
        {
            radTreeMarketsForSelectedNetwork.Nodes.Clear();
        }
    }

    protected void GenerateTreeViewMarketsForSelectedNetwork()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getMarketsForSelectedNetworkAndUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@station", ddNetworks.SelectedItem.Text);
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);

        var radTreeMarketsForSelectedNetwork = (RadTreeView)CmbTreeMarketsForSelectedNetwork.Items[0].FindControl("radTreeMarketsForSelectedNetwork");

        radTreeMarketsForSelectedNetwork.DataFieldID = "m_name";
        radTreeMarketsForSelectedNetwork.DataFieldParentID = "p_name";
        radTreeMarketsForSelectedNetwork.DataTextField = "m_name";
        radTreeMarketsForSelectedNetwork.ExpandAllNodes();
        radTreeMarketsForSelectedNetwork.DataSource = ds.Tables[0];
        radTreeMarketsForSelectedNetwork.DataBind();
    }

    protected void GenerateTreeViewMarkets()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getMarketsForUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);

        var radTreeMarkets = (RadTreeView)CmbTreeMarkets.Items[0].FindControl("radTreeMarkets");

        radTreeMarkets.DataFieldID = "m_name";
        radTreeMarkets.DataFieldParentID = "p_name";
        radTreeMarkets.DataTextField = "m_name";
        radTreeMarkets.ExpandAllNodes();
        radTreeMarkets.DataSource = ds.Tables[0];
        radTreeMarkets.DataBind();

        radTreeMarkets.ExpandAllNodes();
        foreach (RadTreeNode node in radTreeMarkets.GetAllNodes())
        {
            node.Expanded = true;
            node.Checked = true;
        }
    }

    protected void Page_Init(object source, System.EventArgs e)
    {
    }


    protected void setDate()
    {
        DateTime lockDataBefore = DateTime.Parse(Session["lockDataBefore"].ToString());
        //DateTime lockDataBefore = System.DateTime.Now.AddYears(-1);
        DateTime lockDataAfter = DateTime.Parse(Session["lockDataAfter"].ToString());
        dtStart.MinDate = lockDataBefore;
        dtEnd.MinDate = lockDataBefore;

        dtStart.MaxDate = lockDataAfter;
        dtEnd.MaxDate = lockDataAfter;

        Session["lockDataOneYear"] = lockDataBefore;

        dtStart.SelectedDate = System.DateTime.Now;
        dtEnd.SelectedDate = System.DateTime.Now;
    }

    protected void getDateFormat()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("select * from India_Report_New..Country_dateFormat where isSelected=1", conn);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                Session["dateFormat"] = dr["DateFormat"].ToString();
            }
        }

        conn.Close();
    }

    protected void getCities()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getMarketIDandNameOfMarketsForUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);


        ddCity.DataTextField = "name";
        ddCity.DataValueField = "market_id";
        ddCity.DataBind();
    }

    protected string getStationIDs_now()
    {
        string strStations = string.Empty;
        var radTreeStations = (RadTreeView)CmbTreeStations.Items[0].FindControl("radTreeStations");
        //ArrayList nodeCollection = radTreeStations.CheckedNodes;
        int stationID = 0;
        string strStationNames = "";
        foreach (RadTreeNode node in radTreeStations.CheckedNodes)
        {
            stationID = getStationIDOfStation(node.Text);
            if (stationID != 0)
            {
                strStations += stationID.ToString() + ",";
                strStationNames += node.Text + ",";
            }
        }

        return strStations.Substring(0, strStations.Length - 1);
    }

    protected int getStationIDOfStation(string txtStation)
    {
        int stationID = 0;
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("getStationIDOfStation", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@stationName", txtStation);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                stationID = (int)dr["station_id"];
            }
        }
        else
        {
            stationID = 0;
        }

        conn.Close();
        return stationID;
    }


    protected string getStationIDsOfMarket()
    {
        string strStations = string.Empty;
        var radTreeMarkets = (RadTreeView)CmbTreeMarkets.Items[0].FindControl("radTreeMarkets");
        //ArrayList nodeCollection = radTreeMarkets.CheckedNodes;
        string strStationID = "";
        string strStationNames = "";

        foreach (RadTreeNode node in radTreeMarkets.CheckedNodes)
        {
            strStationID = getStationsOfMarket(node.Text);
            if (strStationID != "")
            {
                strStations += strStationID.ToString() + ",";
                strStationNames += node.Text + ",";
            }
        }

        return strStations.Substring(0, strStations.Length - 1);
    }

    protected string getStationsOfMarket(string txtMarket)
    {
        string strStationID = "";
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("getStationIDOfMarket", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@marketName", txtMarket);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                strStationID += dr["station_id"] + ",";
            }
        }
        else
        {
            strStationID = "";
        }

        conn.Close();

        if (strStationID != "")
        {
            strStationID = strStationID.Substring(0, strStationID.Length - 1);
        }

        return strStationID;

    }


    protected string getStationIDsOfNetwork()
    {
        string strStations = string.Empty;
        var radTreeMarketsForSelectedNetwork = (RadTreeView)CmbTreeMarketsForSelectedNetwork.Items[0].FindControl("radTreeMarketsForSelectedNetwork");
        //ArrayList nodeCollectionMarkets = radTreeMarketsForSelectedNetwork.CheckedNodes;
        string strStationID = "";
        string strStationNames = "";
        string strTempStationID = "";
        foreach (RadTreeNode market in radTreeMarketsForSelectedNetwork.CheckedNodes)
        {
            strTempStationID = getStationsOfNetwork(ddNetworks.SelectedItem.Text, market.Text);
            if (strTempStationID != "")
            {
                strStationID += strTempStationID + ",";
                strStationNames += market.Text + ",";
            }
        }


        return strStationID.Substring(0, strStationID.Length - 1);
    }

    protected string getStationsOfNetwork(string txtNetwork, string txtMarket)
    {
        string strStationID = "";
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("getStationIDOfNetworkAndMarket", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@networkName", txtNetwork);
        comm.Parameters.AddWithValue("@marketName", txtMarket);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                strStationID += dr["station_id"].ToString() + ",";
            }
        }
        else
        {
            strStationID = "";
        }

        conn.Close();

        string strReturn = "";
        if (strStationID == "")
        {
            strReturn = strStationID;
        }
        else
        {
            strReturn = strStationID.Substring(0, strStationID.Length - 1);
        }
        return strReturn;

    }

    protected string getStationIDs()
    {
        string strStationIDs = "";

        string strMarkets = "";
        string strNetworks = "";


        bool isNetwork = false;
        bool isMarketOrNetwork = false;

        if (ddSelectBy.Text == "Station")
        {
            strStationIDs = getStationIDs_now();
            isMarketOrNetwork = false;
        }
        else if (ddSelectBy.Text == "Market")
        {
            strStationIDs = getStationIDsOfMarket();
            isMarketOrNetwork = true;
            isNetwork = false;

            strMarkets = "";
        }
        else if (ddSelectBy.Text == "Network")
        {
            isMarketOrNetwork = true;
            isNetwork = true;

            strMarkets = "";
            strNetworks = "";

            strStationIDs = getStationIDsOfNetwork();

        }

        return strStationIDs;
    }

    protected void displayNoRecordFound()
    {
        lblError.Visible = true;
        lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
        radData.Visible = false;
        litExportTo.Visible = false;
        btnExportToExcel.Visible = false;
        btnExportToPdf.Visible = false;
    }

    protected void addAuditLog(DateTime startDate, DateTime endDate, string station,
Boolean exportedToExcel, Boolean exportedToPdf, Boolean exportedToText)
    {
        auditTrail at = new auditTrail();


        at.connectionString = ConfigurationSettings.AppSettings["connectionString"].ToString();
        at.userID = (int)Session["userID"];
        at.IPAddress = Request.ServerVariables["REMOTE_ADDR"];
        at.pageViewed = "National Advertiser/National Advertiser By Brand";
        at.startDate = startDate;
        at.endDate = endDate;
        at.station = station;
        at.exportedToExcel = exportedToExcel;
        at.exportedToPdf = exportedToPdf;
        at.exportedToText = exportedToText;
        at.addUserActivityLogPassingStationIDs();
        at = null;
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        var radTreeMarketsForSelectedNetwork = (RadTreeView)CmbTreeMarketsForSelectedNetwork.Items[0].FindControl("radTreeMarketsForSelectedNetwork");
        var radTreeStations = (RadTreeView)CmbTreeStations.Items[0].FindControl("radTreeStations");
        if (ddSelectBy.SelectedItem.Text != "Network")
        {
            ddNetworks.SelectedIndex = -1;
            radTreeMarketsForSelectedNetwork.Nodes.Clear();
        }

        if (ddSelectBy.SelectedItem.Text == "Market" || ddSelectBy.SelectedItem.Text == "Network")
        {
            radTreeStations.CollapseAllNodes();
        }

        string strStationIDs = getStationIDs();

        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());

        addAuditLog(dtStartDate, dtEndDate, strStationIDs, false, false, false);

        bindGrid("");
    }

    protected void radData_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {

        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());

        string strStartDate = dtStartDate.ToString("d");
        string strEndDate = dtEndDate.ToString("d");

        string strStationIDs = getStationIDs();



        if (e.Item.ItemType == GridItemType.Header)
        {
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            for (int i = 0; i < e.Item.Cells.Count; i++)
            {
                //if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                //{
                //    e.Item.Cells[i].Font.Bold = true;
                //}
                //else
                //{
                //    e.Item.Cells[i].Font.Bold = true;
                //}
                e.Item.Cells[i].Font.Name = "Helvetica";
                e.Item.Cells[i].Font.Size = FontUnit.Parse("14px");
                i++;
            }
            //e.Item.BackColor = System.Drawing.Color.FromArgb(0, 112, 192);
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        }



        if (e.Item.ItemType == Telerik.Web.UI.GridItemType.Item || e.Item.ItemType == Telerik.Web.UI.GridItemType.AlternatingItem)
        {
            string actualDataFontName = "Helvetica";
            string actualDataFontSize = "12px";

            Telerik.Web.UI.GridDataItem dataItem = e.Item as Telerik.Web.UI.GridDataItem;

            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            dataItem.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            dataItem.Font.Size = FontUnit.Parse(Session["dataFontSize"].ToString().Replace("Px", "").Trim());
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                dataItem.Font.Bold = true;
            }
            else
            {
                dataItem.Font.Bold = false;
            }

            if (ddadvtcat.Text == "Advertiser")
            {

                dataItem["Advertiser"].Font.Name = actualDataFontName;
                dataItem["Advertiser"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());

                //dataItem["Advertiser"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
                dataItem["No Of Brands"].Font.Name = actualDataFontName;
                dataItem["No Of Brands"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
                dataItem["No Of Brands"].HorizontalAlign = HorizontalAlign.Center;
                dataItem["No Of Plays"].Font.Name = actualDataFontName;
                dataItem["No Of Plays"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
                dataItem["No Of Plays"].HorizontalAlign = HorizontalAlign.Center;
                dataItem["Total Seconds"].Font.Name = actualDataFontName;
                dataItem["Total Seconds"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
                dataItem["Total Seconds"].HorizontalAlign = HorizontalAlign.Center;

            }
            else if (ddadvtcat.Text == "Both")
            {
                dataItem["Brand Name"].Font.Name = actualDataFontName;
                dataItem["Brand Name"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
                dataItem["Advertiser"].Font.Name = actualDataFontName;
                dataItem["Advertiser"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
                dataItem["Category"].Font.Name = actualDataFontName;
                dataItem["Category"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());

            }
            else if (ddadvtcat.Text == "Brand")
            {
                dataItem["Brand Name"].Font.Name = actualDataFontName;
                dataItem["Brand Name"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
                dataItem["Advertiser"].Font.Name = actualDataFontName;
                dataItem["Advertiser"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
                dataItem["Category"].Font.Name = actualDataFontName;
                dataItem["Category"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());

            }


            //    dataItem["Advertiser"].Font.Name = actualDataFontName;
            //    dataItem["Advertiser"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());

            //    dataItem["Brand Name"].Font.Name = actualDataFontName;
            //    dataItem["Brand Name"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());

            //    dataItem["Category"].Font.Name = actualDataFontName;
            //    dataItem["Category"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());

            //    dataItem["No Of Plays"].Font.Name = actualDataFontName;
            //    dataItem["No Of Plays"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            //    dataItem["No Of Plays"].HorizontalAlign = HorizontalAlign.Center;

            //    dataItem["Total Seconds"].Font.Name = actualDataFontName;
            //    dataItem["Total Seconds"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            //    dataItem["Total Seconds"].HorizontalAlign = HorizontalAlign.Center;

            //    strSong = doStringSanitation(e.Item.Cells[3].Text.ToString());
            //    strMovie = doStringSanitation(e.Item.Cells[4].Text.ToString());

            //    if (!isPdfExport)
            //    {

            //        string strBrandName = doStringSanitation(dataItem["Brand Name"].Text);
            //        string strParentName = doStringSanitation(dataItem["Advertiser"].Text);
            //        string strCategory = doStringSanitation(dataItem["Category"].Text);


            // dataItem["Advertiser"].Text = "<a href='http://yahoo.com'>dfds</a>";
            //        dataItem["Total Seconds"].Text = "<a href='' onclick=\"javascript:return disp_details('" + strBrandName + "','" + strParentName + "','" + strCategory + "')\"  alt='Click here to see the details'>" + dataItem["Total Seconds"].Text + "</a>";
            //    }
            //}

            if (e.Item.ItemType == GridItemType.AlternatingItem)
            {
                if (!isPdfExport)
                {
                    System.Drawing.ColorConverter conv1 = new System.Drawing.ColorConverter();
                    e.Item.BackColor = (System.Drawing.Color)conv1.ConvertFromString((string)Session["alternateRowColor"]);
                }
            }

            if (ddadvtcat.Text == "Advertiser")
            {
                if (!isPdfExport)
                {
                    dataItem["Advertiser"].Text = "<a href='nationalAdvertisersByBrands.aspx?StationIDQ=" + strStationIDs + "&sdate=" + strStartDate + "&edate=" + strEndDate + "&scrtxt=" + txt_top.Text + "&scrcriteria=" + ddadvtcat.Text + "&advt=" + doStringSanitation(dataItem["Advertiser"].Text) + "' style='color:Black;font-size:12 px'>" + dataItem["Advertiser"].Text + "</a>";
                }

            }
            if (ddadvtcat.Text == "Both")
            {
                if (!isPdfExport)
                {
                    dataItem["Brand Name"].Text = "<a href='nationalAdvertisersByStations.aspx?StationIDQ=" + strStationIDs + "&sdate=" + strStartDate + "&edate=" + strEndDate + "&scrtxt=" + txt_top.Text + "&scrcriteria=" + ddadvtcat.Text + "&advt=" + doStringSanitation(dataItem["Advertiser"].Text) + "&brandnames=" + doStringSanitation(dataItem["Brand Name"].Text) + "&tvbrand=" + doStringSanitation(dataItem["Category"].Text) + "' style='color:Black;font-size:12 px'>" + dataItem["Brand Name"].Text + "</a>";

                }

            }
            if (ddadvtcat.Text == "Brand")
            {
                if (!isPdfExport)
                {
                    dataItem["Brand Name"].Text = "<a href='nationalAdvertisersByStations.aspx?StationIDQ=" + strStationIDs + "&sdate=" + strStartDate + "&edate=" + strEndDate + "&scrtxt=" + txt_top.Text + "&scrcriteria=" + ddadvtcat.Text + "&advt=" + doStringSanitation(dataItem["Advertiser"].Text) + "&brandnames=" + doStringSanitation(dataItem["Brand Name"].Text) + "&tvbrand=" + doStringSanitation(dataItem["Category"].Text) + "' style='color:Black;font-size:12 px'>" + dataItem["Brand Name"].Text + "</a>";
                }

            }
        }
        if (e.Item.ItemType == GridItemType.Footer)
        {
            if (!isPdfExport)
            {
                e.Item.Visible = false;
            }
            else
            {
                e.Item.Visible = true;

                GridFooterItem footerItem = (GridFooterItem)e.Item;

                string strDateFormat = (string)Session["dateFormat"];

                DateTime dtDate = System.DateTime.Now;
                string strDate = "";
                strDate = dtDate.ToString("dd/MM/yyyy");


                string strTime = System.DateTime.Now.ToString();
                strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


                string strCountText = "<b>" + Session["totalRecordCount"].ToString() + " rows </b>";

                string strFooterText = "";

                strFooterText = "This data was generated and exported on " + strDate + " at " + strTime + ".";
                strFooterText += " (" + strCountText + ") ";

                strFooterText += "AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved. ";
                strFooterText += "AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India. ";
                strFooterText += "The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.";


                e.Item.Cells[2].ColumnSpan = 2;
                e.Item.Cells[2].Text = strFooterText;
                e.Item.Cells[2].Style["color"] = "red";
                e.Item.Font.Size = FontUnit.Point(5);
                e.Item.Font.Bold = false;

            }

        }
    }

    protected void bindGrid(string strSortOrder)
    {
        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());

        string strStartDate = dtStartDate.ToString("d");
        string strEndDate = dtEndDate.ToString("d");
        //string ss;

        try
        {
            string strStationIDs = getStationIDs();

            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
            SqlDataAdapter da = new SqlDataAdapter("Nationaladvertiser", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.CommandTimeout = 0;

            da.SelectCommand.Parameters.AddWithValue("@stationIDs", strStationIDs);
            da.SelectCommand.Parameters.AddWithValue("@startDate", strStartDate);
            da.SelectCommand.Parameters.AddWithValue("@endDate", strEndDate);
            da.SelectCommand.Parameters.AddWithValue("@SearchText", txt_top.Text);
            da.SelectCommand.Parameters.AddWithValue("@SearchCriteria", ddadvtcat.Text);
            //ss = ddadvtcat.Text;
            da.SelectCommand.Parameters.AddWithValue("@Advertiser", "");
            da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

            Session["strStationIDs"] = strStationIDs;
            Session["strStartDate"] = strStartDate;
            Session["strEndDate"] = strEndDate;

            DataSet ds = new DataSet();
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count == 0)
            {
                displayNoRecordFound();
                Session["totalRecordCount"] = 0;
            }
            else
            {
                radData.DataSource = ds.Tables[0];
                radData.DataBind();
                litExportTo.Visible = true;
                btnExportToExcel.Visible = true;
                btnExportToPdf.Visible = true;
                radData.Visible = true;
                lblError.Visible = false;
                Session["NAds"] = ds;
                Session["totalRecordCount"] = ds.Tables[0].Rows.Count;

            }
        }
        catch
        {

            displayNoRecordFound();
        }
    }


    protected void btnExportToPdf_Click(object sender, EventArgs e)
    {
        string strStationIDs = getStationIDs();

        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());

        addAuditLog(dtStartDate, dtEndDate, strStationIDs, false, true, false);

        isPdfExport = true;
        bindGrid("");

        string strDateFormat = (string)Session["dateFormat"];
        DateTime dt1 = DateTime.Parse(Session["strStartDate"].ToString());
        DateTime dt2 = DateTime.Parse(Session["strEndDate"].ToString());

        string sDt1 = "";
        string sDt2 = "";
        sDt1 = dt1.ToString("dd/MM/yyyy");
        sDt2 = dt2.ToString("dd/MM/yyyy");

        String strDateRange = "From: " + sDt1 + "  -  " + sDt2;

        radData.ExportSettings.FileName = "National Advertiser/National Advertiser By Brand Report";
        radData.ExportSettings.IgnorePaging = true;
        radData.ExportSettings.OpenInNewWindow = true;

        String strReportName = "";
        if (ddSelectBy.Text == "Station")
        {
            strReportName = "AirCheck India National Advertiser/National Advertiser By Brand Report";
        }
        else if (ddSelectBy.Text == "Market")
        {
            strReportName = "AirCheck India National Advertiser/National Advertiser By Brand Report " + getHeaderIfMarketsSelected();
        }
        else if (ddSelectBy.Text == "Network")
        {
            strReportName = "AirCheck India National Advertiser/National Advertiser By Brand Report " + getHeaderIfNetworkSelected();
        }


        radData.ExportSettings.Pdf.PageTitle = strReportName + " " + strDateRange;
        radData.ExportSettings.ExportOnlyData = true;
        radData.MasterTableView.ExportToPdf();

        addAuditLog(Convert.ToDateTime(sDt1), Convert.ToDateTime(sDt2), "", false, true, false);



    }

    protected void img_btn_export_to_excel_Click(object sender, EventArgs e)
    {
        string strStationIDs = getStationIDs();

        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());

        addAuditLog(dtStartDate, dtEndDate, strStationIDs, true, false, false);
        ExportToExcel();
    }

    public void ExportToExcel()
    {
        string strDateFormat = (string)Session["dateFormat"];

        DataSet ds = (DataSet)Session["NAds"];
        DataTable dt = new DataTable("tblExport");
        dt = ds.Tables[0].Copy();



        string str_station_name = "";

        string str_market_name = "";// getMarketOfStation(str_station_name);

        String strStationName = "<BR><BR>Station: " + str_station_name;
        String strMarket = "<BR><BR>Market: " + str_market_name;

        DateTime dt1 = DateTime.Parse(Session["strStartDate"].ToString());
        DateTime dt2 = DateTime.Parse(Session["strEndDate"].ToString());

        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }

        String strDateRange = "<BR><BR>From: " + sDt1 + "  to  " + sDt2;

        ExportToExcel(dt, Response, strStationName, strMarket, strDateRange);
        addAuditLog(Convert.ToDateTime(sDt1), Convert.ToDateTime(sDt2), str_station_name, true, false, false);

    }

    public void ExportToExcel(DataTable dt, HttpResponse response, String strStationName, string strMarket, String strDateRange)
    {

        //first let's clean up the response.object
        response.Clear();
        response.Charset = "";
        //set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel";
        //create a string writer
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //create an htmltextwriter which uses the stringwriter
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //instantiate a datagrid
        DataGrid dg = new DataGrid();
        //set the datagrid datasource to the dataset passed in
        if (dt == null)
        {
            return;
        }
        else if (dt.Rows.Count == 0)
        {
            return;
        }
        dg.DataSource = dt;
        //bind the datagrid
        dg.DataBind();

        dg.RenderControl(htmlWrite);


        String str = stringWrite.ToString();

        int start, end;
        start = 0;
        end = 0;

        int colspan = 0;

        colspan = dt.Columns.Count;


        String strReportName = "";
        if (ddSelectBy.Text == "Station")
        {
            strReportName = getHeaderIfStationsSelected() + " AirCheck India National Advertiser/National Advertiser By Brand Report.";
        }
        else if (ddSelectBy.Text == "Market")
        {
            strReportName = "AirCheck India National Advertiser/National Advertiser By Brand Report " + getHeaderIfMarketsSelected();
        }
        else if (ddSelectBy.Text == "Network")
        {
            strReportName = "AirCheck India National Advertiser/National Advertiser By Brand Report " + getHeaderIfNetworkSelected();
        }


        string strFinal = "<tr><td align=center colspan=" + colspan + " style='background-color:#FFFF66;'>" + strReportName + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strDateRange + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + ">&nbsp;</td></tr>";



        while (str.IndexOf("<select name=") > 0)
        {
            start = str.IndexOf("<select name=");
            end = str.IndexOf("</select>") + 9;
            str = str.Replace(str.Substring(start, end - start), "");
        }

        string strAirTime = "";
        string strPlayTime = "";
        string strNewPlayTime = "";

        int intCount = 0;

        foreach (DataRow dr in dt.Rows)
        {
            strAirTime = dr[0].ToString();
            if (strAirTime == "")
            {
                strPlayTime = dr[2].ToString();
                strNewPlayTime = dr[2].ToString() + " ";

                str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=5 align='Center'>" + strNewPlayTime + "</td></tr>");

            }
            else
            {
                intCount = intCount + 1;
            }
        }


        str = strFinal + "<BR><BR>" + str;


        string strDateFormat = (string)Session["dateFormat"];

        DateTime dtDate = System.DateTime.Now;
        string strDate = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dtDate.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dtDate.ToString("MM/dd/yyyy");
        }


        string strTime = System.DateTime.Now.ToString();
        strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


        string strCountText = "<b>" + intCount + " rows </b>";

        string strFooterText = "";

        strFooterText = "<BR><table>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>This data was generated and exported on " + strDate + " at " + strTime + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>" + strCountText + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><BR></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.</td></tr>";
        strFooterText += "</table>";

        str = "<table>" + str + strFooterText;



        response.Write(str);
        response.End();
    }

    protected string doStringSanitation(string str)
    {
        if (str.IndexOf("'") >= 0)
        {
            str = str.Replace("'", "123123");
        }
        if (str.IndexOf("&") >= 0)
        {
            str = str.Replace("&", "456456");
        }

        return str;
    }




    #region Header For Export To excel
    protected string getHeaderIfStationsSelected()
    {
        string strStations = string.Empty;
        var radTreeStations = (RadTreeView)CmbTreeStations.Items[0].FindControl("radTreeStations");
        //ArrayList nodeCollection = radTreeStations.CheckedNodes;

        foreach (RadTreeNode node in radTreeStations.CheckedNodes)
        {
            if (node.Parent != null)
            {
                strStations += node.Text + ",";
            }
        }
        return strStations.Substring(0, strStations.Length - 1) + " station/s";
    }

    protected string getHeaderIfMarketsSelected()
    {
        string strMarkets = string.Empty;
        var radTreeMarkets = (RadTreeView)CmbTreeMarkets.Items[0].FindControl("radTreeMarkets");
        //ArrayList nodeCollection = radTreeMarkets.CheckedNodes;

        foreach (RadTreeNode node in radTreeMarkets.CheckedNodes)
        {
            if (node.Parent != null)
            {
                strMarkets += node.Text + ",";
            }
        }
        return strMarkets.Substring(0, strMarkets.Length - 1) + " market/s";
    }

    protected string getHeaderIfNetworkSelected()
    {
        string str = "";

        string strNetworks = string.Empty;
        string strMarkets = string.Empty;
        var radTreeMarketsForSelectedNetwork = (RadTreeView)CmbTreeMarketsForSelectedNetwork.Items[0].FindControl("radTreeMarketsForSelectedNetwork");
        //ArrayList nodeCollectionMarkets = radTreeMarketsForSelectedNetwork.CheckedNodes;

        strNetworks = ddNetworks.SelectedItem.Text + ",";

        str = strNetworks.Substring(0, strNetworks.Length - 1); ;
        str = str + " network";

        foreach (RadTreeNode node in radTreeMarketsForSelectedNetwork.CheckedNodes)
        {
            if (node.Parent != null)
            {
                strMarkets += node.Text + ",";
            }
        }

        if (strMarkets != "")
        {
            str = str + " for " + strMarkets.Substring(0, strMarkets.Length - 1) + " market/s";
        }

        return str;
    }


    #endregion




    protected void radData_SortCommand(object source, GridSortCommandEventArgs e)
    {
        bindGrid("[" + e.SortExpression + "] asc");
    }

    protected void formatsearchGrid()
    {
        string headerFontName = (string)Session["headerFontName"];// ConfigurationSettings.AppSettings["headerFontName"].ToString();
        string headerFontSize = (string)Session["headerFontSize"];//ConfigurationSettings.AppSettings["headerFontSize"].ToString();

        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();

        radData.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
        radData.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        radData.HeaderStyle.Font.Name = headerFontName;
        radData.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());

        string actualDataFontName = (string)Session["dataFontName"];//ConfigurationSettings.AppSettings["actualDataFontName"].ToString();
        string actualDataFontSize = (string)Session["dataFontSize"];//ConfigurationSettings.AppSettings["actualDataFontSize"].ToString();

        radData.ItemStyle.Font.Name = actualDataFontName;
        radData.ItemStyle.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
        radData.AlternatingItemStyle.Font.Name = actualDataFontName;
        radData.AlternatingItemStyle.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
        radData.AlternatingItemStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
        radData.ItemStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);
        radData.AlternatingItemStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

        //radData.ClientSettings.Scrolling.AllowScroll = true;
        //radData.ClientSettings.Scrolling.ScrollHeight = Unit.Pixel(300);
        //radData.ClientSettings.Scrolling.UseStaticHeaders = true;
        //radData.GridLines = GridLines.Both;
    }
}
