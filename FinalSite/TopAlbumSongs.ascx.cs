﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class TopAlbumSongs : System.Web.UI.UserControl
{
    DataTable dtInner;
    protected void Page_Load(object sender, EventArgs e)
    {
        //DataGridItem dgi = (DataGridItem)this.BindingContainer;
        //DataSet ds = (DataSet)((System.Data.DataRowView)(dgi.DataItem)).DataView.DataViewManager.DataSet;
        ////DataSet ds = (DataSet)((DataGridItem)this.BindingContainer).DataItem;

        //DG1.DataSource = ds;
        //DG1.DataMember = ds.Tables[1].TableName;
        //DG1.DataBind();
       
    }

    protected void DG1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string strBrandName = "";
        string strParentName = "";
        string strCategory = "";

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.HorizontalAlign = HorizontalAlign.Center;
            
        }

        //string headerFontName = (string)Session["headerFontName"];// ConfigurationSettings.AppSettings["headerFontNameTA"].ToString();
        //string headerFontSize = (string)Session["headerFontSize"];//ConfigurationSettings.AppSettings["headerFontSizeTA"].ToString();

        //string actualDataFontName = (string)Session["dataFontName"];//ConfigurationSettings.AppSettings["actualDataFontNameTA"].ToString();
        //string actualDataFontSize = (string)Session["dataFontSize"];//ConfigurationSettings.AppSettings["actualDataFontSizeTA"].ToString();



        string headerFontName = "Helvetica";
        string headerFontSize = "12px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "10px";

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", headerFontName);
            //e.Item.Style.Add("font-weight", "bold");
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                //e.Item.Style.Add("font-weight", "bold");
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            //e.Item.Style.Add("font-size", headerFontSize);
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            //e.Item.ForeColor = System.Drawing.Color.White;
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
            for (int i = 0; i < e.Item.Cells.Count; i++)
            {
                e.Item.Cells[i].Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            }
        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            //e.Item.Style.Add("font-weight", "bold");
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                //e.Item.Style.Add("font-weight", "bold");
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            //e.Item.Style.Add("font-size", actualDataFontSize);
            for (int ii = 0; ii < e.Item.Cells.Count; ii++)
            {
                e.Item.Cells[ii].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            }
            int i = 0;
            foreach(DataColumn dc in dtInner.Columns)
            {
                
                bool isANumber = false;
                int intNum;

                if (dc.ColumnName == "Song Title")
                {
                    strBrandName = doStringSanitation(e.Item.Cells[i].Text);
                }
                if (dc.ColumnName == "Movie/Music Album")
                {
                    strParentName = doStringSanitation(e.Item.Cells[i].Text);
                }


                if (dc.ColumnName == "No Of Plays" || dc.ColumnName == "Total Seconds")
                {
                    isANumber = int.TryParse(e.Item.Cells[i].Text, out intNum);
                    if (isANumber)
                    {
                        e.Item.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                    }

                    if (isANumber)
                    {
                        e.Item.Cells[i].Text = "<a href='' onclick=\"javascript:return disp_details('" + doStringSanitation(strBrandName) + "','" + doStringSanitation(strParentName) + "')\"  alt='Click here to see the details'>" + e.Item.Cells[i].Text + "</a>";
                    }                    
                }

                i++;
            }



            
        }

        if (e.Item.ItemType == ListItemType.AlternatingItem)
        {
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);

            //e.Item.BackColor = System.Drawing.Color.FromArgb(149, 179, 215);
        }
    }

    protected string doStringSanitation(string str)
    {
        if (str.IndexOf("'") >= 0)
        {
            str = str.Replace("'", "123123");
        }
        if (str.IndexOf("&") >= 0)
        {
            str = str.Replace("&", "456456");
        }

        return str;
    }

    protected void DG1_DataBinding(object sender, EventArgs e)
    {

    }


    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    ///		Required method for Designer support - do not modify
    ///		the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new System.EventHandler(this.Page_Load);
        this.DataBinding += new System.EventHandler(this.SalesList_DataBinding);

    }
    #endregion

    private void SalesList_DataBinding(object sender, System.EventArgs e)
    {
        //if an InvalidCastException occurs in either of the next two lines, 
        //please make sure that you've set the TemplateDataMode to Table (because you want nested grids)
        DataGridItem dgi = (DataGridItem)this.BindingContainer;
        if (!(dgi.DataItem is DataSet))
            throw new ArgumentException("Please change the TemplateDataMode attribute to 'Table' in the HierarGrid declaration");
        DataSet ds = (DataSet)dgi.DataItem;
        dtInner = ds.Tables[1];
        DG1.DataSource = ds;
        DG1.DataMember = ds.Tables[1].TableName;
        DG1.DataBind();
    }

   
}
