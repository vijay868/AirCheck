﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using Telerik.Web.UI;

public partial class commercialsSpots : System.Web.UI.Page
{
    bool isPdfExport = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        //StringBuilder onloadScript = new StringBuilder();
        //onloadScript.Append("<script type='text/javascript'>");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("ShowReport();");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("</script>");
        //this.ClientScript.RegisterStartupScript(this.GetType(), "onLoadCall", onloadScript.ToString());

        ((Label)this.Master.FindControl("lblReportName")).Text = "Commercials/Spots";

        //Literal lit = new Literal();
        //lit.Text = "<script language='javascript'>tdhide('leftMenu', 'ctl00_showhide', 'hide');</script>";
        //Page.Controls.Add(lit);


        getDateFormat();
        string strDateFormat = (string)Session["dateFormat"];

        if (strDateFormat == "DD/MM/YYYY")
        {
            dtStart.DateInput.DateFormat = "dd/MM/yyyy";
            dtStart.DateInput.DisplayDateFormat = "dd/MM/yyyy";

            dtEnd.DateInput.DateFormat = "dd/MM/yyyy";
            dtEnd.DateInput.DisplayDateFormat = "dd/MM/yyyy";
        }


        if (!IsPostBack)
        {
            Session["ds"] = null;
            setDate();
            getUserPreferencesForCurrentUser();
            //getStations(ddStations);
            //getStations(ddStationsToCompare);
            GenerateTreeView();
            if (Session["dataReadOnly"].ToString() == "True")
            {
                btnExportToExcel.Enabled = false;
                btnExportToPdf.Enabled = false;
            }
            else
            {
                btnExportToExcel.Enabled = true;
                btnExportToPdf.Enabled = true;
            }
        }
        Pg_Title.Text = "Commercials/Spots";
    }

    protected void getUserPreferencesForCurrentUser()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getUserPreferenceForAUserAndReport", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@user", (string)Session["user"]);
        da.SelectCommand.Parameters.AddWithValue("@reportName", "Commercials / Spots");

        DataSet ds = new DataSet();
        da.Fill(ds);

        Session["headerFontName"] = ds.Tables[0].Rows[0]["headerFontName"].ToString();
        Session["headerFontSize"] = ds.Tables[0].Rows[0]["headerFontSize"].ToString();
        Session["headerFontColor"] = ds.Tables[0].Rows[0]["headerFontColor"].ToString();
        Session["headerRowColor"] = ds.Tables[0].Rows[0]["headerRowColor"].ToString();
        Session["headerFontStyle"] = ds.Tables[0].Rows[0]["headerFontStyle"].ToString();
        Session["dataFontName"] = ds.Tables[0].Rows[0]["dataFontName"].ToString();
        Session["dataFontSize"] = ds.Tables[0].Rows[0]["dataFontSize"].ToString();
        Session["dataFontColor"] = ds.Tables[0].Rows[0]["dataFontColor"].ToString();
        Session["alternateRowColor"] = ds.Tables[0].Rows[0]["alternateRowColor"].ToString();
        Session["dataFontStyle"] = ds.Tables[0].Rows[0]["dataFontStyle"].ToString();

    }


    protected void GenerateTreeView()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getStationsForUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);


        var txtStationsTree = (RadTreeView)txtStationSelect.Items[0].FindControl("radStationSelect");
        var txtStationsCompareTree = (RadTreeView)txtStationCompare.Items[0].FindControl("radStationComapre");
        txtStationsTree.DataFieldID = "name";
        txtStationsTree.DataFieldParentID = "m_name";
        txtStationsTree.DataTextField = "name";
        txtStationsTree.DataSource = ds.Tables[0];
        txtStationsTree.DataBind();

        txtStationsCompareTree.DataFieldID = "name";
        txtStationsCompareTree.DataFieldParentID = "m_name";
        txtStationsCompareTree.DataTextField = "name";
        txtStationsCompareTree.DataSource = ds.Tables[0];
        txtStationsCompareTree.DataBind();
    }

    protected void setDate()
    {
        DateTime lockDataBefore = DateTime.Parse(Session["lockDataBefore"].ToString());
        DateTime lockDataAfter = DateTime.Parse(Session["lockDataAfter"].ToString());
        dtStart.MinDate = lockDataBefore;
        dtEnd.MinDate = lockDataBefore;

        dtStart.MaxDate = lockDataAfter;
        dtEnd.MaxDate = lockDataAfter;

        dtStart.SelectedDate = System.DateTime.Now;
        dtEnd.SelectedDate = System.DateTime.Now;
    }

    protected void getDateFormat()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("select * from India_Report_New..Country_dateFormat where isSelected=1", conn);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                Session["dateFormat"] = dr["DateFormat"].ToString();
            }
        }

        conn.Close();
    }

    protected void getStations(DropDownList dd)
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);

        SqlCommand comm = default(SqlCommand);
        conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);

        comm = new SqlCommand("getOnlyStationsForUser", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@userName", (string)Session["user"]);
        conn.Open();
        SqlDataReader dr = comm.ExecuteReader();
        dd.DataSource = dr;
        dd.DataTextField = "s_name";
        dd.DataValueField = "station_id";
        dd.DataBind();
        dr.Close();
        conn.Close();
    }

    protected int getStationID(string strStation)
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);

        SqlCommand comm = default(SqlCommand);
        conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);

        comm = new SqlCommand("getStationIDOfStation", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@stationName", strStation);
        conn.Open();
        SqlDataReader dr = comm.ExecuteReader();
        int strStartionID = 0;
        while (dr.Read())
        {
            strStartionID = int.Parse(dr[0].ToString());
        }
        dr.Close();
        conn.Close();
        return strStartionID;
    }


    protected void addAuditLog(DateTime startDate, DateTime endDate, string station,Boolean exportedToExcel, Boolean exportedToPdf, Boolean exportedToText)
    {
        auditTrail at = new auditTrail();
        at.connectionString = ConfigurationSettings.AppSettings["connectionString"].ToString();
        at.userID = (int)Session["userID"];
        at.IPAddress = Request.ServerVariables["REMOTE_ADDR"];
        at.pageViewed = "Commercials/Spots";
        at.startDate = startDate;
        at.endDate = endDate;
        at.station = station;
        at.exportedToExcel = exportedToExcel;
        at.exportedToPdf = exportedToPdf;
        at.exportedToText = exportedToText;
        at.addUserActivityLogPassingStationIDs();
        at = null;
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());
        int strStationID = getStationID(txtStationSelect.Text.Trim());
        addAuditLog(dtStartDate, dtEndDate, strStationID.ToString(), false, false, false);

        isPdfExport = false;
        bindGrid();
    }

    protected void bindGrid()
    {
        DefineGridStructure();

        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        string strStartDate = dtStartDate.ToString("d");
        Session["strStartDate"] = strStartDate;

        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());
        string strEndDate = dtEndDate.ToString("d");
        Session["strEndDate"] = strEndDate;


        int strStationID = getStationID(txtStationSelect.Text.Trim());



        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("commercialsSpots", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@stationID", strStationID);
        da.SelectCommand.Parameters.AddWithValue("@startDate", strStartDate);
        da.SelectCommand.Parameters.AddWithValue("@endDate", strEndDate);

        DataSet ds = new DataSet();
        da.Fill(ds);

        if (ds.Tables[0].Rows.Count == 0)
        {
            displayNoRecordFound();
            radData.Visible = false;
            litExportTo.Visible = false;
            btnExportToExcel.Visible = false;
            btnExportToPdf.Visible = false;
            Session["totalRecordCount"] = 0;
        }
        else
        {
            litExportTo.Visible = true;
            btnExportToExcel.Visible = true;
            btnExportToPdf.Visible = true;
            radData.Visible = true;
            lblError.Visible = false;
            Session["ds"] = ds;
            Session["sumDurationMainPage"] = ds.Tables[1].Rows[0][0].ToString();
            Session["totalRecordCount"] = ds.Tables[0].Rows.Count;
            radData.DataSource = ds.Tables[0];
            radData.DataBind();
        }
    }


    int iRowIndex;
    int iTimeBandIndex;
    string strTimeBand;

    protected void dgResult_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
        //string headerFontName = (string)Session["headerFontName"];// ConfigurationSettings.AppSettings["headerFontNameTA"].ToString();
        //string headerFontSize = (string)Session["headerFontSize"];//ConfigurationSettings.AppSettings["headerFontSizeTA"].ToString();


        string headerFontName = "Helvetica";
        string headerFontSize = "15px";



        if (e.Item.ItemType == GridItemType.Header)
        {
            iRowIndex = 0;
            strTimeBand = "";
            iTimeBandIndex = 0;

            e.Item.Style.Add("font-family", headerFontName);



            //e.Item.Style.Add("font-size", headerFontSize);
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
            for (int i = 0; i < e.Item.Cells.Count; i++)
            {
                e.Item.Cells[i].Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
                e.Item.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                {
                    e.Item.Cells[i].Font.Bold = true;
                }
                else
                {
                    e.Item.Cells[i].Font.Bold = false;
                }
            }
        }

        if (e.Item.ItemType == GridItemType.Footer)
        {
            if (!isPdfExport)
            {
                e.Item.Visible = true;
                e.Item.Cells[7].BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]); //System.Drawing.Color.FromArgb(0, 112, 192);
                e.Item.Cells[7].ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]); //System.Drawing.Color.White;
                e.Item.Cells[8].BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]); //System.Drawing.Color.FromArgb(0, 112, 192);
                e.Item.Cells[8].ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]); //System.Drawing.Color.White;
                e.Item.Cells[7].Text = "Total Seconds";
                e.Item.Cells[7].Style.Add("font-family", headerFontName);
                e.Item.Cells[8].Style.Add("font-family", headerFontName);
                e.Item.Cells[7].HorizontalAlign = HorizontalAlign.Center;
                e.Item.Cells[8].HorizontalAlign = HorizontalAlign.Center;
                e.Item.Cells[8].Text = Session["sumDurationMainPage"].ToString();

                if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                {
                    e.Item.Cells[7].Font.Bold = true;
                    e.Item.Cells[8].Font.Bold = true;
                }
                else
                {
                    e.Item.Cells[7].Font.Bold = false;
                    e.Item.Cells[8].Font.Bold = false;
                }
                e.Item.Cells[7].Font.Size = FontUnit.Parse("14px");
                e.Item.Cells[8].Font.Size = FontUnit.Parse("14px");

                for (int i = 0; i < e.Item.Cells.Count; i++)
                {
                    //e.Item.Cells[i].Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
                }

            }
            else
            {
                e.Item.Visible = true;
                e.Item.Cells[6].BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]); //System.Drawing.Color.FromArgb(0, 112, 192);
                e.Item.Cells[6].ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]); //System.Drawing.Color.White;
                e.Item.Cells[7].BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]); //System.Drawing.Color.FromArgb(0, 112, 192);
                e.Item.Cells[7].ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]); //System.Drawing.Color.White;
                e.Item.Cells[6].Text = "Total Seconds";
                e.Item.Cells[7].HorizontalAlign = HorizontalAlign.Center;
                e.Item.Cells[7].HorizontalAlign = HorizontalAlign.Center;
                e.Item.Cells[7].Text = Session["sumDurationMainPage"].ToString();
            }
        }



        if (isPdfExport && e.Item is GridFooterItem)
        {
            GridFooterItem footerItem = (GridFooterItem)e.Item;

            string strDateFormat = (string)Session["dateFormat"];

            DateTime dtDate = System.DateTime.Now;
            string strDate = "";
            if (strDateFormat == "DD/MM/YYYY")
            {
                strDate = dtDate.ToString("dd/MM/yyyy");
            }
            else
            {
                strDate = dtDate.ToString("MM/dd/yyyy");
            }


            string strTime = System.DateTime.Now.ToString();
            strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


            string strCountText = "<b>" + Session["totalRecordCount"].ToString() + " rows </b>";

            string strFooterText = "";

            strFooterText = "This data was generated and exported on " + strDate + " at " + strTime + ".";
            strFooterText += " (" + strCountText + ") ";

            strFooterText += "AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved. ";
            strFooterText += "AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India. ";
            strFooterText += "The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.";


            e.Item.Cells[2].ColumnSpan = 2;
            e.Item.Cells[2].Text = strFooterText;
            e.Item.Cells[2].Style["color"] = "red";
            e.Item.Font.Size = FontUnit.Point(5);
            e.Item.Font.Bold = false;

        }


        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            //string actualDataFontName = (string)Session["dataFontName"];//ConfigurationSettings.AppSettings["actualDataFontNameCS"].ToString();
            //string actualDataFontSize = (string)Session["dataFontSize"];//ConfigurationSettings.AppSettings["actualDataFontSizeCS"].ToString();


            string actualDataFontName = "Helvetica";
            string actualDataFontSize = "12px";

            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]); //System.Drawing.Color.White;

            if (!isPdfExport)
            {
                e.Item.BorderColor = System.Drawing.Color.Black;
                for (int ii = 0; ii < e.Item.Cells.Count; ii++)
                {
                    e.Item.Cells[ii].Style.Add("font-family", actualDataFontName);
                    e.Item.Cells[ii].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
                    if (Session["dataFontStyle"].ToString().ToLower() == "bold")
                    {
                        e.Item.Cells[ii].Font.Bold = true;
                    }
                    else
                    {
                        e.Item.Cells[ii].Font.Bold = false;
                    }
                }

                if (iRowIndex == 0)
                {
                    strTimeBand = e.Item.Cells[3].Text;
                    iTimeBandIndex = 0;
                }
                else
                {
                    if (strTimeBand == e.Item.Cells[3].Text)
                    {

                    }
                    else
                    {
                        strTimeBand = e.Item.Cells[3].Text;
                        iTimeBandIndex++;
                    }
                    if (iTimeBandIndex % 2 != 0)
                    {
                      //  e.Item.BackColor = System.Drawing.Color.FromArgb(149, 179, 215);
                        e.Item.BackColor = System.Drawing.Color.FromArgb(176,196,222);
                       
                        //System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
                        //e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
                    }
                    else
                    {
                        e.Item.BackColor = System.Drawing.Color.White;
                    }
                }
                iRowIndex++;
            }
            else
            {
                e.Item.Font.Size = FontUnit.Point(8);
                e.Item.Font.Bold = false;
            }
        }
    }

    protected void displayNoRecordFound()
    {
        lblError.Visible = true;
        lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
        radData.Visible = false;
        litExportTo.Visible = false;
        btnExportToExcel.Visible = false;
        btnExportToPdf.Visible = false;
    }

    protected void Page_Init(object source, System.EventArgs e)
    {

    }

    protected void DefineGridStructure()
    {

        int ScreenHeight;

       // ScreenHeight = Convert.ToInt32(Session["SHeight"]) - 250;

        //string headerFontName = (string)Session["headerFontName"];// ConfigurationSettings.AppSettings["headerFontNameDP"].ToString();
        //string headerFontSize = (string)Session["headerFontSize"];//ConfigurationSettings.AppSettings["headerFontSizeDP"].ToString();

        string headerFontName = ConfigurationSettings.AppSettings["headerFontNameDP"].ToString();
        string headerFontSize = ConfigurationSettings.AppSettings["headerFontSizeDP"].ToString();



        radData.ClientSettings.Scrolling.AllowScroll = true;
       // radData.ClientSettings.Scrolling.ScrollHeight = Unit.Pixel(ScreenHeight);
        radData.ClientSettings.Scrolling.UseStaticHeaders = true;
        radData.GridLines = GridLines.Both;

        radData.MasterTableView.EnableColumnsViewState = false;

        GridBoundColumn boundColumn;
        boundColumn = new GridBoundColumn();
        boundColumn.DataField = "Date";
        boundColumn.HeaderText = "Date";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
        boundColumn.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);

        //boundColumn.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(0, 112, 192);
        boundColumn.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        boundColumn.HeaderStyle.Font.Name = headerFontName;
        if (!isPdfExport)
        {
            boundColumn.HeaderStyle.Width = Unit.Percentage(7);
        }
        boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
       // boundColumn.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
        radData.MasterTableView.Columns.Add(boundColumn);


        boundColumn = new GridBoundColumn();
        boundColumn.DataField = "Time Band";
        boundColumn.HeaderText = "Time Band";
        boundColumn.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
        boundColumn.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        boundColumn.HeaderStyle.Font.Name = headerFontName;
        if (!isPdfExport)
        {
            // boundColumn.HeaderStyle.Width = Unit.Pixel(100);
            boundColumn.HeaderStyle.Width = Unit.Percentage(10);
        }
        boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
      //  boundColumn.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
        radData.MasterTableView.Columns.Add(boundColumn);

        boundColumn = new GridBoundColumn();
        boundColumn.DataField = "Spot Time";
        boundColumn.HeaderText = "Spot Time";
        boundColumn.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
        boundColumn.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        boundColumn.HeaderStyle.Font.Name = headerFontName;
        if (!isPdfExport)
        {
            // boundColumn.HeaderStyle.Width = Unit.Pixel(100);
            boundColumn.HeaderStyle.Width = Unit.Percentage(10);
        }
        boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
      //  boundColumn.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
        radData.MasterTableView.Columns.Add(boundColumn);

        boundColumn = new GridBoundColumn();
        boundColumn.DataField = "Spot Name";
        boundColumn.HeaderText = "Spot Name";
        boundColumn.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
        boundColumn.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        boundColumn.HeaderStyle.Font.Name = headerFontName;
        if (!isPdfExport)
        {
            // boundColumn.HeaderStyle.Width = Unit.Pixel(250);
            boundColumn.HeaderStyle.Width = Unit.Percentage(19);
        }
        boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
       // boundColumn.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
        radData.MasterTableView.Columns.Add(boundColumn);

        boundColumn = new GridBoundColumn();
        boundColumn.DataField = "Parent";
        boundColumn.HeaderText = "Parent";
        boundColumn.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
        boundColumn.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        boundColumn.HeaderStyle.Font.Name = headerFontName;
        if (!isPdfExport)
        {
            // boundColumn.HeaderStyle.Width = Unit.Pixel(250);
            boundColumn.HeaderStyle.Width = Unit.Percentage(19);
        }
        boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
      //  boundColumn.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
        radData.MasterTableView.Columns.Add(boundColumn);


        boundColumn = new GridBoundColumn();
        boundColumn.DataField = "Spot Category";
        boundColumn.HeaderText = "Spot Category";
        boundColumn.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
       // boundColumn.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        boundColumn.HeaderStyle.Font.Name = headerFontName;
        if (!isPdfExport)
        {
            //boundColumn.HeaderStyle.Width = Unit.Pixel(200);
            boundColumn.HeaderStyle.Width = Unit.Percentage(22);
        }
        boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
        boundColumn.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
        radData.MasterTableView.Columns.Add(boundColumn);


        boundColumn = new GridBoundColumn();
        boundColumn.DataField = "Spot Duration (in sec)";
        boundColumn.HeaderText = "Spot Duration (in sec)";
        boundColumn.HeaderStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
        boundColumn.HeaderStyle.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        boundColumn.HeaderStyle.Font.Name = headerFontName;
        if (!isPdfExport)
        {
            boundColumn.HeaderStyle.Width = Unit.Pixel(10);
        }
        boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
       // boundColumn.HeaderStyle.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
        radData.MasterTableView.Columns.Add(boundColumn);

        //radData.AlternatingItemStyle.BackColor = System.Drawing.Color.FromArgb(149, 179, 215);
        radData.AlternatingItemStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);

        radData.ItemStyle.BackColor = System.Drawing.Color.White;

        radData.ShowFooter = true;

    }

    protected void img_btn_export_to_excel_Click(object sender, EventArgs e)
    {
        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());
        int strStationID = getStationID(txtStationSelect.Text.Trim());
        addAuditLog(dtStartDate, dtEndDate, strStationID.ToString(), true, false, false);
        ExportToExcel();
    }

    public void ExportToExcel()
    {
        string strDateFormat = (string)Session["dateFormat"];

        DataSet ds = (DataSet)Session["ds"];
        DataTable dt = new DataTable("tblExport");
        dt = ds.Tables[0].Copy();



        string str_station_name = "";

        String strStationName = "<BR><BR>Station: " + str_station_name;

        DateTime dt1 = DateTime.Parse(Session["strStartDate"].ToString());
        DateTime dt2 = DateTime.Parse(Session["strEndDate"].ToString());

        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }

        String strDateRange = "<BR><BR>From: " + sDt1 + "  to  " + sDt2;

        ExportToExcel(dt, Response, strStationName, strDateRange);
    }

    protected DataTable insertTotal(DataTable dt)
    {
        DataRow dr = dt.NewRow();
        dr["Spot Time"] = "";
        dr["Spot Name"] = "";
        dr["Parent"] = "";
        dr["Spot Category"] = "Total Seconds";
        dr["Spot Duration (in sec)"] = Session["sumDurationMainPage"].ToString();

        dt.Rows.Add(dr);
        dt.AcceptChanges();
        return dt;
    }

    public void ExportToExcel(DataTable dt, HttpResponse response, String strStationName, String strDateRange)
    {

        //first let's clean up the response.object
        response.Clear();
        response.Charset = "";
        //set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel";
        //create a string writer
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //create an htmltextwriter which uses the stringwriter
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //instantiate a datagrid
        DataGrid dg = new DataGrid();
        //set the datagrid datasource to the dataset passed in
        if (dt == null)
        {
            return;
        }
        else if (dt.Rows.Count == 0)
        {
            return;
        }
        dg.DataSource = insertTotal(dt);
        //bind the datagrid
        dg.DataBind();

        dg.RenderControl(htmlWrite);


        String str = stringWrite.ToString();

        int start, end;
        start = 0;
        end = 0;

        int colspan = 0;

        colspan = dt.Columns.Count;


        String strReportName = "";
        strReportName = txtStationSelect.Text + " AirCheck India Commercials/Spots Report.";



        string strFinal = "<tr><td align=center colspan=" + colspan + " style='background-color:#FFFF66;'>" + strReportName + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strDateRange + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + ">&nbsp;</td></tr>";



        while (str.IndexOf("<select name=") > 0)
        {
            start = str.IndexOf("<select name=");
            end = str.IndexOf("</select>") + 9;
            str = str.Replace(str.Substring(start, end - start), "");
        }

        string strAirTime = "";
        string strPlayTime = "";
        string strNewPlayTime = "";

        int intCount = 0;

        foreach (DataRow dr in dt.Rows)
        {
            strAirTime = dr[0].ToString();
            if (strAirTime == "")
            {
                strPlayTime = dr[2].ToString();
                strNewPlayTime = dr[2].ToString() + " ";

                str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=5 align='Center'>" + strNewPlayTime + "</td></tr>");

            }
            else
            {
                intCount = intCount + 1;
            }
        }


        str = strFinal + "<BR><BR>" + str;


        string strDateFormat = (string)Session["dateFormat"];

        DateTime dtDate = System.DateTime.Now;
        string strDate = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dtDate.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dtDate.ToString("MM/dd/yyyy");
        }


        string strTime = System.DateTime.Now.ToString();
        strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


        string strCountText = "<b>" + intCount + " rows </b>";

        string strFooterText = "";

        strFooterText = "<BR><table>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>This data was generated and exported on " + strDate + " at " + strTime + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>" + strCountText + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><BR></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.</td></tr>";
        strFooterText += "</table>";

        str = "<table>" + str + strFooterText;



        response.Write(str);
        response.End();
    }


    protected void btnExportToPdf_Click(object sender, EventArgs e)
    {
        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());
        DateTime dtEndDate = DateTime.Parse(dtEnd.SelectedDate.ToString());
        int strStationID = getStationID(txtStationSelect.Text.Trim());
        addAuditLog(dtStartDate, dtEndDate, strStationID.ToString(), false, true, false);

        isPdfExport = true;
        bindGrid();

        string strDateFormat = (string)Session["dateFormat"];
        DateTime dt1 = DateTime.Parse(Session["strStartDate"].ToString());
        DateTime dt2 = DateTime.Parse(Session["strEndDate"].ToString());

        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }

        String strDateRange = "From: " + sDt1 + "  -  " + sDt2;

        radData.ExportSettings.FileName = "Commercials/Spots Report";
        radData.ExportSettings.IgnorePaging = true;
        radData.ExportSettings.OpenInNewWindow = true;
        radData.ExportSettings.Pdf.PaperSize = GridPaperSize.Letter;
        radData.ExportSettings.Pdf.PageLeftMargin = Unit.Pixel(10);
        radData.ExportSettings.Pdf.PageRightMargin = Unit.Pixel(10);

        String strReportName = "";
        strReportName = txtStationSelect.Text + " AirCheck India Commercials/Spots Report";

        radData.ExportSettings.Pdf.PageTitle = strReportName + " " + strDateRange;
        radData.ExportSettings.ExportOnlyData = false;
        radData.MasterTableView.ExportToPdf();

    }
}
