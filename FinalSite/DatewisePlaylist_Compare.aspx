﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DatewisePlaylist_Compare.aspx.cs" Inherits="DatewisePlaylist_Compare" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="x-ua-compatible" content="IE=10">
    <title></title>
    <style>
    .MyGridClass .rgDataDiv
    {
        height : auto !important ;
    }
    </style>
    <script language="javascript">
        function GridCreated(sender, args) {
            var scrollArea = sender.GridDataDiv;
            scrollArea.style.height = document.body.offsetHeight - header.offsetHeight - pagerArea.offsetHeight + "px";  
        }
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
	   <telerik:RadScriptManager runat="server" ID="RadScriptManager1" 
            AsyncPostBackTimeout="500" />
    <div style="width:100%;Height:100%">
     
     <div>

    <div id="ShowGirdContainerButtons" visible="false" runat="server">
         <table style="width: 100%;height:100%">
             <tr>
                 <td>
                     &nbsp;
                     <asp:Label ID="lblTitle" runat="server"></asp:Label>
                     
                 </td>
             </tr>
             <tr>
                 <td>
                  <div style="width:100%;" id="gridContainer">
                      <telerik:RadGrid ID="dgResult" runat="server" 
                         onitemdatabound="dgResult_ItemDataBound" CellSpacing="0" GridLines="None" CssClass="MyGridClass" >
                         
<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>

<MasterTableView>
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>

<NoRecordsTemplate>
      <div>
        There are no records to display</div>
    </NoRecordsTemplate>
</MasterTableView>

                         <ClientSettings>
                             <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                             <ClientEvents OnGridCreated="GridCreated" />
                         </ClientSettings>

<FilterMenu EnableImageSprites="False"></FilterMenu>
                     </telerik:RadGrid>
            </div>
                 </td>
             </tr>
             <tr>
                 <td>
                 
                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="left">
                        <telerik:RadButton ID="btn1" runat="server" Text="Songs" Visible="false" 
                                onclick="btn1_onclick">
                        </telerik:RadButton>
                        <telerik:RadButton ID="btn4" runat="server" Text="Links" Visible="false" 
                                onclick="btn4_onclick">
                        </telerik:RadButton>
                        <telerik:RadButton ID="btn2" runat="server" Text="Spots" Visible="false" 
                                onclick="btn2_onclick">
                        </telerik:RadButton>
                        <telerik:RadButton ID="btn3" runat="server" Text="All" Visible="false" 
                                onclick="btn3_onclick">
                        </telerik:RadButton>
                        </td>
                        <td>&nbsp;</td>
                        <td align="right">
                        
                        </td>
                    </tr>
                 </table>
                 
                    
                 </td>
             </tr>
         </table>
         
         </div>
         
         
     </div>
      <div id="NoRecordsDiv" visible="false" runat="server">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
     <br />
    <br />
    <br />
          
   <center><asp:Label ID="NoRecordsLiteral" runat="server" Text="No Records Found..." Visible="false" Font-Bold="true"></asp:Label> </center>
    <br />
    <br />
    <br />
    <br />
    
    </div>

 </div>
    </form>
</body>
</html>
