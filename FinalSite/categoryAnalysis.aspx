﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="categoryAnalysis.aspx.cs" Inherits="categoryAnalysis" Title="Category Analysis" %>
<%@ Register assembly="IdeaSparx.CoolControls.Web" namespace="IdeaSparx.CoolControls.Web" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
      
        .gvHeaderRow
        {
            background-image: url("images/dhrudde.png");
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: White;
            text-decoration: none;
            line-height: 12px;
            padding-left: 10px;
            border-left: solid 1px white;
            border-right: solid 1px white;
            border-bottom: solid 1px white;
            position: relative;
        }
        
    </style>
<script type="text/javascript" language="javascript">
function ToggleCollapsePane() {
               
               var splitter = $find("<%= Page.Master.FindControl("RadSplitter1").ClientID %>");
               var pane = splitter.getPaneById("<%= Page.Master.FindControl("RadPane1").ClientID %>");
                pane.collapse();
    }
function nodeChecked(sender, args) {
       var comboBox = $find("<%= CmbTreeCities.ClientID %>");            
     
            var tree = comboBox.get_items().getItem(0).findControl("radTreeCities");
            var node = args.get_node();
            var rootNodeSelCount = 0;
            var nodesJ=tree.get_nodes(); 
            var marketsval="";
              for (i=0; i<nodesJ.get_count(); i++) 
               {                
                   if (nodesJ.getNode(i).get_checked()) 
                   { 
                       //alert(nodesJ.getNode(i).get_text());
                       marketsval = marketsval + nodesJ.getNode(i).get_text().toString() + ",";
                       rootNodeSelCount =  rootNodeSelCount + 1;
                   } 
               }
               
               if(rootNodeSelCount > 1)
               {
                   alert('Cannot select more than 1 markets');
                   node.uncheck();
                   return false;
               }
 
            var tempNode = args.get_node();
            if (tempNode.get_text().toString() == "(Select All)") 
            {
                // check or uncheck all the nodes
            } else {
                var nodes = new Array();
                nodes = sender.get_checkedNodes();
                var vals = "";
                var i = 0;
                var icount = 0;

                for (i = 0; i < nodes.length; i++) {
                    var n = nodes[i];
                    var nodeText = n.get_text().toString();
                    if (nodeText != "(Select All)") {
                        if (n._hasChildren() == true) {
                        } else {
                            vals = vals + n.get_text().toString() + ",";
                        }
                    }
                }
             
                comboBox.set_text(marketsval);
            }
    }

    function nodeClicking(sender, args) {
        var comboBox = $find("<%= CmbTreeCities.ClientID %>");

        var node = args.get_node();

        if (node.get_parent() == node.get_treeView()) {
            alert("Please select a station. Market selection not allowed.");

        }
        else {
            comboBox.set_text(node.get_text());
            comboBox.get_items().getItem(0).set_value(node.get_value());
        }

    }

    function OnClientDropDownOpenedHandler(sender, eventArgs) {

        var tree = sender.get_items().getItem(0).findControl("radTreeCities");

        if (!!tree) {
            var selectedNode = tree.get_selectedNode();
            if (selectedNode) {
                selectedNode.scrollIntoView();
            }
        }
    }
        function calcDays()
        {
            ToggleCollapsePane();
            var startDate =$find("<%=dtStart.ClientID %>");
            var endDate =  $find("<%=dtEnd.ClientID %>");
            
            var sDate = startDate.get_selectedDate();
            var eDate = endDate.get_selectedDate();
            
           var lockDataBefore="<%=Session["lockDataBefore"]%>";
		    var lockDataAfter="<%=Session["lockDataAfter"]%>";
            if(sDate == null)
            {
                lockDataBefore = new Date(lockDataBefore);
                var curr_date = lockDataBefore.getDate();
                var curr_month = lockDataBefore.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataBefore.getFullYear();
                lockDataBefore= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights only from " + lockDataBefore);
                return false;
            }
            
            if(eDate == null)
            {
                lockDataAfter = new Date(lockDataAfter);
                var curr_date = lockDataAfter.getDate();
                var curr_month = lockDataAfter.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataAfter.getFullYear();
                lockDataAfter= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights till " + lockDataAfter);
                return false;
            }
                    
            //tdhide('leftMenu', 'ctl00_showHide','hide');            
            
            
            var comboBox = $find("<%= CmbTreeCities.ClientID %>");    
            var treeS = comboBox.get_items().getItem(0).findControl("radTreeCities");
            
            var rootNodesS = treeS.SelectedNode;
            var nodesS = treeS.get_allNodes();
            
            var i;
            var isChecked = 0;
             for(i=0; i<nodesS.length; i++)
             {
                if(!nodesS[i].get_checked())
                {
                
                }
                else
                {
                    isChecked = 1;
                }
             }            
            if (isChecked == 0)
            {
                alert("You have not selected any Market/Station.");
                return false;
            }
            else if(days_between(sDate,eDate) > 30)
            {
                alert("Maximum allowed date range is one month.");
                return false;
            }
            else
            {
                //disp_clock();
                return true;
            }
         
        } 
        
        function OnClientClicked(button, args)
        {
           if (calcDays())
           {
                button.set_autoPostBack(true); 
           }                
            else
            {                              
                button.set_autoPostBack(false);                        
            }
        }
        function getrootcount(sender, args) {

            //        var tree = sender.get_items().getItem(0).findControl("radTreeCities");
            //        var allNodes = tree.get_nodes().getNode(0).get_allNodes();
            //        for (var i = 0; i < allNodes.length; i++) {
            //            var node = allNodes[i];
            //            alert(node.get_text());
            //        }
        }
        function days_between(date1, date2) {

            // The number of milliseconds in one day
            var ONE_DAY = 1000 * 60 * 60 * 24

            // Convert both dates to milliseconds
            var date1_ms = date1.getTime()
            var date2_ms = date2.getTime()

            // Calculate the difference in milliseconds
            var difference_ms = Math.abs(date1_ms - date2_ms)
            
            // Convert back to days and return
            return Math.round(difference_ms/ONE_DAY)

        }
        function HideTreeView() {
        if (document.getElementById("treeDiv") != null)
        {
            document.getElementById("treeDiv").style.visibility = "hidden";
        }
        }
    function disp_details(startDate, endDate, strCategory, strStationName) {
        win = window.open("categoryAnalysis_details.aspx?startDate=" + startDate + "&endDate=" + endDate + "&strCategory=" + strCategory + "&strStationName=" + strStationName, "win", "width=800,height=400,left=0, scrollbars=yes, resizable=yes");
        return false
    }
    function disp_exclusiveReport(startDate, endDate, strStationName, strStationIDs) {
        win = window.open("categoryAnalysisRevisedExclusive.aspx?startDate=" + startDate + "&endDate=" + endDate + "&strStationName=" + strStationName + "&strStationIDs=" + strStationIDs, "win", "width=800,height=400,left=0, scrollbars=yes, resizable=yes");
        return false
    }

    function showOverallPieChart() 
    {
        win = window.open("categoryAnalysisRevisedChart.aspx", "win", "width=750,height=550,left=0, scrollbars=yes, resizable=yes");
        return false
    }
    function showExclusivePieChart() 
    {
        win = window.open("categoryAnalysisExclusiveChart.aspx", "win", "width=750,height=550,left=0, scrollbars=yes, resizable=yes");
        return false
    }    
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div style="width: 99.1%; height: 100%; margin-left:4px">
        <div>
            <table width="100%" background="images/contentheader_bgr.gif" border="0" cellpadding="6px" cellspacing="0px" style="margin-left:-5px; margin-right:300px;">
                <tr>
                    
                    <td align="left">::
                        <asp:Label ID="Pg_Title" runat="server" Font-Bold="True" ForeColor="#000099"></asp:Label>&nbsp;::
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        <asp:Label ID="lbl_Logout" runat="server" Text="Welcome: Administrator[Logout]" 
                            Font-Bold="True" ForeColor="#000099"></asp:Label>
                    </td>
                </tr>
            </table>
    <table>
        <tr>
            <td>
                &nbsp;Start Date
            </td>
            <td>
                &nbsp;End Date
            </td>
            <td>
                &nbsp;Select Market/Station
            </td>
            <td>
                Search
            </td>
            <td>
                &nbsp;&nbsp;
            </td>
            <td>
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;<telerik:RadDatePicker ID="dtStart" runat="server" Culture="English (United States)"
                    MinDate="2004-01-01">
                    <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                    </DateInput>
                </telerik:RadDatePicker>
            </td>
            <td>
                <telerik:RadDatePicker ID="dtEnd" runat="server" Culture="English (United States)"
                    MinDate="2004-01-01">
                    <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                    </DateInput>
                </telerik:RadDatePicker>
                <asp:CompareValidator Display="Dynamic" ID="dateCompareValidator" runat="server"
                    ControlToValidate="dtEnd" ControlToCompare="dtStart" Operator="GreaterThanEqual"
                    Type="Date" ErrorMessage="The End date must be after the Start date." Style="font-size: x-small"></asp:CompareValidator>
            </td>
            <td>
                <telerik:RadComboBox ID="CmbTreeCities" runat="server" AfterClientCheck="AfterCheckHandler"
                    CollapseAnimation-Type="None" EmptyMessage="Choose a destination" ExpandAnimation-Type="None"
                    OnClientDropDownOpened="OnClientDropDownOpenedHandler" ShowToggleImage="True"
                    Style="vertical-align: middle;" Width="200px">
                    <ItemTemplate>
                        <div id="div2">
                            <telerik:RadTreeView ID="radTreeCities" runat="server" Height="200px" OnClientNodeChecked="nodeChecked"
                                OnClientNodeClicked="nodeClicking" CheckBoxes="true" Width="100%" TriStateCheckBoxes="true">
                            </telerik:RadTreeView>
                        </div>
                    </ItemTemplate>
                    <Items>
                        <telerik:RadComboBoxItem Text="" />
                    </Items>
                </telerik:RadComboBox>
            </td>
            <td>
                <telerik:RadTextBox ID="txtSearch" onClick="javascript:return HideTreeView();" runat="server">
                </telerik:RadTextBox>
            </td>
            <td>
                
                <telerik:RadButton ID="btnGenerateReport" runat="server" Text="Generate Report" OnClientClicked="OnClientClicked"
                    OnClick="btnGenerateReport_Click">
                </telerik:RadButton>
            </td>
            <td>
                <a href="" onclick="javascript: return showOverallPieChart();">[View Overall Pie Chart]</a>
                </td>
                <td>
                <a href="" onclick="javascript: return showExclusivePieChart();">[View Exclusive Pie Chart]</a>
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlDate" runat="server" Visible="true" Width="100%">
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 100%">
                    <div id="divWait" style="display: none">
                        <table style="height: 310Px" width="100%">
                            <tr>
                                <td align="center" width="100%">
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <img src="images/aniClock.gif" border="0" name="ProgressBarImg"><br>
                                    <font class="date">Acquiring Data... one moment please!</font>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="DIV1" style="width: 100%">
                        <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label>
                        <cc1:CoolGridView UseAccessibleHeader="true" EnableViewState="false" 
                            ID="gvData" runat="server" Height="410px"
                        FixHeaders="True" ShowFooter="True" width="1320px"
        OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" 
        BorderStyle="None" BorderWidth="0px" AutoGenerateColumns="true" ColumnNosWidth="250px" 
                            FixedColumnsNos="1" DefaultColumnWidth="90px">
    <HeaderStyle CssClass="gvHeaderRow" />
<BoundaryStyle BorderColor="Gray" BorderWidth="0px" BorderStyle="None"></BoundaryStyle>
                        </cc1:CoolGridView>

                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Literal ID="litExportTo" runat="server" Text="Export to: "></asp:Literal>
                    <telerik:RadButton ID="btnExportToExcel" runat="server" Text="Excel" OnClick="img_btn_export_to_excel_Click">
                    </telerik:RadButton>
                </td>
            </tr>
        </table>
        <asp:TextBox ID="txtArray" runat="server" Style="visibility: hidden; height: 0Px"></asp:TextBox>
    </asp:Panel>
    </div>
    </div>
</asp:Content>
