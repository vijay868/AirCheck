﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using Telerik.Web.UI;

public partial class HourlyBreakupOfSpots : System.Web.UI.Page
{
    Boolean shouldAddNewColumn = true;

    protected void Page_Load(object sender, EventArgs e)
    {

        StringBuilder onloadScript = new StringBuilder(); 
        getDateFormat();
        string strDateFormat = (string)Session["dateFormat"];
        if (strDateFormat == "DD/MM/YYYY")
        {
            dtStart.DateInput.DateFormat = "dd/MM/yyyy";

            dtStart.DateInput.DisplayDateFormat = "dd/MM/yyyy";
        }


        if (!IsPostBack)
        {
            getUserPreferencesForCurrentUser();
            litExportTo.Visible = false;
            btnExportToExcel.Visible = false;
            setDate();
            GenerateTreeView();
            if (Session["dataReadOnly"].ToString() == "True")
            {
                btnExportToExcel.Enabled = false;

            }
            else
            {
                btnExportToExcel.Enabled = true;

            }

        }
        Pg_Title.Text = "Hourly Breakup Of Spots";
    }

    protected void getUserPreferencesForCurrentUser()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getUserPreferenceForAUserAndReport", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@user", (string)Session["user"]);
        da.SelectCommand.Parameters.AddWithValue("@reportName", "Hourly Breakup Of Spots (Time in sec.)");

        DataSet ds = new DataSet();
        da.Fill(ds);

        Session["headerFontName"] = ds.Tables[0].Rows[0]["headerFontName"].ToString();
        Session["headerFontSize"] = ds.Tables[0].Rows[0]["headerFontSize"].ToString();
        Session["headerFontColor"] = ds.Tables[0].Rows[0]["headerFontColor"].ToString();
        Session["headerRowColor"] = ds.Tables[0].Rows[0]["headerRowColor"].ToString();
        Session["headerFontStyle"] = ds.Tables[0].Rows[0]["headerFontStyle"].ToString();
        Session["dataFontName"] = ds.Tables[0].Rows[0]["dataFontName"].ToString();
        Session["dataFontSize"] = ds.Tables[0].Rows[0]["dataFontSize"].ToString();
        Session["dataFontColor"] = ds.Tables[0].Rows[0]["dataFontColor"].ToString();
        Session["alternateRowColor"] = ds.Tables[0].Rows[0]["alternateRowColor"].ToString();
        Session["dataFontStyle"] = ds.Tables[0].Rows[0]["dataFontStyle"].ToString();

    }

    protected void GenerateTreeView()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getStationsForUser", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

        DataSet ds = new DataSet();
        da.Fill(ds);
        var radTreeCities = (RadTreeView)CmbTreeStations.Items[0].FindControl("radTreeStations");

        radTreeCities.DataFieldID = "name";
        radTreeCities.DataFieldParentID = "m_name";
        radTreeCities.DataTextField = "name";

        radTreeCities.DataSource = ds.Tables[0];
        radTreeCities.DataBind();
    }

    protected void setDate()
    {
        DateTime lockDataBefore = DateTime.Parse(Session["lockDataBefore"].ToString());
        dtStart.MinDate = lockDataBefore;

        dtStart.SelectedDate = System.DateTime.Now;
    }

    protected void getDateFormat()
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("select * from India_Report_New..Country_dateFormat where isSelected=1", conn);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                Session["dateFormat"] = dr["DateFormat"].ToString();
            }
        }

        conn.Close();
    }


    protected string getStationIDs()
    {
        string strStations = string.Empty;
        var radTreeMarkets = (RadTreeView)CmbTreeStations.Items[0].FindControl("radTreeStations");        
        int stationID = 0;
        string strStationNames = "";
        foreach (RadTreeNode node in radTreeMarkets.CheckedNodes)
        {
            stationID = getStationIDOfStation(node.Text);
            if (stationID != 0)
            {
                strStations += stationID.ToString() + ",";
                strStationNames += node.Text + ",";
            }
        }

        return strStations.Substring(0, strStations.Length - 1);
    }

    protected int getStationIDOfStation(string txtStation)
    {
        int stationID = 0;
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("getStationIDOfStation", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@stationName", txtStation);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                stationID = (int)dr["station_id"];
            }
        }
        else
        {
            stationID = 0;
        }

        conn.Close();
        return stationID;
    }

    protected void addAuditLog(DateTime startDate, DateTime endDate, string station,
Boolean exportedToExcel, Boolean exportedToPdf, Boolean exportedToText)
    {
        auditTrail at = new auditTrail();
        at.connectionString = ConfigurationSettings.AppSettings["connectionString"].ToString();
        at.userID = (int)Session["userID"];
        at.IPAddress = Request.ServerVariables["REMOTE_ADDR"];
        at.pageViewed = "Hourly Breakup Of Spots (Time in sec.)";
        at.startDate = startDate;
        at.endDate = endDate;
        at.station = station;
        at.exportedToExcel = exportedToExcel;
        at.exportedToPdf = exportedToPdf;
        at.exportedToText = exportedToText;
        at.addUserActivityLogPassingStationIDs();
        at = null;
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        pnlDate.Visible = true;
        ViewState["info"] = null;
        string strStationIDs = getStationIDs();





        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());

        string strStartDate = dtStartDate.ToString("d");
        string strEndDate = dtStartDate.ToString("d");

        addAuditLog(dtStartDate, dtStartDate, strStationIDs, false, false, false);

        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("HourlyBreakupOfSpots", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@stationIDs", strStationIDs);
        da.SelectCommand.Parameters.AddWithValue("@startDate", strStartDate);
        da.SelectCommand.Parameters.AddWithValue("@endDate", strEndDate);


        DataSet ds = new DataSet();
        try
        {
            da.Fill(ds);
        }
        catch
        {
            litExportTo.Visible = false;
            btnExportToExcel.Visible = false;
            gvData.Visible = false;
            lblError.Visible = true;
            lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
            return;
        }
        gvData.FooterStyle.CssClass = "FF";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
        gvData.AlternatingRowStyle.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);

        //try
        //{
        if (ds.Tables[0].Rows.Count == 0)
        {
            litExportTo.Visible = false;
            btnExportToExcel.Visible = false;
            gvData.Visible = false;
            lblError.Visible = true;
            lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";
            return;
        }
        else
        {
            DataTable dt = ds.Tables[0];
            dt = returnDataTableWithTotalInFooter(dt);
            Session["data"] = dt;
            litExportTo.Visible = false;
            btnExportToExcel.Visible = false;
            lblError.Text = "";
            lblError.Visible = false;
            gvData.Visible = true;

            try
            {
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    int[] intArray = commaStrToArray(dr[0].ToString());
                    info.AddMergedColumns(intArray, dr[1].ToString());
                }
            }
            catch
            {
                shouldAddNewColumn = false;
            }

            Session["myDataTableExport"] = dt;

            litExportTo.Visible = true;
            btnExportToExcel.Visible = true;

            gvData.DataSource = fillBlank(dt);
            gvData.DataBind();


        }

    }

    protected DataTable fillBlank(DataTable dt)
    {
        foreach (DataRow dr in dt.Rows)
        {
            foreach (DataColumn dc in dt.Columns)
            {
                if (dr[dc].ToString() == "")
                {
                    dr[dc] = "0(0)";
                }
            }
        }
        dt.AcceptChanges();
        return dt;
    }

    protected DataTable returnDataTableWithTotalInFooter(DataTable dt)
    {
        DataRow dr = dt.NewRow();
        foreach (DataColumn dc in dt.Columns)
        {
            if (dc.ColumnName.ToLower() != "time")
            {
                dr[dc] = getColumnTotal(dt, dc);
            }
            else
            {
                dr[dc] = "Total";
            }
        }
        dt.Rows.Add(dr);
        dt.AcceptChanges();
        return dt;
    }

    protected string getColumnTotal(DataTable dt, DataColumn dc)
    {
        int intCountOfPlays = 0;
        int intSumOfPlays = 0;
        string str = "";

        foreach (DataRow dr in dt.Rows)
        {
            foreach (DataColumn dcInner in dt.Columns)
            {
                if (dcInner.ColumnName == dc.ColumnName)
                {
                    try
                    {
                        str = dr[dc].ToString();
                        str = str.Substring(0, str.IndexOf("("));
                        try
                        {
                            intCountOfPlays = intCountOfPlays + int.Parse(str);
                        }
                        catch { }

                        str = dr[dc].ToString();
                        str = str.Substring(str.IndexOf("(") + 1, str.IndexOf(")") - str.IndexOf("(") - 1);
                        try
                        {
                            intSumOfPlays = intSumOfPlays + int.Parse(str);
                        }
                        catch { }
                    }
                    catch { }
                }
            }
        }
        str = intCountOfPlays.ToString() + "(" + intSumOfPlays.ToString() + ")";
        return str;
    }

    private int[] commaStrToArray(string strIntComma)
    {
        string[] strArray;
        strArray = strIntComma.Split(new char[] { ',' });
        int[] intArray = new int[strArray.Length];
        for (int i = 0; i < strArray.Length; i++)
            intArray[i] = int.Parse(strArray[i]);
        return intArray;
    }

    protected void img_btn_export_to_excel_Click(object sender, EventArgs e)
    {
        //Response.ContentType = "application/vnd.ms-excel";
        //Response.AddHeader("Content-Disposition", "attachment; filename=ListOfCurrentSongsRepetition.xls");
        //Response.Charset = "";
        //this.EnableViewState = false;
        //System.IO.StringWriter stringWriter = new System.IO.StringWriter();
        //System.Web.UI.HtmlTextWriter htmlWriter = new System.Web.UI.HtmlTextWriter(stringWriter);

        //DataGrid dgExport = new DataGrid();
        //dgExport.DataSource = (DataTable)Session["data"];
        //dgExport.DataBind();

        //dgExport.RenderControl(htmlWriter);
        //Response.Write(stringWriter.ToString());
        //Response.End();

        string strStationIDs = getStationIDs();
        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());

        addAuditLog(dtStartDate, dtStartDate, strStationIDs, true, false, false);

        ExportToExcel();
    }

    protected DataTable getDataTableToExport()
    {
        string strDateFormat = (string)Session["dateFormat"];
        DataTable dt = new DataTable();
        dt = (DataTable)Session["myDataTableExport"];


        return dt;
    }

    public void ExportToExcel()
    {
        string strDateFormat = (string)Session["dateFormat"];
        DataTable dt = getDataTableToExport();


        string str_station_name = "";

        string str_market_name = "";// getMarketOfStation(str_station_name);

        String strStationName = "<BR><BR>Station: " + str_station_name;
        String strMarket = "<BR><BR>Market: " + str_market_name;

        DateTime dt1 = DateTime.Parse(dtStart.SelectedDate.ToString());

        string sDt1 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
        }

        String strDateRange = sDt1;

        ExportToExcel(dt, Response, strStationName, strMarket, strDateRange);
    }

    public void ExportToExcel(DataTable dt, HttpResponse response, String strStationName, string strMarket, String strDateRange)
    {

        //first let's clean up the response.object
        response.Clear();
        response.Charset = "";
        //set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel";
        //create a string writer
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //create an htmltextwriter which uses the stringwriter
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //instantiate a datagrid
        DataGrid dg = new DataGrid();
        //set the datagrid datasource to the dataset passed in
        if (dt == null)
        {
            return;
        }
        else if (dt.Rows.Count == 0)
        {
            return;
        }
        dg.DataSource = dt;
        //bind the datagrid
        dg.DataBind();

        dg.RenderControl(htmlWrite);


        String str = stringWrite.ToString();

        int start, end;
        start = 0;
        end = 0;

        int colspan = 0;

        colspan = dt.Columns.Count;


        String strReportName = "Aircheck Hourly Breakup Of Spots (Time in sec.)";



        string strFinal = "<tr><td align=center colspan=" + colspan + " style='background-color:#FFFF66;'>" + strReportName + "</td></tr>";
        strFinal = strFinal + "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'>Date: " + strDateRange + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + ">&nbsp;</td></tr>";



        while (str.IndexOf("<select name=") > 0)
        {
            start = str.IndexOf("<select name=");
            end = str.IndexOf("</select>") + 9;
            str = str.Replace(str.Substring(start, end - start), "");
        }
        //str = str.Replace("<tr>\r\n\t\t<td>&nbsp;</td><td>&nbsp;</td>", "<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td>");

        string strAirTime = "";
        string strPlayTime = "";
        string strNewPlayTime = "";

        int intCount = 0;

        foreach (DataRow dr in dt.Rows)
        {
            strAirTime = dr[0].ToString();
            if (strAirTime == "")
            {
                strPlayTime = dr[2].ToString();
                strNewPlayTime = dr[2].ToString() + " ";

                if ((string)Session["category"] == "Song")
                {
                    str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=4 align='Center'>" + strNewPlayTime + "</td></tr>");
                }
                else
                {
                    str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=5 align='Center'>" + strNewPlayTime + "</td></tr>");
                }
            }
            else
            {
                intCount = intCount + 1;
            }
        }


        str = strFinal + "<BR><BR>" + str;


        string strDateFormat = (string)Session["dateFormat"];

        DateTime dtDate = System.DateTime.Now;
        string strDate = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dtDate.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dtDate.ToString("MM/dd/yyyy");
        }


        string strTime = System.DateTime.Now.ToString();
        strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


        string strCountText = "<b>" + intCount + " rows </b>";

        string strFooterText = "";

        strFooterText = "<BR><table>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>This data was generated and exported on " + strDate + " at " + strTime + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>" + strCountText + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><BR></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.</td></tr>";
        strFooterText += "</table>";

        str = "<table>" + str + strFooterText;



        response.Write(str);
        response.End();
    }




    //property for storing of information about merged columns
    private MergedColumnsInfo info
    {
        get
        {
            if (ViewState["info"] == null)
                ViewState["info"] = new MergedColumnsInfo();
            return (MergedColumnsInfo)ViewState["info"];
        }
    }

    protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
    {
        //call the method for custom rendering the columns headers	
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.SetRenderMethodDelegate(RenderHeader);
        }
    }

    private System.Drawing.Color getColor(int i)
    {
        System.Drawing.Color xCol = System.Drawing.Color.FromArgb(0, 112, 192);

        if (i == 0)
        {
            xCol = System.Drawing.Color.FromArgb(0, 112, 192);
        }
        else
        {
            if (i % 2 == 1)
            {
                xCol = System.Drawing.Color.FromArgb(31, 73, 125);
            }
        }

        return xCol;
    }

    private string getColor1(int i)
    {
        System.Drawing.Color c;
        string xCol = "";
        if (i == 0)
        {
            xCol = "#000066";
        }
        else if (i == 1)
        {
            xCol = "#0066FF";
        }
        else if (i == 2)
        {
            xCol = "#003366";
        }
        else if (i == 3)
        {
            xCol = "#996666";
        }
        else if (i == 4)
        {
            xCol = "#CCFF99";
        }
        else if (i == 5)
        {
            xCol = "#FFFF00";
        }
        else if (i == 6)
        {
            xCol = "#00FF66";
        }
        else
        {
            xCol = "#660066";
        }



        c = System.Drawing.ColorTranslator.FromHtml(xCol);
        return xCol;
    }

    //method for rendering the columns headers	
    private void RenderHeader(HtmlTextWriter output, Control container)
    {
        int j = 0;
        System.Drawing.Color c = System.Drawing.Color.SkyBlue;
        string strColWidth = "";
        string strColor = "";
        for (int i = 0; i < container.Controls.Count; i++)
        {
            TableCell cell = (TableCell)container.Controls[i];
            //stretch non merged columns for two rows
            if (!info.MergedColumns.Contains(i))
            {
                cell.Attributes["rowspan"] = "2";
                System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
                cell.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);

                cell.RenderControl(output);
            }
            else //render merged columns common title
                if (info.StartColumns.Contains(i))
                {
                    if (j % 2 == 0)
                    {
                        strColor = (string)Session["headerRowColor"];
                    }
                    else
                    {
                        strColor = (string)Session["headerRowColor"];
                    }

                    strColWidth = (int.Parse(info.StartColumns[i].ToString()) * 100).ToString() + "px";
                    output.Write(string.Format("<th bgcolor='" + strColor + "' align='center' colspan='{0}' style='color:" + (string)Session["headerFontColor"] + ";font-size:" + Session["headerFontSize"].ToString().Replace("Px", "pt") + ";font-weight:" + (string)Session["headerFontStyle"] + ";font-family:" + (string)Session["headerFontName"] + ";'>{1}</th>",
                             info.StartColumns[i], info.Titles[i]));
                    j++;
                }
        }

        //close the first row	
        output.RenderEndTag();
        //set attributes for the second row
        gvData.HeaderStyle.AddAttributesToRender(output);
        //start the second row
        output.RenderBeginTag("tr");

        //render the second row (only the merged columns)
        for (int i = 0; i < info.MergedColumns.Count; i++)
        {
            TableCell cell = (TableCell)container.Controls[info.MergedColumns[i]];
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            cell.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);

            cell.RenderControl(output);
        }
    }

    private string InsertLineBreaks(string _emailString)
    {
        int MaxStringLength = 40;

        if (_emailString.Length > MaxStringLength)
        {
            int indexOfSpace = _emailString.IndexOf(" ", MaxStringLength - 1);

            if ((indexOfSpace != -1) && (indexOfSpace != _emailString.Length - 1))
            {
                string firstString = _emailString.Substring(0, indexOfSpace);
                string secondString = _emailString.Substring(indexOfSpace);

                return firstString + "<br>" + InsertLineBreaks(secondString);
            }
            else
            {
                return _emailString;
            }
        }
        else
        {
            return _emailString;
        }
    }

    protected string doStringSanitation(string str)
    {
        if (str.IndexOf("'") >= 0)
        {
            str = str.Replace("'", "123123");
        }
        if (str.IndexOf("&") >= 0)
        {
            str = str.Replace("&", "456456");
        }

        return str;
    }

    protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        string headerFontName = (string)Session["headerFontName"];// ConfigurationSettings.AppSettings["headerFontName"].ToString();
        string headerFontSize = (string)Session["headerFontSize"];//ConfigurationSettings.AppSettings["headerFontSize"].ToString();

        string footerFontName = (string)Session["headerFontName"];//ConfigurationSettings.AppSettings["footerFontName"].ToString();
        string footerFontSize = (string)Session["headerFontName"];//ConfigurationSettings.AppSettings["footerFontSize"].ToString();

        string actualDataFontName = (string)Session["dataFontName"];//ConfigurationSettings.AppSettings["actualDataFontName"].ToString();
        string actualDataFontSize = (string)Session["dataFontName"];//ConfigurationSettings.AppSettings["actualDataFontSize"].ToString();




        DataTable dt = (DataTable)Session["data"];

        DateTime dtStartDate = DateTime.Parse(dtStart.SelectedDate.ToString());

        string strStartDate = dtStartDate.ToString("d");

        int indexOfBlank = 0;

        if (e.Row.RowType == DataControlRowType.Header)
        {
            int colIndex = 0;
            foreach (DataColumn dc in dt.Columns)
            {
                if (colIndex == 0)
                {
                    e.Row.Cells[colIndex].Style.Add("white-space", "nowrap");
                    e.Row.Cells[colIndex].Style.Add("font-family", headerFontName);

                    e.Row.Cells[colIndex].Width = Unit.Pixel(200);
                }
                else
                {
                    indexOfBlank = e.Row.Cells[colIndex].Text.LastIndexOf(" ");
                    e.Row.Cells[colIndex].Text = e.Row.Cells[colIndex].Text.Substring(0, indexOfBlank) + "<BR>#Plays(#Seconds)";
                    e.Row.Cells[colIndex].Style.Add("white-space", "nowrap");
                    e.Row.Cells[colIndex].Style.Add("font-family", headerFontName);

                }

                if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                {
                    e.Row.Cells[colIndex].Font.Bold = true;
                }
                else
                {
                    e.Row.Cells[colIndex].Font.Bold = false;
                }
                e.Row.Cells[colIndex].Font.Size = FontUnit.Parse(Session["headerFontSize"].ToString().Replace("Px", "").Trim());


                System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
                e.Row.Cells[colIndex].ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

                colIndex++;
            }

        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int j = 0;
            int intNoOfPlays = 0;

            string strSongName = doStringSanitation(e.Row.Cells[0].Text);
            string strArtistName = doStringSanitation(e.Row.Cells[1].Text);

            string strStationName = "";
            foreach (DataColumn dc in dt.Columns)
            {
                System.Drawing.ColorConverter conv1 = new System.Drawing.ColorConverter();
                e.Row.Cells[j].ForeColor = (System.Drawing.Color)conv1.ConvertFromString((string)Session["dataFontColor"]);

                strStationName = dc.ColumnName.Replace("#Plays(#Seconds)", "");

                if (j > 0)
                {
                    if (e.Row.Cells[j].Text == "0(0)")
                    {
                        e.Row.Cells[j].ForeColor = System.Drawing.Color.Black;
                    }
                    else
                    {
                        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
                        e.Row.Cells[j].ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);
                    }
                }

                if (Session["dataFontStyle"].ToString().ToLower() == "bold")
                {
                    e.Row.Cells[j].Font.Bold = true;
                }
                else
                {
                    e.Row.Cells[j].Font.Bold = false;
                }
                e.Row.Cells[j].Font.Size = FontUnit.Parse(Session["dataFontSize"].ToString().Replace("Px", "").Trim());


                if (e.Row.Cells[0].Text.ToLower() == "total")
                {
                    e.Row.Cells[j].Style.Add("font-family", footerFontName);
                    System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
                    e.Row.Cells[j].ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
                    e.Row.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);

                    if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                    {
                        e.Row.Cells[j].Font.Bold = true;
                    }
                    else
                    {
                        e.Row.Cells[j].Font.Bold = false;
                    }
                    e.Row.Cells[j].Font.Size = FontUnit.Parse(Session["headerFontSize"].ToString().Replace("Px", "").Trim());

                }
                else
                {
                    e.Row.Cells[j].Style.Add("font-family", actualDataFontName);
                }


                e.Row.Cells[j].HorizontalAlign = HorizontalAlign.Center;
                e.Row.Cells[j].Style.Add("white-space", "nowrap");

                j++;
            }



        }


    }
}
