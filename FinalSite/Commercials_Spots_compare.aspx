﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Commercials_Spots_compare.aspx.cs"
    Inherits="Commercials_Spots_compare" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Commercials / Ads</title>
    <link href="css/style_risk.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        <!
        -- body
        {
            scrollbar-base-color: #ffffff;
            scrollbar-track-color: #FFFFCC;
            scrollbar-face-color: #CCCCFF;
            scrollbar-highlight-color: #ffffff;
            scrollbar-3dlight-color: #ffffff;
            scrollbar-darkshadow-color: #ffffff;
            scrollbar-shadow-color: #ffffff;
            scrollbar-arrow-color: #009966;
        }
        a
        {
            text-decoration: none;
        }
        -- ></style>
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label>
    
    <telerik:RadGrid ID="radData" runat="server" Visible="false" AutoGenerateColumns="false" Width="100%"
            OnItemDataBound="dgResult_ItemDataBound">
            <MasterTableView></MasterTableView>
    </telerik:RadGrid>
    </form>
</body>
</html>
