<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DateWisePlaylist.aspx.cs" Inherits="DateWisePlaylist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css"> 
 
.rowheightcls td
     {  
       line-height: 8px;
     }  

 
</style> 
    <script type="text/javascript">
    function ToggleCollapsePane() {
               
               var splitter = $find("<%= Page.Master.FindControl("RadSplitter1").ClientID %>");
               var pane = splitter.getPaneById("<%= Page.Master.FindControl("RadPane1").ClientID %>");
                pane.collapse();
              
//               if (!pane) return;
// 
//               if (pane.get_collapsed()) {
//                    pane.expand();
//               }
//               else {
//                    pane.collapse();
//               }
    }
    
    function StopPropagation(e) {
        if (!e) {
            e = window.event;
        }

        e.cancelBubble = true;
    }

    function OnClientDropDownOpenedHandler(sender, eventArgs) {
       
    
    
        var tree = sender.get_items().getItem(0).findControl("radStationSelect");
        var tree1 = sender.get_items().getItem(0).findControl("radStationCompare");
        
        if(!!tree)
        {
            var selectedNode = tree.get_selectedNode();
            if (selectedNode) {
                selectedNode.scrollIntoView();
            }   
        }
        
        if(!!tree1)
        {
            var selectedNode1 = tree1.get_selectedNode();
            if (selectedNode1) {
                selectedNode1.scrollIntoView();
            }
        }
    }
    
    function OnClientDropDownOpenedHandlerCompare(sender, eventArgs) {
        var tree = sender.get_items().getItem(0).findControl("radStationCompare");
        var selectedNode = tree.get_selectedNode();
        if (selectedNode) {
            selectedNode.scrollIntoView();
        }
    }
    
    function nodeClicking1(sender, args) {
        var comboBox = $find("<%= txtStationCompare.ClientID %>");
  var node = args.get_node();

        if (node.get_parent() == node.get_treeView()) {
            alert("Please select a Station. Market selection not allowed.");
//            comboBox.trackChanges();
//            comboBox.set_text("");
//            comboBox.commitChanges();
            comboBox.hideDropDown();
        }
        else {
            //comboBox.trackChanges();
            comboBox.set_text(node.get_text());
            comboBox.get_items().getItem(0).set_text(node.get_text());
            comboBox.get_items().getItem(0).set_value(node.get_value());
//            comboBox.commitChanges();

////////            comboBox.hideDropDown();
//            comboBox.set_text(node.get_text());
//            comboBox.trackChanges();
//            comboBox.get_items().getItem(0).set_text(node.get_text());
//            comboBox.get_items().getItem(0).set_value(node.get_value());
//            comboBox.commitChanges();
//            comboBox.hideDropDown();
        }
    } 

    function nodeClicking(sender, args) {
        var comboBox = $find("<%= txtStationSelect.ClientID %>");

        var node = args.get_node();

        if (node.get_parent() == node.get_treeView()) {
            alert("Please select a Station. Market selection not allowed.");
//            comboBox.trackChanges();
//            comboBox.set_text("");
//            comboBox.commitChanges();
            comboBox.hideDropDown();
        }
        else {
            //comboBox.trackChanges();
            comboBox.set_text(node.get_text());
            comboBox.get_items().getItem(0).set_text(node.get_text());
            comboBox.get_items().getItem(0).set_value(node.get_value());
//            comboBox.commitChanges();

////////            comboBox.hideDropDown();
//            comboBox.set_text(node.get_text());
//            comboBox.trackChanges();
//            comboBox.get_items().getItem(0).set_text(node.get_text());
//            comboBox.get_items().getItem(0).set_value(node.get_value());
//            comboBox.commitChanges();
//            comboBox.hideDropDown();
        }
    } 
    
    function nodeClicked(sender, args) {
        var node = args.get_node();
        if (node.get_checked()) {
            node.uncheck();
        } else {
            node.check();
        }
        nodeChecked(sender, args)

    }

    function nodeChecked(sender, args) {
   
        var comboBox = $find("<%= txtStationSelect.ClientID %>");

        //check if 'Select All' node has been checked/unchecked
        var tempNode = args.get_node();
        if (tempNode.get_text().toString() == "(Select All)") {
            // check or uncheck all the nodes
        } else {
            var nodes = new Array();
            nodes = sender.get_checkedNodes();
            var vals = "";
            var i = 0;

            for (i = 0; i < nodes.length; i++) {
                var n = nodes[i];
                var nodeText = n.get_text().toString();
                if (nodeText != "(Select All)") {
                    vals = vals + n.get_text().toString() + ",";
                }
            }

            //prevent  combo from closing
            supressDropDownClosing = true;
            comboBox.set_text(vals);
        }
    }

    function GridCreated(sender, args) {
        var scrollArea = sender.GridDataDiv;
        scrollArea.style.height = document.documentElement.clientHeight - 240 + "px";

//        alert($find("ctl00_RadSplitter1"));
//        
//        var splitter = $find('<%= this.Page.Master.FindControl("RadSplitter1").ClientID %>');
//        var pane = splitter.getPaneById('<%= this.Page.Master.FindControl("RadPane1").ClientID %>');
//        if (pane) {
//            pane.collapse();
//        }
    }


    //Page Validations Start Here
    
       function calcDays()
        {
            ToggleCollapsePane();
            var startDate =$find("<%=dtStart.ClientID %>");
            var endDate =  $find("<%=dtEnd.ClientID %>");
            
            var sDate = startDate.get_selectedDate();
            var eDate = endDate.get_selectedDate();
            
		    var lockDataBefore="<%=Session["lockDataBefore"]%>";
		    var lockDataAfter="<%=Session["lockDataAfter"]%>";
     
            if(sDate == null)
            {
                lockDataBefore = new Date(lockDataBefore);
                var curr_date = lockDataBefore.getDate();
                var curr_month = lockDataBefore.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataBefore.getFullYear();
                lockDataBefore= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights only from " + lockDataBefore);
                return false;
            }
            
            if(eDate == null)
            {
                lockDataAfter = new Date(lockDataAfter);
                var curr_date = lockDataAfter.getDate();
                var curr_month = lockDataAfter.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataAfter.getFullYear();
                lockDataAfter= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights till " + lockDataAfter);
                return false;
            }
            

		   var w1 = document.getElementById("ctl00_ContentPlaceHolder2_txtStationSelect");
		   var StationLeft = w1.value;
           if(StationLeft == "")
           {
               alert("Select a Station")
		        return false
           }
            else if(days_between(sDate,eDate) > 30)
            {
                alert("Maximum allowed date range is one month.");
                return false;
            }
            else
            {
                disp_clock();
                return true;
            }
         
        }  
        function disp_clock()
		{
		    var lblError = document.getElementById("ctl00_content1_lblError");
		    if (lblError != null)
		    {
		        lblError.style.display='none';
		    }
			OnLoad();
			DIV1.style.display='none';
			divWait.style.display='';
    	}

		function OnLoad() 
		{ 
		    setTimeout("StartAnimation()", 500); 
		} 

		function StartAnimation() 
		{ 
		    if (document.images)
		    {
		        document['ProgressBarImg'].src = "images/aniClock.gif";
		    }
		}        
        function days_between(date1, date2) {

            // The number of milliseconds in one day
            var ONE_DAY = 1000 * 60 * 60 * 24

            // Convert both dates to milliseconds
            var date1_ms = date1.getTime()
            var date2_ms = date2.getTime()

            // Calculate the difference in milliseconds
            var difference_ms = Math.abs(date1_ms - date2_ms)
            
            // Convert back to days and return
            return Math.round(difference_ms/ONE_DAY)

        }
    
    function OnClientClicked(button, args)
        {
           if (calcDays())
           {                        
//               var divobj = document.getElementById("ShowGirdContainerButtons")
//             divobj.style.display = "block";
                button.set_autoPostBack(true); 
           }                
            else
            {                              
                button.set_autoPostBack(false);                        
            }
        }
        
        function ShowLaneDiv()
        {           
            document.getElementById("divLane1").style.display = "block";
            document.getElementById("divLane2").style.display = "block";
        }
    
    function compare_report()
		{
         
            var startDate = $find("<%=dtStart.ClientID %>");
            var endDate = $find("<%=dtEnd.ClientID %>");
            var sDate = startDate.get_selectedDate();
            var eDate = endDate.get_selectedDate();
                                 
            
            var lockDataBefore="<%=Session["lockDataBefore"]%>";
            var lockDataAfter="<%=Session["lockDataAfter"]%>";
//            
            if(sDate == null )
//            if(sDate == null || eDate == null)
           {
                lockDataBefore = new Date(lockDataBefore);
                var curr_date = lockDataBefore.getDate();
                var curr_month = lockDataBefore.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataBefore.getFullYear();
                lockDataBefore= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights only from " + lockDataBefore);
                return false;
            }
            
            if(eDate == null)
            {
                lockDataAfter = new Date(lockDataAfter);
                var curr_date = lockDataAfter.getDate();
                var curr_month = lockDataAfter.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataAfter.getFullYear();
                lockDataAfter= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights till " + lockDataAfter);
                return false;
            }

            
//            var sD = sDate;
//            var eD = eDate;
    
              var sD = sDate.toLocaleDateString();
              var eD = eDate.toLocaleDateString();


            if(days_between(sDate,eDate) > 30)
            {
                alert("Maximum allowed date range is one month.");
                return false;
            }


		    var w1 = document.getElementById("ctl00_ContentPlaceHolder2_txtStationCompare");
            var StationRight = w1.value;

            w1 = document.getElementById("ctl00_ContentPlaceHolder2_txtStationSelect");
            var StationLeft = w1.value;
            
            
            w1 = $find("<%=ddType.ClientID %>"); //document.getElementById("ctl00_ContentPlaceHolder2_ddType");
            var category = w1.get_selectedItem().get_value()
            
           if(StationLeft == "")
           {
               alert("Select a Station")
		        return false
           }
		    else if (StationRight == "" )
		    {
			    alert("Select a Station to Compare")
			    return false
		    }
		    else
		    {
			    win = window.open("compare_reports.aspx?s_date="+sD+"&e_date="+eD+"&StationLeft="+StationLeft+"&StationRight="+StationRight+"&category="+category+"","win","width=1000,height=500,left=0, resizable=yes")
			    return false
		    }
		
		}
		
    </script>

    <script type="text/javascript">
        function centerLoadingPanel() {

        }

        function centerElementOnScreen(element) {

            var scrollTop = document.body.scrollTop;
            var scrollLeft = document.body.scrollLeft;
            var viewPortHeight = document.body.clientHeight;
            var viewPortWidth = document.body.clientWidth;

            if (document.compatMode == "CSS1Compat") {
                viewPortHeight = document.documentElement.clientHeight;
                viewPortWidth = document.documentElement.clientWidth;
                scrollTop = document.documentElement.scrollTop;
                scrollLeft = document.documentElement.scrollLeft;
            }
            var topOffset = Math.ceil(viewPortHeight / 2 - element.offsetHeight / 2);
            var leftOffset = Math.ceil(viewPortWidth / 2 - element.offsetWidth / 2);

            var top = scrollTop + topOffset;
            var left = scrollLeft + leftOffset;

            element.style.top = top + "px";
            element.style.left = left + "px";
        }   
        
        
            
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div style="width: 99.7%; height: 100%; margin-left:4px">
        <div>
            <table width="100%" background="images/contentheader_bgr.gif" border="0" cellpadding="6px" cellspacing="0px" style="margin-left:-5px; margin-right:300px;">
                <tr>
                    
                    <td align="left">::
                        <asp:Label ID="Pg_Title" runat="server" Font-Bold="True" ForeColor="#000099"></asp:Label>&nbsp;::
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        <asp:Label ID="lbl_Logout" runat="server" Text="Welcome: Administrator[Logout]" 
                            Font-Bold="True" ForeColor="#000099"></asp:Label>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td class="style1" align="left">
                        Start Date:
                    </td>
                    <td class="style2"  align="left">
                        End Date:
                    </td>
                    <td class="style3"  align="left">
                        &nbsp;
                    </td>
                    <td  align="left">
                        Select Station:
                    </td>
                    <td  align="left">
                        &nbsp;
                    </td  align="left">
                    <td>
                        &nbsp;
                    </td  align="left">
                    <td>
                        Select Station to compare
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style1" style="margin-left: 40px">
                        <telerik:RadDatePicker ID="dtStart" runat="server" Culture="English (United States)"
                            MinDate="2004-01-01">
                            <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                            </Calendar>
                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                            <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                            </DateInput>
                        </telerik:RadDatePicker>
                    </td>
                    <td class="style2">
                        <telerik:RadDatePicker ID="dtEnd" runat="server" Culture="English (United States)"
                            MinDate="2004-01-01">
                            <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                            </Calendar>
                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                            <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                            </DateInput>
                        </telerik:RadDatePicker>
                    </td>
                    <td class="style3">
                    </td>
                    <td style="margin-left: 40px">
                        <telerik:RadComboBox ID="txtStationSelect" runat="server" AfterClientCheck="AfterCheckHandler"
                            CollapseAnimation-Type="None" EmptyMessage="Choose a Station" ExpandAnimation-Type="None"
                            OnClientDropDownOpened="OnClientDropDownOpenedHandler" ShowToggleImage="True"
                            Style="vertical-align: middle;" Width="200px">
                            <ItemTemplate>
                                <div id="div2">
                                    <telerik:RadTreeView ID="radStationSelect" runat="server" Height="200px" Width="100%" OnClientNodeClicked="nodeClicking" 
                                    OnClientNodeClicking="nodeClicking" >
                                    </telerik:RadTreeView>
                                </div>
                            </ItemTemplate>
                            <Items>
                                <telerik:RadComboBoxItem Text="" />
                            </Items>
                        </telerik:RadComboBox>
                    </td>
                    <td style="margin-left: 40px">
                        <telerik:RadComboBox ID="ddType" runat="server" Width="75px" Sort="Ascending" SortCaseSensitive="true">
                        </telerik:RadComboBox>
                    </td>
                    <td>
                        <telerik:RadButton ID="btnGo" runat="server" Skin="Web20" Text="GO" OnClientClicked="OnClientClicked"
                            OnClick="btnGo_Click" ToolTip="Go">
                            <Icon PrimaryIconCssClass="rbSearch" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
                        </telerik:RadButton>
                    </td>
                    <td>
                        <telerik:RadComboBox ID="txtStationCompare" runat="server" AfterClientCheck="AfterCheckHandler"
                            CollapseAnimation-Type="None" EmptyMessage="Choose a Station" ExpandAnimation-Type="None"
                            OnClientDropDownOpened="OnClientDropDownOpenedHandler" ShowToggleImage="True"
                            Style="vertical-align: middle;" Width="200px">
                            <ItemTemplate>
                                <div id="divcom">
                                    <telerik:RadTreeView ID="radStationComapre" runat="server" Height="200px" Width="100%" OnClientNodeClicked="nodeClicking1" 
                                    OnClientNodeClicking="nodeClicking1" >
                                    </telerik:RadTreeView>
                                </div>
                            </ItemTemplate>
                            <Items>
                                <telerik:RadComboBoxItem Text="" />
                            </Items>
                        </telerik:RadComboBox>
                    </td>
                    <td>
                        <telerik:RadButton ID="BtnCompare" runat="server" Skin="Web20" Text="Compare" ToolTip="Compare" AutoPostBack="false">
                            <Icon PrimaryIconCssClass="rbSearch" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
                        </telerik:RadButton>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
               
            </table>
 
            <div id="ShowGirdContainerButtons" visible="true" runat="server">
                <table style="width: 100%; height: 100%">
                    <tr>
                        <td>
                            <div id="divWait" style="display: none">
                                <table style="height: 310Px" width="100%">
                                    <tr>
                                        <td align="center" width="100%">
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <img src="images/aniClock.gif" border="0" name="ProgressBarImg" alt=""><br />
                                            <font class="date">Acquiring Data... one moment please!</font>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label>
                            <div style="width: 100%;" id="DIV1">
                                <telerik:RadGrid ID="dgResult" runat="server" OnItemDataBound="dgResult_ItemDataBound"
                                    CellSpacing="0" GridLines="None" Skin="Web20" Width="99.4%"
                                    Visible="False" EnableViewState="False" CellPadding="0" 
                                    Font-Names="Helvetica" >

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Web20"></HeaderContextMenu>

                                    <AlternatingItemStyle Font-Names="Helvetica" Font-Size="XX-Small" CssClass="rowheightcls"
                                         />

                                    <ItemStyle Font-Names="Helvetica" Font-Size="XX-Small" CssClass="rowheightcls" />

<MasterTableView EnableViewState="False">
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column"></ExpandCollapseColumn>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>

                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />

                                    <ClientSettings>
                                        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                        <ClientEvents OnGridCreated="GridCreated" />
                                    </ClientSettings>
                                                                        
<FilterMenu EnableImageSprites="False"></FilterMenu>
                                                                        
                                </telerik:RadGrid>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" id="divLane1" style="display: none;">
                                        <asp:Literal ID="litExportTo" runat="server" Text="Export to: " Visible="false"></asp:Literal>
                                        <telerik:RadButton ID="btnExportToExcel" runat="server" Text="Excel" Visible="false"
                                            OnClick="btnExportToExcel_Click">
                                        </telerik:RadButton>
                                        <telerik:RadButton ID="btnExportToCsv" runat="server" Text="Text" Visible="false"
                                            OnClick="btnExportToCsv_Click">
                                        </telerik:RadButton>
                                        <telerik:RadButton ID="btnExportToPdf" runat="server" Text="PDF" Visible="false"
                                            OnClick="btnExportToPdf_Click">
                                        </telerik:RadButton>
                                        <telerik:RadButton ID="btnPrint" runat="server" Text="PDF" Visible="false">
                                        </telerik:RadButton>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td align="right" id="divLane2" style="display: none;">
                                        <telerik:RadButton ID="btn1" runat="server" Text="Songs" Visible="false" OnClick="btn1_onclick">
                                        </telerik:RadButton>
                                        <telerik:RadButton ID="btn4" runat="server" Text="Links" Visible="false" OnClick="btn4_onclick">
                                        </telerik:RadButton>
                                        <telerik:RadButton ID="btn2" runat="server" Text="Spots" Visible="false" OnClick="btn2_onclick">
                                        </telerik:RadButton>
                                        <telerik:RadButton ID="btn3" runat="server" Text="All" Visible="false" OnClick="btn3_onclick">
                                        </telerik:RadButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="NoRecordsDiv" visible="false" runat="server">
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <center>
                <asp:Label ID="NoRecordsLiteral" runat="server" Text="No Records Found..." Visible="false"
                    Font-Bold="true"></asp:Label>
            </center>
            <br />
            <br />
            <br />
            <br />
        </div>
    </div>

</asp:Content>
