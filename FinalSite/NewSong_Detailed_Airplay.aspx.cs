﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Telerik.Web.UI;

public partial class NewSong_Detailed_Airplay : System.Web.UI.Page
{
    bool isPdfExport = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        //StringBuilder onloadScript = new StringBuilder();
        //onloadScript.Append("<script type='text/javascript'>");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("ShowReport();");
        //onloadScript.Append(Environment.NewLine);
        //onloadScript.Append("</script>");
        //this.ClientScript.RegisterStartupScript(this.GetType(), "onLoadCall", onloadScript.ToString());


        if (!IsPostBack)
        {
            Session["Hrds"] = null;
            bindGrid(Request.QueryString["stationName"], Request.QueryString["sdate"], Request.QueryString["edate"], Request.QueryString["songTitle"], Request.QueryString["movieTitle"], Request.QueryString["SelectedDate"], Request.QueryString["playedhour"]);
            if (Session["dataReadOnly"].ToString() == "True")
            {
                btnExportToExcel.Enabled = false;
                btnExportToPdf.Enabled = false;
            }
            else
            {
                btnExportToExcel.Enabled = true;
                btnExportToPdf.Enabled = true;
            }
        }
    }
    protected void img_btn_export_to_excel_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }
    private string GetUsDate(string indDate)
    {
        string str = "";
        char[] sep = { '/' };
        string[] vals = indDate.Split(sep);
        str = vals[1] + "/" + vals[0] + "/" + vals[2];
        return str;
    }

    protected void btnExportToPdf_Click(object sender, EventArgs e)
    {


        DateTime dtStartDate = DateTime.Parse(Request.QueryString["sdate"]);
        DateTime dtEndDate = DateTime.Parse(Request.QueryString["edate"]);

        //addAuditLog(dtStartDate, dtEndDate, Request.QueryString["stationIDs"], false, true, false);

        isPdfExport = true;
        bindGrid(Request.QueryString["stationName"], Request.QueryString["sdate"], Request.QueryString["edate"], Request.QueryString["songTitle"], Request.QueryString["movieTitle"], Request.QueryString["SelectedDate"], Request.QueryString["playedhour"]);

        string strDateFormat = (string)Session["dateFormat"];
        DateTime dt1 = DateTime.Parse(Session["strStartDate"].ToString());
        DateTime dt2 = DateTime.Parse(Session["strEndDate"].ToString());

        string sDt1 = "";
        string sDt2 = "";
        sDt1 = dt1.ToString("dd/MM/yyyy");
        sDt2 = dt2.ToString("dd/MM/yyyy");

        String strDateRange = "From: " + sDt1 + "  -  " + sDt2;

        radData.ExportSettings.FileName = "New Songs Detailed Airplay Report";
        radData.ExportSettings.IgnorePaging = true;
        radData.ExportSettings.OpenInNewWindow = true;

        String strReportName = "AirCheck India Detailed Airplay Report - " + Request.QueryString["stationName"];

        radData.MasterTableView.GetColumn("ColColon").HeaderText = "";
        radData.MasterTableView.GetColumn("ColValue").HeaderText = "";
        radData.MasterTableView.GetColumn("ColName").HeaderText = "";

        radData.ExportSettings.Pdf.PageTitle = strReportName + " " + strDateRange;
        radData.ExportSettings.ExportOnlyData = true;
        
        radData.MasterTableView.ExportToPdf();

        //addAuditLog(Convert.ToDateTime(GetUsDate(sDt1)), Convert.ToDateTime(GetUsDate(sDt2)), "", false, true, false);

    }
    public void ExportToExcel()
    {
        string strDateFormat = (string)Session["dateFormat"];

        DataSet ds = (DataSet)Session["hrds"];
        DataTable dt = new DataTable("tblExport");
        dt = ds.Tables[0].Copy();



        string str_station_name = "";

        string str_market_name = "";// getMarketOfStation(str_station_name);

        String strStationName = "<BR><BR>Station: " + str_station_name;
        String strMarket = "<BR><BR>Market: " + str_market_name;

        DateTime dt1 = DateTime.Parse(Session["strStartDate"].ToString());
        DateTime dt2 = DateTime.Parse(Session["strEndDate"].ToString());

        string sDt1 = "";
        string sDt2 = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            sDt1 = dt1.ToString("dd/MM/yyyy");
            sDt2 = dt2.ToString("dd/MM/yyyy");
        }
        else
        {
            sDt1 = dt1.ToString("MM/dd/yyyy");
            sDt2 = dt2.ToString("MM/dd/yyyy");
        }

        String strDateRange = "<BR><BR>From: " + sDt1 + "  to  " + sDt2;

        ExportToExcel(dt, Response, strStationName, strMarket, strDateRange);
        //addAuditLog(Convert.ToDateTime(GetUsDate(sDt1)), Convert.ToDateTime(GetUsDate(sDt2)), str_station_name, true, false, false);

    }

    public void ExportToExcel(DataTable dt, HttpResponse response, String strStationName, string strMarket, String strDateRange)
    {

        //first let's clean up the response.object
        response.Clear();
        response.Charset = "";
        //set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel";
        //create a string writer
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //create an htmltextwriter which uses the stringwriter
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //instantiate a datagrid
        DataGrid dg = new DataGrid();
        //set the datagrid datasource to the dataset passed in
        if (dt == null)
        {
            return;
        }
        else if (dt.Rows.Count == 0)
        {
            return;
        }
        dg.DataSource = dt;
        //bind the datagrid
        dg.DataBind();

        dg.RenderControl(htmlWrite);


        String str = stringWrite.ToString();

        str = str.Replace("ColName", "");
        str = str.Replace("ColColon", "");
        str = str.Replace("ColValue", "");

        int start, end;
        start = 0;
        end = 0;

        int colspan = 0;

        colspan = dt.Columns.Count;


        String strReportName = "";

        strReportName = " AirCheck India Detailed Song Airplay Report.";
        //if (ddSelectBy.Text == "Station")
        //{
        //    strReportName = getHeaderIfStationsSelected() + " AirCheck India New Songs Report.";
        //}
        //else if (ddSelectBy.Text == "Market")
        //{
        //    strReportName = "AirCheck India New Songs Report " + getHeaderIfMarketsSelected();
        //}
        //else if (ddSelectBy.Text == "Network")
        //{
        //    strReportName = "AirCheck India New Songs Report " + getHeaderIfNetworkSelected();
        //}


        string strFinal = "<tr><td align=center colspan=" + colspan + " style='background-color:#FFFF66;'>" + strReportName + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + " style='background-color:#FFFF66;'>" + strDateRange + "</td></tr>";
        strFinal = strFinal + "<tr><td colspan=" + colspan + ">&nbsp;</td></tr>";



        while (str.IndexOf("<select name=") > 0)
        {
            start = str.IndexOf("<select name=");
            end = str.IndexOf("</select>") + 9;
            str = str.Replace(str.Substring(start, end - start), "");
        }

        string strAirTime = "";
        string strPlayTime = "";
        string strNewPlayTime = "";

        int intCount = 0;

        foreach (DataRow dr in dt.Rows)
        {
            strAirTime = dr[0].ToString();
            if (strAirTime == "")
            {
                strPlayTime = dr[2].ToString();
                strNewPlayTime = dr[2].ToString() + " ";

                str = str.Replace("<tr  style='background-color:#FFCCFF'><td>&nbsp;</td><td>&nbsp;</td><td>" + strPlayTime + "</td><td>&nbsp;</td><td>&nbsp;</td>\r\n\t</tr>", "<tr  style='background-color:#FFCCFF'><td colspan=5 align='Center'>" + strNewPlayTime + "</td></tr>");

            }
            else
            {
                intCount = intCount + 1;
            }
        }


        str = strFinal + "<BR><BR>" + str;


        string strDateFormat = (string)Session["dateFormat"];

        DateTime dtDate = System.DateTime.Now;
        string strDate = "";
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dtDate.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dtDate.ToString("MM/dd/yyyy");
        }


        string strTime = System.DateTime.Now.ToString();
        strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


        string strCountText = "<b>" + intCount + " rows </b>";

        string strFooterText = "";

        strFooterText = "<BR><table>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>This data was generated and exported on " + strDate + " at " + strTime + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><b>" + strCountText + "</b></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;'><BR></td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India.</td></tr>";
        strFooterText += "<tr><td align=left colspan=" + colspan + " style='background-color:#FFFF66;font-size: 6px;'>The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.</td></tr>";
        strFooterText += "</table>";

        str = "<table>" + str + strFooterText;



        response.Write(str);
        response.End();
    }
    protected void displayNoRecordFound()
    {
        lblError.Visible = true;
        lblError.Text = "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Record Found";

        radData.Visible = false;
        litExportTo.Visible = false;
        btnExportToExcel.Visible = false;
        btnExportToPdf.Visible = false;
    }

    //    protected void addAuditLog(DateTime startDate, DateTime endDate, string station,
    //Boolean exportedToExcel, Boolean exportedToPdf, Boolean exportedToText)
    //    {
    //        auditTrail at = new auditTrail();
    //        at.connectionString = ConfigurationSettings.AppSettings["connectionString"].ToString();
    //        at.userID = (int)Session["userID"];
    //        at.IPAddress = Request.ServerVariables["REMOTE_ADDR"];
    //        //at.pageViewed = "National Advertiser/Detailed Spot";
    //        at.pageViewed = "New Songs";
    //        at.startDate = startDate;
    //        at.endDate = endDate;
    //        at.station = station;
    //        at.exportedToExcel = exportedToExcel;
    //        at.exportedToPdf = exportedToPdf;
    //        at.exportedToText = exportedToText;
    //        at.addUserActivityLogPassingStationIDs();
    //        at = null;
    //    }

    protected void bindGrid(string strStationName, string strStartDate, string strEndDate, string songTitle, string movieTitle, string SelectedDate, string playedhour)
    {
        //rep_str_advt = rep_str_advt.Replace("456456", "&");

        try
        {
            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
            SqlDataAdapter da = new SqlDataAdapter("DetailedSongAirplayReport", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.CommandTimeout = 0;

            da.SelectCommand.Parameters.AddWithValue("@stationName", strStationName);
            da.SelectCommand.Parameters.AddWithValue("@startDate", strStartDate);
            da.SelectCommand.Parameters.AddWithValue("@endDate", strEndDate);
            da.SelectCommand.Parameters.AddWithValue("@songTitle", songTitle);
            da.SelectCommand.Parameters.AddWithValue("@movieTitle", movieTitle);
            da.SelectCommand.Parameters.AddWithValue("@SelectedDate", SelectedDate);
            da.SelectCommand.Parameters.AddWithValue("@playedhour", playedhour);
            //da.SelectCommand.Parameters.AddWithValue("@username", (string)Session["user"]);

            Session["stationName"] = strStationName;
            Session["strStartDate"] = strStartDate;
            Session["strEndDate"] = strEndDate;

            DataSet ds = new DataSet();
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count == 0)
            {
                displayNoRecordFound();
                Session["totalRecordCount"] = 0;
            }
            else
            {
                radData.DataSource = ds.Tables[0];
                radData.DataBind();
                litExportTo.Visible = true;
                btnExportToExcel.Visible = true;
                btnExportToPdf.Visible = true;
                radData.Visible = true;
                lblError.Visible = false;
                Session["hrds"] = ds;

                Session["totalRecordCount"] = ds.Tables[0].Rows.Count;

            }
            //addAuditLog(Convert.ToDateTime(strStartDate), Convert.ToDateTime(strEndDate), "", false, false, false);
        }
        catch
        {
            displayNoRecordFound();
        }
    }

    protected void radData_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {


        if (e.Item.ItemType == GridItemType.Header)
        {
            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            for (int i = 0; i < e.Item.Cells.Count; i++)
            {

                if (Session["headerFontStyle"].ToString().ToLower() == "bold")
                {
                    e.Item.Cells[i].Font.Bold = true;
                }
                else
                {
                    e.Item.Cells[i].Font.Bold = true;
                }
                e.Item.Cells[i].Font.Size = FontUnit.Parse(Session["headerFontSize"].ToString().Replace("Px", "").Trim());
                i++;
            }
            //e.Item.BackColor = System.Drawing.Color.FromArgb(0, 112, 192);
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);
        }



        if (e.Item.ItemType == Telerik.Web.UI.GridItemType.Item || e.Item.ItemType == Telerik.Web.UI.GridItemType.AlternatingItem)
        {
            string actualDataFontName = (string)Session["dataFontName"];//ConfigurationSettings.AppSettings["actualDataFontNameMarketShareOfEachStation"].ToString();
            string actualDataFontSize = (string)Session["dataFontSize"];//ConfigurationSettings.AppSettings["actualDataFontSizeMarketShareOfEachStation"].ToString();

            Telerik.Web.UI.GridDataItem dataItem = e.Item as Telerik.Web.UI.GridDataItem;


            System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();
            dataItem.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            dataItem.Font.Size = FontUnit.Parse(Session["dataFontSize"].ToString().Replace("Px", "").Trim());
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                dataItem.Font.Bold = true;
            }
            else
            {
                dataItem.Font.Bold = false;
            }


            if (e.Item.ItemType == GridItemType.AlternatingItem)
            {
                if (!isPdfExport)
                {
                    System.Drawing.ColorConverter conv1 = new System.Drawing.ColorConverter();
                    e.Item.BackColor = (System.Drawing.Color)conv1.ConvertFromString((string)Session["alternateRowColor"]);
                }
            }

            dataItem["ColName"].Font.Name = actualDataFontName;
            dataItem["ColName"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            dataItem["ColColon"].Font.Name = actualDataFontName;
            dataItem["ColColon"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            dataItem["ColValue"].Font.Name = actualDataFontName;
            dataItem["ColValue"].Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());

            //dataItem["ColName"].Text = "";
            //dataItem["ColColon"].Text = "";
            //dataItem["ColValue"].Text = "";

        }
        if (e.Item.ItemType == GridItemType.Footer)
        {
            if (!isPdfExport)
            {
                e.Item.Visible = false;
            }
            else
            {
                e.Item.Visible = true;

                GridFooterItem footerItem = (GridFooterItem)e.Item;

                string strDateFormat = (string)Session["dateFormat"];

                DateTime dtDate = System.DateTime.Now;
                string strDate = "";
                strDate = dtDate.ToString("dd/MM/yyyy");


                string strTime = System.DateTime.Now.ToString();
                strTime = String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);


                string strCountText = "<b>" + Session["totalRecordCount"].ToString() + " rows </b>";

                string strFooterText = "";

                strFooterText = "This data was generated and exported on " + strDate + " at " + strTime + ".";
                strFooterText += " (" + strCountText + ") ";

                strFooterText += "AirCheck India services are © Copyright 2002-2009 AirCheck India. All Rights Reserved. ";
                strFooterText += "AirCheck India, its logo, and 'The New Broadcast Monitoring' are registered trademarks of AirCheck India. ";
                strFooterText += "The AirCheck India broadcast content recognition process is protected by U.S. Patents 5,437,050 and 7,386,047 with additional patents pending.";


                e.Item.Cells[2].ColumnSpan = 2;
                e.Item.Cells[2].Text = strFooterText;
                e.Item.Cells[2].Style["color"] = "red";
                e.Item.Font.Size = FontUnit.Point(5);
                e.Item.Font.Bold = false;

            }

        }

    }


    protected int getStationIDOfStation(string txtStation)
    {
        int stationID = 0;
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("getStationIDOfStation", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@stationName", txtStation);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                stationID = (int)dr["station_id"];
            }
        }
        else
        {
            stationID = 0;
        }

        conn.Close();
        return stationID;
    }




    protected string getStationsOfMarket(string txtMarket)
    {
        string strStationID = "";
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("getStationIDOfMarket", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@marketName", txtMarket);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                strStationID += dr["station_id"] + ",";
            }
        }
        else
        {
            strStationID = "";
        }

        conn.Close();

        if (strStationID != "")
        {
            strStationID = strStationID.Substring(0, strStationID.Length - 1);
        }

        return strStationID;

    }



    protected string getStationsOfNetwork(string txtNetwork, string txtMarket)
    {
        string strStationID = "";
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("getStationIDOfNetworkAndMarket", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@networkName", txtNetwork);
        comm.Parameters.AddWithValue("@marketName", txtMarket);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                strStationID += dr["station_id"].ToString() + ",";
            }
        }
        else
        {
            strStationID = "";
        }

        conn.Close();

        string strReturn = "";
        if (strStationID == "")
        {
            strReturn = strStationID;
        }
        else
        {
            strReturn = strStationID.Substring(0, strStationID.Length - 1);
        }
        return strReturn;

    }
}
