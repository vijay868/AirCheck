﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="topCategories.aspx.cs" Inherits="topCategories" Title="Top Categories" %>

<%@ Register Assembly="DBauer.Web.UI.WebControls.HierarGrid" Namespace="DBauer.Web.UI.WebControls"
    TagPrefix="DBWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <style type="text/css">
        .gvHeaderRow
        {
            background-image: url("images/dhrudde.png");
            font-family: Helvetica;
            font-size: 12px;
            color: White;
            text-decoration: none;
            line-height: 12px;
            padding-left: 10px;
            border-left: solid 1px white;
            border-right: solid 1px white;
            border-bottom: solid 1px white;
            position: relative;
        }
        .rowheightcls td
        {
            line-height: 11px;
        }
    </style>

    <script type="text/javascript" language="javascript">
function ToggleCollapsePane() {               
               var splitter = $find("<%= Page.Master.FindControl("RadSplitter1").ClientID %>");
               var pane = splitter.getPaneById("<%= Page.Master.FindControl("RadPane1").ClientID %>");
                pane.collapse();
    }
    function disp_details(strBrandName,strParentName,strCategory) {
        win = window.open("topCategoriesDetails.aspx?strBrandName=" + strBrandName + "&strParentName=" + strParentName + "&strCategory=" + strCategory, "win", "width=800,height=400,left=0, scrollbars=yes, resizable=yes");
        return false
    }
     function days_between(date1, date2) {

            // The number of milliseconds in one day
            var ONE_DAY = 1000 * 60 * 60 * 24

            // Convert both dates to milliseconds
            var date1_ms = date1.getTime()
            var date2_ms = date2.getTime()

            // Calculate the difference in milliseconds
            var difference_ms = Math.abs(date1_ms - date2_ms)
            
            // Convert back to days and return
            return Math.round(difference_ms/ONE_DAY)

        }
        function OnClientClicked(button, args)
        {
           if (calcDays())
           {                        
//               var divobj = document.getElementById("ShowGirdContainerButtons")
//             divobj.style.display = "block";
                button.set_autoPostBack(true); 
           }                
            else
            {                              
                button.set_autoPostBack(false);                        
            }
        }
        function calcDays()
        {
            ToggleCollapsePane();
            var startDate =$find("<%=dtStart.ClientID %>");
            var endDate =  $find("<%=dtEnd.ClientID %>");
            
            var sDate = startDate.get_selectedDate();
            var eDate = endDate.get_selectedDate();
            
             var lockDataBefore="<%=Session["lockDataBefore"]%>";
		    var lockDataAfter="<%=Session["lockDataAfter"]%>";
            if(sDate == null)
//            if(sDate == null || eDate == null)
            {
                lockDataBefore = new Date(lockDataBefore);
                var curr_date = lockDataBefore.getDate();
                var curr_month = lockDataBefore.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataBefore.getFullYear();
                lockDataBefore= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights only from " + lockDataBefore);
                return false;
            }
            
            if(eDate == null)
            {
                lockDataAfter = new Date(lockDataAfter);
                var curr_date = lockDataAfter.getDate();
                var curr_month = lockDataAfter.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataAfter.getFullYear();
                lockDataAfter= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights till " + lockDataAfter);
                return false;
            }
        
            if(days_between(sDate,eDate) > 30)
            {
                alert("Maximum allowed date range is 1 month.");
                return false;
            }
        
            var dd = document.getElementById("ctl00_ContentPlaceHolder2_ddSelectBy");
		    var valSelectBy = dd.value;
		                
            if (valSelectBy == "")
            {
                alert("Choose option from 'Select By'");            
                return false;
            }

          

            var isStationSelected = 0;
            var isMarketSelected = 0;
            var isNetworkMarketSelected = 0;            
            
           // var treeS = sender.get_items().getItem(0).findControl("radTreeStations");
            
            var comboBox = $find("<%= CmbTreeStations.ClientID %>");    
            var treeS = comboBox.get_items().getItem(0).findControl("radTreeStations");
            
            var rootNodesS = treeS.SelectedNode;
            var nodesS = treeS.get_allNodes();

            var iS;
            
             for(iS=0; iS<nodesS.length; iS++)
             {
                if(!nodesS[iS].get_checked())
                {
                
                }
                else
                {
                    isStationSelected = 1;
                }
             }            

            
            //var tree = sender.get_items().getItem(0).findControl("radTreeMarkets");
            
            var comboBox = $find("<%= CmbTreeMarkets.ClientID %>");    
            var tree = comboBox.get_items().getItem(0).findControl("radTreeMarkets");
            
            var rootNodes = tree.SelectedNode;
            var nodes = tree.get_allNodes(); 

            var i;
            
             for(i=0; i<nodes.length; i++)
             {
                if(!nodes[i].get_checked())
                {
                
                }
                else
                {
                    isMarketSelected = 1;
                }
             }
            
            //var treeNM = sender.get_items().getItem(0).findControl("radTreeMarketsForSelectedNetwork");
            
            var comboBox = $find("<%= CmbTreeMarketsForSelectedNetwork.ClientID %>");    
            var treeNM = comboBox.get_items().getItem(0).findControl("radTreeMarketsForSelectedNetwork");
            
            var rootNodes = treeNM.SelectedNode;
            var nodesNM = treeNM.get_allNodes();

            var iNM;
            
             for(iNM=0; iNM<nodesNM.length; iNM++)
             {
                if(!nodesNM[iNM].get_checked())
                {
                
                }
                else
                {
                    isNetworkMarketSelected = 1;
                }
             }
             

            var ddNetworks = document.getElementById("ctl00_ContentPlaceHolder2_ddNetworks");
		    var valNetwork = ddNetworks.value;
		                
            
            
            
            if (isStationSelected == 0 && valSelectBy == "Station")
            {
                alert("You have not selected any station/s");
                return false;
            }
            
            if (isMarketSelected == 0 && valSelectBy == "Market")
            {
                alert("You have not selected any  Market/s");
                return false;
            }

            if (valNetwork == "" && valSelectBy == "Network")
            {
                alert("You have not selected any Network");
                return false;
            }
            
            if (isNetworkMarketSelected == 0 && valSelectBy == "Network")
            {
                alert("You have not selected any  Market/s");
                return false;
            }

           
            //tdhide('leftMenu', 'ctl00_showHide','hide');
                        
            disp_clock();
            return true;

                
        }  
        
		function disp_clock()
		{
		    var lblError = document.getElementById("ctl00_content1_lblError");
		    if (lblError != null)
		    {
		        lblError.style.display='none';
		    }
			OnLoad();
			DIV1.style.display='none';
			divWait.style.display='';
    	}

		function OnLoad() 
		{ 
		    setTimeout("StartAnimation()", 500); 
		} 

		function StartAnimation() 
		{ 
		    if (document.images)
		    {
		        document['ProgressBarImg'].src = "images/aniClock.gif";
		    }
		}
		
	    var showMode = 'block';


        function ExpandStationNodes()
        {        
           var comboBox = $find("<%= CmbTreeStations.ClientID %>");    
           var tree = comboBox.get_items().getItem(0).findControl("radTreeStations");
           var nodes = tree.get_allNodes();            
           for (var i=0; i<nodes.length; i++)
           {
                if (nodes[i].get_allNodes() != null)
                {
                    nodes[i].expand();
                    //tree.AllNodes[i].Collapse();
                }
           }
        }

        function ExpandMarketNodes()
        {        
           var comboBox = $find("<%= CmbTreeMarkets.ClientID %>");    
           var tree = comboBox.get_items().getItem(0).findControl("radTreeMarkets");
           var nodes = tree.get_allNodes();            
           for (var i=0; i<nodes.length; i++)
           {
                if (nodes[i].get_allNodes() != null)
                {
                    nodes[i].expand();
                    //tree.AllNodes[i].Collapse();
                }
           }
        }
        
        function ExpandNetworkMarketNodes()
        {           
           var comboBox = $find("<%= CmbTreeMarketsForSelectedNetwork.ClientID %>"); 
           var tree = comboBox.get_items().getItem(0).findControl("radTreeMarketsForSelectedNetwork");
           var nodes = tree.get_allNodes();            
           for (var i=0; i<nodes.length; i++)
           {
                if (nodes[i].get_allNodes() != null)
                {
                    nodes[i].expand();
                    //tree.AllNodes[i].Collapse();
                }
           }
        }       
        
		function OnddSelectByIndexChanged(sender,eventArgs) {
		    var valSelectBy = sender.get_text();
		    //alert(valSelectBy);
		    document.getElementById("ctl00_ContentPlaceHolder2_hdnddSelectByVal").value = valSelectBy;
		    if(valSelectBy == "Station")
		    {
		        document.getElementById("tdStext").style.display = "block";
		        document.getElementById("tdMtext").style.display = "none";
		        document.getElementById("tdNtext").style.display = "none";
		        document.getElementById("tdNMtext").style.display = "none";
		        
		        document.getElementById("tdS").style.display = "block";
		        document.getElementById("tdM").style.display = "none";
		        document.getElementById("tdN").style.display = "none";
		        document.getElementById("tdNM").style.display = "none";
		    }
		    else if(valSelectBy == "Market")
		    {
		        document.getElementById("tdStext").style.display = "none";
		        document.getElementById("tdMtext").style.display = "block";
		        document.getElementById("tdNtext").style.display = "none";
		        document.getElementById("tdNMtext").style.display = "none";
		        
		        document.getElementById("tdS").style.display = "none";
		        document.getElementById("tdM").style.display = "block";
		        document.getElementById("tdN").style.display = "none";
		        document.getElementById("tdNM").style.display = "none";
		    } 
		    else if(valSelectBy == "Network")
		    {
		        document.getElementById("tdStext").style.display = "none";
		        document.getElementById("tdMtext").style.display = "none";
		        document.getElementById("tdNtext").style.display = "block";
		        document.getElementById("tdNMtext").style.display = "block";
		        
		        document.getElementById("tdS").style.display = "none";
		        document.getElementById("tdM").style.display = "none";
		        document.getElementById("tdN").style.display = "block";
		        document.getElementById("tdNM").style.display = "block";
		    } 
		    else if(valSelectBy == "" || valSelectBy == null)
		    {
		         document.getElementById("tdStext").style.display = "none";
            document.getElementById("tdMtext").style.display = "none";
            document.getElementById("tdNtext").style.display = "none";
            document.getElementById("tdNMtext").style.display = "none";

            document.getElementById("tdS").style.display = "none";
            document.getElementById("tdM").style.display = "none";
            document.getElementById("tdN").style.display = "none";
            document.getElementById("tdNM").style.display = "none";
		    }                             
                
           // ExpandStationNodes();
           // ExpandMarketNodes();
          //  ExpandNetworkMarketNodes();

           // HideTreeViewS();
//            HideTreeViewN();
//            HideTreeViewM();
//            
//		    if (valSelectBy == "Station") 
//		    {
//		            
//		        cells1 = document.getElementsByName("selectStation");

//		        for(j = 0; j < cells1.length; j++) 
//		        {
//		            cells1[j].style.display = showMode;
//                }
//                
//                cells2 = document.getElementsByName("selectNetwork");
//		        for(j = 0; j < cells2.length; j++) 
//		        {
//		            cells2[j].style.display = 'none';
//                }

//                cells3 = document.getElementsByName("selectMarket");
//		        for(j = 0; j < cells3.length; j++) 
//		        {
//		            cells3[j].style.display = 'none';
//                }
//                               

//		    }
//		    else if (valSelectBy == "Market") {
//		    
//		        var radTreeMarketsForSelectedNetwork =document.getElementById("ctl00_ContentPlaceHolder2_CmbTreeMarketsForSelectedNetwork_DropDown")		                                
//		        radTreeMarketsForSelectedNetwork.style.display = 'none';
//		        
//		        var radTreeMarkets =document.getElementById("ctl00_ContentPlaceHolder2_CmbTreeMarkets_DropDown");
//		        radTreeMarkets.style.display = showMode;
//		        
//		        
//		        cells1 = document.getElementsByName("selectStation");

//		        for(j = 0; j < cells1.length; j++) 
//		        {
//		            cells1[j].style.display = 'none';
//                }
//                
//                cells2 = document.getElementsByName("selectNetwork");
//		        for(j = 0; j < cells2.length; j++) 
//		        {
//		            cells2[j].style.display = 'none';
//                }

//                cells3 = document.getElementsByName("selectMarket");
//		        for(j = 0; j < cells3.length; j++) 
//		        {
//		            cells3[j].style.display = showMode;
//                }
//		    }
//		    else if (valSelectBy == "Network") {
//		        cells1 = document.getElementsByName("selectStation");

//		        var radTreeMarketsForSelectedNetwork =document.getElementById("ctl00_ContentPlaceHolder2_CmbTreeMarketsForSelectedNetwork_DropDown");
//		        radTreeMarketsForSelectedNetwork.style.display = showMode;
//		        
//		        var radTreeMarkets =document.getElementById("ctl00_ContentPlaceHolder2_CmbTreeMarkets_DropDown");
//		        radTreeMarkets.style.display = 'none';
//		        
//		        for(j = 0; j < cells1.length; j++) 
//		        {
//		            cells1[j].style.display = 'none';
//                }
//                
//                cells2 = document.getElementsByName("selectNetwork");
//		        for(j = 0; j < cells2.length; j++) 
//		        {
//		            cells2[j].style.display = showMode;
//                }

//                cells3 = document.getElementsByName("selectMarket");
//		        for(j = 0; j < cells3.length; j++) 
//		        {
//		            cells3[j].style.display = showMode;
//                }
//		    }
//		    else {
//		        cells1 = document.getElementsByName("selectStation");

//		        for(j = 0; j < cells1.length; j++) 
//		        {
//		            cells1[j].style.display = 'none';
//                }
//                
//                cells2 = document.getElementsByName("selectNetwork");
//		        for(j = 0; j < cells2.length; j++) 
//		        {
//		            cells2[j].style.display = 'none';
//                }

//                cells3 = document.getElementsByName("selectMarket");
//		        for(j = 0; j < cells3.length; j++) 
//		        {
//		            cells3[j].style.display = 'none';
//                }
//		        
//		    }
		}

//		function showStationsColumn() {
//		    document.getElementById("ctl00_content1_txtStation").style.visibility = "visible";
//		    document.getElementById("imgStations").style.visibility = "visible";
//		    document.getElementById("treeDivS").style.visibility = "hidden";
//		}

//		function hideStationsColumn() {
//		    document.getElementById("ctl00_content1_txtStation").style.visibility = "hidden";
//		    document.getElementById("imgStations").style.visibility = "hidden";
//		    document.getElementById("treeDivS").style.visibility = "hidden";
//		}
//		function showNetworksColumn() {
//		    document.getElementById("ctl00_content1_txtNetwork").style.visibility = "visible";
//		    document.getElementById("imgNetworks").style.visibility = "visible";
//		    document.getElementById("treeDivN").style.visibility = "hidden";
//		}

//		function hideNetworksColumn() {
//		    document.getElementById("ctl00_content1_txtNetwork").style.visibility = "hidden";
//		    document.getElementById("imgNetworks").style.visibility = "hidden";
//		    document.getElementById("treeDivN").style.visibility = "hidden";
//		}

//		function showMarketsColumn() {
//		    document.getElementById("ctl00_content1_txtMarket").style.visibility = "visible";
//		    document.getElementById("imgMarkets").style.visibility = "visible";
//		    document.getElementById("treeDivM").style.visibility = "hidden";
//		}

//		function hideMarketsColumn() {
//		    document.getElementById("ctl00_content1_txtMarket").style.visibility = "hidden";
//		    document.getElementById("imgMarkets").style.visibility = "hidden";
//		    document.getElementById("treeDivM").style.visibility = "hidden";
//		}

		function UpdateAllChildren(nodes, checked) {
		    var i;
		    for (i = 0; i < nodes.length; i++) {
		        if (checked)
		            nodes[i].Check();
		        else
		            nodes[i].UnCheck();

		        if (nodes[i].Nodes.length > 0)
		            UpdateAllChildren(nodes[i].Nodes, checked);
		    }
		}



		function AfterCheck(node) {



		    if (!node.Checked && node.Parent != null) {
		        node.Parent.UnCheck();
		    }

		    var siblingCollection = (node.Parent != null) ? node.Parent.Nodes : node.TreeView.Nodes;

		    var allChecked = true;
		    for (var i = 0; i < siblingCollection.length; i++) {
		        if (!siblingCollection[i].Checked) {
		            allChecked = false;
		            break;
		        }
		    }
		    if (allChecked && node.Parent != null) {
		        node.Parent.Check();
		    }

		    UpdateAllChildren(node.Nodes, node.Checked);
		}

//		function ToggleTreeViewS() {
//		    if (document.getElementById("treeDivS").style.visibility == "visible")
//		        HideTreeViewS();
//		    else
//		        ShowTreeViewS();
//		}

//		function ShowTreeViewS() {
//		    document.getElementById("treeDivS").style.visibility = "visible";
//		}

//		function HideTreeViewS() {
//		    if (document.getElementById("treeDivS") != null) {
//		        document.getElementById("treeDivS").style.visibility = "hidden";
//		    }
//		}

//		function ToggleTreeViewN() {
//		    if (document.getElementById("treeDivN").style.visibility == "visible")
//		        HideTreeViewN();
//		    else
//		        ShowTreeViewN();
//		}

//		function ShowTreeViewN() {
//		    document.getElementById("treeDivN").style.visibility = "visible";
//		}

//		function HideTreeViewN() {
//		    if (document.getElementById("treeDivN") != null) {
//		        document.getElementById("treeDivN").style.visibility = "hidden";
//		    }
//		}

//		function ToggleTreeViewM() {
//		    if (document.getElementById("treeDivM").style.visibility == "visible")
//		    {
//		        HideTreeViewM();
//		    }
//		    else
//		    {
//		        ShowTreeViewM();
//		    }
//		}

//		function ShowTreeViewM() {
//		    document.getElementById("treeDivM").style.visibility = "visible";
//		}

//		function HideTreeViewM() {
//		    if (document.getElementById("treeDivM") != null) {
//		        document.getElementById("treeDivM").style.visibility = "hidden";
//		    }
//		}	
		function nodeCheckedTreeStations(sender, args) {

        var comboBox = $find("<%= CmbTreeStations.ClientID %>");
        var tree = comboBox.get_items().getItem(0).findControl("radTreeStations");
            var node = args.get_node();
            var rootNodeSelCount = 0;
            var nodesJ=tree.get_nodes(); 
            var marketsval="";
              for (i=0; i<nodesJ.get_count(); i++) 
               {                
                   if (nodesJ.getNode(i).get_checked()) 
                   { 
                       //alert(nodesJ.getNode(i).get_text());
                       marketsval = marketsval + nodesJ.getNode(i).get_text().toString() + ",";
                       rootNodeSelCount =  rootNodeSelCount + 1;
                   } 
               }
               
               if(rootNodeSelCount > 1)
               {
                   alert('Cannot select more than 1 markets');
                   node.uncheck();
                   return false;
               }
 
            var tempNode = args.get_node();
            if (tempNode.get_text().toString() == "(Select All)") 
            {
                // check or uncheck all the nodes
            } else {
                var nodes = new Array();
                nodes = sender.get_checkedNodes();
                var vals = "";
                var i = 0;
                var icount = 0;

                for (i = 0; i < nodes.length; i++) {
                    var n = nodes[i];
                    var nodeText = n.get_text().toString();
                    if (nodeText != "(Select All)") {
                        if (n._hasChildren() == true) {
                        } else {
                            vals = vals + n.get_text().toString() + ",";
                        }
                    }
                }
             
                comboBox.set_text(marketsval);
            }
    }	
    function nodeCheckedTreeMarkets(sender, args) {

        var comboBox = $find("<%= CmbTreeMarkets.ClientID %>");



        //check if 'Select All' node has been checked/unchecked
        var tempNode = args.get_node();
        if (tempNode.get_text().toString() == "(Select All)") {
            // check or uncheck all the nodes
        } else {
            var nodes = new Array();
            nodes = sender.get_checkedNodes();
            var vals = "";
            var i = 0;
            var icount = 0;

            for (i = 0; i < nodes.length; i++) {
                var n = nodes[i];
                var nodeText = n.get_text().toString();
                
                if (nodeText != "(Select All)") {
                    if (n._hasChildren() == true) {
//                        icount = icount + 1;
//                        alert(icount);
                    } else {
                        vals = vals + n.get_text().toString() + ",";
                    }
                }
            }

            //prevent  combo from closing
          //  supressDropDownClosing = true;
            comboBox.set_text(vals);

            getrootcount(sender, args);
        }
    }	
    function nodeCheckedTreeMarketsForSelectedNetwork(sender, args) {

        var comboBox = $find("<%= CmbTreeMarketsForSelectedNetwork.ClientID %>");



        //check if 'Select All' node has been checked/unchecked
        var tempNode = args.get_node();
        if (tempNode.get_text().toString() == "(Select All)") {
            // check or uncheck all the nodes
        } else {
            var nodes = new Array();
            nodes = sender.get_checkedNodes();
            var vals = "";
            var i = 0;
            var icount = 0;

            for (i = 0; i < nodes.length; i++) {
                var n = nodes[i];
                var nodeText = n.get_text().toString();
                
                if (nodeText != "(Select All)") {
                    if (n._hasChildren() == true) {
//                        icount = icount + 1;
//                        alert(icount);
                    } else {
                        vals = vals + n.get_text().toString() + ",";
                    }
                }
            }

            //prevent  combo from closing
          //  supressDropDownClosing = true;
            comboBox.set_text(vals);

            getrootcount(sender, args);
        }
    }				
    function getrootcount(sender, args) {
    }	
    function nodeClickingTreeStations(sender, args) {
        var comboBox = $find("<%= CmbTreeStations.ClientID %>");

        var node = args.get_node();

        if (node.get_parent() == node.get_treeView()) {
            alert("Please select a station. Market selection not allowed.");

        }
        else {
            comboBox.set_text(node.get_text());
            comboBox.get_items().getItem(0).set_value(node.get_value());
        }
        
    } 	
    function nodeClickingTreeMarkets(sender, args) {
        var comboBox = $find("<%= CmbTreeMarkets.ClientID %>");

        var node = args.get_node();

        if (node.get_parent() == node.get_treeView()) {
            alert("Please select a station. Market selection not allowed.");

        }
        else {
            comboBox.set_text(node.get_text());
            comboBox.get_items().getItem(0).set_value(node.get_value());
        }
        
    } 	
    function nodeClickingTreeMarketsForSelectedNetwork(sender, args) {
        var comboBox = $find("<%= CmbTreeMarketsForSelectedNetwork.ClientID %>");

        var node = args.get_node();

        if (node.get_parent() == node.get_treeView()) {
            alert("Please select a station. Market selection not allowed.");

        }
        else {
            comboBox.set_text(node.get_text());
            comboBox.get_items().getItem(0).set_value(node.get_value());
        }
        
    }
    function OnClientDropDownOpenedHandler1(sender, eventArgs) {

        var tree = sender.get_items().getItem(0).findControl("radTreeStations");
        //var tree1 = sender.get_items().getItem(0).findControl("radStationCompare");
        
        if(!!tree)
        {
            var selectedNode = tree.get_selectedNode();
            if (selectedNode) {
                selectedNode.scrollIntoView();
            }   
        }
        
//        if(!!tree1)
//        {
//            var selectedNode1 = tree1.get_selectedNode();
//            if (selectedNode1) {
//                selectedNode1.scrollIntoView();
//            }
//        }
    }
    function OnClientDropDownOpenedHandler2(sender, eventArgs) {

        var tree = sender.get_items().getItem(0).findControl("radTreeMarkets");
        //var tree1 = sender.get_items().getItem(0).findControl("radStationCompare");
        
        if(!!tree)
        {
            var selectedNode = tree.get_selectedNode();
            if (selectedNode) {
                selectedNode.scrollIntoView();
            }   
        }
        
//        if(!!tree1)
//        {
//            var selectedNode1 = tree1.get_selectedNode();
//            if (selectedNode1) {
//                selectedNode1.scrollIntoView();
//            }
//        }
    }
    function OnClientDropDownOpenedHandler3(sender, eventArgs) {

        var tree = sender.get_items().getItem(0).findControl("nodeCheckedTreeMarketsForSelectedNetwork");
        //var tree1 = sender.get_items().getItem(0).findControl("radStationCompare");
        
        if(!!tree)
        {
            var selectedNode = tree.get_selectedNode();
            if (selectedNode) {
                selectedNode.scrollIntoView();
            }   
        }
        
//        if(!!tree1)
//        {
//            var selectedNode1 = tree1.get_selectedNode();
//            if (selectedNode1) {
//                selectedNode1.scrollIntoView();
//            }
//        }
    }
    
    </script>

    <div style="width: 99.7%; height: 100%; margin-left: 4px">
        <div>
            <table width="100%" background="images/contentheader_bgr.gif" border="0" cellpadding="6px"
                cellspacing="0px" style="margin-left: -5px; margin-right: 300px;">
                <tr>
                    <td align="left">
                        ::
                        <asp:Label ID="Pg_Title" runat="server" Font-Bold="True" ForeColor="#000099"></asp:Label>&nbsp;::
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        <asp:Label ID="lbl_Logout" runat="server" Text="Welcome: Administrator[Logout]" Font-Bold="True"
                            ForeColor="#000099"></asp:Label>
                    </td>
                </tr>
            </table>
            <table border="0">
                <tr>
                    <td class="style1">
                        Start Date:
                    </td>
                    <td class="style2">
                        End Date:
                    </td>
                    <td class="style3">
                        Select By :
                    </td>
                    <td id="tdStext">
                        Select Station:
                    </td>
                    <td id="tdMtext">
                        Select Market:
                    </td>
                    <td id="tdNtext">
                        Select NetWork:
                    </td>
                    <td id="tdNMtext">
                        Select Market:
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr valign="top">
                    <td class="style1">
                        <telerik:RadDatePicker ID="dtStart" runat="server" Culture="English (United States)"
                            MinDate="2004-01-01">
                            <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                            </Calendar>
                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                            <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                            </DateInput>
                        </telerik:RadDatePicker>
                    </td>
                    <td class="style1">
                        <telerik:RadDatePicker ID="dtEnd" runat="server" Culture="English (United States)"
                            MinDate="2004-01-01">
                            <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                            </Calendar>
                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                            <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                            </DateInput>
                        </telerik:RadDatePicker>
                    </td>
                    <td class="style1">
                        <telerik:RadComboBox ID="ddSelectBy" runat="server" EmptyMessage="Choose One" OnClientSelectedIndexChanged="OnddSelectByIndexChanged"
                            Width="80px">
                            <Items>
                                <telerik:RadComboBoxItem Text="" Value="" />
                                <telerik:RadComboBoxItem Text="Station" Value="Station" />
                                <telerik:RadComboBoxItem Text="Market" Value="Market" />
                                <telerik:RadComboBoxItem Text="Network" Value="Network" />
                            </Items>
                        </telerik:RadComboBox>
                        <asp:DropDownList ID="ddCity" runat="server" Visible="false">
                        </asp:DropDownList>
                    </td>
                    <td id="tdS">
                        <telerik:RadComboBox ID="CmbTreeStations" runat="server" AfterClientCheck="AfterCheckHandler"
                            CollapseAnimation-Type="None" EmptyMessage="Choose a destination" ExpandAnimation-Type="None"
                            OnClientDropDownOpened="OnClientDropDownOpenedHandler1" ShowToggleImage="True"
                            Style="vertical-align: middle;" Width="200px">
                            <ItemTemplate>
                                <div id="div2">
                                    <telerik:RadTreeView ID="radTreeStations" runat="server" Height="200px" OnClientNodeChecked="nodeCheckedTreeStations"
                                        OnClientNodeClicked="nodeClickingTreeStations" CheckBoxes="true" Width="100%"
                                        TriStateCheckBoxes="true">
                                    </telerik:RadTreeView>
                                </div>
                            </ItemTemplate>
                            <Items>
                                <telerik:RadComboBoxItem Text="" />
                            </Items>
                        </telerik:RadComboBox>
                    </td>
                    <td id="tdM">
                        <telerik:RadComboBox ID="CmbTreeMarkets" runat="server" AfterClientCheck="AfterCheckHandler"
                            CollapseAnimation-Type="None" EmptyMessage="Choose a destination" ExpandAnimation-Type="None"
                            OnClientDropDownOpened="OnClientDropDownOpenedHandler2" ShowToggleImage="True"
                            Style="vertical-align: middle;" Width="200px">
                            <ItemTemplate>
                                <div id="div2">
                                    <telerik:RadTreeView ID="radTreeMarkets" runat="server" Height="200px" OnClientNodeChecked="nodeCheckedTreeMarkets"
                                        OnClientNodeClicked="nodeClickingTreeMarkets" CheckBoxes="true" Width="100%"
                                        TriStateCheckBoxes="true">
                                    </telerik:RadTreeView>
                                </div>
                            </ItemTemplate>
                            <Items>
                                <telerik:RadComboBoxItem Text="" />
                            </Items>
                        </telerik:RadComboBox>
                    </td>
                    <td id="tdN" valign="top">
                        <telerik:RadComboBox ID="ddNetworks" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddNetworks_SelectedIndexChanged"
                            Width="100px">
                        </telerik:RadComboBox>
                    </td>
                    <td id="tdNM" style="margin-left: 40px">
                        <telerik:RadComboBox ID="CmbTreeMarketsForSelectedNetwork" runat="server" AfterClientCheck="AfterCheckHandler"
                            CollapseAnimation-Type="None" EmptyMessage="Choose a destination" ExpandAnimation-Type="None"
                            OnClientDropDownOpened="OnClientDropDownOpenedHandler3" ShowToggleImage="True"
                            EnableLoadOnDemand="True" Style="vertical-align: middle;" Width="200px">
                            <ItemTemplate>
                                <div id="div2">
                                    <telerik:RadTreeView ID="radTreeMarketsForSelectedNetwork" runat="server" Height="200px"
                                        OnClientNodeChecked="nodeCheckedTreeMarketsForSelectedNetwork" OnClientNodeClicked="nodeClickingTreeMarketsForSelectedNetwork"
                                        CheckBoxes="true" Width="100%" TriStateCheckBoxes="true">
                                    </telerik:RadTreeView>
                                </div>
                            </ItemTemplate>
                            <Items>
                                <telerik:RadComboBoxItem Text="" />
                            </Items>
                        </telerik:RadComboBox>
                    </td>
                    <td>
                        Select Top:<asp:TextBox ID="txt_top" runat="server" Width="40px" Text="30"></asp:TextBox>
                    </td>
                    <td>
                        Movie/Album
                    </td>
                    <td class="myCustomCls">
                        <telerik:RadButton ID="btnGenerateReport" runat="server" Text="Generate Report" OnClientClicked="OnClientClicked"
                            OnClick="btnGenerateReport_Click">
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="left" style="width: 100%">
                        <div id="divWait" style="display: none">
                            <table style="height: 310Px" width="100%">
                                <tr>
                                    <td align="center" width="100%">
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <img src="images/aniClock.gif" border="0" name="ProgressBarImg" alt=""><br />
                                        <font class="date">Acquiring Data... one moment please!</font>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label>
                        <div id="DIV1" style="width: 100%">
                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="400px" Width="100%">
                            <DBWC:HierarGrid ID="dgHierarchical" runat="server" TemplateDataMode="Table" LoadControlMode="Template"
                                TemplateCachingBase="Tablename" ShowHeader="True" AutoGenerateColumns="True"
                                EnableViewState="false" Width="100%" OnTemplateSelection="dgHierarchical_TemplateSelection"
                                OnItemDataBound="dgHierarchical_ItemDataBound" Visible="false" AllowSorting="false"
                                OnSortCommand="dgHierarchical_SortCommand" CssClass="dgHierarchical">
                                <HeaderStyle HorizontalAlign="Center" />
                            </DBWC:HierarGrid>
                        </asp:Panel>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <asp:Literal ID="litExportTo" Visible="false" runat="server" Text="Export to: "></asp:Literal>
                        <telerik:RadButton ID="btnExportToExcel" runat="server" Visible="false" CausesValidation="false"
                            Text="Excel" OnClick="img_btn_export_to_excel_Click">
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnddSelectByVal" runat="server" />
        </div>
    </div>

    <script language="javascript" type="text/javascript">
        var valSelectBy = document.getElementById("ctl00_ContentPlaceHolder2_hdnddSelectByVal").value;

        if (valSelectBy == "Station") {
            document.getElementById("tdStext").style.display = "block";
            document.getElementById("tdMtext").style.display = "none";
            document.getElementById("tdNtext").style.display = "none";
            document.getElementById("tdNMtext").style.display = "none";

            document.getElementById("tdS").style.display = "block";
            document.getElementById("tdM").style.display = "none";
            document.getElementById("tdN").style.display = "none";
            document.getElementById("tdNM").style.display = "none";
        }
        else if (valSelectBy == "Market") {
            document.getElementById("tdStext").style.display = "none";
            document.getElementById("tdMtext").style.display = "block";
            document.getElementById("tdNtext").style.display = "none";
            document.getElementById("tdNMtext").style.display = "none";

            document.getElementById("tdS").style.display = "none";
            document.getElementById("tdM").style.display = "block";
            document.getElementById("tdN").style.display = "none";
            document.getElementById("tdNM").style.display = "none";
        }
        else if (valSelectBy == "Network") {
            document.getElementById("tdStext").style.display = "none";
            document.getElementById("tdMtext").style.display = "none";
            document.getElementById("tdNtext").style.display = "block";
            document.getElementById("tdNMtext").style.display = "block";

            document.getElementById("tdS").style.display = "none";
            document.getElementById("tdM").style.display = "none";
            document.getElementById("tdN").style.display = "block";
            document.getElementById("tdNM").style.display = "block";
        }
        else if (valSelectBy == "" || valSelectBy == null) {
            document.getElementById("tdStext").style.display = "none";
            document.getElementById("tdMtext").style.display = "none";
            document.getElementById("tdNtext").style.display = "none";
            document.getElementById("tdNMtext").style.display = "none";

            document.getElementById("tdS").style.display = "none";
            document.getElementById("tdM").style.display = "none";
            document.getElementById("tdN").style.display = "none";
            document.getElementById("tdNM").style.display = "none";
        }         
    </script>

</asp:Content>
