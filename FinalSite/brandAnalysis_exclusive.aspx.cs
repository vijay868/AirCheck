﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class brandAnalysis_exclusive : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        DateTime dtStartDate = DateTime.Parse(Request.QueryString["startDate"]);
        DateTime dtEndDate = DateTime.Parse(Request.QueryString["endDate"]);

        string strStartDate = getFormattedDate(dtStartDate);
        string strEndDate = getFormattedDate(dtEndDate);


        label_Date_Range.Text = strStartDate + " to " + strEndDate;

        string strStationName, strMarketName;
        int station_id;
        get_station_and_market(Request.QueryString["strStationName"].Replace("(Played)", ""), out station_id, out strStationName, out strMarketName);

        Session["strStationNameBrandAnalysisExclusive"] = strStationName;

        if (Session["dataReadOnly"].ToString() == "True")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "HideOutIcons", "HideIcons();", true);
        }

        label_station.Text = strStationName;
        label_market.Text = strMarketName;
        if (!IsPostBack)
        {
            bindGrid(strStationName, "Account");
            if (Session["dataReadOnly"].ToString() == "True")
            {
                btnExportToExcel.Enabled = false;

            }
            else
            {
                btnExportToExcel.Enabled = true;

            }
        }
    }

    protected string getFormattedDate(DateTime dt)
    {
        string strDate = "";
        string strDateFormat = (string)Session["dateFormat"];
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dt.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dt.ToString("MM/dd/yyyy");
        }
        return strDate;
    }

    protected void bindGrid(string strStationName, string sortColumn)
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("BrandAnalysisExclusive", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@stationIDs", Request.QueryString["strStationIDs"]);
        da.SelectCommand.Parameters.AddWithValue("@startDate", Request.QueryString["startDate"]);
        da.SelectCommand.Parameters.AddWithValue("@endDate", Request.QueryString["endDate"]);
        da.SelectCommand.Parameters.AddWithValue("@searchString", Session["searchString"].ToString());
        da.SelectCommand.Parameters.AddWithValue("@stationName", Request.QueryString["strStationName"]);
        da.SelectCommand.Parameters.AddWithValue("@sortColumn", sortColumn);

        DataSet ds = new DataSet();
        da.Fill(ds);

        Session["dtBrandAnalysisExclusive"] = ds.Tables[0];

        datagrid_disp_details.DataSource = ds;
        datagrid_disp_details.DataBind();
    }

    protected void get_station_and_market(string strStationShort, out int stationID, out string strStationName, out string strMarketName)
    {
        stationID = 0;
        strStationName = "";
        strMarketName = "";

        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("get_station_and_market_for_full_station_name", conn);
        comm.CommandType = CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@stationName", strStationShort);
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                stationID = int.Parse(dr["station_id"].ToString());
                strStationName = dr["Station"].ToString();
                strMarketName = dr["Market"].ToString();
            }
        }

        conn.Close();
    }

    protected void datagrid_disp_details_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "12px";

        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Cells[0].HorizontalAlign = HorizontalAlign.Left;
            e.Item.Cells[1].HorizontalAlign = HorizontalAlign.Left;
            e.Item.Cells[2].HorizontalAlign = HorizontalAlign.Left;
            e.Item.Cells[3].HorizontalAlign = HorizontalAlign.Center;



            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }
        }


    }

    protected void datagrid_disp_details_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        bindGrid(Session["strStationNameBrandAnalysisExclusive"].ToString(), "[" + e.SortExpression + "]");
    }

    protected void img_btn_export_to_excel_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    public void ExportToExcel()
    {
        DataTable dt = (DataTable)Session["dtBrandAnalysisExclusive"];

        ExportToExcel(dt, Response);
    }

    public void ExportToExcel(DataTable dt, HttpResponse response)
    {

        //first let's clean up the response.object
        response.Clear();
        response.Charset = "";
        //set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel";
        //create a string writer
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //create an htmltextwriter which uses the stringwriter
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //instantiate a datagrid
        DataGrid dg = new DataGrid();
        //set the datagrid datasource to the dataset passed in
        if (dt == null)
        {
            return;
        }
        else if (dt.Rows.Count == 0)
        {
            return;
        }
        dg.DataSource = dt;
        //bind the datagrid
        dg.DataBind();

        dg.RenderControl(htmlWrite);


        String str = stringWrite.ToString();





        response.Write(str);
        response.End();
    }
}
