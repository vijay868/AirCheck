﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;

public partial class topAdvertisersDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["TotalPlays"] = null;
            Session["TotalSeconds"] = null;

            if (Session["dataReadOnly"].ToString() == "True")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "HideOutIcons", "HideIcons();", true);

            }

            bindGrid();
        }
        loadHeader();
    }

    protected void loadHeader()
    {
        DateTime dtStartDate = DateTime.Parse(Session["strStartDate"].ToString());
        DateTime dtEndDate = DateTime.Parse(Session["strEndDate"].ToString());
        string strStartDate = getFormattedDate(dtStartDate);
        string strEndDate = getFormattedDate(dtEndDate);

        label_Date_Range.Text = strStartDate + " to " + strEndDate;

        lblBrand.Text = undoStringSanitation(Request.QueryString["strBrandName"]);
        lblAdvertiser.Text = undoStringSanitation(Request.QueryString["strParentName"]);
        lblCategory.Text = undoStringSanitation(Request.QueryString["strCategory"]);

    }


    protected string undoStringSanitation(string str)
    {
        if (str.IndexOf("123123") >= 0)
        {
            str = str.Replace("123123", "'");
        }
        if (str.IndexOf("456456") >= 0)
        {
            str = str.Replace("456456", "&");
        }

        return str;
    }

    protected string getFormattedDate(DateTime dt)
    {
        string strDate = "";
        strDate = dt.ToString("dd/MM/yyyy");
        return strDate;
    }

    protected void bindGrid()
    {
        string strStationIDs = Session["strStationIDs"].ToString();




        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("getMarketNamesOfMultipleStations", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@stationIDs", strStationIDs);

        DataSet ds = new DataSet();
        da.Fill(ds);

        int i = 1;
        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                bindGridForAMarket(i, dr[0].ToString(), strStationIDs);
                i++;
            }

            int intTotalPlays = int.Parse(Session["TotalPlays"].ToString());
            int intTotalSeconds = int.Parse(Session["TotalSeconds"].ToString());
            int intDuration = intTotalSeconds / intTotalPlays;
            lblTotalNoOfPlays.Text = intTotalPlays.ToString();
            lblTotalNoOfSeconds.Text = intTotalSeconds.ToString();
            lblDuration.Text = intDuration.ToString("N0") + " Second(s)";
        }
        else
        {

        }

    }

    protected void bindGridForAMarket(int i, string strMarketName, string strStationIDs)
    {
        DateTime StartDate = DateTime.Parse(Session["strStartDate"].ToString());
        DateTime EndDate = DateTime.Parse(Session["strEndDate"].ToString());

        string strPanel = "Panel" + i.ToString();
        Panel pPanel = (Panel)Page.FindControl(strPanel);
        if (pPanel == null)
        {
            return;
        }
        pPanel.Visible = true;

        string strLabel = "label_station" + i.ToString();
        Label label_station = (Label)Page.FindControl(strLabel);
        label_station.Text = strMarketName;

        string strLabelNoStation = "label_no_station" + i.ToString();
        Label label_no_station = (Label)Page.FindControl(strLabelNoStation);

        string strGrid = "datagrid_station" + i.ToString();
        DataGrid datagrid_station = (DataGrid)Page.FindControl(strGrid);


        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("topAdvertisersDetails", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@marketName", strMarketName);
        da.SelectCommand.Parameters.AddWithValue("@brandName", undoStringSanitation(Request.QueryString["strBrandName"]).Replace("'", "''"));
        da.SelectCommand.Parameters.AddWithValue("@parentName", undoStringSanitation(Request.QueryString["strParentName"]).Replace("'", "''"));
        da.SelectCommand.Parameters.AddWithValue("@Category", undoStringSanitation(Request.QueryString["strCategory"]).Replace("'", "''"));
        da.SelectCommand.Parameters.AddWithValue("@startDate", StartDate);
        da.SelectCommand.Parameters.AddWithValue("@endDate", EndDate);
        da.SelectCommand.Parameters.AddWithValue("@strStationIDs", strStationIDs);

        DataSet ds = new DataSet();
        da.Fill(ds);

        label_station.Text = strMarketName;

        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                label_no_station.Visible = false;
                datagrid_station.Visible = true;

                Session["dtTopAdvertisersDetails"] = ds.Tables[0];

                datagrid_station.DataSource = ds.Tables[0];
                datagrid_station.DataBind();



                if (Session["TotalPlays"] != null)
                {
                    if (Session["TotalPlays"].ToString() != "")
                    {
                        int intTotalPlays = int.Parse(Session["TotalPlays"].ToString());
                        int intTotalSeconds = int.Parse(Session["TotalSeconds"].ToString());

                        Session["TotalPlays"] = intTotalPlays + int.Parse(ds.Tables[1].Rows[0][0].ToString());
                        Session["TotalSeconds"] = intTotalSeconds + int.Parse(ds.Tables[1].Rows[0][1].ToString());
                    }
                    else
                    {
                        Session["TotalPlays"] = ds.Tables[1].Rows[0][0].ToString();
                        Session["TotalSeconds"] = ds.Tables[1].Rows[0][1].ToString();
                    }
                }
                else
                {
                    Session["TotalPlays"] = ds.Tables[1].Rows[0][0].ToString();
                    Session["TotalSeconds"] = ds.Tables[1].Rows[0][1].ToString();
                }
            }
            else
            {
                label_no_station.Visible = true;
                datagrid_station.Visible = false;
            }
        }
        else
        {
            label_no_station.Visible = true;
            datagrid_station.Visible = false;
        }
    }


    protected void datagrid_station1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();

        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }


            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);



            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }


        }

    }
    protected void datagrid_station2_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();

        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }

    }
    protected void datagrid_station3_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }

    }
    protected void datagrid_station4_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }

    }
    protected void datagrid_station5_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }

    }
    protected void datagrid_station6_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }

    }
    protected void datagrid_station7_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }
    }
    protected void datagrid_station8_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }

    }
    protected void datagrid_station9_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }

    }
    protected void datagrid_station10_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }

    }
    protected void datagrid_station11_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }

    }
    protected void datagrid_station12_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }

    }

    protected void datagrid_station13_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }

    }

    protected void datagrid_station14_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }
    }

    protected void datagrid_station15_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }
    }

    protected void datagrid_station16_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }
    }

    protected void datagrid_station17_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }
    }

    protected void datagrid_station18_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "14px";
        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();


        DataTable dt = (DataTable)Session["dtTopAdvertisersDetails"];

        if (e.Item.ItemType == ListItemType.Header)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("white-space", "nowrap");
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }

            int indexOfBlank = e.Item.Cells[0].Text.LastIndexOf(" ");
            e.Item.Cells[0].Text = e.Item.Cells[0].Text.Substring(0, indexOfBlank);

            foreach (DataColumn dc in dt.Columns)
            {
                if (e.Item.ItemIndex == 0)
                {
                    e.Item.Cells[3].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                    e.Item.Cells[4].Attributes.Add("rowspan", dt.Rows.Count.ToString());
                }
                else
                {
                    try
                    {
                        e.Item.Cells.Remove(e.Item.Cells[3]);
                        e.Item.Cells.Remove(e.Item.Cells[4]);
                    }
                    catch { }
                }
            }
        }

    }
}
