﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SpotPlacement.aspx.cs" Inherits="SpotPlacement" Title="Spot Placement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<style type="text/css">
        .gvHeaderRow
        {
            background-image: url("images/dhrudde.png");
            font-family: Helvetica;
            font-size: 12px;
            color: White;
            text-decoration: none;
            line-height: 12px;
            padding-left: 10px;
            border-left: solid 1px white;
            border-right: solid 1px white;
            border-bottom: solid 1px white;
            position: relative;
        }
        .rowheightcls td
        {
            line-height: 11px;
        }
        .anchCls
        {
        	float:left;
        }
    </style>

    <script type="text/javascript" language="javascript">
function ToggleCollapsePane() {               
               var splitter = $find("<%= Page.Master.FindControl("RadSplitter1").ClientID %>");
               var pane = splitter.getPaneById("<%= Page.Master.FindControl("RadPane1").ClientID %>");
                pane.collapse();
    }
    function disp_details(breakStartTime, position) 
    {
        win = window.open("SpotPlacementDetails.aspx?position="+ position +"&breakStartTime=" + breakStartTime, "win", "width=800,height=400,left=0, scrollbars=yes, resizable=yes")
        return false
    }
    function OnClientDropDownOpenedHandler(sender, eventArgs) {
        var tree = sender.get_items().getItem(0).findControl("radStationSelect");       

        if (!!tree) {
            var selectedNode = tree.get_selectedNode();
            if (selectedNode) {
                selectedNode.scrollIntoView();
            }
        }       
    }
    function nodeCheckedTreeStations(sender, args) {

        var comboBox = $find("<%= txtStationSelect.ClientID %>");        
        var tempNode = args.get_node();
        if (tempNode.get_text().toString() == "(Select All)") {
            // check or uncheck all the nodes
        } else {
            var nodes = new Array();
            nodes = sender.get_checkedNodes();
            var vals = "";
            var i = 0;
            var icount = 0;

            for (i = 0; i < nodes.length; i++) {
                var n = nodes[i];
                var nodeText = n.get_text().toString();

                if (nodeText != "(Select All)") {
                    if (n._hasChildren() == true) {
                        //    icount = icount + 1;
                        //    alert(icount);
                    } else {
                        vals = vals + n.get_text().toString() + ",";
                    }
                }
            }            
            comboBox.set_text(vals);

            getrootcount(sender, args);
        }
    }
    function nodeClickingTreeStations(sender, args) {
        var comboBox = $find("<%= txtStationSelect.ClientID %>");

        var node = args.get_node();

        if (node.get_parent() == node.get_treeView()) {
            alert("Please select a station. Market selection not allowed.");

        }
        else {
            comboBox.set_text(node.get_text());
            comboBox.get_items().getItem(0).set_value(node.get_value());
        }

    }
    function OnClientClicked(button, args) {
        if (calcDays()) {
            //               var divobj = document.getElementById("ShowGirdContainerButtons")
            //             divobj.style.display = "block";
            button.set_autoPostBack(true);
        }
        else {
            button.set_autoPostBack(false);
        }
    }
    function days_between(date1, date2) {
            
            var ONE_DAY = 1000 * 60 * 60 * 24
            
            var date1_ms = date1.getTime()
            var date2_ms = date2.getTime()
            
            var difference_ms = Math.abs(date1_ms - date2_ms)            
            
            return Math.round(difference_ms/ONE_DAY)

        }
    function calcDays()
        {
            ToggleCollapsePane();
            var startDate =$find("<%=dtStart.ClientID %>");
            var endDate =  $find("<%=dtEnd.ClientID %>");
            
            var sDate = startDate.get_selectedDate();
            var eDate = endDate.get_selectedDate();
            
             var lockDataBefore="<%=Session["lockDataBefore"]%>";
		    var lockDataAfter="<%=Session["lockDataAfter"]%>";
            if(sDate == null)
            {
                lockDataBefore = new Date(lockDataBefore);
                var curr_date = lockDataBefore.getDate();
                var curr_month = lockDataBefore.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataBefore.getFullYear();
                lockDataBefore= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights only from " + lockDataBefore);
                return false;
            }
            
            if(eDate == null)
            {
                lockDataAfter = new Date(lockDataAfter);
                var curr_date = lockDataAfter.getDate();
                var curr_month = lockDataAfter.getMonth();
                curr_month = curr_month + 1;
                var curr_year = lockDataAfter.getFullYear();
                lockDataAfter= curr_date + '/'+ curr_month + '/'+ curr_year;
                alert("Data Access Rights till " + lockDataAfter);
                return false;
            }
        
            if(days_between(sDate,eDate) > 30)
            {
                alert("Maximum allowed date range is 1 month.");
                return false;
            }       
            

          

            var isStationSelected = 0;
            var isMarketSelected = 0;
            var isNetworkMarketSelected = 0;            
            
           // var treeS = sender.get_items().getItem(0).findControl("radTreeStations");
            
            var comboBox = $find("<%= txtStationSelect.ClientID %>");    
            var treeS = comboBox.get_items().getItem(0).findControl("radStationSelect");
            
            var rootNodesS = treeS.SelectedNode;
            var nodesS = treeS.get_allNodes();

            var iS;
            
             for(iS=0; iS<nodesS.length; iS++)
             {
                if(!nodesS[iS].get_checked())
                {
                
                }
                else
                {
                    isStationSelected = 1;
                }
             }
             
           // tdhide('leftMenu', 'ctl00_showHide','hide');
                        
            disp_clock();
            return true;                
        }  
        function disp_clock()
		{
		    var lblError = document.getElementById("ctl00_content1_lblError");
		    if (lblError != null)
		    {
		        lblError.style.display='none';
		    }
			OnLoad();
			DIV1.style.display='none';
			divWait.style.display='';
    	}
    	function getrootcount(sender, args) {
        }
        function OnLoad() 
		{ 
		    setTimeout("StartAnimation()", 500); 
		} 

		function StartAnimation() 
		{ 
		    if (document.images)
		    {
		        document['ProgressBarImg'].src = "images/aniClock.gif";
		    }
		}
		function nodeClicking(sender, args) {
        var comboBox = $find("<%= txtStationSelect.ClientID %>");

        var node = args.get_node();

        if (node.get_parent() == node.get_treeView()) {
            alert("Please select a station. Market selection not allowed.");
//            comboBox.trackChanges();
//            comboBox.set_text("");
//            comboBox.commitChanges();
            comboBox.hideDropDown();
        }
        else {
            //comboBox.trackChanges();
            comboBox.set_text(node.get_text());
            comboBox.get_items().getItem(0).set_value(node.get_value());
//            comboBox.commitChanges();

            comboBox.hideDropDown();
        }
    } 
    
    function nodeClicked(sender, args) {
        var node = args.get_node();
        if (node.get_checked()) {
            node.uncheck();
        } else {
            node.check();
        }
        nodeChecked(sender, args)

    }

    function nodeChecked(sender, args) {
   
        var comboBox = $find("<%= txtStationSelect.ClientID %>");

        //check if 'Select All' node has been checked/unchecked
        var tempNode = args.get_node();
        if (tempNode.get_text().toString() == "(Select All)") {
            // check or uncheck all the nodes
        } else {
            var nodes = new Array();
            nodes = sender.get_checkedNodes();
            var vals = "";
            var i = 0;

            for (i = 0; i < nodes.length; i++) {
                var n = nodes[i];
                var nodeText = n.get_text().toString();
                if (nodeText != "(Select All)") {
                    vals = vals + n.get_text().toString() + ",";
                }
            }

            //prevent  combo from closing
            supressDropDownClosing = true;
            comboBox.set_text(vals);
        }
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">  
<div style="width: 99.7%; height: 100%; margin-left: 4px">
        <div>
            <table width="100%" background="images/contentheader_bgr.gif" border="0" cellpadding="6px"
                cellspacing="0px" style="margin-left: -5px; margin-right: 300px;">
                <tr>
                    <td align="left">
                        ::
                        <asp:Label ID="Pg_Title" runat="server" Font-Bold="True" ForeColor="#000099"></asp:Label>&nbsp;::
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        <asp:Label ID="lbl_Logout" runat="server" Text="Welcome: Administrator[Logout]" Font-Bold="True"
                            ForeColor="#000099"></asp:Label>
                    </td>
                </tr>
            </table>
            <table border="0">
                <tr>
                    <td class="style1">
                        Start Date:
                    </td>
                    <td class="style1">
                        End Date:
                    </td>
                    <td class="style1">
                        Select Station:
                    </td>
                    <td class="style1">
                        Search
                    </td>
                    <td class="style1">
                        &nbsp;
                    </td>
                </tr>
                <tr valign="top">
                    <td class="style1">
                        &nbsp;<telerik:RadDatePicker ID="dtStart" runat="server" Culture="English (United States)"
                            MinDate="2004-01-01">
                            <calendar usecolumnheadersasselectors="False" userowheadersasselectors="False" viewselectortext="x">
                        </calendar>
                            <datepopupbutton hoverimageurl="" imageurl="" />
                            <dateinput dateformat="dd/MM/yyyy" displaydateformat="dd/MM/yyyy">
                        </dateinput>
                        </telerik:RadDatePicker>
                    </td>
                    <td class="style1">
                        <telerik:RadDatePicker ID="dtEnd" runat="server" Culture="English (United States)"
                            MinDate="2004-01-01">
                            <calendar usecolumnheadersasselectors="False" userowheadersasselectors="False" viewselectortext="x">
                        </calendar>
                            <datepopupbutton hoverimageurl="" imageurl="" />
                            <dateinput dateformat="dd/MM/yyyy" displaydateformat="dd/MM/yyyy">
                        </dateinput>
                        </telerik:RadDatePicker>
                        <asp:CompareValidator Display="Dynamic" ID="dateCompareValidator" runat="server"
                            ControlToValidate="dtEnd" ControlToCompare="dtStart" Operator="GreaterThanEqual"
                            Type="Date" ErrorMessage="The End date must be after the Start date." Style="font-size: x-small"></asp:CompareValidator>
                    </td>
                    <td class="style1">
                        <telerik:RadComboBox ID="txtStationSelect" runat="server" AfterClientCheck="AfterCheckHandler"
                            CollapseAnimation-Type="None" EmptyMessage="Choose a destination" ExpandAnimation-Type="None"
                            OnClientDropDownOpened="OnClientDropDownOpenedHandler" ShowToggleImage="True"
                            Style="vertical-align: middle;" Width="200px">
                            <itemtemplate>
                                <div id="div2">
                                    <telerik:RadTreeView ID="radStationSelect" runat="server" Height="200px" OnClientNodeChecked="nodeClicking"
                                        OnClientNodeClicked="nodeClicking">
                                    </telerik:RadTreeView>
                                </div>
                            </itemtemplate>
                            <items>
                                <telerik:RadComboBoxItem Text="" />
                            </items>
                        </telerik:RadComboBox>
                    </td>
                    <td class="style1">                        
                            <telerik:RadTextBox ID="txtSearchText" runat="server" Style="float: left;">
                            </telerik:RadTextBox>
                    </td>
                    <td class="style1">
                        <telerik:RadButton ID="btnGenerateReport" runat="server" Text="Generate Report" OnClientClicked="OnClientClicked"
                            OnClick="btnGenerateReport_Click">
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>
            <br />
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="left" style="width: 100%">
                        <div id="divWait" style="display: none">
                            <table style="height: 310Px" width="100%">
                                <tr>
                                    <td align="center" width="100%">
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <img src="images/aniClock.gif" border="0" name="ProgressBarImg"><br>
                                        <font class="date">Acquiring Data... one moment please!</font>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="DIV1" style="width: 1140Px; height: 100%;">
                            <asp:Label ID="lblError" runat="server" ForeColor="#FA9000" Font-Bold="true" Font-Size="15Px"></asp:Label>
                            <asp:Panel Width="100%" Visible="false" ID="pnlSearchedCriteria" runat="server">
                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Panel ID="pnlText" runat="server">
                                                <b style="font-size: 12Px">Generate a report by clicking on the <u style="font-size: 12Px">
                                                    Account title</u> or the <u style="font-size: 12Px">Parent title</u> for a detailed
                                                    report.</b></asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblNoSearchFound" runat="server" ForeColor="#FA9000" Font-Bold="true" 
                                                Font-Size="15Px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">                                           
                                           
                                            <telerik:RadGrid ID="gvSearch" runat="server" AutoGenerateColumns="False" OnItemDataBound="gvSearch_RowDataBound"
                                                OnItemCommand="gvSearch_RowCommand">                                                
                                                <mastertableview>
                                                    <Columns>
                                                        <telerik:GridTemplateColumn>
                                                            <ItemTemplate>                                                           
                                                                <asp:LinkButton ID="lnlAccount" ForeColor="Black" runat="server" CommandName="accountName"
                                                                    CommandArgument='<%# ((System.Data.DataRowView)Container.DataItem)["AccountName"]%>'
                                                                    Text='<%# ((System.Data.DataRowView)Container.DataItem)["AccountName"]%>' CssClass="anchCls"></asp:LinkButton>                                                            
                                                            </ItemTemplate>
                                                             <HeaderTemplate>
                                                                <label id="hd1" class="anchCls" style="font-family:Helvetica;font-size:14px;">Account Name</label>
                                                            </HeaderTemplate>  
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnlParent" ForeColor="Black" runat="server" CommandName="parentName"
                                                                    CommandArgument='<%# ((System.Data.DataRowView)Container.DataItem)["ParentName"]%>'
                                                                    Text='<%# ((System.Data.DataRowView)Container.DataItem)["ParentName"]%>' CssClass="anchCls"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <HeaderTemplate>
                                                                <label id="Label1" class="anchCls" style="font-family:Helvetica;font-size:14px;">Parent Name</label>
                                                            </HeaderTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </mastertableview>
                                        
                                            </telerik:RadGrid>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel Visible="false" ID="pnlback" runat="server">
                                <a href="#" onclick="history.go(-1)">Back</a><br />
                                <br />
                            </asp:Panel>
                            <telerik:RadGrid Visible="false" ID="radData" runat="server" Width="100%" AutoGenerateColumns="False"
                                OnItemDataBound="dgResult_ItemDataBound">
                                <mastertableview>
                        </mastertableview>
                            </telerik:RadGrid>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <asp:Literal ID="litNote" runat="server"></asp:Literal><br />
                        <asp:Literal ID="litExportTo" Visible="false" runat="server" Text="Export to: "></asp:Literal>
                        <asp:Button ID="btnExportToExcel" runat="server" Visible="False" CausesValidation="False"
                            Text="Excel" ForeColor="white" Font-Names="Arial" Font-Bold="true" BackColor="#336699"
                            Font-Size="8" OnClick="img_btn_export_to_excel_Click"></asp:Button>
                        <asp:Button ID="btnExportToPdf" runat="server" Visible="False" CausesValidation="False"
                            Text="Pdf" OnClick="btnExportToPdf_Click" ForeColor="white" Font-Names="Arial"
                            Font-Bold="true" BackColor="#336699" Font-Size="8"></asp:Button><br />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
