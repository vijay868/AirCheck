﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class ListOfCurrentSongsRepetition_details : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DateTime dtStartDate = DateTime.Parse(Request.QueryString["startDate"]);
        DateTime dtEndDate = DateTime.Parse(Request.QueryString["endDate"]);

        string strStartDate = getFormattedDate(dtStartDate);
        string strEndDate = getFormattedDate(dtEndDate);


        label_Date_Range.Text = strStartDate + " to " + strEndDate;

        string strStationName, strMarketName;
        int station_id;
        getStationAndMarketNames(out station_id, out strStationName, out strMarketName);
        label_station.Text = strStationName;
        label_market.Text = strMarketName;
        label_songName.Text = undoStringSanitation(Request.QueryString["strSongName"]);
        label_ArtistName.Text = undoStringSanitation(Request.QueryString["strArtistName"]);

        bindGrid(station_id);
    }

    protected void doFormatting(Label lblHeader, Label lblData)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "12px";

        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();

        lblHeader.Style.Add("font-family", headerFontName);
        if (Session["headerFontStyle"].ToString().ToLower() == "bold")
        {
            lblHeader.Font.Bold = true;
        }
        else
        {
            lblHeader.Font.Bold = false;
        }
        lblHeader.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
        lblHeader.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);


        lblData.Style.Add("font-family", actualDataFontName);
        if (Session["dataFontStyle"].ToString().ToLower() == "bold")
        {
            lblData.Font.Bold = true;
        }
        else
        {
            lblData.Font.Bold = false;
        }
        lblData.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
        lblData.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);




    }

    protected string getFormattedDate(DateTime dt)
    {
        string strDate = "";
        string strDateFormat = (string)Session["dateFormat"];
        if (strDateFormat == "DD/MM/YYYY")
        {
            strDate = dt.ToString("dd/MM/yyyy");
        }
        else
        {
            strDate = dt.ToString("MM/dd/yyyy");
        }
        return strDate;
    }

    protected string undoStringSanitation(string str)
    {
        if (str.IndexOf("123123") >= 0)
        {
            str = str.Replace("123123", "'");
        }

        if (str.IndexOf("456456amp;") >= 0)
        {
            str = str.Replace("456456amp;", "&");
        }
        else if (str.IndexOf("456456") >= 0)
        {
            str = str.Replace("456456", "&");
        }

        return str;
    }

    protected void bindGrid(int station_id)
    {
        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlDataAdapter da = new SqlDataAdapter("ListOfCurrentSongsRepetitionDetail", conn);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        da.SelectCommand.CommandTimeout = 0;
        da.SelectCommand.Parameters.AddWithValue("@stationIDs", station_id);
        da.SelectCommand.Parameters.AddWithValue("@startDate", Request.QueryString["startDate"]);
        da.SelectCommand.Parameters.AddWithValue("@endDate", Request.QueryString["endDate"]);
        da.SelectCommand.Parameters.AddWithValue("@stationName", Request.QueryString["strStationName"]);
        da.SelectCommand.Parameters.AddWithValue("@songName", undoStringSanitation(Request.QueryString["strSongName"]));
        da.SelectCommand.Parameters.AddWithValue("@artistName", undoStringSanitation(Request.QueryString["strArtistName"]));

        DataSet ds = new DataSet();
        da.Fill(ds);

        lblDuration.Text = getMinutesAndSeconds(int.Parse(ds.Tables[0].Rows[0][0].ToString()));

        datagrid_disp_details.DataSource = ds.Tables[1];
        datagrid_disp_details.DataBind();
    }

    protected string getMinutesAndSeconds(int intSec)
    {
        int min = intSec / 60;
        int sec = intSec % 60;

        string strDuration = (min + " Minute(s) and " + sec + " Second(s)");
        return strDuration;
    }

    protected void getStationAndMarketNames(out int station_id, out string strStationName, out string strMarketName)
    {
        station_id = 0;
        strStationName = "";
        strMarketName = "";

        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["connectionString"]);
        SqlCommand comm;
        SqlDataReader dr;

        comm = new SqlCommand("SELECT station_id,[name] AS 'Station',(SELECT [name] FROM aircheck..Markets WHERE market_id=Stations.market_id) AS 'Market' FROM Aircheck..Stations WHERE name = @stationPassed", conn);
        comm.Parameters.AddWithValue("@stationPassed", Request.QueryString["strStationName"].Trim());
        conn.Open();
        dr = comm.ExecuteReader();
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                station_id = int.Parse(dr["station_id"].ToString());
                strStationName = dr["Station"].ToString();
                strMarketName = dr["Market"].ToString();
            }
        }
        strStationName = strStationName.Replace(strMarketName.ToUpper(), "");
        conn.Close();
    }

    protected void datagrid_disp_details_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        string headerFontName = "Helvetica";
        string headerFontSize = "14px";

        string actualDataFontName = "Helvetica";
        string actualDataFontSize = "12px";



        System.Drawing.ColorConverter conv = new System.Drawing.ColorConverter();

        if (e.Item.ItemType == ListItemType.Header)
        {

            e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerRowColor"]);
            e.Item.Style.Add("font-family", headerFontName);
            if (Session["headerFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(headerFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["headerFontColor"]);

        }

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            e.Item.Style.Add("font-family", actualDataFontName);
            if (Session["dataFontStyle"].ToString().ToLower() == "bold")
            {
                e.Item.Font.Bold = true;
            }
            else
            {
                e.Item.Font.Bold = false;
            }
            e.Item.Font.Size = FontUnit.Parse(actualDataFontSize.Replace("Px", "").Trim());
            e.Item.ForeColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["dataFontColor"]);

            e.Item.Cells[0].Text = getFormattedDate(DateTime.Parse(e.Item.Cells[0].Text));

            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.BackColor = System.Drawing.Color.White;
            }
            else
            {
                e.Item.BackColor = (System.Drawing.Color)conv.ConvertFromString((string)Session["alternateRowColor"]);
            }
        }
    }

}
